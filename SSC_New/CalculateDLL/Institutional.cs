﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class Institutional
    {
        #region CRUD Operation

        /// <summary>
        /// Use for insert data into table Institutional and UserLoginDetails
        /// </summary>
        public void insertInstitutionalDetails(InstitutionalTO objInstitutional)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("InstitutionAdminID", objInstitutional.InstitutionAdminID));
                listCustomeDetails.Add(new QueryParameter("InstitutionName", objInstitutional.InstitutionName));
                listCustomeDetails.Add(new QueryParameter("Firstname", objInstitutional.Firstname));
                listCustomeDetails.Add(new QueryParameter("Lastname", objInstitutional.Lastname));
                listCustomeDetails.Add(new QueryParameter("Email", objInstitutional.Email));
                listCustomeDetails.Add(new QueryParameter("Password", objInstitutional.Password));
                listCustomeDetails.Add(new QueryParameter("Role", objInstitutional.Role));
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                dbInstitutional.RunStoredProcedureSelectParameter("insertInstitutionalDetails", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Return list of institutional
        /// </summary>
        /// <param name="searchName"></param>
        /// <returns></returns>
        public DataTable getInstitutionalDetails()
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionalDetails", listCustomerID);
                
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateInstitutionSubscription(string institutionID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("InstitutionID", institutionID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateInstitutionSubscription", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }
        }


        /// <summary>
        /// Return list of institutional
        /// </summary>
        /// <param name="searchName"></param>
        /// <returns></returns>
        public DataTable getInstitutionalDetailsFilter(string searchName = null, string updateDate = null, string searchText = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                if (!String.IsNullOrEmpty(searchName))
                    listCustomerID.Add(new QueryParameter("InstitutionName", searchName));
                else
                    listCustomerID.Add(new QueryParameter("InstitutionName", ""));
                if (!String.IsNullOrEmpty(updateDate))
                    listCustomerID.Add(new QueryParameter("UpdateDate", updateDate));
                else
                    listCustomerID.Add(new QueryParameter("UpdateDate", ""));
                if (!String.IsNullOrEmpty(searchText))
                    listCustomerID.Add(new QueryParameter("searchName", searchText));
                else
                    listCustomerID.Add(new QueryParameter("searchName", ""));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionalDetailsFilter", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Return row based on ID
        /// </summary>
        /// <param name="InstitutionID"></param>
        /// <param name="searchName"></param>
        /// <returns></returns>
        public DataTable getInstitutionalDetailsByID(string InstitutionID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("InstitutionAdminID", InstitutionID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionalDetailsByID", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateInstitutionalSubscriptionPlan(string institutionID, string plan)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("InstitutionalID", institutionID));
                lstCustomeDetails.Add(new QueryParameter("choosePlan", plan));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateInstitutionalSubscriptionPlan", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }
        }


        /// <summary>
        /// Update Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateInstitutionalInfo(InstitutionalTO objInstitution)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("InstitutionAdminID", objInstitution.InstitutionAdminID));
                listInstitutionalDetails.Add(new QueryParameter("Firstname", objInstitution.Firstname));
                listInstitutionalDetails.Add(new QueryParameter("Lastname", objInstitution.Lastname));
                listInstitutionalDetails.Add(new QueryParameter("InstitutionName", objInstitution.InstitutionName));
                listInstitutionalDetails.Add(new QueryParameter("Email", objInstitution.Email));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateInstitutionalDetails", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }
        }


        public void updateInstitutionaPassword(InstitutionalTO objInstitution)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("InstitutionAdminID", objInstitution.InstitutionAdminID));
                listInstitutionalDetails.Add(new QueryParameter("Password", objInstitution.Email));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateInstitutionalDetails", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }

        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteInstitutionalInfo(InstitutionalTO objInstitution)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("InstitutionAdminID", objInstitution.InstitutionAdminID));

                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteInstitutionByAdminID", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
            }
        }

        #endregion
    }
}
