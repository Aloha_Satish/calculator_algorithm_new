﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL.TO
{
    public class InstitutionalTO
    {
        #region Properties
        public string InstitutionAdminID { get; set; }
        public string InstitutionName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DelFlag { get; set; }
        public string UpdateDate { get; set; }
        public string Role { get; set; }
        public string isSubscription { get; set; }
        public string ChoosePlan { get; set; }
        #endregion
    }
}
