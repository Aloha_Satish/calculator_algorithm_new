﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL.TO
{
    public class AdvisorTO
    {
        #region Properties
        public string AdvisorID { get; set; }
        public string InstitutionAdminID { get; set; }
        public string AdvisorName { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UpdateDate { get; set; }
        public string Role { get; set; }
        public string DelFlag { get; set; }
        public string PasswordResetFlag { get; set; }
        public string isSubscription { get; set; }
        public string isInstitutionSubscription { get; set; }
        public string ChoosePlan { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Dealer { get; set; }
        #endregion
    }
}
