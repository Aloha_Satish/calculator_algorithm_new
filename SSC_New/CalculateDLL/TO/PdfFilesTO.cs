﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL
{
    public class PdfFilesTO
    {
        #region Properties
        public string CustomerID { get; set; }
        public string PdfName { get; set; }
        public string ContentType { get; set; }
        public byte[] PdfContent { get; set; }
        public string Email { get; set; }
        
        #endregion
    }
}
