﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class StoreTransaction
    {
        #region CRUD Operation

         /// <summary>
        /// Use for insert data into Store Table
        /// </summary>
        public void insertStoreTransactionDetails(StoreTO pObjStore)
        {
            try
            {
                List<QueryParameter> listStoreDetails = new List<QueryParameter>();
                listStoreDetails.Add(new QueryParameter("BookName", pObjStore.BookName));
                listStoreDetails.Add(new QueryParameter("ComponentID", pObjStore.ComponentID));
                listStoreDetails.Add(new QueryParameter("Price", pObjStore.Price));
                listStoreDetails.Add(new QueryParameter("Unit", pObjStore.Unit));          
                /* Initialize Database Object */
                Database dbStore = new Database();
               dbStore.RunStoredProcedureSelectParameter("insertStoreTransactionDetails", listStoreDetails);
                
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Store Details - Error - " + ex.ToString());
                
            }
        }

        /// <summary>
        /// This method return list of Store 
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getStoreTransactionDetails()
        {
            try
            {
               
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getStoreTransactionDetails", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Store Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Return sorted advisor lists based on selection criteria
        /// </summary>
        /// <param name="searchName"></param>
        /// <param name="updateDate"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public DataTable getStoreDetailsFilter(string searchName = null, string updateDate = null, string searchText = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                if (!String.IsNullOrEmpty(searchName))
                    listCustomerID.Add(new QueryParameter("BookName", searchName));
                else
                    listCustomerID.Add(new QueryParameter("BookName", ""));
                if (!String.IsNullOrEmpty(updateDate))
                    listCustomerID.Add(new QueryParameter("UpdateDate", updateDate));
                else
                    listCustomerID.Add(new QueryParameter("UpdateDate", ""));
                if (!String.IsNullOrEmpty(searchText))
                    listCustomerID.Add(new QueryParameter("searchName", searchText));
                else
                    listCustomerID.Add(new QueryParameter("searchName", ""));                
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getStoreDetailsFilter", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Book Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of Store 
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getStoreTransactionDetailsLastID()
        {
            try
            {
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getStoreTransactionDetailsLastID", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Store Details - Error - " + ex.ToString());
                return null;
            }
        }

        
        /// <summary>
        /// Update Store Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateStoreTransactionDetails(StoreTO pobjStore)
        {
            try
            {
                List<QueryParameter> listStoreDetails = new List<QueryParameter>();
                listStoreDetails.Add(new QueryParameter("ChargifySubscribtionID", pobjStore.ChargifySubscribtionID));
                listStoreDetails.Add(new QueryParameter("ChargifyTransactionID", pobjStore.ChargifyTransactionID));
                listStoreDetails.Add(new QueryParameter("Amount", pobjStore.Amount));
                listStoreDetails.Add(new QueryParameter("CustomerName", pobjStore.CustomerName));
                listStoreDetails.Add(new QueryParameter("CustomerEmail", pobjStore.CustomerEmail));
                listStoreDetails.Add(new QueryParameter("CustomerShippingAddress", pobjStore.CustomerShippingAddress));
                listStoreDetails.Add(new QueryParameter("ChargifyMemo", pobjStore.ChargifyMemo));
                listStoreDetails.Add(new QueryParameter("PaymentMade", pobjStore.PaymentMade));
                listStoreDetails.Add(new QueryParameter("StoreID", pobjStore.StoreID));
                Database dbStore = new Database();
                dbStore.RunStoredProcedureSelectParameter("updateStoreTransactionDetails", listStoreDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Book Details - Error - " + ex.ToString());                
            }
        }

        /// <summary>
        /// Delete Stored Book Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteBookInfo(StoreTO objStore)
        {
            try
            {
                List<QueryParameter> listBookDetails = new List<QueryParameter>();
                listBookDetails.Add(new QueryParameter("StoreID", objStore.StoreID));

                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteBookByStoredID", listBookDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Book Details - Error - " + ex.ToString());
            }
        }

        #endregion      
    }
}
