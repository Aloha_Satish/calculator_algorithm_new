﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class MenuItems
    {
        /// <summary>
        /// This method used to get Navigation menu based on role.
        /// </summary>
        public DataTable generateMenuItems(string pRole)
        {
            try
            {
                List<QueryParameter> listRole = new List<QueryParameter>();
                QueryParameter paramRole = new QueryParameter("Role", pRole);
                listRole.Add(paramRole);

                Database objDatabase = new Database();
                DataTable dtNavigation = objDatabase.RunStoredProcedureSelectParameter("generateMenuItems", listRole);
                return dtNavigation;
            }
            catch (Exception ex)
            { 
                throw ex; 
            }
        }
    }
}
