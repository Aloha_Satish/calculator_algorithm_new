﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Security Information for Divorced Customer
    /// </summary>
    public class SecurityInfoMarried
    {
        #region Properties

        public string CustomerID
        {
            get; 
            set;
        }

        public string WifesBirthDate 
        { 
            get; 
            set; 
        }

        public string WifesRetAgeBenefit
        {
            get;
            set;
        }

        public string HusbandsBirthDate
        {
            get;
            set;
        }

        public string HusbandsRetAgeBenefit
        {
            get;
            set;
        }

        public string SurvivorBenefit
        {
            get;
            set;
        }

        public string Cola
        {
            get;
            set;
        }        

        #endregion Properties        
    }
}
