﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL
{
    public class Theme
    {
        #region CRUD Operation

        /// <summary>
        /// Use for insert data into table ThemeDetails
        /// </summary>
        public void insertThemeDetails(ThemesTO pObjTheme)
        {
            try
            {
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                dbInstitutional.RunStoredProcedureMultiParameterWithByteForTheme("insertThemeDetails", pObjTheme.InstitutionId, pObjTheme.InstitutionName, pObjTheme.BackGroundHeaderColor, pObjTheme.BackGroundFooterColor, pObjTheme.BackGroundBodyColor, pObjTheme.InstitutionLogo);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return list of Institutional
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getInstitutionIdByName(string InstitutionName)
        {
            try
            {
                List<QueryParameter> listInstiName = new List<QueryParameter>();
                listInstiName.Add(new QueryParameter("InstitutionName", InstitutionName));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionIdByName", listInstiName);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of Themes
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getThemeDetails(string LetterUpdateBy = null)
        {
            try
            {
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getThemeDetails", null);
                //DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getBannerThemesDetails", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of Themes
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getThemeDetailsByID(string InstitutionID)
        {
            try
            {
                List<QueryParameter> listInstiName = new List<QueryParameter>();
                listInstiName.Add(new QueryParameter("InstitutionID", InstitutionID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getThemeDetailsBYID", listInstiName);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Update Theme Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateThemeInfo(ThemesTO pObjTheme)
        {
            try
            {
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureMultiParameterWithByteForUpdateTheme("updateThemeDetails", pObjTheme.ThemeId, pObjTheme.InstitutionId, pObjTheme.InstitutionName, pObjTheme.BackGroundHeaderColor, pObjTheme.BackGroundFooterColor, pObjTheme.BackGroundBodyColor, pObjTheme.InstitutionLogo);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Update Theme Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateThemeInfoWithoutLogo(ThemesTO pObjTheme)
        {
            try
            {
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureMultiParameterWithByteForUpdateThemeWithoutLogo("updateThemeDetailsWithoutLogo", pObjTheme.ThemeId, pObjTheme.InstitutionId, pObjTheme.InstitutionName, pObjTheme.BackGroundHeaderColor, pObjTheme.BackGroundFooterColor, pObjTheme.BackGroundBodyColor);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
            }
        }

        public void insertBannerThemeDetails(ThemesTO pObjTheme)
        {
            try
            {
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                //List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                //listCustomeDetails.Add(new QueryParameter("DomainName", pObjTheme.DomainName));
                //listCustomeDetails.Add(new QueryParameter("BannerImage", pObjTheme.InstitutionLogo);
                //listCustomeDetails.Add(new QueryParameter("URLString", pObjTheme.URLString));
                dbInstitutional.RunStoredProcedureWithByteForTheme("insertThemeDetail", pObjTheme.DomainName, pObjTheme.InstitutionLogo, pObjTheme.URLString);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteThemeInfo(string ThemeId)
        {
            try
            {
                List<QueryParameter> listThemeDetails = new List<QueryParameter>();
                listThemeDetails.Add(new QueryParameter("ThemeId", ThemeId));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteThemeById", listThemeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method is used to check Theme already exist for selected Institution or not.
        /// </summary>
        /// <param name="pEmail"></param>
        /// <returns>bool</returns>
        public bool isThemeExist(string InstitutionName)
        {
            try
            {
                List<QueryParameter> listInstitution = new List<QueryParameter>();
                QueryParameter paramEmail = new QueryParameter("InstitutionName", InstitutionName);
                listInstitution.Add(paramEmail);

                Database dbCustomer = new Database();
                DataTable dtFlag = dbCustomer.RunStoredProcedureSelectParameter("isThemeExist", listInstitution);
                if (dtFlag.Rows[0]["Flag"].ToString() == "T")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to check Theme already exist for Advisor.
        /// </summary>
        /// <param name="pEmail"></param>
        /// <returns>string</returns>
        public string isThemeExistForAdvisor(string AdvisorId)
        {
            try
            {
                List<QueryParameter> listInstitution = new List<QueryParameter>();
                QueryParameter paramEmail = new QueryParameter("AdvisorID", AdvisorId);
                listInstitution.Add(paramEmail);

                Database dbCustomer = new Database();
                DataTable dtFlag = dbCustomer.RunStoredProcedureSelectParameter("isThemeExistForAdvisor", listInstitution);
                if (dtFlag.Rows[0]["Flag"].ToString() == "T")
                    return dtFlag.Rows[0]["InstiId"].ToString();
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to check Theme already exist for Customer.
        /// </summary>
        /// <param name="pEmail"></param>
        /// <returns>string</returns>
        public string isThemeExistForCustomer(string CustomerId)
        {
            try
            {
                List<QueryParameter> listInstitution = new List<QueryParameter>();
                QueryParameter paramEmail = new QueryParameter("CustomerID", CustomerId);
                listInstitution.Add(paramEmail);

                Database dbCustomer = new Database();
                DataTable dtFlag = dbCustomer.RunStoredProcedureSelectParameter("isThemeExistForCustomer", listInstitution);
                if (dtFlag.Rows[0]["Flag"].ToString() == "T")
                    return dtFlag.Rows[0]["InstiId"].ToString();
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method is used to check Theme already exist for selected Institution or not.
        /// </summary>
        /// <param name="pEmail"></param>
        /// <returns>bool</returns>
        public byte[] FetchImage(string ThemeId)
        {
            try
            {
                List<QueryParameter> listInstitution = new List<QueryParameter>();
                QueryParameter paramEmail = new QueryParameter("ThemeId", ThemeId);
                listInstitution.Add(paramEmail);

                Database dbCustomer = new Database();
                DataTable dtFlag = dbCustomer.RunStoredProcedureSelectParameter("getImageByThemeId", listInstitution);
                return (byte[])dtFlag.Rows[0]["InstitutionLogo"];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetBannerDetailsByDomainName(string DomainName)
        {
            try
            {
                List<QueryParameter> listInstiName = new List<QueryParameter>();
                listInstiName.Add(new QueryParameter("DomainName", DomainName));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("GetBannerDetailsByDomainName", listInstiName);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Theme Details - Error - " + ex.ToString());
                return null;
            }
        }
        #endregion
    }
}
