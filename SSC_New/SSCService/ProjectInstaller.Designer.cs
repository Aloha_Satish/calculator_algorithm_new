﻿namespace SSCService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            serviceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            serviceInstaller = new System.ServiceProcess.ServiceInstaller();
            
            // serviceProcessInstaller
            serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            //serviceProcessInstaller.Username = "bridou599";
            //serviceProcessInstaller.Password = "H72Linc55_";

            serviceProcessInstaller.Username = "bridougc";
            serviceProcessInstaller.Password = "G98Beac39!";

            serviceInstaller.Description = "Notification service for the calculator subscription expiration";
            serviceInstaller.DisplayName = "CalculatorNotification";
            serviceInstaller.ServiceName = "CalculatorNotificationService";
            
            // ProjectInstaller
            Installers.AddRange(new System.Configuration.Install.Installer[] {
            serviceProcessInstaller,
            serviceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller serviceInstaller;
    }
}