﻿using System.ServiceProcess;

namespace SSCService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CalculatorNotificationService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
