﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Security Information for Divorced Customer
    /// </summary>
    public class NewsLetterAdvisor
    {
        #region CRUD Operation

        /// <summary>
        /// Use for insert data into table Institutional and UserLoginDetails
        /// </summary>
        public void insertNewsLetterDetails(NewsLetterAdvisorTO pObjNewsLetter)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("LetterBody", pObjNewsLetter.LetterBody));
                listNewsLetterDetails.Add(new QueryParameter("LetterSubject", pObjNewsLetter.LetterSubject));
                listNewsLetterDetails.Add(new QueryParameter("LetterUpdateBy", pObjNewsLetter.LetterUpdateBy));
                listNewsLetterDetails.Add(new QueryParameter("PublishDate", pObjNewsLetter.PublishDate));
                listNewsLetterDetails.Add(new QueryParameter("Periodic", pObjNewsLetter.Periodic));
                listNewsLetterDetails.Add(new QueryParameter("AdvisorList", pObjNewsLetter.AdvisorList));
                listNewsLetterDetails.Add(new QueryParameter("InstitutionID", pObjNewsLetter.InstitutionID));
                listNewsLetterDetails.Add(new QueryParameter("CustomersFlag", pObjNewsLetter.CustomersFlag));
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                dbInstitutional.RunStoredProcedureSelectParameter("insertPeriodicNewsLetterForAdvisor", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return list of Institutional
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getNewsLetterDetailsByID(string InstitutionID)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("InstitutionID", InstitutionID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getNewsLetterDetailsForAdvisor", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Return sorted advisor lists based on selection criteria
        /// </summary>
        /// <param name="searchName"></param>
        /// <param name="updateDate"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public DataTable getNewsLetterDetailsFilter(string searchName = null, string updateDate = null, string searchText = null)
        {
            try
            {
                List<QueryParameter> listNewsLetterID = new List<QueryParameter>();
                if (!String.IsNullOrEmpty(searchName))
                    listNewsLetterID.Add(new QueryParameter("LetterSubject", searchName));
                else
                    listNewsLetterID.Add(new QueryParameter("LetterSubject", ""));
                if (!String.IsNullOrEmpty(updateDate))
                    listNewsLetterID.Add(new QueryParameter("LetterUpdateDate", updateDate));
                else
                    listNewsLetterID.Add(new QueryParameter("LetterUpdateDate", ""));
                if (!String.IsNullOrEmpty(searchText))
                    listNewsLetterID.Add(new QueryParameter("LetterSubject", searchText));
                else
                    listNewsLetterID.Add(new QueryParameter("LetterSubject", ""));
                
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getNewsLetterDetailsFilter", listNewsLetterID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Update Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateNewsLetterInfo(NewsLetterAdvisorTO pObjNewsLetter)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("LetterBody", pObjNewsLetter.LetterBody));
                listNewsLetterDetails.Add(new QueryParameter("LetterSubject", pObjNewsLetter.LetterSubject));
                listNewsLetterDetails.Add(new QueryParameter("LetterUpdateBy", pObjNewsLetter.LetterUpdateBy));
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", pObjNewsLetter.NewsLetterId));
                listNewsLetterDetails.Add(new QueryParameter("PublishDate", pObjNewsLetter.PublishDate));
                listNewsLetterDetails.Add(new QueryParameter("Periodic", pObjNewsLetter.Periodic));
                listNewsLetterDetails.Add(new QueryParameter("AdvisorList", pObjNewsLetter.AdvisorList));
                listNewsLetterDetails.Add(new QueryParameter("CustomersFlag", pObjNewsLetter.CustomersFlag));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateNewsLetterDetailsForAdvisor", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteNewsLetterInfo(string NewsLetterID)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteNewsLetterByIDForAdvisor", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateNewsLetterStatus(string NewsLetterID,string Status)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                listNewsLetterDetails.Add(new QueryParameter("LetterStatus", Status));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateNewsLetterStatusByIDForAdvisor", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Use for insert data into table AttachmentFiles and Newsletter
        /// </summary>
        /// <param name="File"></param>
        public void insertAttachmentFileDetails(AttachmentFilesTO File)
        {
            try
            {
                Database dbAttachment = new Database();
                dbAttachment.RunStoredProcedureMultiParameterWithByte("insertAttachmentFileDetailsForAdvisor", File.NewsLetterId, File.FileName, File.ContentType, File.Content, File.FileSize);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - insertAttachmentFileDetails - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return latest NewsletterID
        /// </summary>
        public DataTable getNewsLetterIdLatest(string LetterUpdateBy = null)
        {
            try
            {
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getNewsLetterIdLatestForAdvisor", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Id - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of FileName and size
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getAttachmentDetailsByNewsLetterId(string NewsLetterID)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAttachmentDetailsByNewLetterIdForAdvisor", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Delete Attachment
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteAttachmentByNewsLetterId(string NewsLetterId, string FileName)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterId));
                listNewsLetterDetails.Add(new QueryParameter("FileName", FileName));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteAttachmentByNewsLetterIdForAdvisor", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return Advisors Mail List
        /// </summary>
        /// <param name="pCustomerID"></param>
        public string getAdvisorsMailByID(string AdvisorId)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("AdvisorID", AdvisorId));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorEmailById", listNewsLetterDetails);
                return dtDetails.Rows[0]["Email"].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisors Email - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return Customers Mail List
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getCustomersMailByID(string AdvisorId)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("AdvisorId", AdvisorId));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomersEmailById", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customers Email - Error - " + ex.ToString());
                return null;
            }
        }

        #endregion  
    }
}
