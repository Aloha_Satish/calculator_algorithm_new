﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class Advisor
    {
        #region CRUD Operation

         /// <summary>
        /// Use for insert data into table Institutional and UserLoginDetails
        /// </summary>
        public void insertAdvisorDetails(AdvisorTO objAdvisor)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("InstitutionAdminID", objAdvisor.InstitutionAdminID));
                listCustomeDetails.Add(new QueryParameter("AdvisorID", objAdvisor.AdvisorID));
                listCustomeDetails.Add(new QueryParameter("AdvisorName", objAdvisor.AdvisorName));
                listCustomeDetails.Add(new QueryParameter("Firstname", objAdvisor.Firstname));
                listCustomeDetails.Add(new QueryParameter("Lastname", objAdvisor.Lastname));
                listCustomeDetails.Add(new QueryParameter("Email", objAdvisor.Email));
                listCustomeDetails.Add(new QueryParameter("Password", objAdvisor.Password));
                listCustomeDetails.Add(new QueryParameter("Role", objAdvisor.Role));
                listCustomeDetails.Add(new QueryParameter("isInstitutionSubscription", objAdvisor.isInstitutionSubscription));
                listCustomeDetails.Add(new QueryParameter("DelFlag", objAdvisor.DelFlag));
                listCustomeDetails.Add(new QueryParameter("isSubscription", objAdvisor.isSubscription));
                listCustomeDetails.Add(new QueryParameter("ChoosePlan", objAdvisor.ChoosePlan));
                listCustomeDetails.Add(new QueryParameter("PasswordResetFlag", objAdvisor.PasswordResetFlag));
                listCustomeDetails.Add(new QueryParameter("City", objAdvisor.City));
                listCustomeDetails.Add(new QueryParameter("State", objAdvisor.State));
                listCustomeDetails.Add(new QueryParameter("Dealer", objAdvisor.Dealer));
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                dbInstitutional.RunStoredProcedureSelectParameter("insertAdvisorDetails", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update basic information of customers.
        /// </summary>
        public void ActivateAdvisor(string AdvisorID)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("AdvisorID", AdvisorID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("AdvisorDetailsActivate", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateAdvisorSubscription(string advisorID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("AdvisorID", advisorID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateAdvisorSubscription", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return list of Institutional
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getAdvisorDetails(string InstitutionID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("InstitutionAdminID", InstitutionID);
                listCustomerID.Add(paramCustomerID);
                
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorDetails", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of All Advisor except provided one
        /// </summary>
        /// <param name="pCustomerID">AdvisorID</param>
        public DataTable getALLAdvisors(string AdvisorID = null,string strInstitutionID=null)
        {
            try
            {
                List<QueryParameter> listAdvisorID = new List<QueryParameter>();
                QueryParameter paramAdvisorID = new QueryParameter("AdvisorID", AdvisorID);
                QueryParameter paramInstitutionID = new QueryParameter("institutionAdminID", strInstitutionID);
                listAdvisorID.Add(paramAdvisorID);
                listAdvisorID.Add(paramInstitutionID);

                Database dbAdvisor = new Database();
                DataTable dtDetails = dbAdvisor.RunStoredProcedureSelectParameter("getAllAdvisor", listAdvisorID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("getAllAdvisor - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of All Advisor except provided one
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getALLCustomers(string AdvisorID = null)
        {
            try
            {
                List<QueryParameter> listAdvisorID = new List<QueryParameter>();
                QueryParameter paramAdvisorID = new QueryParameter("AdvisorID", AdvisorID);
                listAdvisorID.Add(paramAdvisorID);
                Database dbAdvisor = new Database();
                DataTable dtDetails = dbAdvisor.RunStoredProcedureSelectParameter("getAllCustomerByAdvisorID", listAdvisorID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("getALLCustomers - Error - " + ex.ToString());
                return null;
            }
        }
        

        /// <summary>
        /// Return sorted advisor lists based on selection criteria
        /// </summary>
        /// <param name="searchName"></param>
        /// <param name="updateDate"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public DataTable getAdvisorDetailsFilter(string searchName = null, string updateDate = null, string searchText = null, string InstitutionAdminID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                if (!String.IsNullOrEmpty(searchName))
                    listCustomerID.Add(new QueryParameter("AdvisorName", searchName));
                else
                    listCustomerID.Add(new QueryParameter("AdvisorName", ""));
                if (!String.IsNullOrEmpty(updateDate))
                    listCustomerID.Add(new QueryParameter("UpdateDate", updateDate));
                else
                    listCustomerID.Add(new QueryParameter("UpdateDate", ""));
                if (!String.IsNullOrEmpty(searchText))
                    listCustomerID.Add(new QueryParameter("searchName", searchText));
                else
                    listCustomerID.Add(new QueryParameter("searchName", ""));
                listCustomerID.Add(new QueryParameter("InstitutionAdminID", InstitutionAdminID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorDetailsFilter", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institutional Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateAdvisorSubscriptionPlan(string advisorID, string plan)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("AdvisorID", advisorID));
                lstCustomeDetails.Add(new QueryParameter("choosePlan", plan));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateAdvisorSubscriptionPlan", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return selected Institution ID
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getAdvisorDetailsByID(string AdvisorID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("AdvisorAdminID", AdvisorID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorDetailsByID", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
                return null;
            }
        }

        /* Dev:- Aloha  Date:- 03/12/2014*/
        /// <summary>
        /// This method return selected Institution ID
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getDefaultAdvisorDetailsByInstitutionID(string InstitutionID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("AdvisorAdminID", InstitutionID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getDefaultAdvisorDetailsByInstitutionID", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Update Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateAdvisorInfo(AdvisorTO objAdvisor)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("AdvisorID", objAdvisor.AdvisorID));
                listInstitutionalDetails.Add(new QueryParameter("Firstname", objAdvisor.Firstname));
                listInstitutionalDetails.Add(new QueryParameter("Lastname", objAdvisor.Lastname));
                listInstitutionalDetails.Add(new QueryParameter("Dealer", objAdvisor.Dealer));
                listInstitutionalDetails.Add(new QueryParameter("State", objAdvisor.State));
                listInstitutionalDetails.Add(new QueryParameter("Email", objAdvisor.Email));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateAdvisorDetails", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());                
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteAdvisorInfo(AdvisorTO objAdvisor)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("AdvisorAdminID", objAdvisor.AdvisorID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteAdvisorByAdminID", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Assign New Advisor to Customer when adviser is deleted
        public void updateAssignAdvisor(string strAdvisorID, string strCustomerID)
        {
            try
            {
                List<QueryParameter> AssignAdvisor = new List<QueryParameter>();
                AssignAdvisor.Add(new QueryParameter("AdvisorID", strAdvisorID));
                AssignAdvisor.Add(new QueryParameter("CustomerID", strCustomerID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateAssignAdvisor", AssignAdvisor);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor - updateAssignAdvisor() - Error - " + ex.ToString());
            }
        }

        #endregion      
    }
}
