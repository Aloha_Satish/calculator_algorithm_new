﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL.TO
{
    public class StoreTO
    {
        #region Properties
        public string BookName { get; set; }
        public string StoreID { get; set; }
        public string ChargifySubscribtionID { get; set; }
        public string ChargifyTransactionID { get; set; }
        public string Amount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerShippingAddress { get; set; }
        public string ChargifyMemo { get; set; }
        public string PaymentMade { get; set; }
        public string Price { get; set; }
        public string ComponentID { get; set; }
        public string Unit { get; set; }
        public string CreatedDate { get; set; }
        #endregion
    }
}
