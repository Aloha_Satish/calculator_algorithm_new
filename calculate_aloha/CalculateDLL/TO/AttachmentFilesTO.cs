﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL.TO
{
    public class AttachmentFilesTO
    {
        #region Properties
        public string AttachmentID { get; set; }
        public string NewsLetterId { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public string FileSize { get; set; }

        #endregion
    }
}
