﻿using System;
using System.Collections.Generic;
using System.Data;
using CalculateDLL.TO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Net.Mail;
namespace CalculateDLL
{
    public class Customers
    {
        public Customers()
        { }

        #region Variable Declaration
        private string m_customerID;
        private string m_firstName;
        private string m_lastName;
        private string m_birthdate;
        private string m_martialStatus;
        private string m_email;
        private string m_password;
        private string m_role;
        private string m_spouseFirstname;
        private string m_spouseLastname;
        private string m_spouseBirthdate;
        private string m_advisorID;
        private string m_chooseplan;
        private string m_isSubscription;
        private string m_DelFlag;
        private string m_PasswordResetFlag;
        private string m_WifesRetAgeBenefit;
        private string m_HusbandsRetAgeBenefit;
        private string m_ClaimedBenefit;
        private string m_ClaimedAge;
        private string m_ClaimedPerson;
        #endregion Variable Declaration

        #region Properties

        public string CustomerID
        {
            get { return m_customerID; }
            set { m_customerID = value; }
        }
        public string DelFlag
        {
            get { return m_DelFlag; }
            set { m_DelFlag = value; }
        }
        public string PasswordResetFlag
        {
            get { return m_PasswordResetFlag; }
            set { m_PasswordResetFlag = value; }
        }

        public string ChoosePlan
        {
            get { return m_chooseplan; }
            set { m_chooseplan = value; }
        }

        public string isSubscription
        {
            get { return m_isSubscription; }
            set { m_isSubscription = value; }
        }

        public string AdvisorID
        {
            get { return m_advisorID; }
            set { m_advisorID = value; }
        }

        public string FirstName
        {
            get { return m_firstName; }
            set { m_firstName = value; }
        }

        public string LastName
        {
            get { return m_lastName; }
            set { m_lastName = value; }
        }

        public string Birthdate
        {
            get { return m_birthdate; }
            set { m_birthdate = value; }
        }

        public string MartialStatus
        {
            get { return m_martialStatus; }
            set { m_martialStatus = value; }
        }

        public string Email
        {
            get { return m_email; }
            set { m_email = value; }
        }

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public string Role
        {
            get { return m_role; }
            set { m_role = value; }
        }

        public string SpouseFirstname
        {
            get { return m_spouseFirstname; }
            set { m_spouseFirstname = value; }
        }

        public string SpouseLastname
        {
            get { return m_spouseLastname; }
            set { m_spouseLastname = value; }
        }

        public string SpouseBirthdate
        {
            get { return m_spouseBirthdate; }
            set { m_spouseBirthdate = value; }
        }

        public string WifesRetAgeBenefit
        {
            get { return m_WifesRetAgeBenefit; }
            set { m_WifesRetAgeBenefit = value; }
        }
        public string HusbandsRetAgeBenefit
        {
            get { return m_HusbandsRetAgeBenefit; }
            set { m_HusbandsRetAgeBenefit = value; }
        }
        public string ClaimedBenefit
        {
            get { return m_ClaimedBenefit; }
            set { m_ClaimedBenefit = value; }
        }
        public string ClaimedAge
        {
            get { return m_ClaimedAge; }
            set { m_ClaimedAge = value; }
        }
        public string ClaimedPerson
        {
            get { return m_ClaimedPerson; }
            set { m_ClaimedPerson = value; }
        }
        
        //set Selected customer Customerlist
        public string CustomersList { get; set; }

        #endregion Properties

        #region Personal Info Operation

        /// <summary>
        /// Use for insert data into table Customers and UserLoginDetails
        /// Date: 10 June 2014
        /// </summary>
        /// <param name="pCustomerName"></param>
        /// <param name="pEmail"></param>
        /// <param name="pPassword"></param>
        /// <param name="pRole"></param>
        /// <returns>void</returns>
        public void insertCustomerDetails(Customers pCustomer)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("CustomerID", pCustomer.CustomerID));
                listCustomeDetails.Add(new QueryParameter("Firstname", pCustomer.FirstName));
                listCustomeDetails.Add(new QueryParameter("Lastname", pCustomer.LastName));
                listCustomeDetails.Add(new QueryParameter("Email", pCustomer.Email));
                listCustomeDetails.Add(new QueryParameter("Password", pCustomer.Password));
                listCustomeDetails.Add(new QueryParameter("Role", pCustomer.Role));
                listCustomeDetails.Add(new QueryParameter("AdvisorID", pCustomer.AdvisorID));
                listCustomeDetails.Add(new QueryParameter("Birthdate", pCustomer.Birthdate));
                listCustomeDetails.Add(new QueryParameter("MartialStatus", pCustomer.MartialStatus));
                listCustomeDetails.Add(new QueryParameter("SpouseFirstname", pCustomer.SpouseFirstname));
                listCustomeDetails.Add(new QueryParameter("SpouseBirthdate", pCustomer.SpouseBirthdate));
                listCustomeDetails.Add(new QueryParameter("isSubscription", pCustomer.isSubscription));
                listCustomeDetails.Add(new QueryParameter("DelFlag", pCustomer.DelFlag));
                listCustomeDetails.Add(new QueryParameter("PasswordResetFlag", pCustomer.PasswordResetFlag));
                listCustomeDetails.Add(new QueryParameter("HusbandsRetAgeBenefit", pCustomer.HusbandsRetAgeBenefit));
                listCustomeDetails.Add(new QueryParameter("WifesRetAgeBenefit", pCustomer.WifesRetAgeBenefit));

                //Restrict App changes
                listCustomeDetails.Add(new QueryParameter("ClaimedPerson", pCustomer.ClaimedPerson));
                listCustomeDetails.Add(new QueryParameter("ClaimedAge", pCustomer.ClaimedAge));
                listCustomeDetails.Add(new QueryParameter("ClaimedBenefit", pCustomer.ClaimedBenefit));

                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("insertCustomerDetails", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Used to mail report to client
        /// </summary>
        /// <param name="pdfDoc">PDF attachment</param>
        /// <param name="strEmailTo">Reciever mail address</param>
        /// <param name="memoryStream">MemoryStream of report</param>
        /// <param name="writer">PDF Writer</param>
        /// <param name="strClientName">Client Name</param>
        /// <returns>Returns status of mail</returns>
        public bool MailReport(Document pdfDoc, string strEmailTo, MemoryStream memoryStream, PdfWriter writer, string strClientName)
        {
            try
            {
                var smtp = new SmtpClient();
                MailMessage mail = new MailMessage();
                /* Add Subject to mail object */
                mail.Subject = "Social Security Analysis Report";
                /* Add Body to mail object */
                mail.Body = "Hi " + strClientName + ", <br /><br />        Please find below the attachment of Social Security Analysis Report. <br /><br /> Thanks & Regards,<br /> Advisor";
                /* Add To Recepient Address where we need to send mail */
                mail.To.Add(new MailAddress(strEmailTo));
                mail.IsBodyHtml = true;
                writer.CloseStream = false;
                pdfDoc.Close();
                // Build email
                memoryStream.Position = 0;
                mail.Attachments.Add(new Attachment(memoryStream, "Social Security Analysis Report.pdf"));
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {

                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return false;
            }
        }


        /// <summary>
        /// Use for insert data into table Customers and UserLoginDetails
        /// Date: 10 June 2014
        /// </summary>
        /// <param name="pCustomerName"></param>
        /// <param name="pEmail"></param>
        /// <param name="pPassword"></param>
        /// <param name="pRole"></param>
        /// <returns>void</returns>
        public void insertPdfDetails(PdfFilesTO pPdfFile)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureParameterWithByte("insertPdfDetails", pPdfFile.CustomerID, pPdfFile.PdfName, pPdfFile.ContentType, pPdfFile.PdfContent);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        public void updateCustomerTableInfo(Customers pCustomers)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("CustomerID", pCustomers.CustomerID));
                listCustomeDetails.Add(new QueryParameter("Birthdate", pCustomers.Birthdate));
                listCustomeDetails.Add(new QueryParameter("SpouseBirthdate", pCustomers.SpouseBirthdate));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerTableInfo", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// code to get the number of months to be displayed when birth year between 1955 & 1959
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns></returns>
        public int FRAMonthsRemaining(int BirthYear)
        {
            try
            {
                int FRA_Age;
                if (BirthYear >= 1943 && BirthYear <= 1954)
                    BirthYear = 1954;
                if (BirthYear >= 1960 && BirthYear <= 2000)
                    BirthYear = 2000;
                switch (BirthYear)
                {
                    case 1938:
                        FRA_Age = 10;
                        break;
                    case 1939:
                        FRA_Age = 8;
                        break;
                    case 1940:
                        FRA_Age = 6;
                        break;
                    case 1941:
                        FRA_Age = 4;
                        break;
                    case 1942:
                        FRA_Age = 2;
                        break;
                    case 1955:
                        FRA_Age = 10;
                        break;
                    case 1956:
                        FRA_Age = 8;
                        break;
                    case 1957:
                        FRA_Age = 6;
                        break;
                    case 1958:
                        FRA_Age = 4;
                        break;
                    case 1959:
                        FRA_Age = 2;
                        break;
                    default:
                        FRA_Age = 0;
                        break;
                }
                return 12 - FRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return 0;
            }
        }

        /// <summary>
        /// code to get the FRA year and Months in words to be displayed in PDF
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns></returns>
        public string FRA(int BirthYear)
        {
            try
            {
                string FRA_Age;
                if (BirthYear >= 1900 && BirthYear <= 1937)
                    BirthYear = 1937;
                if (BirthYear >= 1943 && BirthYear <= 1954)
                    BirthYear = 1954;
                if (BirthYear >= 1960 && BirthYear <= 2000)
                    BirthYear = 2000;
                switch (BirthYear)
                {
                    case 1937:
                        FRA_Age = "65";
                        break;
                    case 1938:
                        FRA_Age = "65 Years and 2 Months";
                        break;
                    case 1939:
                        FRA_Age = "65 Years and 4 Months";
                        break;
                    case 1940:
                        FRA_Age = "65 Years and 6 Months";
                        break;
                    case 1941:
                        FRA_Age = "65 Years and 8 Months";
                        break;
                    case 1942:
                        FRA_Age = "65 Years and 10 Months";
                        break;
                    case 1954:
                        FRA_Age = "66";
                        break;
                    case 1955:
                        FRA_Age = "66 Years and 2 Months";
                        break;
                    case 1956:
                        FRA_Age = "66 Years and 4 Months";
                        break;
                    case 1957:
                        FRA_Age = "66 Years and 6 Months";
                        break;
                    case 1958:
                        FRA_Age = "66 Years and 8 Months";
                        break;
                    case 1959:
                        FRA_Age = "66 Years and 10 Months";
                        break;
                    case 2000:
                        FRA_Age = "67";
                        break;
                    default:
                        FRA_Age = "0";
                        break;
                }
                return FRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// code to get widow FRA years as diffrent from rest of the startegies
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns></returns>
        public string WidowFRA(int BirthYear)
        {
            string FRA_Age;
            if (BirthYear <= 1939)
                BirthYear = 1937;
            if (BirthYear >= 1945 && BirthYear <= 1956)
                BirthYear = 1938;
            if (BirthYear >= 1962)
                BirthYear = 2000;
            switch (BirthYear)
            {
                case 1937:
                    FRA_Age = "65";
                    break;
                case 1938:
                    FRA_Age = "66";
                    break;
                case 1940:
                    FRA_Age = "65 Years and 2 Months";
                    break;
                case 1941:
                    FRA_Age = "65 Years and 4 Months";
                    break;
                case 1942:
                    FRA_Age = "65 Years and 6 Months";
                    break;
                case 1943:
                    FRA_Age = "65 Years and 8 Months";
                    break;
                case 1944:
                    FRA_Age = "65 Years and 10 Months";
                    break;
                case 1957:
                    FRA_Age = "66 Years and 2 Months";
                    break;
                case 1958:
                    FRA_Age = "66 Years and 4 Months";
                    break;
                case 1959:
                    FRA_Age = "66 Years and 6 Months";
                    break;
                case 1960:
                    FRA_Age = "66 Years and 8 Months";
                    break;
                case 1961:
                    FRA_Age = "66 Years and 10 Months";
                    break;
                case 2000:
                    FRA_Age = "67";
                    break;
                default:
                    FRA_Age = string.Empty;
                    break;
            }
            return FRA_Age;
        }


        public int WidowFRAMonths(int BirthYear)
        {
            int FRA_Month;
            if (BirthYear <= 1939)
                BirthYear = 1937;
            if (BirthYear >= 1945 && BirthYear <= 1956)
                BirthYear = 1938;
            if (BirthYear >= 1962)
                BirthYear = 2000;
            switch (BirthYear)
            {
                case 1957:
                    FRA_Month = 2;
                    break;
                case 1958:
                    FRA_Month = 4;
                    break;
                case 1959:
                    FRA_Month = 6;
                    break;
                case 1960:
                    FRA_Month = 8;
                    break;
                case 1961:
                    FRA_Month = 10;
                    break;
                case 2000:
                    FRA_Month = 0;
                    break;
                default:
                    FRA_Month = 0;
                    break;
            }
            return FRA_Month;
        }

        /// <summary>
        /// This method used for update basic information of customers.
        /// </summary>
        /// 

        public void updateCustomerPersonalInfo(Customers pCustomers)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("CustomerID", pCustomers.CustomerID));
                listCustomeDetails.Add(new QueryParameter("Firstname", pCustomers.FirstName));
                listCustomeDetails.Add(new QueryParameter("Lastname", pCustomers.LastName));
                listCustomeDetails.Add(new QueryParameter("Birthdate", pCustomers.Birthdate));
                listCustomeDetails.Add(new QueryParameter("MartialStatus", pCustomers.MartialStatus));
                listCustomeDetails.Add(new QueryParameter("SpouseFirstname", pCustomers.m_spouseFirstname));
                listCustomeDetails.Add(new QueryParameter("SpouseLastname", pCustomers.SpouseLastname));
                listCustomeDetails.Add(new QueryParameter("SpouseBirthdate", pCustomers.SpouseBirthdate));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerDetails", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update basic information of customers.
        /// </summary>
        public void ActivateCustomer(string CustomerID)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("CustomerID", CustomerID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("CustomerDetailsActivate", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update basic information of customers.
        /// </summary>
        /// Date: 11 June 2014
        /// <param name="pCustomerID"></param>
        /// <param name="pFirstname"></param>
        /// <param name="pLastname"></param>
        /// <param name="pEmailAddress"></param>
        public void updateCustomerDetails(Customers pCustomer)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("CustomerID", pCustomer.CustomerID));
                lstCustomeDetails.Add(new QueryParameter("Firstname", pCustomer.FirstName));
                lstCustomeDetails.Add(new QueryParameter("Lastname", pCustomer.LastName));
                lstCustomeDetails.Add(new QueryParameter("Email", pCustomer.Email));
                lstCustomeDetails.Add(new QueryParameter("Password", pCustomer.Password));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerAccountDetails", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update basic information of customers.
        /// </summary>
        /// Date: 11 June 2014
        /// <param name="pCustomerID"></param>
        /// <param name="pFirstname"></param>
        /// <param name="pLastname"></param>
        /// <param name="pEmailAddress"></param>
        public void updateCustomerDetailsByAdvisor(Customers pCustomer)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("CustomerID", pCustomer.CustomerID));
                listCustomeDetails.Add(new QueryParameter("Firstname", pCustomer.FirstName));
                listCustomeDetails.Add(new QueryParameter("Lastname", pCustomer.LastName));
                listCustomeDetails.Add(new QueryParameter("Email", pCustomer.Email));
                listCustomeDetails.Add(new QueryParameter("Password", pCustomer.Password));
                listCustomeDetails.Add(new QueryParameter("Birthdate", pCustomer.Birthdate));
                listCustomeDetails.Add(new QueryParameter("MartialStatus", pCustomer.MartialStatus));
                listCustomeDetails.Add(new QueryParameter("SpouseLastname", pCustomer.SpouseLastname));
                listCustomeDetails.Add(new QueryParameter("SpouseFirstname", pCustomer.SpouseFirstname));
                listCustomeDetails.Add(new QueryParameter("SpouseBirthdate", pCustomer.SpouseBirthdate));
                listCustomeDetails.Add(new QueryParameter("WifesRetAgeBenefit", pCustomer.WifesRetAgeBenefit));
                listCustomeDetails.Add(new QueryParameter("HusbandsRetAgeBenefit", pCustomer.HusbandsRetAgeBenefit));

                //Restrict App changes
                listCustomeDetails.Add(new QueryParameter("ClaimedPerson", pCustomer.ClaimedPerson));
                listCustomeDetails.Add(new QueryParameter("ClaimedAge", pCustomer.ClaimedAge));
                listCustomeDetails.Add(new QueryParameter("ClaimedBenefit", pCustomer.ClaimedBenefit));

                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerAccountDetailsByAdvisor", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return details of customers
        /// </summary>
        /// Date: 11 Jan 2014
        /// <param name="pCustomerID"></param>
        public DataTable getCustomerDetails(string pCustomerID)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("CustomerID", pCustomerID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomerDetails", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        public DataTable getAccountDetails(string pCustomerID)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("UserID", pCustomerID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAccountDetails", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateCustomerDetails(string customerID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("CustomerID", customerID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerSubscription", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Used to update the user IsSubscription column by verifying the user is 
        /// exist or not in InfusionSoft 
        /// </summary>
        /// <param name="strEmailID">Email Id</param>
        public void UpdateIsSubscriptionUsingInfusionSoft(string strEmailID)
        {
            try
            {
                string strUserID = string.Empty;
                Users objUsers = new Users();
                DataTable dtUserDetails = objUsers.getUserDetails(strEmailID);
                strUserID = dtUserDetails.Rows[0]["UserId"].ToString();
                //Update Is Subscription By ID
                UpdateIsSubscription(strUserID);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Update Customer IsSubscription Using InfusionSoft - Error - " + ex.ToString());
            }
            
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void UpdateIsSubscription(string customerID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("CustomerID", customerID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateIsSubscription", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void UpdateIsSubscriptionByEmailID(string emailID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("Email", emailID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateIsSubscription", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }


        /// <summary>
        /// This method used for update customers subscription.
        /// </summary>
        public void updateCustomerSubscriptionPlan(string customerID, string plan)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("CustomerID", customerID));
                lstCustomeDetails.Add(new QueryParameter("choosePlan", plan));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateCustomerSubscriptionPlan", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return details of customers
        /// </summary>
        /// Date: 11 Jan 2014
        /// <param name="pCustomerID"></param>
        public DataTable getpdfFile(string pCustomerID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("CustomerID", pCustomerID.ToString());
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getPdfFiles", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return details of customers
        /// </summary>
        /// Date: 11 Jan 2014
        /// <param name="pCustomerID"></param>
        public DataTable getCustomerDetailsByAdvisor(string pAdvisorID)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                QueryParameter paramCustomerID = new QueryParameter("AdvisorID", pAdvisorID);
                listCustomerID.Add(paramCustomerID);
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomerDetailsByAdvisor", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Return sorted advisor lists based on selection criteria
        /// </summary>
        /// <param name="pSearchName"></param>
        /// <param name="pUpdateDate"></param>
        /// <param name="pSearchText"></param>
        /// <returns></returns>
        public DataTable getCustomerDetailsFilter(string pSearchName = null, string pUpdateDate = null, string pSearchText = null, string pAdvisorID = null)
        {
            try
            {
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                if (!String.IsNullOrEmpty(pSearchName))
                    listCustomerID.Add(new QueryParameter("Lastname", pSearchName));
                else
                    listCustomerID.Add(new QueryParameter("Lastname", ""));
                if (!String.IsNullOrEmpty(pUpdateDate))
                    listCustomerID.Add(new QueryParameter("UpdateDate", pUpdateDate));
                else
                    listCustomerID.Add(new QueryParameter("UpdateDate", ""));
                if (!String.IsNullOrEmpty(pSearchText))
                    listCustomerID.Add(new QueryParameter("searchName", pSearchText));
                else
                    listCustomerID.Add(new QueryParameter("searchName", ""));
                listCustomerID.Add(new QueryParameter("AdvisorID", pAdvisorID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomerDetailsFilter", listCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method is used to check customer already exist or not.
        /// Date: 11 June 2014
        /// </summary>
        /// <param name="pEmail"></param>
        /// <returns>bool</returns>
        public bool isCustomerExist(Customers pObjCustomers)
        {
            try
            {
                List<QueryParameter> listEmail = new List<QueryParameter>();
                QueryParameter paramEmail = new QueryParameter("Email", pObjCustomers.Email);
                listEmail.Add(paramEmail);
                Database dbCustomer = new Database();
                DataTable dtFlag = dbCustomer.RunStoredProcedureSelectParameter("isCustomerExist", listEmail);
                if (dtFlag.Rows[0]["Flag"].ToString() == "T")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return false;
            }
        }


        /// <summary>
        /// Used to generate passowrd
        /// </summary>
        /// <returns>string of password</returns>
        public string GeneratePassword()
        {
            try
            {
                string allowedChars = string.Empty;
                allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
                allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
                allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";
                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);
                string passwordString = string.Empty;
                string temp = string.Empty;
                Random rand = new Random();
                for (int i = 0; i < 7; i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    passwordString += temp;
                }
                return passwordString;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("GeneratePassword - Error - " + ex.ToString());
                return string.Empty;
            }

        }

        /// <summary>
        /// Used to update customer password status
        /// </summary>
        /// <param name="pCustomers"></param>
        public void updateCustomerPasswordStatus(Customers pCustomers)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("Email", pCustomers.Email));
                listCustomeDetails.Add(new QueryParameter("Password", pCustomers.Password));
                listCustomeDetails.Add(new QueryParameter("PasswordResetFlag", pCustomers.PasswordResetFlag));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("UpdatePasswordFlag", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Used to update customer password 
        /// </summary>
        /// <param name="pCustomers"></param>
        public void updateCustomerPassword(Customers pCustomers)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("Password", pCustomers.Password));
                listCustomeDetails.Add(new QueryParameter("PasswordResetFlag", pCustomers.PasswordResetFlag));
                listCustomeDetails.Add(new QueryParameter("Email", pCustomers.Email));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateUserPassword", listCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Used to get toal number of users under any advisor
        /// </summary>
        /// <param name="pAdvisorID"></param>
        /// <returns></returns>
        public DataTable GetTotalNumberofUsers(string pAdvisorID)
        {
            try
            {
                List<QueryParameter> listCustomeDetails = new List<QueryParameter>();
                listCustomeDetails.Add(new QueryParameter("AdvisorID", pAdvisorID));
                Database dbCustomer = new Database();
                DataTable intCustomerTotals = dbCustomer.RunStoredProcedureSelectParameter("GetTotalNumberofUsers", listCustomeDetails);
                return intCustomerTotals;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }




        #endregion Personal Info Operation

        #region Financial Info operation

        /// <summary>
        /// This method return details of customers getCustomerFinancialDetailsByStatus
        /// </summary>
        /// Date: 11 June 2014
        /// <param name="pCustomerID"></param>
        public DataTable getCustomerFinancialDetailsByStatus(string pCustomerID, string pMartialStatus)
        {
            try
            {
                List<QueryParameter> lstCustomerID = new List<QueryParameter>();
                lstCustomerID.Add(new QueryParameter("CustomerID", pCustomerID));
                lstCustomerID.Add(new QueryParameter("MartialStatus", pMartialStatus));

                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("GetSecurityInfoByMartialStatus", lstCustomerID);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method used for UpdateCustomerFinancialDetailsForMarried
        /// </summary>
        public void UpdateCustomerFinancialDetailsForMarried(SecurityInfoMarried securityInfoMarried)
        {
            try
            {
                List<QueryParameter> deleteFinancialDetails = new List<QueryParameter>();
                deleteFinancialDetails.Add(new QueryParameter("CustomerID", securityInfoMarried.CustomerID));
                List<QueryParameter> lstFinancialDetailsForMarried = new List<QueryParameter>();
                lstFinancialDetailsForMarried.Add(new QueryParameter("CustomerID", securityInfoMarried.CustomerID));
                lstFinancialDetailsForMarried.Add(new QueryParameter("WifesBirthDate", securityInfoMarried.WifesBirthDate));
                lstFinancialDetailsForMarried.Add(new QueryParameter("WifesRetAgeBenefit", securityInfoMarried.WifesRetAgeBenefit));
                lstFinancialDetailsForMarried.Add(new QueryParameter("HusbandsBirthDate", securityInfoMarried.HusbandsBirthDate));
                lstFinancialDetailsForMarried.Add(new QueryParameter("HusbandsRetAgeBenefit", securityInfoMarried.HusbandsRetAgeBenefit));
                lstFinancialDetailsForMarried.Add(new QueryParameter("SurvivorBenefit", securityInfoMarried.SurvivorBenefit));
                lstFinancialDetailsForMarried.Add(new QueryParameter("Cola", securityInfoMarried.Cola));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureParameter("deleteSecurityInfoByCustomerID", deleteFinancialDetails);
                dbCustomer.RunStoredProcedureSelectParameter("insertCustomerSecurityInfoMarried", lstFinancialDetailsForMarried);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for UpdateCustomerFinancialDetailsForSingle
        /// </summary>
        public void UpdateCustomerFinancialDetailsForSingle(SecurityInfoSingle securitySingleInfo)
        {
            try
            {
                List<QueryParameter> deleteFinancialDetails = new List<QueryParameter>();
                deleteFinancialDetails.Add(new QueryParameter("CustomerID", securitySingleInfo.CustomerID));
                List<QueryParameter> lstFinancialDetailsForSingle = new List<QueryParameter>();
                lstFinancialDetailsForSingle.Add(new QueryParameter("CustomerID", securitySingleInfo.CustomerID));
                lstFinancialDetailsForSingle.Add(new QueryParameter("BirthDate", securitySingleInfo.BirthDate));
                lstFinancialDetailsForSingle.Add(new QueryParameter("RetAgeBenefit", securitySingleInfo.RetAgeBenefit));
                lstFinancialDetailsForSingle.Add(new QueryParameter("Cola", securitySingleInfo.Cola));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureParameter("deleteSecurityInfoByCustomerID", deleteFinancialDetails);
                dbCustomer.RunStoredProcedureSelectParameter("insertCustomerSecurityInfoSingle", lstFinancialDetailsForSingle);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for UpdateCustomerFinancialDetailsForWidowed
        /// </summary>
        public void UpdateCustomerFinancialDetailsForWidowed(SecurityInfoWidowed securityInfoWidowed)
        {
            try
            {
                List<QueryParameter> deleteFinancialDetails = new List<QueryParameter>();
                deleteFinancialDetails.Add(new QueryParameter("CustomerID", securityInfoWidowed.CustomerID));
                List<QueryParameter> lstFinancialDetailsForWidowed = new List<QueryParameter>();
                lstFinancialDetailsForWidowed.Add(new QueryParameter("CustomerID", securityInfoWidowed.CustomerID));
                lstFinancialDetailsForWidowed.Add(new QueryParameter("BirthDate", securityInfoWidowed.BirthDate));
                lstFinancialDetailsForWidowed.Add(new QueryParameter("RetAgeBenefit", securityInfoWidowed.RetAgeBenefit));
                lstFinancialDetailsForWidowed.Add(new QueryParameter("SpousesRetAgeBenefit", securityInfoWidowed.SpousesRetAgeBenefit));
                lstFinancialDetailsForWidowed.Add(new QueryParameter("Cola", securityInfoWidowed.Cola));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureParameter("deleteSecurityInfoByCustomerID", deleteFinancialDetails);
                dbCustomer.RunStoredProcedureSelectParameter("insertCustomerSecurityInfoWidowed", lstFinancialDetailsForWidowed);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used for UpdateCustomerFinancialDetailsForDivorced
        /// </summary>
        public void UpdateCustomerFinancialDetailsForDivorced(SecurityInfoDivorced divorced)
        {
            try
            {
                List<QueryParameter> deleteFinancialDetails = new List<QueryParameter>();
                deleteFinancialDetails.Add(new QueryParameter("CustomerID", divorced.CustomerID));
                List<QueryParameter> lstFinancialDetailsForDivorced = new List<QueryParameter>();
                lstFinancialDetailsForDivorced.Add(new QueryParameter("CustomerID", divorced.CustomerID));
                lstFinancialDetailsForDivorced.Add(new QueryParameter("BirthDate", divorced.BirthDate));
                lstFinancialDetailsForDivorced.Add(new QueryParameter("RetAgeBenefit", divorced.RetAgeBenefit));
                lstFinancialDetailsForDivorced.Add(new QueryParameter("ExSpousesBirthDate", divorced.ExSpousesBirthDate));
                lstFinancialDetailsForDivorced.Add(new QueryParameter("SlimyExsRetAgeBenefit", divorced.SlimyExsRetAgeBenefit));
                lstFinancialDetailsForDivorced.Add(new QueryParameter("Cola", divorced.Cola));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureParameter("deleteSecurityInfoByCustomerID", deleteFinancialDetails);
                dbCustomer.RunStoredProcedureSelectParameter("insertCustomerSecurityInfoDivorced", lstFinancialDetailsForDivorced);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Customer Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteCustomerInfo(Customers objCustomer)
        {
            try
            {
                List<QueryParameter> listInstitutionalDetails = new List<QueryParameter>();
                listInstitutionalDetails.Add(new QueryParameter("CustomerID", objCustomer.CustomerID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteCustomerByCustomerID", listInstitutionalDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

     

        #endregion Financial Info operation
    }
}
