﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Security Information for Divorced Customer
    /// </summary>
    public class SecurityInfoWidowed
    {
        
        #region Properties

        public string CustomerID
        {
            get; 
            set;
        }

        public string BirthDate 
        { 
            get; 
            set; 
        }

        public string RetAgeBenefit
        {
            get;
            set;
        }

        public string SpousesRetAgeBenefit
        {
            get;
            set;
        }

        public string Cola
        {
            get;
            set;
        }        
        #endregion Properties        
    }
}
