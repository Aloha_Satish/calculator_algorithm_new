﻿using Symbolics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class Pricing : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Used to set advisor selected plan as monthly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Monthly_Plan_Click(object sender, EventArgs e)
        {
            Globals.Instance.strAdvisorPlan = Constants.MonthlyPlan;
            Response.Redirect(Constants.RedirectMonthlyAdvisorRegistration, false);
        }

        /// <summary>
        /// Used to set advisor selected plan as annual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Annual_Plan_Click(object sender, EventArgs e)
        {
            Globals.Instance.strAdvisorPlan = Constants.AnnualPlan;
            Response.Redirect(Constants.RedirectAnnualAdvisorRegistration, false);
        }

        /// <summary>
        /// Pricing page's page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Demo"] = null;
        }
        #endregion Events

    }
}