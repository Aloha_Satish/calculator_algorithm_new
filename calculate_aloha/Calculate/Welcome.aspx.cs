﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Reflection;
using System.Web.UI;

namespace Calculate
{
    public partial class Welcome : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// function when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                if (Request.QueryString[Constants.reference] != null)
                {
                    customer.updateCustomerDetails(Request.QueryString[Constants.reference].ToString());
                    //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "SubscribeStatus();", true);
                }
                if (Session[Constants.SessionUserId] == null)
                    Response.Redirect(Constants.RedirectRegister, true);
                if (!IsPostBack)
                    getCustomerDetails();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcome, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcome + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Redirect to Social Security
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void redirectToSocialSecurity(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                string sIsSubscription = dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString();
                string sMaritalStatus = dtCustomerDetails.Rows[Constants.zero][Constants.MartialStatus].ToString();
                if (sIsSubscription.Equals(Constants.FlagNo))
                {
                    if (sMaritalStatus.Equals("Single"))
                        Response.Redirect(Constants.RedirectCalcSingle, false);
                    else if (sMaritalStatus.Equals("Married"))
                        Response.Redirect(Constants.RedirectCalcMarried, false);
                    else if (sMaritalStatus.Equals("Divorced"))
                        Response.Redirect(Constants.RedirectCalcDivorced, false);
                    else
                        Response.Redirect(Constants.RedirectCalcWidowed, false);
                    
                    
                    
                    //Added for testing the various cases
                    //Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                }
                else
                {
                    Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcome, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcome + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Customers customer = new Customers();
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);

                //Changes for live Server
                Response.Redirect(Constants.TrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);

                // Redirect to chargify url 
                //Response.Redirect(Constants.TrialLiveServerURL  + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcome, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcome + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWithoutTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Customers customer = new Customers();
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.TrialPeriod);

                //Changes for live Server
                Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);

                // Redirect to chargify url 
                //Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcome, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcome + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// get Customer Details to display user name
        /// </summary>
        private void getCustomerDetails()
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                lblWelcome.Text = " " + dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();

                /* Comment Dev :- Aloha Date:-02/12/2014 */
                //if (Request.QueryString["ref"] != null)
                //{
                //    customer.updateCustomerDetails(Request.QueryString["ref"].ToString());
                //    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "SubscribeStatus();", true);
                //}

                /* Comment Dev :- Aloha Date:-01/11/2014 */
                //if (Convert.ToChar(dtCustomerDetails.Rows[0]["isSubscription"]).Equals('N'))
                //{
                //    if (Session[Constants.SessionRegistered] != null)
                //    {
                //        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(true);", true);
                //    }
                //    else
                //    {
                //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(false);", true);
                //    }
                //}

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcome, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcome + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion
    }
}