﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CancelPolicy.aspx.cs" Inherits="Calculate.CancelPolicy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cancellations and Refunds Policy</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />

    <script type="text/javascript">
        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div style="background-color: white; max-height: calc(100% - 120px); padding: 0px 10%;" data-role="main" class="ui-content mainbodydiv">
            <div style="align-content: center; margin-left: 20px; margin-right: 20px; font-family: Arial" class="row">
                <h1 style="color: red">Cancellations and Refunds Policy</h1>
                <h2>The Paid to Wait Social Security Calculator</h2>

                <div>
                    <p style="font-size: large; font: bold; color: black">Digital Purchases and Subscriptions</p>
                    <p>
                        You can change or cancel your digital subscription at any time by calling 1-844-924-8270 or emailing Luke@GettingPaidToWait.com.  Corporate subscription billing cycles and terms of cancellations may differ.
                <br />
                    </p>
                    <p style="font-size: large; font: bold; color: black">Monthly Billing </p>
                    <p>
                        If you cancel the monthly subscription within 30 days of purchase, your access and other benefits will end immediately and you will receive a full refund. After 30 days, when you cancel a subscription based on a monthly billing cycle, you cancel only future charges associated with your subscription. You may notify us of your intent to cancel at any time, but the cancellation will become effective at the end of your current billing period.
                <br />
                        <br />
                        Cancellations are effective the following billing cycle. You will not receive a refund for the current billing cycle. You will continue to have the same access and benefits of your product for the remainder of the current billing period.
                <br />
                        <br />
                        However, if the subscription is cancelled within the first month, you will receive a refund for that month. This refund applies only to the first month.
                <br />
                    </p>
                    <p style="font-size: large; font: bold; color: black">Annual Billing </p>
                    <p>
                        If you cancel the annual subscription within 30 days of purchase, your access and other benefits will end immediately and you will receive a full refund. If you cancel after 30 days of purchase but before 6 months of your subscription year, your access and other benefits will end immediately and you will receive a refund of 50% of your purchase cost. After the first six months, you will not receive a refund for the annual subscription.
                <br />
                    </p>
                    <p style="font-size: large; font: bold; color: black">Single Use </p>
                    <p>
                        No refunds are available for single use purchases.
                <br />
                        <br />
                        We reserve the right to issue refunds or credits at our sole discretion. If we issue a refund or credit, we are under no obligation to issue the same or similar refund in the future.
                <br />
                    </p>
                    <p style="font-size: large; font: bold; color: black">Books and other products</p>
                    <p>
                        We accept returns. You can return unopened items in the original packaging within 30 days of your purchase with receipt or proof of purchase. If 30 days or more have passed since your purchase, we cannot offer you a refund or an exchange.
                <br />
                        <br />
                        Upon receipt of the returned item, we will examine it and notify you via email, within a reasonable period of time, whether you are entitled to a return. If you are entitled to a return, we will refund your purchase price and a credit will automatically be applied to your original method of payment. 
                <br />
                        <br />
                        Refunds do not include any shipping and handling charges shown on the packaging slip or invoice. Shipping charges for all returns must be prepaid and insured by you. Shipping and handling charges are not refundable. 
                <br />
                        <br />
                        We reserve the right to issue refunds or credits at our sole discretion. If we issue a refund or credit, we are under no obligation to issue the same or similar refund in the future.
                <br />
                        <br />
                        <br />
                    </p>
                    <p>
                        Flitech, LLC
                <br />
                        3056 New Williamsburg Dr,<br />
                        Schenectady NY, 12303<br />
                        <br />
                        Luke@GettingPaidToWait.com<br />
                        1-844-924-8270
                    </p>
                </div>
            </div>
        </div>

        <%--Footer--%>
        <div class="footerbg">
            <div>
                <div class="clearfix">
                    <div class="col-md-12 column removepaddingleftright">
                        <div id="footer" runat="server" style="background-color: #333333; height: auto; text-align: center;">
                            <table class="table-responsive">
                                <tr class="col-md-12 col-sm-12">
                                    <td class="col-md-2 col-sm-2"></td>
                                    <td class="col-md-2 col-sm-2">
                                        <div id="footer-left">
                                            &copy; 2016 Filtech, LLC. All Rights Reserved.
                                                <br />
                                            3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                            Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                            <br />
                                            <p style="padding-top: 10px;">
                                                <%if (Session["UserID"] != null)
                                                  {%>
                                                <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                <%}
                                                  else
                                                  { %>
                                                <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                <%} %>
                                            </p>
                                        </div>
                                    </td>
                                    <td class="col-md-2 col-sm-2">
                                        <table style="width: 100px">
                                            <tr>
                                                <td>
                                                    <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                        <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                        <img src="/Images/fbicon.png" class="img-responsive" />
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://twitter.com/BrianDoherty57">
                                                        <img src="/Images/twittericon.png" class="img-responsive" />
                                                    </a>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
