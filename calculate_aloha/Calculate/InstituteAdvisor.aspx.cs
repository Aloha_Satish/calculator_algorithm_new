﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using Calculate.Objects;
using System.Reflection;
using System.Web.UI;

namespace Calculate
{
    public partial class InstituteAdvisor : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        List<string> listState = new List<string> {"","AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI",
                                                   "ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN",
                                                   "MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH",
                                                   "OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA",
                                                   "WV","WI","WY"};
        #endregion Variables

        #region Events

        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                ddlState.DataSource = null;
                ddlState.DataSource = listState;
                ddlState.DataBind();
                ddlState.SelectedValue = string.Empty;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorInstituteAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorInstituteAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }
        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                addAdvisor(false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorInstituteAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorInstituteAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }
        #endregion Events 

        #region Methods
        /// <summary>
        ///Used to add advisor
        ///private Customers addAdvisor(Boolean delFlag, Boolean subscriptionFlag)
        /// </summary>
        /// <param name="delFlag"></param>
        /// <returns></returns>
        private AdvisorTO addAdvisor(Boolean delFlag)
        {
            try
            {
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                InsfisuionSoftProcesses infusion = new InsfisuionSoftProcesses();
                InfusionUser infusionUser = new InfusionUser();
                objCustomer.Email = txtEmail.Text;
                lblErrorMessage.Visible = true;
                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* initialize Instance of classes */
                    AdvisorTO objAdvisorTO = new AdvisorTO();
                    Advisor objAdvisor = new Advisor();
                    #region set Insert Parameters
                    objAdvisorTO.AdvisorID = Database.GenerateGID("A");
                    //TouchStone Id is constant to InstitutionAdminID 
                    objAdvisorTO.InstitutionAdminID = Constants.TouchStoneID;
                    objAdvisorTO.AdvisorName = textInfo.ToTitleCase(txtFirstname.Text);
                    objAdvisorTO.Role = Constants.Advisor;
                    objAdvisorTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                    objAdvisorTO.Lastname = txtLastname.Text;
                    objAdvisorTO.Email = txtEmail.Text;
                    objAdvisorTO.State = ddlState.Text;
                    objAdvisorTO.Dealer = txtDealer.Text;
                    objAdvisorTO.City = txtCity.Text;
                    objAdvisorTO.isSubscription = Constants.FlagYes;
                    //Generate Random Password
                    string stPassword = objCustomer.GeneratePassword();
                    objAdvisorTO.Password = Encryption.Encrypt(stPassword);
                    objAdvisorTO.isInstitutionSubscription = Constants.FlagYes;
                    if (delFlag)
                        objAdvisorTO.DelFlag = Constants.FlagYes;
                    else
                        objAdvisorTO.DelFlag = Constants.FlagNo;
                    objAdvisorTO.PasswordResetFlag = Constants.FlagNo;
                    #endregion set Insert Parameters

                    txtEmail.Enabled = true;
                    /* Insert institution details into Database */
                    objAdvisor.insertAdvisorDetails(objAdvisorTO);

                    #region Mail credentials to Institute
                    string strBody = "Hi " + txtFirstname.Text + ", <br /><br />       You have been successfully registered with the Paid to Wait Calculator.  <br />Your login is " + txtEmail.Text + "<br />Your password is " + stPassword + " <br /> Please <a href='http://calculatemybenefits.com/?ref=login'>Click here</a> to login. <br /><br /> Thanks & Regards,<br /> Brian Doherty";
                    MailHelper.sendMail(txtEmail.Text, "Social Security Calculator", strBody, string.Empty);
                    #endregion Mail credentials to Institute

                    #region Add new contact of user into InfusionSoft
                    infusionUser.Name = txtFirstname.Text.Trim();
                    infusionUser.LastName = txtLastname.Text.Trim();
                    infusionUser.Email = txtEmail.Text.Trim();
                    infusionUser.State = ddlState.Text.Trim();
                    infusionUser.City = txtCity.Text.Trim();
                    infusion.AddContactintoInfusionSoft(infusionUser);
                    #endregion Add new contact of user into InfusionSoft

                    ResetControls();

                    lblErrorMessage.Text = Constants.AdvisorAdded;
                    lblErrorMessage.ForeColor = Color.Green;

                    //btnNo.Visible = true;
                    //btnYes.Visible = true;
                    lblMessage.Text = "You have been successfully registered.<br>Your account credentials have been sent to your e-mail.";
                    MailSentPopUp.Show();

                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, "SuccessFunction();", true);

                    //Redirect to login page
                    // Response.Redirect(Constants.RedirectToLogin, false);

                    return objAdvisorTO;
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.AdvisorEmialExists, true);
                    lblErrorMessage.Text = Constants.ErrorAdvisorEmailExist;
                    lblErrorMessage.ForeColor = Color.Red;
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorInstituteAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorInstituteAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
                return null;
            }
        }

        /// <summary>
        /// Used to reset all fields
        /// </summary>
        private void ResetControls()
        {
            try
            {
                txtFirstname.Text = string.Empty;
                txtLastname.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtDealer.Text = string.Empty;
                txtCity.Text = string.Empty;
                ddlState.SelectedItem.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorInstituteAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorInstituteAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }
        #endregion Methods

    }
}