﻿using Calculate.Objects;
using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
namespace Calculate
{
    public partial class SiteNew : System.Web.UI.MasterPage
    {
        #region variables
        public static string strFirstname = string.Empty;
        public static string strSpouseFirstname = string.Empty;
        public static string strHusbandExplanation = string.Empty;
        public static string strWifeExplanation = string.Empty;
        public static string strCustomerMaritalStatus = string.Empty;
        public static string age = string.Empty;
        public virtual bool CausesValidation { get; set; }
        public static string HusbDOB = string.Empty;
        public static DateTime limit = DateTime.Parse("01/01/1954");
        public static bool WifeChange = false;
        public static bool HusbChange = false;
        public static string WifeLastChange = string.Empty;
        public static string HusbLastChange = string.Empty;





        #endregion variables

        #region Events

        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateUserOnPageLoad();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Validate User
        /// </summary>
        public void ValidateUserOnPageLoad()
        {
            try
            {
                //GenerateMenus();
                if (Session[Constants.SessionRole] != null)
                {
                    string url = Request.Url.AbsoluteUri;
                    DataTable dtCustomerDetails = new DataTable();
                    // Theme Implementation
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        DataTable dtTheme = objTheme.getThemeDetailsByID(Session[Constants.SessionAdminId].ToString());
                        string js = String.Empty;
                        if (dtTheme.Rows.Count > Constants.zero)
                        {
                            string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                            string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                            string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                            byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                            // heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                            // heading.Attributes.Remove(Constants.classCss);
                            // heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                            string src = string.Empty;
                            if (imageBytes != null)
                                src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                            js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);

                        Institutional objInstitutional = new Institutional();
                        dtCustomerDetails = objInstitutional.getInstitutionalDetailsByID(Session[Constants.SessionAdminId].ToString());
                        //if (!dtCustomerDetails.Rows[0]["isSubscription"].ToString().Equals("Y") && !url.Contains("WelcomeAdmin.aspx"))
                        //    Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        string InstitutionId = objTheme.isThemeExistForAdvisor(Session[Constants.SessionAdminId].ToString());
                        if (InstitutionId != string.Empty)
                        {
                            DataTable dtTheme = objTheme.getThemeDetailsByID(InstitutionId);
                            if (dtTheme.Rows.Count > Constants.zero)
                            {
                                string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                                string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                                string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                                byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                                //heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                                // heading.Attributes.Remove(Constants.classCss);
                                //heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                                string src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                                string js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                            }
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        string InstitutionId = objTheme.isThemeExistForCustomer(Session[Constants.SessionUserId].ToString());
                        if (InstitutionId != string.Empty)
                        {
                            DataTable dtTheme = objTheme.getThemeDetailsByID(InstitutionId);
                            if (dtTheme.Rows.Count > Constants.zero)
                            {
                                string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                                string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                                string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                                byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                                //heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                                //heading.Attributes.Remove(Constants.classCss);
                                // heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                                string src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                                string js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                            }
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);

                    //if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) && !url.Contains(Constants.WelcomeAdmin))
                    //{
                    //    Advisor objAdvisor = new Advisor();
                    //    dtCustomerDetails = objAdvisor.getAdvisorDetailsByID(Session[Constants.SessionAdminId].ToString());
                    //    if (dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                    //    {
                    //        Response.Redirect(Constants.RedirectManageCustomers, false);
                    //    }
                    //    else
                    //    {
                    //        Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                    //    }
                    //}
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                SetBanner();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }


        private void SetBanner()
        {
            if (Session[Constants.BannerImage].ToString().Equals(Constants.SessionBanner))
            {
                string strUrlName = Request.Url.Host.ToLower();
                Theme themes = new Theme();
                DataTable dtTheme = themes.GetBannerDetailsByDomainName(strUrlName);
                if (dtTheme.Rows.Count > 0)
                {
                    byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.BannerImage];
                    Globals.Instance.ChangeBanner = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                }
                else
                {
                    Globals.Instance.ChangeBanner = Constants.DefaultBanner;
                }
            }
            else
            {
                Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);

            try
            {
                if (Session[Constants.BannerImage].ToString().Equals("Y"))
                    Globals.Instance.ChangeBanner = "/Images/BannerGrand.jpg";
                else
                    Globals.Instance.ChangeBanner = "/Images/BannerRegular.png";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        #endregion Events

        #region  Common Functions used in all pages

        /// <summary>
        /// Generate Menus
        /// </summary>
        //public void GenerateMenus()
        //{
        //    try
        //    {
        //        if (Session[Constants.Role] != null && Session[Constants.Role].Equals(Constants.Institutional))
        //        {
        //            // Check Side Menu contain items or not 
        //            if (ClientMenu.Items.Count <= 0)
        //            {
        //                // Create Object of Menu Items
        //                MenuItems objMenuItems = new MenuItems();
        //                // Get list of menu items based on user roles
        //                DataTable dtNavigation = objMenuItems.generateMenuItems(Session[Constants.SessionRole].ToString());
        //                /*  DataView dv = dtNavigation.DefaultView;
        //                  dv.Sort = "MenuID ASC";
        //                  dtNavigation = dv.ToTable(); */
        //                // For loop to iterate menu items
        //                for (int iMenus = 0; iMenus < dtNavigation.Rows.Count; iMenus++)
        //                {
        //                    MenuItem menuItem = new MenuItem();
        //                    menuItem.Text = dtNavigation.Rows[iMenus][Constants.MenuItem].ToString();
        //                    menuItem.NavigateUrl = dtNavigation.Rows[iMenus][Constants.URL].ToString();
        //                    if (!menuItem.Text.Equals(Constants.MyAccount))
        //                        ClientMenu.Items.Add(menuItem);
        //                }
        //                //content.Style.Add("border-left", "solid");
        //                //content.Style.Add("style", "border:solid");
        //                //MenuItem menuItemLogout = new MenuItem();
        //                //menuItemLogout.Text = Symbolics.Constants.LogOut;
        //                //menuItemLogout.NavigateUrl = "SignUp.aspx";
        //                //ClientMenu.Items.Add(menuItemLogout);
        //            }
        //        }
        //        else
        //        {
        //            sideMenu.Style.Add("Display", "none");
        //            centerDiv.Attributes.Add("class", "col-md-12 removepaddingleftright");  //add removepaddingleftright css 11.06.2015
        //            // content.Style.Add("style", "border:none;"); 
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
        //    }
        //}

        /// <summary>
        /// Numeric validation - check integer and range
        /// </summary>
        /// <param name="textCntl"></param>
        /// <param name="lowEnd"></param>
        /// <param name="highEnd"></param>
        /// <param name="cntlName"></param>
        /// <param name="isValid"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public static int ValidateNumeric(TextBox textCntl, int lowEnd, int highEnd, string cntlName, ref bool isValid, Label ErrorMessage)
        {
            try
            {
                textCntl.CssClass = string.Empty;
                int ageWork = Constants.zero;
                if (!int.TryParse(textCntl.Text, out ageWork))
                {
                    if (ErrorMessage.Text != string.Empty)
                        ErrorMessage.Text += " <Br />";
                    ErrorMessage.Text += cntlName + " must be a numeric.";
                    textCntl.CssClass = "StyleError";
                    isValid = false;
                }
                else if (ageWork < lowEnd || ageWork > highEnd)
                {
                    if (ErrorMessage.Text != string.Empty) ErrorMessage.Text += " <Br />";
                    ErrorMessage.Text += cntlName + " must be between " + lowEnd.ToString() + " and " + highEnd.ToString() + ".";
                    textCntl.CssClass = "StyleError";
                    isValid = false;
                }
                return ageWork;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return -1;
            }
        }

        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Update Active Flag for All user based on UserId
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="gvDetails"></param>
        public static void updateActiveFlag(object sender, GridView gvDetails)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Redirect to Advisor UI with institution Id */
                    Label InstitutionID = (Label)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.lblUserID);
                    ImageButton imgButton = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.imgStatus);
                    Users objUser = new Users();
                    /* Check if Active flag matched with value */
                    if (Convert.ToString(imgButton.AlternateText).Equals(Constants.FlagYes))
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagNo;
                        imgButton.ImageUrl = Constants.DeactivateImage;
                        imgButton.Attributes.Add(Constants.title, Constants.Activate);
                    }
                    else
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagYes;
                        imgButton.ImageUrl = Constants.ActivateImage;
                        imgButton.Attributes.Add(Constants.title, Constants.Suspend);
                    }
                    /* Update Active Flag into User login Details */
                    objUser.updateUserDetails(InstitutionID.Text, imgButton.AlternateText);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// code when logout button is clicked
        /// session made null and the banner url again sent to the home page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Logout(object sender, EventArgs e)
        {
            try
            {
                LogOutApplication();
            }
            catch (Exception ex)
            {
                ClearSession();
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "Logout() - " + ex.ToString());
            }
        }

        /// <summary>
        /// Abandon Sessions
        /// </summary>
        public void LogOutApplication()
        {
            try
            {
                if (HttpContext.Current.Session[Constants.BannerImage] != null)
                {
                    if (HttpContext.Current.Session[Constants.BannerImage].ToString().Equals("Y"))
                        HttpContext.Current.Response.Redirect(Constants.RedirectToGettingPaidToWaitHome);
                    else
                        HttpContext.Current.Response.Redirect(Constants.RedirectToGettingPaidToWaitHome);
                }
                else
                {
                    HttpContext.Current.Response.Redirect(Constants.RedirectToGettingPaidToWaitHome);
                }
                ClearSession();
            }
            catch (Exception ex)
            {
                ClearSession();
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "Logout() - " + ex.ToString());

            }
        }

        /// <summary>
        /// code to clear the sessions
        /// </summary>
        private void ClearSession()
        {
            try
            {
                HttpContext.Current.Session.Clear();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "ClearSession() - " + ex.ToString());
            }
        }

        /// <summary>
        /// Used ti add disclaimer into pdf report
        /// </summary>
        /// <param name="pdfDoc">PDF Document</param>
        public static void AddDisclaimerIntoPDFReport(ref Document pdfDoc)
        {
            try
            {
                pdfDoc.NewPage();
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.Disclaimer, false, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DisclPara1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DisclPara2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DisclPara3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DisclPara4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DisclPara5, false, false, false));

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "AddDisclaimerIntoPDFReport() - " + ex.ToString());
            }
        }

        #region Convert Data into PDF

        /// <summary>
        /// Pdf Font Styling
        /// </summary>
        /// <returns>Font</returns>
        public static iTextSharp.text.Font pdfFontStyle()
        {
            try
            {
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 16);
                return font;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Display image in pdf with formatting
        /// </summary>
        /// <param name="imageLocation"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static iTextSharp.text.Image generateImage(string imageLocation, float height, float width)
        {
            try
            {
                /* Create an image instance for PDF */
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~") + imageLocation);
                /* Changes in image properties */
                image.ScaleAbsolute(height, width);
                image.Alignment = iTextSharp.text.Image.ALIGN_LEFT;
                return image;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Display data in paragraph with formatting
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="Indentation"></param>
        /// <param name="AlignCenter"></param>
        /// <param name="bold"></param>
        /// <returns></returns>
        public static iTextSharp.text.Paragraph generateParagraph(string Content, Boolean Indentation, Boolean AlignCenter, Boolean bold)
        {
            try
            {
                Indentation = false;
                /* Create an paragraph instance for PDF */
                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(Content, pdfFontStyle());
                /* Check if alignment of paragraph is center or not */
                if (AlignCenter)
                    paragraph.Alignment = Element.ALIGN_CENTER;
                else
                {
                    paragraph.Alignment = Element.ALIGN_LEFT;
                    if (Indentation)
                    {
                        paragraph.FirstLineIndent = Constants.mediumIndentation;
                        paragraph.IndentationLeft = 140f;
                        paragraph.IndentationRight = Constants.smallIndentation;
                        paragraph.Alignment = Element.ALIGN_JUSTIFIED;
                    }
                    else
                    {
                        paragraph.IndentationLeft = Constants.smallIndentation;
                        paragraph.IndentationRight = Constants.smallIndentation;
                        paragraph.Alignment = Element.ALIGN_JUSTIFIED;
                    }
                }
                /* Check if paragraph required bold or not */
                if (bold)
                    paragraph.Font.SetStyle(iTextSharp.text.Font.BOLD);

                return paragraph;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Used to print keys into PDF
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="Indentation"></param>
        /// <param name="AlignCenter"></param>
        /// <param name="bold"></param>
        /// <returns></returns>
        public static iTextSharp.text.Paragraph generateParagraphKeys(string Content, Boolean Indentation, Boolean AlignCenter, Boolean bold)
        {
            try
            {
                Indentation = false;
                /* Create an paragraph instance for PDF */
                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(Content, pdfFontStyle());
                /* Check if alignment of paragraph is center or not */
                if (AlignCenter)
                    paragraph.Alignment = Element.ALIGN_CENTER;
                else
                {
                    paragraph.Alignment = Element.ALIGN_LEFT;
                    if (Indentation)
                    {
                        paragraph.FirstLineIndent = -220f;
                        paragraph.IndentationLeft = 260f;
                        paragraph.IndentationRight = Constants.smallIndentation;
                        paragraph.Alignment = Element.ALIGN_JUSTIFIED;
                    }
                    else
                    {
                        paragraph.IndentationLeft = Constants.smallIndentation;
                        paragraph.IndentationRight = Constants.smallIndentation;
                        paragraph.Alignment = Element.ALIGN_JUSTIFIED;
                    }
                }
                /* Check if paragraph required bold or not */
                if (bold)
                    paragraph.Font.SetStyle(iTextSharp.text.Font.BOLD);

                return paragraph;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Display data in paragraph with formatting
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="Indentation"></param>
        /// <param name="AlignCenter"></param>
        /// <param name="bold"></param>
        /// <returns></returns>
        public static iTextSharp.text.Paragraph generateChartHeading(string Content)
        {
            try
            {
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 18);
                font.SetColor(169, 169, 169);
                /* Create an paragraph instance for PDF */
                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph(Content, font);
                /* Check if alignment of paragraph is center or not */
                paragraph.Alignment = Element.ALIGN_CENTER;
                paragraph.Font = font;
                paragraph.Font.SetColor(169, 169, 169);
                paragraph.Font.SetStyle(iTextSharp.text.Font.BOLD);
                return paragraph;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Display data in phrase with formatting
        /// </summary>
        /// <param name="Content"></param>
        /// <returns></returns>
        public static iTextSharp.text.Phrase generatePhrase(string Content)
        {
            try
            {
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 14);
                /* Create an paragraph instance for PDF */
                iTextSharp.text.Paragraph paragraphContent = new iTextSharp.text.Paragraph(Content, font);
                /* Check if alignment of paragraph is center or not */
                paragraphContent.Alignment = Element.ALIGN_LEFT;
                paragraphContent.Font = font;
                paragraphContent.Font.SetStyle(iTextSharp.text.Font.BOLD);
                paragraphContent.IndentationLeft = 10f;
                return paragraphContent;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Render Grid View into Stacked Column Asp.net Chart
        /// </summary>
        /// <param name="gvInfo"></param>
        /// <param name="chart"></param>
        /// <param name="lblInfo"></param>
        /// <param name="maritalStatus"></param>
        public static void renderGridViewIntoStackedChart(GridView gvInfo, Chart chart, Label lblInfo, string maritalStatus, Boolean stackedColumn)
        {
            try
            {
                #region code to set the variables
                /* Get Customer Details for showing up in PDF  */
                Calc calc = new Calc() { };
                string strUserName = string.Empty, strSpouseName = string.Empty;
                Customers customer = new Customers();
                DataTable dtCustomerDetails = new DataTable();

                if (HttpContext.Current.Session["Demo"] != null)
                {
                    HttpContext.Current.Session["custFirstName"] = HttpContext.Current.Session["DemoHusbandName"].ToString();
                    HttpContext.Current.Session["Sname"] = HttpContext.Current.Session["DemoWifeName"].ToString();
                    strSpouseName = HttpContext.Current.Session["DemoWifeName"].ToString();
                    strUserName = HttpContext.Current.Session["DemoHusbandName"].ToString();
                    if (maritalStatus.Equals(Constants.Married))
                    {
                        DateTime dtHusb = Convert.ToDateTime(HttpContext.Current.Session["DemoHusbandBDate"]);
                        //HttpContext.Current.Session[Constants.WifeCurrentAge] = DateTime.Today.Year - dtWife.Year;
                        HttpContext.Current.Session[Constants.HusbandCurrentAge] = DateTime.Today.Year - dtHusb.Year;
                    }
                }
                else
                {
                    if (HttpContext.Current.Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || HttpContext.Current.Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || HttpContext.Current.Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        dtCustomerDetails = customer.getCustomerDetails(HttpContext.Current.Session["AdvisorCustomer"].ToString());
                    else
                        dtCustomerDetails = customer.getCustomerDetails(HttpContext.Current.Session[Constants.SessionUserId].ToString());
                    HttpContext.Current.Session["custFirstName"] = dtCustomerDetails.Rows[0][Constants.CustomerFirstName];
                    HttpContext.Current.Session["Sname"] = dtCustomerDetails.Rows[0][Constants.CustomerSpouseFirstName];
                    strUserName = Convert.ToString(dtCustomerDetails.Rows[0][Constants.CustomerFirstName]);
                    strSpouseName = Convert.ToString(dtCustomerDetails.Rows[0][Constants.CustomerSpouseFirstName]);
                    if (maritalStatus.Equals(Constants.Married))
                    {
                        DateTime dtHusb = Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate]);
                        //HttpContext.Current.Session[Constants.WifeCurrentAge] = DateTime.Today.Year - dtWife.Year;
                        HttpContext.Current.Session[Constants.HusbandCurrentAge] = DateTime.Today.Year - dtHusb.Year;
                    }

                }
                /* Export rows from GridView to table */
                string[] xAxis = new string[gvInfo.Rows.Count];
                int[] yAxisSeries1 = new int[gvInfo.Rows.Count];
                int[] yAxisSeries2 = new int[gvInfo.Rows.Count];
                #endregion code to set the variables

                #region code to set the calcualtions for all strategies
                for (int rowIndex = 0; rowIndex < gvInfo.Rows.Count; rowIndex++)
                {
                    /* Check if rowtype matches with datacontrol row type or not */
                    if (gvInfo.Rows[rowIndex].RowType == DataControlRowType.DataRow)
                    {
                        /* Fill data into array */
                        //the chart is plotted montly for all strategies
                        //to change the values change the column numbers of the gird(gvInfo)
                        //the value on the X-axis of the ages of the user for all strategies
                        xAxis[rowIndex] = HttpContext.Current.Server.HtmlDecode(gvInfo.Rows[rowIndex].Cells[0].Text).ToString();
                        //the y-axis values of husband as the stacked chart it will have 2 y-series
                        if (maritalStatus.Equals(Constants.Married))
                        {
                            yAxisSeries1[rowIndex] = Convert.ToInt32(HttpContext.Current.Server.HtmlDecode(gvInfo.Rows[rowIndex].Cells[2].Text).Replace("$", "").Replace(",", "").Replace("*", ""));
                        }
                        //this is y-axis values for Widow,Single,Divorce
                        //the chart is plotted monthly income
                        else
                        {
                            yAxisSeries1[rowIndex] = Convert.ToInt32(HttpContext.Current.Server.HtmlDecode(gvInfo.Rows[rowIndex].Cells[1].Text).Replace("$", "").Replace(",", "").Replace("*", ""));
                        }
                        //this will be the 2 Y-axis series for the married case for the montly values of the wife.
                        // Change the code for graph - old code
                        // Date : 28 april 2015
                        // yAxisSeries2[rowIndex] = Convert.ToInt32(HttpContext.Current.Server.HtmlDecode(gvInfo.Rows[rowIndex].Cells[1].Text).Replace("$", "").Replace(",", "").Replace("*", ""));
                        if (stackedColumn)
                            yAxisSeries2[rowIndex] = Convert.ToInt32(HttpContext.Current.Server.HtmlDecode(gvInfo.Rows[rowIndex].Cells[1].Text).Replace("$", string.Empty).Replace(",", string.Empty).Replace("*", string.Empty));
                    }
                }
                #endregion code to set the calcualtions for all strategies

                #region code setting chart properties
                /* Implement and changes in asp.net chart properties */
                chart.Series[0].Points.DataBindXY(xAxis, yAxisSeries1);
                /* Check if stacked column then embedded data into series level */
                if (stackedColumn)
                {
                    chart.Series[1].Points.DataBindXY(xAxis, yAxisSeries2);
                    chart.Series[1].Name = strSpouseName + Constants.yAxisSeriesLabel;
                    chart.Series[1].Font = new System.Drawing.Font(FontFamily.GenericSansSerif, Constants.fontSize12, FontStyle.Bold);
                }

                /* Chart Properties */
                System.Web.UI.DataVisualization.Charting.Grid gd = new System.Web.UI.DataVisualization.Charting.Grid();
                gd.LineWidth = Constants.widthNone;
                chart.BorderlineColor = System.Drawing.Color.LightGray;
                chart.ChartAreas[0].AxisX.MajorGrid = gd;
                chart.ChartAreas[0].AxisX.LineColor = System.Drawing.Color.LightGray;
                chart.ChartAreas[0].AxisY.LabelStyle.Format = Constants.xAxisFormat;
                chart.ChartAreas[0].AxisY.LabelStyle.Font = new System.Drawing.Font(FontFamily.GenericSansSerif, Constants.fontSize10, FontStyle.Regular);
                chart.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font(FontFamily.GenericSansSerif, Constants.fontSize12, FontStyle.Regular);
                chart.ChartAreas[0].AxisY.TitleForeColor = System.Drawing.Color.LightGray;
                chart.ChartAreas[0].AxisY.LineColor = System.Drawing.Color.LightGray;
                chart.ChartAreas[0].Area3DStyle.Enable3D = false;

                if (strUserName.Equals(strSpouseName))
                    chart.Series[0].Name = strUserName + "_User" + Constants.yAxisSeriesLabel;
                else
                    chart.Series[0].Name = strUserName + Constants.yAxisSeriesLabel;

                chart.Series[0].Font = new System.Drawing.Font(FontFamily.GenericSansSerif, Constants.fontSize12, FontStyle.Bold);
                chart.Legends[0].Enabled = true;
                chart.ChartAreas[0].AxisX.Interval = 1;
                chart.Width = Constants.chartWidth;
                chart.Height = Constants.chartHeight;
                chart.BorderlineColor = System.Drawing.Color.LightGray;
                chart.BorderlineWidth = Constants.chartImageBorderWidth;
                #endregion code setting chart properties
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Convert Asp.net Charts into Image for Pdf
        /// </summary>
        /// <param name="chart"></param>
        /// <param name="pdfDoc"></param>
        public static void convertChartToImage(Chart chart, ref Document pdfDoc)
        {
            try
            {
                /* Initialize Memory Stream Object */
                using (MemoryStream stream = new MemoryStream())
                {
                    /* Save Chart Image into memory stream object */
                    chart.SaveImage(stream, ChartImageFormat.Png);
                    /* Get byte data from stream object for Image */
                    iTextSharp.text.Image chartImage = iTextSharp.text.Image.GetInstance(stream.GetBuffer());
                    /* Define Chart Image Width and Height */
                    chartImage.ScaleAbsolute(Constants.chartImageWidth, Constants.chartImageHeight);
                    chartImage.Alignment = Element.ALIGN_CENTER;
                    pdfDoc.Add(chartImage);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Rendered Customer Steps into PDF
        /// </summary>
        /// <param name="label"></param>
        /// <param name="pdfDoc"></param>
        public static void renderStepsIntoPdf(String strGridName, ref Document pdfDoc, String mstatus, string WifeFra, string HusbFRA)
        {
            try
            {
                #region code to set variables
                String strCustomerFirstName = HttpContext.Current.Session["custFirstName"].ToString();
                String strSpouseFirstName = HttpContext.Current.Session["Sname"].ToString();
                HusbDOB = HusbFRA;
                string completeHusbFRA = HusbFRA;
                string completeWifeFRA = WifeFra;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                if (!String.IsNullOrEmpty(HusbFRA)) //Added on 4May to show steps for singles
                    HusbFRA = HusbFRA.Substring(0, 2);
                if (!String.IsNullOrEmpty(WifeFra))  //Added on 4May to show steps for singles
                    WifeFra = WifeFra.Substring(0, 2);
                pdfDoc.Add(SiteNew.generateParagraph(Constants.steps, true, false, true));
                #endregion code to set variables

                #region Keys for Divorce
                //Code to Check if the grid are from Divorce page
                if (mstatus.Equals(Constants.Divorced))
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep3, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridStandard))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep6, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridSpouse))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep9, true, false, false));
                    }
                }
                #endregion Keys for Divorce

                #region Keys for Single
                //Code to Check if the grid are from Single page
                if (mstatus.Equals(Constants.Single))
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep2, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridStandard))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep4, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridLatest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep6, true, false, false));
                    }
                }
                #endregion Keys for Single

                #region Keys for Widowed
                //Code to Check if the grid are from Widowed page
                if (mstatus.Equals(Constants.Widowed))
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep3, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridClaim67))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep6, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridSpouse))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep9, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridLatest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep10, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep11, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep12, true, false, false));
                    }
                }

                #endregion Keys for Widowed

                #region Keys for Married
                //Code to Check if the grid are from Married page
                if (mstatus.Equals(Constants.Married))
                {
                    if (strGridName.Equals(Constants.GridBest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep6, true, false, false));

                    }
                    else if (strGridName.Equals(Constants.GridMax))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep9, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep10, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridBestLater))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep11, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep12, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep13, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep14, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep15, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridEarly))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep16, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep17, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep18, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep19, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep20, true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridZero))
                    {
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep21, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep22, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strStep23, true, false, false));

                    }
                }
                #endregion Keys for Married
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Render Customer Keys into Pdf
        /// </summary>
        /// <param name="pdfDoc"></param>
        public static void renderKeysIntoPdf(string FirstName, ref Document pdfDoc, string KeyFigureName, String martialstatus, string strGridName)
        {
            try
            {
                String strSpouseFirstName = HttpContext.Current.Session["Sname"].ToString();
                string strName = string.Empty;
                string strColumnName = string.Empty;
                pdfDoc.Add(SiteNew.generateParagraph(Constants.keyFigures, true, false, true));
                //Code to Check if the grids are from Divorced page
                #region Keys for Divorce
                if (martialstatus == Constants.Divorced)
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        if (!string.IsNullOrEmpty(CommonVariables.strKey1))
                        {
                            pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey1, true, false, false));
                            pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        }
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridStandard))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey6, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridSpouse))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey9, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey10, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKeyDivorceRestrictIncome, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                }
                #region Old code for Divorce
                //if (martialstatus == Constants.Divorced)
                //{
                //    if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridAsap))
                //    {
                //        if (Globals.Instance.WifeDateOfBirth > limit)
                //        {
                //            if (HttpContext.Current.Session[Constants.GridAsapDivorceCumm] != null)
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeAsap].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce])
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.StartingValueForDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewWorking, true, false, false));

                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //        }
                //        else if (Globals.Instance.strDivorceStarting != Constants.spousal)
                //        {
                //            if (HttpContext.Current.Session[Constants.GridAsapDivorceCumm] != null)
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeAsap].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce])
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.StartingValueForDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewWorking, true, false, false));
                //            if (HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue] != null)
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //        }
                //        else
                //        {
                //            if (HttpContext.Current.Session[Constants.GridAsapDivorceCumm] != null)
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.CummuText3s + Constants.CummuText4s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeAsap].ToString()) + 1) + Constants.CommaandSpace + HttpContext.Current.Session[Constants.GridAsapDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce])
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.StartingValueForDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            if (HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue] != null)
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //        }
                //    }

                //    if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridStandard))
                //    {
                //        if (HttpContext.Current.Session[Constants.GridStandardDivorceCumm] != null)
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridStandardDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridStandardDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeStandard].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridStandardDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //        }
                //        if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce])
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridStandardDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //        else
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.StartingValueForDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewWorking, true, false, false));
                //        if (HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //    }

                //    if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridSpouse))
                //    {

                //        if (Globals.Instance.WifeDateOfBirth > limit)
                //        {
                //            if (HttpContext.Current.Session[Constants.GridSpouseDivorceCumm] != null)
                //            {
                //                if (HttpContext.Current.Session[Constants.GridSpouseDivorce] == null)
                //                    pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2Work + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeSpouse].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //                else
                //                    pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeSpouse].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            }
                //            else
                //            {
                //                if (HttpContext.Current.Session[Constants.GridSpouseDivorceCumm] != null)
                //                {
                //                    if (HttpContext.Current.Session[Constants.GridSpouseDivorceCumm] != null)
                //                        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2Work + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeSpouse].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //                    else
                //                        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeSpouse].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //                }
                //            }
                //            if (HttpContext.Current.Session[Constants.GridSpouseDivorce] != null && HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce] + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.WorkBenefitsKey, true, false, false));
                //            }
                //            else if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce] && HttpContext.Current.Session[Constants.GridSpouseDivorce].Equals(null))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            }
                //            else if (Globals.Instance.strDivorceStartingSpouse.Equals(Constants.Working))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewWorking, true, false, false));
                //            }
                //            else if (HttpContext.Current.Session[Constants.dhusband] == HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce] && !HttpContext.Current.Session[Constants.dhusband].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue] != null)
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //        }
                //        else
                //        {
                //            if (HttpContext.Current.Session[Constants.GridSpouseDivorceCumm] != null)
                //            {
                //                if (!(HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce].Equals(Constants.Number70)))
                //                    pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2s + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + (int.Parse(HttpContext.Current.Session[Constants.cummAgeSpouse].ToString()) + 1) + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //                else
                //                    pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2sWork + HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.CummuText3s + Constants.Number70 + Constants.CommaandSpace + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseDivorceCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.GridSpouseDivorce] != null && HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce] + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.WorkBenefitsKey, true, false, false));
                //            }
                //            else if (HttpContext.Current.Session[Constants.dhusband] != HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce] && !HttpContext.Current.Session[Constants.dhusband].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            }
                //            else if (HttpContext.Current.Session[Constants.dhusband] == HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce] && HttpContext.Current.Session[Constants.GridSpouseDivorce].Equals(null))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewWorking, true, false, false));
                //            }
                //            else if (HttpContext.Current.Session[Constants.dhusband] == HttpContext.Current.Session[Constants.AgeWorkingBenefitDivorce] && !HttpContext.Current.Session[Constants.dhusband].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseDivorce].ToString() + Constants.keyFigures14 + Constants.steps25 + FirstName + Constants.stepsNewSpousal, true, false, false));
                //            }
                //            if (HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue] != null)
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.DivorceAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //        }
                //    }
                //}
                #endregion Old code for Divorce
                #endregion Keys for Divorce
                //Code to Check if the grid are from Single page
                #region Keys for Single

                if (martialstatus == Constants.Single)
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridStandard))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridLatest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey6, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                }

                #region Old Code Single

                //if (martialstatus == Constants.Single)
                //{
                //            }
                //            else
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.GridAsapSingle].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.steps6, true, false, false));
                //            }
                //        }
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridStandard))
                //    {
                //        if (!HttpContext.Current.Session[Constants.shusband].Equals(null))
                //        {
                //            if (HttpContext.Current.Session[Constants.shusband].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.GridStandardSingle].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.steps6WHB, true, false, false));
                //            }
                //            else
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.GridStandardSingle].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.steps6, true, false, false));
                //            }
                //        }
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridLatest))
                //    {
                //        if (!HttpContext.Current.Session[Constants.shusband].Equals(null))
                //        {
                //            if (HttpContext.Current.Session[Constants.shusband].ToString().Equals(Constants.Number70))
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.GridLatestSingle].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.steps6WHB, true, false, false));
                //            }
                //            else
                //            {
                //                pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.GridLatestSingle].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.steps6, true, false, false));
                //            }
                //        }
                //    }
                //    pdfDoc.Add(SiteNew.generateParagraph(HttpContext.Current.Session[Constants.SingleAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //}

                #endregion Old code single

                #endregion Keys for Single
                //Code to Check if the grid are from Widowed page
                #region Keys for Widowed

                if (martialstatus == Constants.Widowed)
                {
                    if (strGridName.Equals(Constants.GridAsap))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridClaim67))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey6, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridSpouse))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey9, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridLatest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey10, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey11, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey12, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }

                }

                #region Old Code Widowed
                //if (martialstatus == Constants.Widowed)
                //{
                //    if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridAsap))
                //    {
                //        if (HttpContext.Current.Session[Constants.GridAsapWidowCumm] != null)
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapWidowCumm].ToString() + Constants.keyFigures15 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2sWidowSurvivor + HttpContext.Current.Session[Constants.GridAsapWidow] + Constants.CummuText3s + HttpContext.Current.Session[Constants.AgeWorkingBenefit].ToString() + ", " + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridAsapWidowCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            HttpContext.Current.Session[Constants.GridAsapWidowCumm] = null;
                //        }
                //        if (HttpContext.Current.Session[Constants.GridAsapWidow] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridAsapWidow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.SurvivorBenefitsKey, true, false, false));
                //        //key for the annual income
                //        if (HttpContext.Current.Session[Constants.WidowAnnualIncomeValue] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.WidowAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridLatest))
                //    {
                //        if (HttpContext.Current.Session[Constants.GridLatestWidowCumm] != null)
                //        {
                //            if (Globals.Instance.WidowChangeAt70)
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridLatestWidowCumm].ToString() + Constants.keyFigures15 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2sWork + HttpContext.Current.Session[Constants.GridLatestWidow] + Constants.CummuText3 + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridLatestWidowCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridLatestWidowCumm].ToString() + Constants.keyFigures15 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2sWidowSurvivor + HttpContext.Current.Session[Constants.GridLatestWidow] + Constants.CummuText3 + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridLatestWidowCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            HttpContext.Current.Session[Constants.GridLatestWidowCumm] = null;
                //            Globals.Instance.WidowChangeAt70 = false;
                //        }
                //        if (HttpContext.Current.Session[Constants.GridLatestWidow] != null)
                //        {
                //            if (HttpContext.Current.Session[Constants.AgeWorkingBenefit].ToString().Equals(Constants.Number70))
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridLatestWidow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.WorkBenefitsKey, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridLatestWidow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.keyFigures16, true, false, false));
                //        }
                //        //key for the annual income
                //        if (HttpContext.Current.Session[Constants.WidowAnnualIncomeValue] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.WidowAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridSpouse))
                //    {
                //        if (HttpContext.Current.Session[Constants.GridSpouseWidowCumm] != null)
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseWidowCumm].ToString() + Constants.keyFigures15 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2Work + HttpContext.Current.Session[Constants.GridSpouseWidow].ToString() + Constants.CummuText3s + HttpContext.Current.Session[Constants.AgeWorkingBenefit].ToString() + ", " + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridSpouseWidowCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            HttpContext.Current.Session[Constants.GridSpouseWidowCumm] = null;
                //        }
                //        // if (Globals.Instance.WidowStrategy)
                //        if (HttpContext.Current.Session[Constants.GridSpouseWidow] != null)
                //        {
                //            if (HttpContext.Current.Session[Constants.AgeWorkingBenefit].ToString().Equals(Constants.Number70))
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseWidow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.WorkBenefitsKey, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridSpouseWidow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.WHBenefitsKey, true, false, false));
                //        }
                //        //key for the annual income
                //        if (HttpContext.Current.Session[Constants.WidowAnnualIncomeValue] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.WidowAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridClaim67))
                //    {
                //        if (HttpContext.Current.Session[Constants.GridClaim67WidowCumm] != null)
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridClaim67WidowCumm].ToString() + Constants.keyFigures15 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.keyFigures7 + Constants.CummuText1s + FirstName + Constants.CummuText2sWork + HttpContext.Current.Session[Constants.GridClaim67Widow] + Constants.CummuText3 + FirstName + Constants.CummuText4s + HttpContext.Current.Session[Constants.GridClaim67WidowCumm].ToString() + Constants.CummuText5s, true, false, false));
                //            HttpContext.Current.Session[Constants.GridClaim67WidowCumm] = null;
                //        }
                //        if (HttpContext.Current.Session[Constants.GridClaim67Widow] != null)
                //        {
                //            if (HttpContext.Current.Session[Constants.AgeWorkingBenefit].ToString().Equals(Constants.Number70))
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridClaim67Widow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.WorkBenefitsKey, true, false, false));
                //            else
                //                pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridClaim67Widow].ToString() + Constants.keyFigures14 + Constants.steps7 + FirstName + Constants.keyFigures16, true, false, false));
                //        }
                //        //key for the annual income
                //        if (HttpContext.Current.Session[Constants.WidowAnnualIncomeValue] != null)
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.WidowAnnualIncomeValue].ToString() + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey, true, false, false));
                //    }
                //}

                #endregion Old Code Widowed

                #endregion Keys for Widowed
                //Code to Check if the grid are from Married page
                #region Keys for Married
                if (martialstatus == Constants.Married)
                {
                    if (strGridName.Equals(Constants.GridBest))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey1, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey2, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey3, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strRestrictKeyBest, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridMax))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey4, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey5, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey6, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strRestrictKeyMax, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridBestLater))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey7, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey8, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey9, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strRestrictKeyFull, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridEarly))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey10, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey11, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey12, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strRestrictKeyEarly, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }
                    else if (strGridName.Equals(Constants.GridZero))
                    {
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey13, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey14, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys(CommonVariables.strKey15, true, false, false));
                        pdfDoc.Add(SiteNew.generateParagraphKeys("     ", true, false, false));
                    }

                }
                #region Old Code for Married
                //if (martialstatus == Constants.Married)
                //{
                //    if (Convert.ToDecimal(HttpContext.Current.Session[Constants.WifeBenefit].ToString()) > Convert.ToDecimal(HttpContext.Current.Session[Constants.HusbandBenefit]))
                //    {
                //        strName = strSpouseFirstName;
                //        strColumnName = Constants.keyFigures14;
                //    }
                //    else
                //    {
                //        strName = FirstName;
                //        strColumnName = Constants.keyFigures13;
                //    }
                //    if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridBest))
                //    {
                //        if (!HttpContext.Current.Session[Constants.GridMarriedCumm].ToString().Equals(Constants.ZeroWithOutAsterisk))
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.And + strSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + HttpContext.Current.Session[Constants.GridMarried].ToString() + Constants.CummuText3 + Constants.CummuText4 + HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.CummuText5, true, false, false));
                //            HttpContext.Current.Session[Constants.GridMarriedCumm] = null;
                //        }
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarried].ToString() + strColumnName + Constants.steps7 + strName + Constants.keyFigures8, true, false, false));
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.CombinedBenefit].ToString() + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridSuspendLater))
                //    {
                //        if (!HttpContext.Current.Session[Constants.GridMarriedCumm].ToString().Equals(Constants.ZeroWithOutAsterisk))
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.And + strSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + HttpContext.Current.Session[Constants.GridMarried].ToString() + Constants.CummuText3 + Constants.CummuText4 + HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.CummuText5, true, false, false));
                //            HttpContext.Current.Session[Constants.GridMarriedCumm] = null;
                //        }
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarried].ToString() + strColumnName + Constants.steps7 + strName + Constants.keyFigures8, true, false, false));
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.CombinedBenefit].ToString() + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridBestLater))
                //    {
                //        if (!HttpContext.Current.Session[Constants.GridMarriedCumm].ToString().Equals(Constants.ZeroWithOutAsterisk))
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.And + strSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + HttpContext.Current.Session[Constants.GridMarried].ToString() + Constants.CummuText3 + Constants.CummuText4 + HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.CummuText5, true, false, false));
                //            HttpContext.Current.Session[Constants.GridMarriedCumm] = null;
                //        }
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarried].ToString() + strColumnName + Constants.steps7 + strName + Constants.keyFigures8, true, false, false));
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.CombinedBenefit].ToString() + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9, true, false, false));
                //    }
                //    else if (HttpContext.Current.Session[Constants.Grid].ToString().Equals(Constants.GridMax))
                //    {
                //        if (!HttpContext.Current.Session[Constants.GridMarriedCumm].ToString().Equals(Constants.ZeroWithOutAsterisk))
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.And + strSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + HttpContext.Current.Session[Constants.GridMarried].ToString() + Constants.CummuText3 + Constants.CummuText4 + HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.CummuText5, true, false, false));
                //            HttpContext.Current.Session[Constants.GridMarriedCumm] = null;
                //        }
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarried].ToString() + strColumnName + Constants.steps7 + strName + Constants.keyFigures8, true, false, false));
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.CombinedBenefit].ToString() + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9, true, false, false));
                //    }
                //    else
                //    {
                //        if (!HttpContext.Current.Session[Constants.GridMarriedCumm].ToString().Equals(Constants.ZeroWithOutAsterisk))
                //        {
                //            pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + FirstName + Constants.And + strSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + HttpContext.Current.Session[Constants.GridMarried].ToString() + Constants.CummuText3 + Constants.CummuText4 + HttpContext.Current.Session[Constants.GridMarriedCumm].ToString() + Constants.CummuText5, true, false, false));
                //            HttpContext.Current.Session[Constants.GridMarriedCumm] = null;
                //        }
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.GridMarried].ToString() + strColumnName + Constants.steps7 + strName + Constants.keyFigures8, true, false, false));
                //        pdfDoc.Add(SiteNew.generateParagraphKeys(HttpContext.Current.Session[Constants.CombinedBenefit].ToString() + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9, true, false, false));
                //    }


                //}
                #endregion  Old Code for Married

                #endregion Keys for Married
                //Doug's surviour benefits line in the PDF
                //pdfDoc.Add(SiteNew.generateParagraph("(" + FirstName + Constants.keyFigures3, true, false, false));
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Pdf Front Page Design
        /// </summary>
        /// <param name="dtCustomerDetails"></param>
        /// <param name="pdfDoc"></param>
        public static void PdfFrontPage(DataTable dtCustomerDetails, ref Document pdfDoc)
        {
            try
            {
                string strLName = dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString();
                string strFname = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHomeImage, Constants.pdfImageWidth, Constants.pdfHomeImageHeight));
                pdfDoc.Add(Chunk.NEWLINE);
                pdfDoc.Add(SiteNew.generateParagraph(strFname + " " + strLName, true, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.report, true, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.preparedOn + DateTime.Now.ToString(Constants.DateFormat), true, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.preparedBy, true, false, false));
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Common Page Layout Design
        /// </summary>
        /// <param name="label"></param>
        /// <param name="grid"></param>
        /// <param name="pdfDoc"></param>
        public static void commonPageLayout(Label label, GridView grid, ref Document pdfDoc, string FirstName, String status, string GridName, string HusbFra, string WifeFra)
        {
            try
            {
                /* Add Header Image into New Page */
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(label.Text, true, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.convertGridViewIntoTable(grid));
                pdfDoc.Add(new Paragraph(" "));
                //Print Anual Benefit Explainations
                if (status.Equals(Constants.Married))
                    PrintExplanationsForMarried(GridName, ref pdfDoc);
                else if (status.Equals(Constants.Divorced))
                    PrintExplanationForDivorce(GridName, ref pdfDoc);
                else if (status.Equals(Constants.Widowed))
                    PrintExplanationForWidowed(GridName, ref pdfDoc);
                else
                    PrintExplanationForSingle(GridName, ref pdfDoc);
                pdfDoc.NewPage();
                /* Render Keys into Pdf */
                SiteNew.renderKeysIntoPdf(FirstName, ref pdfDoc, label.Text, status, GridName);
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                /* Render Steps into Pdf */
                SiteNew.renderStepsIntoPdf(GridName, ref pdfDoc, status, HusbFra, WifeFra);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Used to print benefit explanations in pdf for married case
        /// </summary>
        /// <param name="strGridaName">Grid Name</param>
        /// <param name="pdfDoc">PDF Document</param>
        private static void PrintExplanationsForMarried(string strGridaName, ref Document pdfDoc)
        {
            try
            {
                switch (strGridaName)
                {
                    case Constants.GridBest:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband0, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife0, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strHusbExplain1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strWifeExplain1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife, true, true, false));
                        break;
                    case Constants.GridMax:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband6, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife6, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strHusbExplain4, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strWifeExplain4, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband7, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife7, true, true, false));
                        break;
                    case Constants.GridBestLater:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband5, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife5, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strHusbExplain3, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strWifeExplain3, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband4, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife4, true, true, false));
                        break;
                    case Constants.GridEarly:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strHusbExplain2, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strWifeExplain2, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband3, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife3, true, true, false));
                        break;

                    case Constants.GridZero:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife11, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband8, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife12, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband9, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife13, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteHusband10, true, true, false));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Used to print the benefit explanations in pdf for divorce case
        /// </summary>
        /// <param name="strGridaName">Grid Name</param>
        /// <param name="pdfDoc">PDF Document</param>
        private static void PrintExplanationForDivorce(string strGridaName, ref Document pdfDoc)
        {
            try
            {
                switch (strGridaName)
                {
                    case Constants.GridAsap:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife0, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife3, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife6, true, true, false));
                        break;

                    case Constants.GridStandard:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife4, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife7, true, true, false));
                        break;

                    case Constants.GridSpouse:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife2, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife5, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife8, true, true, false));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Used to print the benefit explanations in pdf for widowed case
        /// </summary>
        /// <param name="strGridaName">Grid Name</param>
        /// <param name="pdfDoc">PDF Document</param>
        private static void PrintExplanationForWidowed(string strGridaName, ref Document pdfDoc)
        {
            try
            {
                switch (strGridaName)
                {
                    case Constants.GridAsap:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife1, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife9, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife5, true, true, false));
                        break;

                    case Constants.GridClaim67:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife4, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife8, true, true, false));
                        break;

                    case Constants.GridSpouse:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife2, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife6, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife10, true, true, false));

                        break;

                    case Constants.GridLatest:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife3, true, true, false));
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife7, true, true, false));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Used to print the benefit explanations in pdf for single case
        /// </summary>
        /// <param name="strGridaName">Grid Name</param>
        /// <param name="pdfDoc">PDF Document</param>
        private static void PrintExplanationForSingle(string strGridaName, ref Document pdfDoc)
        {
            try
            {
                switch (strGridaName)
                {
                    case Constants.GridAsap:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife1, true, true, false));
                        break;

                    case Constants.GridStandard:
                        pdfDoc.Add(SiteNew.generateParagraph(CommonVariables.strNoteWife0, true, true, false));
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }
        /// <summary>
        /// Customer Details with GridView Data fill into Pdf
        /// </summary>
        /// <param name="dtCustomerDetails"></param>
        /// <param name="lblFinInfo"></param>
        /// <param name="grid"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="maritalStatus"></param>
        public static void customerDetailswithGrid(DataTable dtCustomerDetails, Label lblFinInfo, GridView gvFinInfo, ref Document pdfDoc)
        {
            try
            {
                decimal Benefit, BenefitSpouse;
                string Bene = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsRetAgeBenefit].ToString();
                string BeneSpouse = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedWifesRetAgeBenefit].ToString();
                decimal.TryParse(Bene, out Benefit);
                decimal.TryParse(BeneSpouse, out BenefitSpouse);
                /* Add Customer Details into Pdf */
                pdfDoc.Add(SiteNew.generateParagraph(Constants.customerDob + string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsBirthDate].ToString())) + "   " + string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedWifesBirthDate].ToString())), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerPIA + Benefit.ToString(Constants.AMT_FORMAT) + "          " + BenefitSpouse.ToString(Constants.AMT_FORMAT), true, false, false));
                // pdfDoc.Add(SiteNew.generateParagraph(Constants. + Benefit.ToString(Constants.AMT_FORMAT) + "          " + BenefitSpouse.ToString(Constants.AMT_FORMAT), true, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.PdfCustomerMaritalStatus + dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString() + "         " + dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), true, false, false));
                if (dtCustomerDetails.Rows[0][Constants.SecurityInfoCola].ToString() == "T")
                    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA + "Yes                Yes", true, false, false));
                else
                    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA + "No                 No", true, false, false));
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDoc.Add(SiteNew.generatePhrase(lblFinInfo.Text));
                pdfDoc.Add(new Paragraph(" "));
                /* Convert Grid View Data into Table in Pdf */
                pdfDoc.Add(SiteNew.convertGridViewIntoTable(gvFinInfo));
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                /* Render Keys into Pdf */
                //SiteNew.renderKeysIntoPdf(dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString(), ref pdfDoc, lblFinInfo.Text, "");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Generate Grid View Content into PDF Table 
        /// </summary>
        /// <param name="gvFinInfo"></param>
        /// <returns></returns>
        public static PdfPTable convertGridViewIntoTable(GridView gvFinInfo)
        {
            try
            {
                #region code to set the variables
                Calc calc = new Calc() { };
                // Initialize table cell object
                PdfPCell tblCell;
                string cellText;
                /* Color and font for Table */
                iTextSharp.text.Font fontTable = pdfFontStyle();
                fontTable.SetColor(Constants.zero, Constants.zero, Constants.zero);
                /* link button column is excluded from the list */
                int colCount = gvFinInfo.Columns.Count;
                /*Create a table */
                PdfPTable table = new PdfPTable(colCount);
                table.HorizontalAlignment = Element.ALIGN_CENTER;
                /* Set Table Width in Percentage */
                table.WidthPercentage = 92;
                /* Align Table into Center */
                table.HorizontalAlignment = Element.ALIGN_CENTER;
                /* Create an array to store column widths */
                int[] colWidthsSuspendLater = new int[gvFinInfo.Columns.Count];
                //array to store the column headers
                string[] headerText = new string[] { "A", "B", "C", "D", "E", "F" };
                #endregion code to set the variables

                #region code to loop through the grid and create the table
                //to add the column headers as A,B,C... as requested by client
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    /* Set the column width */
                    colWidthsSuspendLater[columnIndex] = (int)gvFinInfo.Columns[columnIndex].ItemStyle.Width.Value;
                    /* Fetch the header text */
                    cellText = (headerText[columnIndex].ToString());
                    /* Create a new cell with header text */
                    tblCell = new PdfPCell(new Phrase(cellText, fontTable));
                    /* Set the background color for the header cell */
                    tblCell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml(Constants.pdfTableHeader));
                    tblCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    /* Add cell into table Element */
                    table.AddCell(tblCell);
                }
                /* Create the header row */
                for (int columnIndex = 0; columnIndex < colCount; columnIndex++)
                {
                    /* Set the column width */
                    colWidthsSuspendLater[columnIndex] = (int)gvFinInfo.Columns[columnIndex].ItemStyle.Width.Value;
                    /* Fetch the header text */
                    cellText = HttpContext.Current.Server.HtmlDecode(gvFinInfo.HeaderRow.Cells[columnIndex].Text.Substring(13));
                    /* Create a new cell with header text */
                    tblCell = new PdfPCell(new Phrase(cellText, fontTable));
                    /* Set the background color for the header cell */
                    tblCell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml(Constants.pdfTableHeader));
                    tblCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    /* Add cell into table Element */
                    table.AddCell(tblCell);
                }
                /* Export rows from GridView to table */
                for (int rowIndex = 0; rowIndex < gvFinInfo.Rows.Count; rowIndex++)
                {
                    if (gvFinInfo.Rows[rowIndex].RowType == DataControlRowType.DataRow)
                    {
                        for (int columnIndex = 0; columnIndex < gvFinInfo.Columns.Count; columnIndex++)
                        {
                            /* Fetch the column value of the current row */
                            cellText = HttpContext.Current.Server.HtmlDecode(gvFinInfo.Rows[rowIndex].Cells[columnIndex].Text);
                            /* create a new cell with column value */
                            tblCell = new PdfPCell(new Phrase(cellText, fontTable));
                            //code to change the color of the cell backgroung as according to the grid
                            Color color = (gvFinInfo.Rows[rowIndex].Cells[columnIndex].BackColor);
                            if (color.IsEmpty)
                            {
                                tblCell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml(Constants.oddBackgroundColor));
                            }
                            else
                            {
                                tblCell.BackgroundColor = new BaseColor(gvFinInfo.Rows[rowIndex].Cells[columnIndex].BackColor);
                            }
                            tblCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            /* Add the cell to the table */
                            table.AddCell(tblCell);
                        }
                    }
                }
                return table;
                #endregion code to loop through the grid and create the table
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// Used to create strategy details table
        /// </summary>
        /// <param name="pdfDocument">PDF Doc</param>
        /// <param name="strPaidToWaitText">Paid to Wait Text</param>
        /// <param name="listPaidToWaitAmount">List of Paid to Wait Amount</param>
        /// <param name="strAnnualIncomeText">Annual Income Text</param>
        /// <param name="listAnnualIncomeAmount">List of Annual Income Amount</param>
        /// <param name="intStrategyCount">Integer Strategy Count</param>
        public static void CreateStrategyDetailsTableForPDF(ref Document pdfDocument,
                                                            string strPaidToWaitText,
                                                            System.Collections.Generic.List<string> listPaidToWaitAmount,
                                                            string strAnnualIncomeText,
                                                            System.Collections.Generic.List<string> listAnnualIncomeAmount,
                                                            int intStrategyCount,
                                                            string strCase)
        {
            try
            {
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font fontForRegular = new iTextSharp.text.Font(customfont, 16);
                iTextSharp.text.Font fontStrategyHeader = new iTextSharp.text.Font(customfont, 16, 1, BaseColor.BLACK);
                iTextSharp.text.Font fontForValues = new iTextSharp.text.Font(customfont, 16, 0, BaseColor.BLUE);
                PdfPTable pdfTable;
                ///Set cell width as per strategy count
                iTextSharp.text.Rectangle rect = pdfDocument.PageSize;
                float width = rect.Width / 3;
                if (intStrategyCount == 3)
                {
                    pdfTable = new PdfPTable(new float[] { width, width, width });
                }
                else if (intStrategyCount == 2)
                {
                    pdfTable = new PdfPTable(new float[] { width, width });
                }
                else
                {
                    pdfTable = new PdfPTable(new float[] { width });
                }
                pdfTable.TotalWidth = 500f;
                //table.HorizontalAlignment = Element.ALIGN_LEFT;
                pdfTable.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                PdfPCell pdfCell;
                Paragraph paraGraph;
                //Add Strategy Header with new cell
                for (int intStratIndex = 0; intStratIndex < intStrategyCount; intStratIndex++)
                {
                    pdfCell = new PdfPCell();
                    pdfCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    pdfCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    pdfCell.Padding = 10f;
                    int intIndex = intStratIndex + 1;
                    //Add Strategy Header
                    paraGraph = new Paragraph(Constants.Strategy + intIndex, fontStrategyHeader);
                    paraGraph.Alignment = Element.ALIGN_CENTER;
                    pdfCell.AddElement(paraGraph);
                    //Used to add new line after strategy header
                    pdfTable.AddCell(pdfCell);
                }
                //Add Paid To Wait Amount and Annual Income Details
                for (int intIndex = 0; intIndex < intStrategyCount; intIndex++)
                {
                    string strAnnualIncomeAmnt = listAnnualIncomeAmount[intIndex];

                    pdfCell = new PdfPCell();
                    pdfCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    pdfCell.Padding = 10f;
                    if (!strCase.Equals(Constants.Single))
                    {
                        string strPaidToWaitAmnt = listPaidToWaitAmount[intIndex];
                        if (!string.IsNullOrEmpty(strPaidToWaitAmnt))
                        {
                            //Add PaidToWait Text and Value for particular cell
                            paraGraph = new Paragraph(strPaidToWaitText, fontForRegular);
                            paraGraph.Alignment = Element.ALIGN_CENTER;
                            pdfCell.AddElement(paraGraph);
                            paraGraph = new Paragraph(strPaidToWaitAmnt, fontForValues);
                            paraGraph.Alignment = Element.ALIGN_CENTER;
                            pdfCell.AddElement(paraGraph);
                        }
                    }
                    if (!string.IsNullOrEmpty(strAnnualIncomeAmnt))
                    {
                        //Add Annual Icome Text and Value for particular cell
                        paraGraph = new Paragraph(strAnnualIncomeText, fontForRegular);
                        paraGraph.Alignment = Element.ALIGN_CENTER;
                        pdfCell.AddElement(paraGraph);
                        paraGraph = new Paragraph(strAnnualIncomeAmnt, fontForValues);
                        paraGraph.Alignment = Element.ALIGN_CENTER;
                        pdfCell.AddElement(paraGraph);
                    }
                    //Print Restricted Application Income in PDF Strategy Title Section
                    if (strCase.Equals(Constants.Married) || strCase.Equals(Constants.Divorced))
                    {
                        //string strRestrictPaidAmnt = listRestrictUpdated[intIndex];
                        string strRestrictPaidAmnt = string.Empty;
                        //if (strCase.Equals(Constants.Married))
                        //{


                        //if (Calc.listRestrictAppIcome.Count > intIndex)
                        //{
                        //    strRestrictPaidAmnt = Calc.listRestrictAppIcome[intIndex];
                        //}
                        if (intStrategyCount == 1)
                            strRestrictPaidAmnt = Calc.listRestrictAppIcome[1];
                        else if (intStrategyCount == 2)
                        {
                            switch (intIndex)
                            {
                                case 0:
                                    strRestrictPaidAmnt = Calc.listRestrictAppIcome[0];
                                    break;

                                case 1:
                                    strRestrictPaidAmnt = Calc.listRestrictAppIcome[2];
                                    break;
                            }
                        }
                        else
                            strRestrictPaidAmnt = Calc.listRestrictAppIcome[intIndex];
                        //}
                        //else
                        //{
                        //    strRestrictPaidAmnt = Calc.listRestrictAppIcome[intIndex];
                        //}
                        if (!string.IsNullOrEmpty(strRestrictPaidAmnt))
                        {
                            //Add Restricted Application Imcome Text and Value for particular cell
                            paraGraph = new Paragraph(Constants.RestrictedAppIcomeTextForPDF, fontForRegular);
                            paraGraph.Alignment = Element.ALIGN_CENTER;
                            pdfCell.AddElement(paraGraph);
                            paraGraph = new Paragraph(strRestrictPaidAmnt, fontForValues);
                            paraGraph.Alignment = Element.ALIGN_CENTER;
                            pdfCell.AddElement(paraGraph);
                            //Add new line 
                            pdfCell.AddElement(new Paragraph(Environment.NewLine, fontForRegular));
                        }


                    }
                    //Add cell into table
                    pdfTable.AddCell(pdfCell);
                }
                pdfDocument.Add(new Paragraph(" "));
                pdfDocument.Add(pdfTable);
                pdfDocument.Add(new Paragraph(" "));
                pdfDocument.Add(new Paragraph(" "));
                pdfDocument.Add(new Paragraph(" "));
                pdfDocument.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDocument.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDocument.NewPage();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }

        }
        #endregion

        #endregion Common Functions used in all pages
    }
}