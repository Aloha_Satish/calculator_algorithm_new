﻿using Calculate.RestrictAppIncome;
using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class RestrictAppUserInfo : System.Web.UI.Page
    {
        #region Properties
        private int Day_Your
        {
            get
            {
                if (Request.Form[ddlDay_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Your();
                ddlDay_Your.ClearSelection();
                ddlDay_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Your
        {
            get
            {
                if (Request.Form[ddlMonth_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlMonth_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlMonth_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateMonth_Your();
                ddlMonth_Your.ClearSelection();
                ddlMonth_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Your
        {
            get
            {
                if (Request.Form[ddlYear_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlYear_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlYear_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateYear_Your();
                ddlYear_Your.ClearSelection();
                ddlYear_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Your
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Your + "/" + this.Day_Your + "/" + this.Year_Your);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Your = value.Year;
                    this.Month_Your = value.Month;
                    this.Day_Your = value.Day;
                }
            }
        }

        private void PopulateDay_Your()
        {
            ddlDay_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Your.Items.Add(lt);
            int days = DateTime.DaysInMonth(this.Year_Your, 1);
            for (int i = 1; i <= days; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Your.Items.Add(lt);
            }
            ddlDay_Your.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Your()
        {
            ddlMonth_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Your.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Your.Items.Add(lt);
            }
            ddlMonth_Your.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Your()
        {
            ddlYear_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Your.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Your.Items.Add(lt);
            }
            ddlYear_Your.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        private int Day_Spouse
        {
            get
            {
                if (Request.Form[ddlDay_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Spouse();
                ddlDay_Spouse.ClearSelection();
                ddlDay_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Spouse
        {
            get
            {
                if (Request.Form[ddlMonth_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlMonth_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlMonth_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateMonth_Spouse();
                ddlMonth_Spouse.ClearSelection();
                ddlMonth_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Spouse
        {
            get
            {
                if (Request.Form[ddlYear_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlYear_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlYear_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateYear_Spouse();
                ddlYear_Spouse.ClearSelection();
                ddlYear_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Spouse
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Spouse + "/" + this.Day_Spouse + "/" + this.Year_Spouse);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Spouse = value.Year;
                    this.Month_Spouse = value.Month;
                    this.Day_Spouse = value.Day;
                }
            }
        }

        private void PopulateDay_Spouse()
        {
            ddlDay_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Spouse.Items.Add(lt);
            //int days = DateTime.DaysInMonth(this.Year_Spouse, this.Month_Spouse);
            int days = DateTime.DaysInMonth(this.Year_Spouse, 1);
            for (int i = 1; i <= days; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Spouse.Items.Add(lt);
            }
            ddlDay_Spouse.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Spouse()
        {
            ddlMonth_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Spouse.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Spouse.Items.Add(lt);
            }
            ddlMonth_Spouse.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Spouse()
        {
            ddlYear_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Spouse.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Spouse.Items.Add(lt);
            }
            ddlYear_Spouse.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }
        #endregion Properties

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Fill Up the drop downs for User and spouse
                this.PopulateYear_Your();
                this.PopulateMonth_Your();
                this.PopulateDay_Your();

                this.PopulateYear_Spouse();
                this.PopulateMonth_Spouse();
                this.PopulateDay_Spouse();

                ddlDay_Your.SelectedIndex = 0;
                ddlMonth_Your.SelectedIndex = 0;
                ddlYear_Your.SelectedIndex = 0;
                ddlDay_Spouse.SelectedIndex = 0;
                ddlMonth_Spouse.SelectedIndex = 0;
                ddlYear_Spouse.SelectedIndex = 0;

                ddlHusbClaimAge.Items.Add("62");
                ddlHusbClaimAge.Items.Add("63");
                ddlHusbClaimAge.Items.Add("64");
                ddlHusbClaimAge.Items.Add("65");
                ddlHusbClaimAge.Items.Add("66");
                ddlHusbClaimAge.Items.Add("67");
                ddlHusbClaimAge.Items.Add("68");
                ddlHusbClaimAge.Items.Add("69");
                ddlHusbClaimAge.Items.Add("70");

                ddlWifeClaimAge.Items.Add("62");
                ddlWifeClaimAge.Items.Add("63");
                ddlWifeClaimAge.Items.Add("64");
                ddlWifeClaimAge.Items.Add("65");
                ddlWifeClaimAge.Items.Add("66");
                ddlWifeClaimAge.Items.Add("67");
                ddlWifeClaimAge.Items.Add("68");
                ddlWifeClaimAge.Items.Add("69");
                ddlWifeClaimAge.Items.Add("70");

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Naviaget to property page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateToReportPage(object sender, EventArgs e)
        {
            try
            {
                //
                RestrictAppIncomeProp.Instance.BoolRestrictNewCase = false;
                RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = false;
                Globals.Instance.BoolIsShowRestrictFull = false;
                Globals.Instance.BoolIsHusbandClaimedBenefit = false;
                Globals.Instance.BoolIsWifeClaimedBenefit = false;
                Session["FBUserClaimedAge"] = ddlHusbClaimAge.Text;
                Session["FBSpouseClaimedAge"] = ddlWifeClaimAge.Text;
                Session["FBUserClaimedBenefit"] = txtHusbCliamBenefit.Text;
                Session["FBSpouseClaimedBenefit"] = txtWifeCliamBenefit.Text;
                DateTime UserDob = Convert.ToDateTime(SelectedDate_Your.ToString());
                DateTime SpouseDob = Convert.ToDateTime(SelectedDate_Spouse.ToString());
                DateTime dtLimit = new DateTime(1954, 01, 02);
                decimal decBeneHusb = Convert.ToDecimal(txtHusbCurrBen.Text);
                decimal decBeneWife = Convert.ToDecimal(txtWifeCurrBen.Text);
                if (rdbtnuserclaim.Checked)
                    Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                else if (rdbtnspouseclaim.Checked)
                    Globals.Instance.BoolIsWifeClaimedBenefit = true;
                Session[Constants.SessionRole] = Constants.Customer;
                if (AddCustomer())
                {
                    if (rdbtnuserclaim.Checked)
                        Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                    else if (rdbtnspouseclaim.Checked)
                        Globals.Instance.BoolIsWifeClaimedBenefit = true;

                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        if (!string.IsNullOrEmpty(txtHusbCurrBen.Text))
                            RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(txtHusbCurrBen.Text);
                        RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ddlHusbClaimAge.Text);
                        if (!string.IsNullOrEmpty(txtHusbCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(txtHusbCliamBenefit.Text);
                    }
                    if (Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (!string.IsNullOrEmpty(txtWifeCurrBen.Text))
                            RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(txtWifeCurrBen.Text);
                        RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ddlWifeClaimAge.Text);
                        if (!string.IsNullOrEmpty(txtWifeCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(txtWifeCliamBenefit.Text);
                    }

                    //Old cases
                    //Determine show/hide full strategy
                    if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                        {
                            if (UserDob < dtLimit)
                            {
                                Globals.Instance.BoolIsShowRestrictFull = true;
                                Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                            }
                            else
                                Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                        {
                            if (SpouseDob < dtLimit)
                            {
                                Globals.Instance.BoolIsShowRestrictFull = true;
                                Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                            }
                            else
                                Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);

                    }
                    else
                    {
                        Response.Redirect(Constants.RedirectCalcMarried);

                        //if (((decBeneHusb >= decBeneWife && UserDob > dtLimit && SpouseDob < dtLimit) ||
                        //    (decBeneHusb < decBeneWife && SpouseDob > dtLimit && UserDob < dtLimit)))
                        //{
                        //    RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = true;
                        //    //Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        //}
                        //else
                        //{
                        //    //Redirect to new strategies of 62 and 66 mentioned on 24/02/2017
                        //    //Strategy Explaination
                        //    //Strategy-1 : Lower Benefit spouse will claim WHB at his age 62.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70
                        //    //Strategy-2 : Lower Benefit spouse will claim WHB at his age 66.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70

                        //    RestrictAppIncomeProp.Instance.BoolRestrictNewCase = true;
                        //    //Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        //}

                    }



                    //if (!rdbtnuserclaim.Checked && !rdbtnspouseclaim.Checked)
                    //{
                    //    //Calculate regular strategies
                    //    Response.Redirect(Constants.RedirectCalcMarried);
                    //}
                    //else
                    //{
                    //    //Case 1
                    //    if ((!Globals.Instance.BoolIsHusbandClaimedBenefit && UserDob < dtLimit) || (!Globals.Instance.BoolIsWifeClaimedBenefit && SpouseDob < dtLimit))
                    //    {
                    //        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy);
                    //    }
                    //    else
                    //    {
                    //        Response.Redirect(Constants.RedirectCalcMarried);
                    //    }

                    //}
                }



                //RestrictAppIncomeProp.Instance.BoolRestrictNewCase = false;
                //Globals.Instance.BoolIsHusbandClaimedBenefit = false;
                //Globals.Instance.BoolIsWifeClaimedBenefit = false;
                //Globals.Instance.BoolIsShowRestrictFull = false;
                //RestrictAppIncomeProp.Instance.intHusbandClaimAge = 0;
                //RestrictAppIncomeProp.Instance.intWifeClaimAge = 0;
                //RestrictAppIncomeProp.Instance.intHusbClaimBenefit = 0;
                //RestrictAppIncomeProp.Instance.intWifeClaimBenefit = 0;
                //DateTime dtLimit = new DateTime(1954, 01, 02);
                //Decimal decBeneHusb = Convert.ToDecimal(txtHusbCurrBen.Text);
                //Decimal decBeneWife = Convert.ToDecimal(txtWifeCurrBen.Text);
                //Session["FBUserBDate"] = SelectedDate_Your.ToString();
                //Session["FBSpouseDate"] = SelectedDate_Spouse.ToString();
                //Session[Constants.SessionRole] = "FBUser";
                //Session["FBUsername"] = txtFirstname.Text;
                //Session["FBSpousename"] = txtSpouseFirstname.Text;

                //if (rdbtnMarried.Checked)
                //    Session["FBUserBenefit"] = txtHusbCurrBen.Text;
                //else
                //    Session["FBUserBenefit"] = txtUserBenefit.Text;

                //if (rdbtnMarried.Checked)
                //    Session["FBSpouseBenefit"] = txtWifeCurrBen.Text;
                //else
                //    Session["FBSpouseBenefit"] = txtSpouseBenefit.Text;

                //Session["FBUserClaimedAge"] = Session["calclaimage"] != null ? Session["calclaimage"] : ddlHusbClaimAge.Text;
                //Session["FBSpouseClaimedAge"] = ddlWifeClaimAge.Text;
                //Session["FBUserClaimedBenefit"] = txtHusbCliamBenefit.Text;
                //Session["FBSpouseClaimedBenefit"] = txtWifeCliamBenefit.Text;


                //DateTime UserDob = Convert.ToDateTime(SelectedDate_Your.ToString());
                //DateTime SpouseDob = Convert.ToDateTime(SelectedDate_Spouse.ToString());

                //if (rdbtnuserclaim.Checked)
                //{
                //    if (!string.IsNullOrEmpty(txtHusbCurrBen.Text))
                //        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(txtHusbCurrBen.Text);
                //    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ddlHusbClaimAge.Text);
                //    if (!string.IsNullOrEmpty(txtHusbCliamBenefit.Text))
                //        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(txtHusbCliamBenefit.Text);
                //    Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                //}
                //if (rdbtnspouseclaim.Checked)
                //{
                //    if (!string.IsNullOrEmpty(txtWifeCurrBen.Text))
                //        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(txtWifeCurrBen.Text);
                //    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ddlWifeClaimAge.Text);
                //    if (!string.IsNullOrEmpty(txtWifeCliamBenefit.Text))
                //        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(txtWifeCliamBenefit.Text);
                //    Globals.Instance.BoolIsWifeClaimedBenefit = true;
                //}

                ////Determine show/hide full strategy
                //if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                //{
                //    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                //    {
                //        if (UserDob < dtLimit)
                //            Globals.Instance.BoolIsShowRestrictFull = true;
                //    }
                //    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                //    {
                //        if (SpouseDob < dtLimit)
                //            Globals.Instance.BoolIsShowRestrictFull = true;
                //    }
                //    RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = false;
                //    Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                //}
                //else
                //{

                //    if (((decBeneHusb >= decBeneWife && UserDob > dtLimit && SpouseDob < dtLimit) ||
                //        (decBeneHusb < decBeneWife && SpouseDob > dtLimit && UserDob < dtLimit)))
                //    {
                //        RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = true;
                //        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                //    }
                //    else
                //    {
                //        //Redirect to new strategies of 62 and 66 mentioned on 24/02/2017
                //        //Strategy Explaination
                //        //Strategy-1 : Lower Benefit spouse will claim WHB at his age 62.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70
                //        //Strategy-2 : Lower Benefit spouse will claim WHB at his age 66.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70



                //        RestrictAppIncomeProp.Instance.BoolRestrictNewCase = true;
                //        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                //    }

                //}
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;

            }
        }

        /// <summary>
        /// Used to add customer into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected bool AddCustomer()
        {
            try
            {
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                #region Fill data into Object
                /* Fill data into customer object */
                objCustomer.CustomerID = Database.GenerateGID("C");
                objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                //objCustomer.LastName = txtLastName.Text;
                objCustomer.Email = txtEmail.Text;
                objCustomer.Password = Encryption.Encrypt("123456");
                objCustomer.Role = Symbolics.Constants.Customer;
                objCustomer.AdvisorID = String.Empty;
                objCustomer.Birthdate = this.SelectedDate_Your.ToShortDateString();
                //objCustomer.HusbandsRetAgeBenefit = txtHusbCurrBen.Text;

                Session[Constants.UserId] = objCustomer.CustomerID;
                if (rdbtnDivorced.Checked)
                {
                    objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                    objCustomer.MartialStatus = Constants.Divorced;
                    objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;
                }
                else if (rdbtnMarried.Checked)
                {
                    objCustomer.HusbandsRetAgeBenefit = txtHusbCurrBen.Text;
                    objCustomer.MartialStatus = Constants.Married;
                    objCustomer.WifesRetAgeBenefit = txtWifeCurrBen.Text;
                }
                else if (rdbtnSingle.Checked)
                {
                    objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                    objCustomer.MartialStatus = Constants.Single;
                    objCustomer.WifesRetAgeBenefit = string.Empty;
                }
                else
                {
                    objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                    objCustomer.MartialStatus = Constants.Widowed;
                    Session[Constants.RadioChecked] = Constants.Widowed;
                    objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;
                }
                objCustomer.SpouseLastname = String.Empty;
                objCustomer.LastName = string.Empty;
                //objCustomer.Email = string.Empty;
                objCustomer.isSubscription = Constants.FlagYes;
                objCustomer.SpouseBirthdate = this.SelectedDate_Spouse.ToShortDateString();
                objCustomer.SpouseFirstname = textInfo.ToTitleCase(txtSpouseFirstname.Text);
                objCustomer.DelFlag = Constants.FlagNo;
                objCustomer.PasswordResetFlag = Constants.FlagNo;

                objCustomer = SetCustomerProperties(objCustomer);
                #endregion
                /* Check if user already exists in table or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* Insert Customer details into table */
                    objCustomer.insertCustomerDetails(objCustomer);
                    lblUserCreate.Visible = true;
                    lblUserCreate.Text = Constants.customerCreated;
                    lblUserCreate.ForeColor = Color.Green;
                    ClearFields();
                    Session[Constants.SessionRole] = Symbolics.Constants.Customer;
                    Session[Constants.SessionUserId] = objCustomer.CustomerID;
                    // Response.Redirect(Constants.RedirectCalcMarried);
                    return true;
                }
                else
                {
                    lblUserCreate.Visible = true;
                    lblUserCreate.Text = "Customer already exixts!";
                    lblUserCreate.ForeColor = Color.Red;
                    return false;
                }


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

      /// <summary>
        /// Set New Restricted Application Properties
        /// </summary>
        /// <param name="objCustomer">Customer Object</param>
        /// <returns>Customer Object</returns>
        private Customers SetCustomerProperties(Customers objCustomer)
        {
            try
            {
                //Added on 5 may 2017
                #region Restricted Application Income Changes Assign global values
                if (rdbtnAnyClaimYes.Checked)
                {
                    if (rdbtnuserclaim.Checked)
                    {
                        Globals.Instance.ClaimedPerson = "HUSBAND";
                        Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                        RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ddlHusbClaimAge.Text);
                        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(objCustomer.HusbandsRetAgeBenefit);
                        if (!string.IsNullOrEmpty(txtHusbCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(txtHusbCliamBenefit.Text);
                    }
                    else if (rdbtnspouseclaim.Checked)
                    {
                        Globals.Instance.ClaimedPerson = "WIFE";
                        Globals.Instance.BoolIsWifeClaimedBenefit = true;
                        RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ddlWifeClaimAge.Text);
                        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(objCustomer.WifesRetAgeBenefit);
                        if (!string.IsNullOrEmpty(txtWifeCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(txtWifeCliamBenefit.Text);
                    }
                }
                else
                {
                    Globals.Instance.ClaimedPerson = "NONE";
                }
                #endregion Restricted Application Income Changes Assign global values

                //Added on 5 may 2017
                #region Restricted Application Income Changes
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intHusbandClaimAge.ToString();
                        objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intHusbClaimBenefit.ToString();
                        objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;

                    }
                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intWifeClaimAge.ToString();
                        objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intWifeClaimBenefit.ToString();
                        objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                    }
                }
                else
                {
                    objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                    objCustomer.ClaimedAge = "";
                    objCustomer.ClaimedBenefit = "";
                }
                #endregion Restricted Application Income Changes
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

            return objCustomer;
        }

        /// <summary>
        /// Used to go back to home
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoBackToHome(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.Redirecthome, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }



        /// <summary>
        /// Clear text fields
        /// </summary>
        public void ClearFields()
        {
            try
            {
                txtFirstname.Text = string.Empty;
                txtUserBenefit.Text = string.Empty;
                txtSpouseBenefit.Text = string.Empty;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        private int CalculateAgeDifference(DateTime dtUserDOB, DateTime dtSpouseDOB)
        {
            try
            {
                if (dtUserDOB >= dtSpouseDOB)
                    return Convert.ToInt32(dtUserDOB - dtSpouseDOB);
                else
                    return Convert.ToInt32(dtSpouseDOB - dtUserDOB);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        #endregion

    }
}