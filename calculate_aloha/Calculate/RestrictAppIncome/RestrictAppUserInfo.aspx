﻿<%@ Page Title="Restricted Application User Info" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="RestrictAppUserInfo.aspx.cs" Inherits="Calculate.RestrictAppUserInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="modal-body">
        <%--row1--%>
        <div class="row">
            <br />
            <div class="col-md-12">
                <asp:Label ForeColor="#787878" ID="lblMaritalStatus" Font-Bold="true" runat="server" Text="Choose Your Marital Status"></asp:Label><br />
            </div>
            <div class="col-md-2 text-center">
                <asp:Label ID="lblMarr" runat="server" Font-Bold="true" Font-Size="13px" Text="Married"></asp:Label><br />
                <asp:RadioButton ID="rdbtnMarried" ForeColor="#787878" Checked="true" onclick="customerSpouseinformation();" Width="45px" runat="server" GroupName="MaritalStatus" />
            </div>
            <div class="col-md-2 col-sm-2 text-center">
                <asp:Label ID="lbSing" runat="server" Font-Bold="true" Font-Size="13px" Text="Single"></asp:Label><br />
                <asp:RadioButton ID="rdbtnSingle" Checked="false" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" />
            </div>
            <div class="col-md-2 col-sm-2 text-center">
                <asp:Label ID="lblWid" runat="server" Font-Bold="true" Font-Size="13px" Text="Widowed"></asp:Label><br />
                <asp:RadioButton ID="rdbtnWidowed" Checked="false" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" />
            </div>
            <div class="col-md-2 text-center">
                <asp:Label ID="lblDiv" runat="server" Font-Bold="true" Font-Size="13px" Text="Divorced"></asp:Label><br />
                <asp:RadioButton ID="rdbtnDivorced" ForeColor="#787878" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" /><br />
            </div>
            <div class="col-md-4 col-sm-4">
            </div>

        </div>
        <br />
        <div>
            <div class="row col-md-12 RestrictMarriedArea">
                <asp:Label runat="server" ForeColor="#787878" ID="lblAnyClaim" Text="Have either you or your spouse already claimed your Social Security Benefits?"></asp:Label>
                <br />
                <asp:RadioButton runat="server" ID="rdbtnAnyClaimYes" GroupName="YesNo" ForeColor="#787878" Text="Yes" onclick="ShowHideRestrictSection();" />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                <asp:RadioButton runat="server" ID="rdbtnAnyClaimNo" GroupName="YesNo" ForeColor="#787878" Text="No" onclick="ShowHideRestrictSection();" />
                <br />
                <asp:Label ID="lblSelectYesNoError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>

                <div class="row fakediv" style="height: 165px">
                </div>
            </div>

        </div>


        <div class="DefaultSection">
            <%--Row First Name Last Name--%>
            <div class="row">

                <div class="col-md-4 marginTop ">
                    <asp:Label ForeColor="#787878" ID="lblFirstname" runat="server" Text="First Name"></asp:Label><br />
                    <asp:TextBox CssClass="textEntry form-control control" TabIndex="1" ID="txtFirstname" MaxLength="20" ValidationGroup="RegisterUserValidationGroup" runat="server"></asp:TextBox>
                    <asp:TextBox CssClass="textEntry form-control" ID="passwordTextBox" Visible="false" MaxLength="20" runat="server"></asp:TextBox>
                    <asp:TextBox CssClass="textEntry form-control" ID="txtCustomerID" runat="server" Visible="false"></asp:TextBox>
                    <asp:Label ID="lblFNameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
                <%-- <div class="col-md-6 marginTop ">
                <asp:Label ForeColor="#787878" ID="lblLastname" runat="server" Text="Last Name"></asp:Label><br />
                <asp:TextBox CssClass="textEntry form-control control " TabIndex="5" ID="txtLastName" MaxLength="20" ValidationGroup="RegisterUserValidationGroup" runat="server"></asp:TextBox>

                <asp:Label ID="lblLNameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
            </div>--%>

                <div class="col-md-4 hideRow">
                    <br />
                    <asp:Label ForeColor="#787878" ID="lblSpouseFirstname" runat="server" Text="Spouse First Name"></asp:Label><br />
                    <asp:TextBox CssClass="textEntry form-control" ID="txtSpouseFirstname" MaxLength="20" TabIndex="2" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="lblSpouseFirstnameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>

                 <div class="col-md-4">
                    <br />
                    <asp:Label ForeColor="#787878" ID="Label1" runat="server" Text="Email"></asp:Label><br />
                    <asp:TextBox CssClass="textEntry form-control" ID="txtEmail"  TabIndex="3" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="Label2" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
            </div>
            <%--Row Email-Bdate--%>
            <div class="row ">
                <%--<div class="col-md-6 marginTop">
                <asp:Label ForeColor="#787878" ID="lblEmail" runat="server" Text="Email"></asp:Label><br />
                <asp:TextBox CssClass="textEntry form-control" TabIndex="6" ID="txtEmail" ValidationGroup="RegisterUserValidationGroup" MaxLength="50" AutoComplete="off" runat="server"></asp:TextBox>
                <asp:Label ID="lblEmailError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
            </div>--%>
                <div class="col-md-6 marginTop hideformarrried">
                    <asp:Label ForeColor="#787878" ID="lblUserBenefit" runat="server" Text="Your Full Retirement Age Benefit"></asp:Label><br />
                    <asp:TextBox ID="txtUserBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="4" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="lblUserBenefitError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>

                <div class="col-md-6 hideRow2 hideformarrried">
                    <br />
                    <asp:Label ForeColor="#787878" ID="lblSpouseBenefit" runat="server" Text="Spouse's monthly Full Retirement Age Benefit"></asp:Label><br />
                    <asp:TextBox ID="txtSpouseBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="5" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="lblSpouseBenefitError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 marginTop">
                    <asp:Label ID="lblBirthDate" ForeColor="#787878" runat="server" Text="Birth Date :"></asp:Label>
                    <br />
                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label runat="server" ForeColor="#787878" Text="Month:"></asp:Label>
                        <asp:DropDownList ID="ddlMonth_Your" runat="server" TabIndex="6" Class="Space" onchange="PopulateDays_Your()" />
                        <br />
                        <asp:Label ID="lblYourMonthError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                        <asp:Label ID="lblUserAgeError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:Label runat="server" ForeColor="#787878" Text="Day:"></asp:Label>
                        <asp:DropDownList ID="ddlDay_Your" runat="server" TabIndex="7" Class="Space" />
                        <br />
                        <asp:Label ID="lblYourDayError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:Label runat="server" ForeColor="#787878" Text="Year:"></asp:Label>
                        <asp:DropDownList ID="ddlYear_Your" runat="server" TabIndex="8" onchange="PopulateDays_Your()" />
                        <br />
                        <asp:Label ID="lblYourYearError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>

                    </div>
                    <br />
                </div>
            </div>

            <div class="row hideRow1">
                <div class="col-md-12 marginTop">
                    <asp:Label ID="lblSpouseBirthdate" ForeColor="#787878" runat="server" Text="Spouse Birth Date :"></asp:Label>
                    <br />
                </div>
                <div class="clearfix">
                    <div class="col-md-3 col-sm-3">
                        <asp:Label runat="server" ForeColor="#787878" Text="Month:"></asp:Label>
                        <asp:DropDownList ID="ddlMonth_Spouse" TabIndex="9" runat="server" Class="Space" onchange="PopulateDays_Spouse()" />
                        <br />
                        <asp:Label ID="lblSpouseMonthError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                        <asp:Label ID="lblSpouseAgeError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:Label runat="server" ForeColor="#787878" Text="Day:"></asp:Label>
                        <asp:DropDownList ID="ddlDay_Spouse" TabIndex="10" runat="server" Class="Space" />
                        <br />
                        <asp:Label ID="lblSpouseDayError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:Label runat="server" ForeColor="#787878" Text="Year:"></asp:Label>
                        <asp:DropDownList ID="ddlYear_Spouse" TabIndex="11" runat="server" onchange="PopulateDays_Spouse()" />
                        <br />
                        <asp:Label ID="lblSpouseYearError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                    <br />
                </div>
            </div>


            <div class="col-md-6 marginTop" style="display: none">
                <asp:Label ForeColor="#787878" ID="lblpassword" runat="server" Text="Password"></asp:Label><br />
                <asp:TextBox CssClass="textEntry form-control" MaxLength="20" Text="123465" ID="txtPassword" runat="server"></asp:TextBox>
            </div>

            <%--Row UserBenefit--%>
            <div class="row">
                <div class="col-md-6"></div>
            </div>
            <%--row3--%>
            <div class="row " style="display: none">
                <div class="col-md-6 marginTop">
                    <asp:Label ForeColor="#787878" ID="lblConfirmPassword" runat="server" Text="Confirm Password"></asp:Label><br />
                    <asp:TextBox CssClass="textEntry form-control" MaxLength="20" Text="123465" ID="txtConfirmPassword" runat="server"></asp:TextBox>
                    <img id="imgpass" class="img-responsive" runat="server" src="/images/confirmPassword.png" style="margin-left: 205px; margin-top: -26px; display: none;" border="0" />
                </div>
                <div class="col-md-6"></div>
            </div>
            <%--row4--%>
            <div>
                <div class="row">
                    <%--<div class="col-md-6 hideRow">
                    <br />
                    <asp:Label ForeColor="#787878" ID="lblSpouseFirstname" runat="server" Text="Spouse First Name"></asp:Label><br />
                    <asp:TextBox CssClass="textEntry form-control" ID="txtSpouseFirstname" MaxLength="20" TabIndex="16" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="lblSpouseFirstnameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>--%>
                    <%-- <div class="col-md-6 hideRow2 hideformarrried">
                    <br />
                    <asp:Label ForeColor="#787878" ID="lblSpouseBenefit" runat="server" Text="Spouse's monthly Full Retirement Age Benefit"></asp:Label><br />
                    <asp:TextBox ID="txtSpouseBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="17" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:Label ID="lblSpouseBenefitError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>--%>
                </div>

            </div>
            <br />
        </div>



        <div class="RestrictSection displayNone">
            <div class="row">
                <div class="col-md-6 hideRow">
                    <br />
                    <asp:RadioButton runat="server" GroupName="claimgroup" ForeColor="#787878" ID="rdbtnuserclaim" onclick="ShowSpousalRestrictSection();" Text="I have already claimed Social Security Benefits" />
                    <br />
                    <asp:Label ID="lblSelectClaimingUser" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
                <div class="col-md-6 hideRow">
                    <br />
                    <asp:RadioButton runat="server" ID="rdbtnspouseclaim" ForeColor="#787878" GroupName="claimgroup" onclick="ShowSpousalRestrictSection();" Text="My spouse already claimed Social Security Benefits" />
                </div>
            </div>
        </div>

        <div>
            <div class="row" style="margin-top: 15px">
                <%--Husband Section--%>
                <div class="col-md-6">
                    <div class="claimsection displayNone">
                        <%--User Claimed Age--%>
                        <div class="hideRow claimuser displayNone">
                            <asp:Label ForeColor="#787878" ID="lblClaimedAgeHusb" runat="server" Text="Age when benefit claimed"></asp:Label><br />
                            <asp:DropDownList runat="server" ID="ddlHusbClaimAge" TabIndex="9" CssClass="input-text textEntry form-control"></asp:DropDownList>
                        </div>
                        <%--User Claimed Benefit--%>
                        <div class="hideRow claimuser displayNone">
                            <asp:Label ForeColor="#787878" ID="lblHusbCliamBen" runat="server" Text="Amount of claimed benefit"></asp:Label><br />
                            <asp:TextBox runat="server" ID="txtHusbCliamBenefit" TabIndex="10" CssClass="input-text textEntry form-control"></asp:TextBox>
                            <asp:Label ID="lblHusbClaimBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                        </div>

                    </div>
                    <%--User Current Benefit--%>
                    <div class="currentbenefit displayNone">
                        <asp:Label ForeColor="#787878" ID="lblHusbCurBen" runat="server" Text="Amount of current benefit"></asp:Label><br />
                        <asp:TextBox runat="server" ID="txtHusbCurrBen" TabIndex="11" CssClass="input-text textEntry form-control"></asp:TextBox>
                        <asp:Label ID="lblHusbCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                </div>

                <%--Wife Section--%>
                <div class="col-md-6">
                    <div class="claimsection displayNone">
                        <%--Wife Claimed Age--%>
                        <div class="hideRow claimspouse displayNone">
                            <asp:Label ForeColor="#787878" ID="lblClimedAgeWife" runat="server" Text="Spouse's age when benefit claimed"></asp:Label><br />
                            <asp:DropDownList runat="server" ID="ddlWifeClaimAge" TabIndex="12" CssClass="input-text textEntry form-control"></asp:DropDownList>
                        </div>
                        <%--Wife Claimed Benefit--%>
                        <div class="hideRow claimspouse displayNone">
                            <asp:Label ForeColor="#787878" ID="lblWifeCliemBen" runat="server" Text="Amount of spouse's claimed benefit"></asp:Label><br />
                            <asp:TextBox runat="server" ID="txtWifeCliamBenefit" TabIndex="13" CssClass="input-text textEntry form-control"></asp:TextBox>
                            <asp:Label ID="lblWifeClaimBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                        </div>

                    </div>
                    <%--Wife Current Benefit--%>
                    <div class="currentbenefit displayNone">
                        <asp:Label ForeColor="#787878" ID="lblWifeCurBen" runat="server" Text="Amount of spouse's current benefit"></asp:Label><br />
                        <asp:TextBox runat="server" ID="txtWifeCurrBen" TabIndex="14" CssClass="input-text textEntry form-control"></asp:TextBox>
                        <asp:Label ID="lblWifeCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </div>

        </div>


        <%--   <div>
            <div class="row ">
                <div class="col-md-6  currentbenefit displayNone">
                    <asp:Label ForeColor="#787878" ID="lblHusbCurBen" runat="server" Text="Your Current Benefit"></asp:Label><br />
                    <asp:TextBox runat="server" ID="txtHusbCurrBen" CssClass="input-text textEntry form-control"></asp:TextBox>
                    <asp:Label ID="lblHusbCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
                <div class="col-md-6  currentbenefit displayNone">
                    <asp:Label ForeColor="#787878" ID="lblWifeCurBen" runat="server" Text="Spouse Current Benefit"></asp:Label><br />
                    <asp:TextBox runat="server" ID="txtWifeCurrBen" CssClass="input-text textEntry form-control"></asp:TextBox>
                    <asp:Label ID="lblWifeCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
        <div>
            <div class="row claimsection displayNone">
                <div>
                    <div class="col-md-6 hideRow claimuser displayNone" style="height: 100px; width: 400px; position: absolute;">
                        <asp:Label ForeColor="#787878" ID="lblClaimedAgeHusb" runat="server" Text="Your Claimed Age"></asp:Label><br />
                        <asp:DropDownList runat="server" ID="ddlHusbClaimAge" CssClass="input-text textEntry form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-6 hideRow claimspouse displayNone" style="height: 100px; width: 400px; position: absolute;">
                    <asp:Label ForeColor="#787878" ID="lblClimedAgeWife" runat="server" Text="Spouse Claimed Age"></asp:Label><br />
                    <asp:DropDownList runat="server" ID="ddlWifeClaimAge" CssClass="input-text textEntry form-control"></asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <div class="row claimsection displayNone">
                <div class="col-md-6 hideRow claimuser displayNone">
                    <asp:Label ForeColor="#787878" ID="lblHusbCliamBen" runat="server" Text="Your Claimed Benefit"></asp:Label><br />
                    <asp:TextBox runat="server" ID="txtHusbCliamBenefit" CssClass="input-text textEntry form-control"></asp:TextBox>
                </div>
                <div class="col-md-6 hideRow claimspouse displayNone">
                    <asp:Label ForeColor="#787878" ID="lblWifeCliemBen" runat="server" Text="Spouse Claimed Benefit"></asp:Label><br />
                    <asp:TextBox runat="server" ID="txtWifeCliamBenefit" CssClass="input-text textEntry form-control"></asp:TextBox>
                </div>
            </div>
        </div>--%>

        <br />


        <br />
        <div id="space" runat="server" class="spaceSingle displayNone">
            <br />
            <br />
        </div>
        <%--row5--%>
        <div class="row">
            <div style="text-align: center">
                <asp:Button TabIndex="15" ID="btnSave" runat="server" Text="View Report" OnClientClick="return Validate_UserData();" OnClick="NavigateToReportPage" CssClass="button_login marginpopbottom" />&nbsp;&nbsp;
                <asp:Button TabIndex="16" ID="btnCancel" OnClick="GoBackToHome" runat="server" Text="Back" CssClass="button_login" />

                <br />
                <br />
                <br />
                <asp:Label ID="lblUserCreate" CssClass="setGreen" runat="server" Visible="False" Style="text-align: center"></asp:Label>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
            </div>
        </div>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function SetTarget() {
            document.forms[0].target = "_blank";
        }

        <%--var userID = '<%= Session["UserID"].ToString() %> ';--%>
        $(document).ready(function () {

            customerSpouseinformation();

            $('#<%=btnSave.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
        });

        function PopulateDays_Your() {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            var y_Your = ddlYear_Your.options[ddlYear_Your.selectedIndex].value;
            var m_Your = ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0;
            if (ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0 && ddlYear_Your.options[ddlYear_Your.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Your.options[ddlYear_Your.selectedIndex].value, ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value - 1, 32).getDate();
                // ddlDay_Your.options.length = 0;
                AddOption_Your(ddlDay_Your, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Your(ddlDay_Your, i, i);
                }
            }
        }

        function AddOption_Your(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);

        }

        function Validate_Your(sender, args) {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            args.IsValid = (ddlDay_Your.selectedIndex != 0 && ddlMonth_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0)
        }


        function PopulateDays_Spouse() {

            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            var y_Spouse = ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value;
            var m_Spouse = ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0;
            if (ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0 && ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value, ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value - 1, 32).getDate();
                //ddlDay_Spouse.options.length = 0;
                AddOption_Spouse(ddlDay_Spouse, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Spouse(ddlDay_Spouse, i, i);
                }
            }
        }

        function AddOption_Spouse(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }

        function Validate_Spouse(sender, args) {
            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            args.IsValid = (ddlDay_Spouse.selectedIndex != 0 && ddlMonth_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0)
        }


        function ShowHideRestrictSection() {
            if ($("[id$=rdbtnAnyClaimYes]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "block");
                $(".currentbenefit").css("display", "none");
                $("[id$=lblSelectYesNoError]").text("");

            }
            else if ($("[id$=rdbtnAnyClaimNo]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "none");
                $(".claimsection").css("display", "none");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                $("[id$=lblWifeCurBen]").text("Amount of spouse's monthly full retirement age benefit");
                $("[id$=lblSelectYesNoError]").text("");
            }

            $("[id$=rdbtnuserclaim]").prop("checked", false);
            $("[id$=rdbtnspouseclaim]").prop("checked", false);

        }


        function ShowSpousalRestrictSection() {
            $(".claimsection").css("display", "block");
            if ($("[id$=rdbtnuserclaim]").prop("checked")) {
                $(".claimuser").css("display", "block");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of current benefit");
                $("[id$=lblWifeCurBen]").text("Amount of Spouse's monthly full retirement age benefit");
                $("[id$=lblSelectClaimingUser]").text("");
            }
            else
                $(".claimuser").css("display", "none");

            if ($("[id$=rdbtnspouseclaim]").prop("checked")) {
                $(".claimspouse").css("display", "block");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                $("[id$=lblWifeCurBen]").text("Amount of spouse's current benefit");
                $("[id$=lblSelectClaimingUser]").text("");
            }
            else
                $(".claimspouse").css("display", "none");




        }

        function ResetRestrictSection() {
            $("[id$=rdbtnuserclaim]").prop("checked", false);
            $("[id$=rdbtnspouseclaim]").prop("checked", false);
            $("[id$=rdbtnAnyClaimYes]").prop("checked", false);
            $("[id$=rdbtnAnyClaimNo]").prop("checked", false);

            $(".RestrictSection").css("display", "none");
            $(".currentbenefit").css("display", "none");

            ShowHideRestrictSection();
        }

        function customerSpouseinformation() {

            $(".claimsection").css("display", "none");
            ResetRestrictSection();


            $(".BasicInfo").hide();
            $(".BasicInfoDemo").hide();

            ResetFields();

            //$("[id$=lblYourError]").text('');
            //$("[id$=lblSpouseNamerequired]").text('');
            //$("[id$=lblSpouseError]").text('');

            if ($("[id$=rdbtnMarried]").prop("checked")) {


                $(".DefaultSection").hide();
                $(".fakediv").show();


                $("#btnContinueStep2").show();

                $("#btnBackContinueStep2Demo").show();
                $("#dummyDiv").hide();
                $("#dummyDivDemo").hide();
                $(".hideRow").show();
                $(".hideRow1").show();
                $(".DivorceText").hide();
                $(".spacehide").hide();
                $(".BasicInfo").show();
                $(".BasicInfoDemo").show();
                $(".hideRow2").show();
                $(".RestrictMarriedArea").css("display", "block");
                $(".hideformarrried").hide();


                var lblSpouseBirthDate = document.getElementById("<%=lblSpouseBirthdate.ClientID%>");
                if (lblSpouseBirthDate != null) {
                    document.getElementById("<%=lblSpouseBirthdate.ClientID%>").innerHTML = "What is your spouse's date of birth?";


                }

                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, true);
                //ValidatorEnable(custValidYearSpouse, true);
                //ValidatorEnable(custValidMonthSpouse, true);

            }
            else if ($("[id$=rdbtnDivorced]").prop("checked")) {
                $(".DefaultSection").show();
                $("#dummyDiv").hide();
                $("#btnBackContinueStep2Demo").show();
                $(".hideRow").hide();
                $(".hideRow1").show();
                $(".DivorceText").show();
                $("#dummyDivDemo").hide();
                $(".spacehide").hide();
                $(".BasicInfo").show();
                $("#btnContinueStep2").show();
                $(".BasicInfoDemo").show();
                $(".RestrictMarriedArea").css("display", "none");
                $(".hideformarrried").show();

                var lblSpouseBirthDate = document.getElementById("<%=lblSpouseBirthdate.ClientID%>");
                if (lblSpouseBirthDate != null) {
                    document.getElementById("<%=lblSpouseBirthdate.ClientID%>").innerHTML = "What is your Ex-spouse's date of birth?";
                }
               <%-- var Label5 = document.getElementById("<%=Label5.ClientID%>");
                if (Label5 != null) {
                    $("[id$=Label5]").text("What is your Ex-spouse's date of birth?");
                }--%>

                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, true);
                //ValidatorEnable(custValidYearSpouse, true);
                //ValidatorEnable(custValidMonthSpouse, true);
            }
            else if ($("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnSingle]").prop("checked")) {
                $(".DefaultSection").show();
                $(".hideformarrried").show();
                $("#dummyDiv").hide();
                $("#btnBackContinueStep2Demo").show();
                $("#dummyDivDemo").hide();
                $(".hideRow").hide();
                $(".hideRow1").hide();
                $(".spacehide").show();
                $(".DivorceText").hide();
                $(".BasicInfo").show();
                $("#btnContinueStep2").show();
                $(".BasicInfoDemo").show();
                $(".hideRow2").show();
                $(".RestrictMarriedArea").css("display", "none");

                if ($("[id$=rdbtnSingle]").prop("checked")) {
                    $(".hideRow2").hide();
                }
                //document.getElementById("lblSpouseBirthDate").innerHTML = "What is your Ex-Spouse's date of birth?";



                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, false);
                //ValidatorEnable(custValidYearSpouse, false);
                //ValidatorEnable(custValidMonthSpouse, false);


            }
    }

    function ResetFields() {

        $("[id$=lblFNameError]").text('');
        $("[id$=lblLNameError]").text('');
        $("[id$=lblUserBenefitError]").text('');
        $("[id$=lblSpouseBenefitError]").text('');
        $("[id$=lblEmailError]").text('');
        $("[id$=lblSpouseFirstnameError]").text('');
        $("[id$=lblUserAgeError]").text('');
        $("[id$=lblSpouseAgeError]").text('');
        $("[id$=lblHusbClaimBenError]").text('');

        $("[id$=lblHusbCurrBenError]").text('');
        $("[id$=lblWifeClaimBenError]").text('');
        $("[id$=lblWifeCurrBenError]").text('');
        $("[id$=lblSelectClaimingUser]").text('');




        $("[id$=txtFirstname]").removeClass("error");
        $("[id$=txtLastName]").removeClass("error");
        $("[id$=txtUserBenefit]").removeClass("error");
        $("[id$=txtSpouseBenefit]").removeClass("error");
        $("[id$=txtEmail]").removeClass("error");
        $("[id$=txtSpouseFirstname]").removeClass("error");
        $("[id$=txtHusbCliamBenefit]").removeClass("error");
        $("[id$=txtHusbCurrBen]").removeClass("error");
        $("[id$=txtWifeCliamBenefit]").removeClass("error");
        $("[id$=txtWifeCurrBen]").removeClass("error");



        $("[id$=ddlMonth_Your]").removeClass("error");
        $("[id$=lblYourMonthError]").text('');
        $("[id$=ddlYear_Your]").removeClass("error");
        $("[id$=lblYourYearError]").text('');
        $("[id$=ddlDay_Your]").removeClass("error");
        $("[id$=lblYourDayError]").text('');
        $("[id$=ddlMonth_Spouse]").removeClass("error");
        $("[id$=lblSpouseMonthError]").text('');
        $("[id$=ddlYear_Spouse]").removeClass("error");
        $("[id$=lblSpouseYearError]").text('');
        $("[id$=ddlDay_Spouse]").removeClass("error");
        $("[id$=lblSpouseDayError]").text('');


    }


    function Validate_UserData() {


        ResetFields();

        /* Validate Form Fields */
        /* Initialize Variable */
        var isValidate = true;
        //var alphaNumericRegex = /[^0-9a-z]/i;
        var alphaNumericRegex = /^[A-Za-z]+$/;
        //var numericRegex = '^[0-9]*$';
        var numericRegex = /^\d+$/;
        var errorMessage = '';

        var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
        var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
        var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");

        //Check radio button is selected or not
        if ($("[id$=rdbtnMarried]").prop("checked")) {
            if (!$("[id$=rdbtnAnyClaimYes]").prop("checked") && !$("[id$=rdbtnAnyClaimNo]").prop("checked")) {
                $("[id$=lblSelectYesNoError]").text("* Please select an option");
                isValidate = false;
                return isValidate;
            }
            else
                $("[id$=lblSelectYesNoError]").text("");

            if ($("[id$=rdbtnAnyClaimYes]").prop("checked")) {
                if (!$("[id$=rdbtnuserclaim]").prop("checked") && !$("[id$=rdbtnspouseclaim]").prop("checked")) {
                    $("[id$=lblSelectClaimingUser]").text("* Please select claiming person.");
                    isValidate = false;
                }
                else
                    $("[id$=lblSelectClaimingUser]").text("");
            }
        }



        /* Check if birth date field exists in current form or not */
        if (ddlMonth_Your.selectedIndex == 0) {
            isValidate = false;
            /* Add error css class */
            $("[id$=ddlMonth_Your]").addClass("error");
            $("[id$=lblYourMonthError]").text("* Month Required");
        }
        if (ddlYear_Your.selectedIndex == 0) {
            isValidate = false;
            /* Add error css class */
            $("[id$=ddlYear_Your]").addClass("error");
            $("[id$=lblYourYearError]").text("* Year Required");
        }

        if (ddlDay_Your.selectedIndex == 0) {
            isValidate = false;
            /* Add error css class */
            $("[id$=ddlDay_Your]").addClass("error");
            $("[id$=lblYourDayError]").text("* Day Required");
        }


        //Check User age is less than 70
        if (ddlMonth_Your.selectedIndex != 0 && ddlDay_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0) {

            var day = $("#ctl00_ctl00_MainContent_BodyContent_ddlDay_Your ").val();
            var year = $("#ctl00_ctl00_MainContent_BodyContent_ddlYear_Your").val();
            var month = $("#ctl00_ctl00_MainContent_BodyContent_ddlMonth_Your").val();
            var age = calculate_age(month, day, year);
            if (age > 70) {
                isValidate = false;
                $("[id$=lblUserAgeError]").text("*Birth date should not exceed 70 years.");
                $("[id$=ddlDay_Your]").addClass("error");
                $("[id$=ddlYear_Your]").addClass("error");
                $("[id$=ddlMonth_Your]").addClass("error");
            }
            else if (age <= 0) {
                isValidate = false;
                $("[id$=lblUserAgeError]").text("*Birth date can not be in future");
                $("[id$=ddlDay_Your]").addClass("error");
                $("[id$=ddlYear_Your]").addClass("error");
                $("[id$=ddlMonth_Your]").addClass("error");
            }
        }


        /* Check if field exists in current form or not */
        if ($("[id$=txtFirstname]").val() != null) {
            /* Validate First Name Field */
            if ($("[id$=txtFirstname]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtFirstname]").addClass("error");
                $("[id$=lblFNameError]").text("* First Name Required");
            }
            else {
                /* Check AlphaNumeric String */
                if (!alphaNumericRegex.test($("[id$=txtFirstname]").val())) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtFirstname]").addClass("error");
                    $("[id$=lblFNameError]").text("* First Name should be Character");

                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtFirstname]").removeClass("error");

                }
            }
        }

        /* Check if field exists in current form or not */
        if ($("[id$=txtLastName]").val() != null) {
            /* Validate First Name Field */
            if ($("[id$=txtLastName]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtLastName]").addClass("error");
                $("[id$=lblLNameError]").text("* Last Name Required");
            }
            else {
                /* Check AlphaNumeric String */
                if (!alphaNumericRegex.test($("[id$=txtLastName]").val())) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtLastName]").addClass("error");
                    $("[id$=lblLNameError]").text("* Last Name should be Character");

                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtLastName]").removeClass("error");

                }
            }
        }

        /* Check if field exists in current form or not */
        if ($("[id$=rdbtnMarried]").prop("checked")) {
            if ($("[id$=txtHusbCurrBen]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtHusbCurrBen]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtHusbCurrBen]").addClass("error");
                    $("[id$=lblHusbCurrBenError]").text("* User Benefit Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!numericRegex.test($("[id$=txtHusbCurrBen]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtHusbCurrBen]").addClass("error");
                        $("[id$=lblHusbCurrBenError]").text("* User Benefit should be Numeric");

                    }
                        // else if ($("[id$=txtUserBenefit]").val() == '0') {
                        //     isValidate = false;
                        //    /* Add error css class */
                        //     $("[id$=txtUserBenefit]").addClass("error");
                        //    $("[id$=lblHusbCurrBenError]").text("* User Benefit should not be Zero");
                        // }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtHusbCurrBen]").removeClass("error");
                        $("[id$=lblHusbCurrBenError]").text("");

                    }
                }
            }

        }
        else {
            if ($("[id$=txtUserBenefit]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtUserBenefit]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtUserBenefit]").addClass("error");
                    $("[id$=lblUserBenefitError]").text("* User Benefit Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!numericRegex.test($("[id$=txtUserBenefit]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtUserBenefit]").addClass("error");
                        $("[id$=lblUserBenefitError]").text("* User Benefit should be Numeric");

                    }
                        // else if ($("[id$=txtUserBenefit]").val() == '0') {
                        //     isValidate = false;
                        //    /* Add error css class */
                        //     $("[id$=txtUserBenefit]").addClass("error");
                        //    $("[id$=lblUserBenefitError]").text("* User Benefit should not be Zero");
                        // }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtUserBenefit]").removeClass("error");

                    }
                }
            }
        }
        //Check validation of spousal benefits
        if ($("[id$=rdbtnMarried]").prop("checked") || $("[id$=rdbtnDivorced]").prop("checked")) {

            if ($("[id$=rdbtnMarried]").prop("checked") || $("[id$=rdbtnDivorced]").prop("checked")) {
                var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
                var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
                var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");

                if (ddlMonth_Spouse.selectedIndex == 0) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=ddlMonth_Spouse]").addClass("error");
                    $("[id$=lblSpouseMonthError]").text("* Month Required");
                }
                if (ddlYear_Spouse.selectedIndex == 0) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=ddlYear_Spouse]").addClass("error");
                    $("[id$=lblSpouseYearError]").text("* Year Required");
                }

                if (ddlDay_Spouse.selectedIndex == 0) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=ddlDay_Spouse]").addClass("error");
                    $("[id$=lblSpouseDayError]").text("* Day Required");
                }


                //Check spouse age is less than 70
                if (ddlMonth_Spouse.selectedIndex != 0 && ddlDay_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0) {
                    var day = $("#ctl00_ctl00_MainContent_BodyContent_ddlDay_Spouse").val();
                    var year = $("#ctl00_ctl00_MainContent_BodyContent_ddlYear_Spouse").val();
                    var month = $("#ctl00_ctl00_MainContent_BodyContent_ddlMonth_Spouse").val();
                    var age = calculate_age(month, day, year);
                    if (age > 70) {
                        isValidate = false;
                        $("[id$=ddlDay_Spouse]").addClass("error");
                        $("[id$=ddlYear_Spouse]").addClass("error");
                        $("[id$=ddlMonth_Spouse]").addClass("error");
                        $("[id$=lblSpouseAgeError]").text("* Birth date should not exceed 70 years.");
                    }
                    else if (age <= 0) {
                        isValidate = false;
                        $("[id$=ddlDay_Spouse]").addClass("error");
                        $("[id$=ddlYear_Spouse]").addClass("error");
                        $("[id$=ddlMonth_Spouse]").addClass("error");
                        $("[id$=lblSpouseAgeError]").text("* Birth date can not be in future");
                    }
                }


            }

            if ($("[id$=rdbtnMarried]").prop("checked")) {

                if ($("[id$=txtWifeCurrBen]").val() != null) {
                    /* Validate First Name Field */
                    if ($("[id$=txtWifeCurrBen]").val() == '') {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtWifeCurrBen]").addClass("error");
                        $("[id$=lblWifeCurrBenError]").text("* Spouse Benefit Required");
                    }
                    else {
                        /* Check AlphaNumeric String */
                        if (!numericRegex.test($("[id$=txtWifeCurrBen]").val())) {
                            isValidate = false;
                            /* Add error css class */
                            $("[id$=txtWifeCurrBen]").addClass("error");
                            $("[id$=lblWifeCurrBenError]").text("* Spouse Benefit should be Numeric");

                        }
                            //else if ($("[id$=txtSpouseBenefit]").val() == '0') {
                            //    isValidate = false;
                            //    /* Add error css class */
                            //    $("[id$=txtSpouseBenefit]").addClass("error");
                            //    $("[id$=lblWifeCurrBenError]").text("* Spouse Benefit should not be zero");
                            //}
                        else {
                            /* Remove Css Class */
                            $("[id$=txtWifeCurrBen]").removeClass("error");
                            $("[id$=lblWifeCurrBenError]").text("");

                        }
                    }
                }

                //Check claimed benefit & year if user checked Yes only
                if ($("[id$=rdbtnAnyClaimYes]").prop("checked") && $("[id$=rdbtnuserclaim]").prop("checked")) {
                    if ($("[id$=txtHusbCliamBenefit]").val() != null) {
                        /* Validate First Name Field */
                        if ($("[id$=txtHusbCliamBenefit]").val() == '') {
                            isValidate = false;
                            /* Add error css class */
                            $("[id$=txtHusbCliamBenefit]").addClass("error");
                            $("[id$=lblHusbClaimBenError]").text("*Your Claimed Benefit Required");
                        }
                        else {
                            /* Check AlphaNumeric String */
                            if (!numericRegex.test($("[id$=txtHusbCliamBenefit]").val())) {
                                isValidate = false;
                                /* Add error css class */
                                $("[id$=txtHusbCliamBenefit]").addClass("error");
                                $("[id$=lblHusbClaimBenError]").text("* Your Claimed Benefit should be Numeric");

                            }
                            else {
                                /* Remove Css Class */
                                $("[id$=txtHusbCliamBenefit]").removeClass("error");

                            }
                        }
                    }
                }

                //Check claimed benefit & year if user checked Yes only
                if ($("[id$=rdbtnAnyClaimYes]").prop("checked") && $("[id$=rdbtnspouseclaim]").prop("checked")) {
                    if ($("[id$=txtWifeCliamBenefit]").val() != null) {
                        /* Validate First Name Field */
                        if ($("[id$=txtWifeCliamBenefit]").val() == '') {
                            isValidate = false;
                            /* Add error css class */
                            $("[id$=txtWifeCliamBenefit]").addClass("error");
                            $("[id$=lblWifeClaimBenError]").text("* Spouse's Claimed Benefit Required");
                        }
                        else {
                            /* Check AlphaNumeric String */
                            if (!numericRegex.test($("[id$=txtWifeCliamBenefit]").val())) {
                                isValidate = false;
                                /* Add error css class */
                                $("[id$=txtWifeCliamBenefit]").addClass("error");
                                $("[id$=lblWifeClaimBenError]").text("* Spouse's Claimed Benefit should be Numeric");

                            }
                            else {
                                /* Remove Css Class */
                                $("[id$=txtWifeCliamBenefit]").removeClass("error");

                            }
                        }
                    }
                }

            }

        }


        if ($("[id$=rdbtnDivorced]").prop("checked") || $("[id$=rdbtnWidowed]").prop("checked")) {
            /* Check if field exists in current form or not */
            if ($("[id$=txtSpouseBenefit]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtSpouseBenefit]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseBenefit]").addClass("error");
                    $("[id$=lblSpouseBenefitError]").text("* Spouse Benefit Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!numericRegex.test($("[id$=txtSpouseBenefit]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtSpouseBenefit]").addClass("error");
                        $("[id$=lblSpouseBenefitError]").text("* Spouse Benefit should be Numeric");

                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtSpouseBenefit]").removeClass("error");

                    }
                }
            }
        }

        /* Check if field exists in current form or not */
        //if ($("[id$=txtEmail]").val() != null) {
        //    /* Validate Email Field */
        //    if ($("[id$=txtEmail]").val() == '') {
        //        isValidate = false;
        //        /* Add error css class */
        //        $("[id$=txtEmail]").addClass("error");
        //        $("[id$=lblEmailError]").text("* Email Address Required");
        //    }
        //    else {
        //        // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //        var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
        //        /* Validation Email Address Format */
        //        if (!emailfilter1.test($("[id$=txtEmail]").val())) {
        //            $("[id$=lblEmailError]").text("* Invalid Email Address");
        //            /* Add error css class */
        //            $("[id$=txtEmail]").addClass("error");
        //            isValidate = false;
        //        }
        //        else {
        //            /* Remove Css Class */
        //            $("[id$=txtEmail]").removeClass("error");
        //        }
        //    }
        //}


        /* Check if field exists in current form or not */
        if ($("[id$=txtSpouseFirstname]").val() != null) {
            if ($("[id$=rdbtnMarried]").prop("checked")) {
                /* Validate First Name Field */
                if ($("[id$=txtSpouseFirstname]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseFirstname]").addClass("error");
                    $("[id$=lblSpouseFirstnameError]").text("* Spouse First Name Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!alphaNumericRegex.test($("[id$=txtSpouseFirstname]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtSpouseFirstname]").addClass("error");
                        $("[id$=lblSpouseFirstnameError]").text("* Spouse first name should be Character");
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtSpouseFirstname]").removeClass("error");
                    }
                }
            }
            else {
                $("[id$=txtSpouseFirstname]").removeClass("error");
            }
        }

        return isValidate;

    }

    function calculate_age(birth_month, birth_day, birth_year) {
        var today_date = new Date();
        var today_year = today_date.getFullYear();
        var today_month = today_date.getMonth();
        today_day = today_date.getDate();
        age = today_year - birth_year;

        if (today_month < (birth_month - 1)) {
            age--;
        }
        if (((birth_month - 1) == today_month) && (today_day < birth_day)) {
            age--;
        }
        return age;
    }

    //Google Analytics
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-88761127-1', 'auto');
    ga('send', 'pageview');

    </script>

</asp:Content>
