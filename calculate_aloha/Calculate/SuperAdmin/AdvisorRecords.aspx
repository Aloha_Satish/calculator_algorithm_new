﻿<%@ Page Title="Upload Advisor Records" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="AdvisorRecords.aspx.cs" Inherits="Calculate.SuperAdmin.AdvisorRecords" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        row.style.backgroundColor = "#c9cdcd";
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        //and change rowcolor back to original
                        if (row.rowIndex % 2 == 0) {
                            //Alternating Row Color
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }


        //////////////////////
        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "#c9cdcd";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "#C2D69B";
                }
                else {
                    row.style.backgroundColor = "white";
                }
            }
            //Get the reference of GridView
            var GridView = row.parentNode;
            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");
            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            //for (var i = 0; i < inputList.length; i++) {
            //    //Based on all or none checkboxes
            //    //are checked check/uncheck Header Checkbox
            //    var checked = true;
            //    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
            //        if (!inputList[i].checked) {
            //            checked = false;
            //            break;
            //        }
            //    }
            //}
            headerCheckBox.checked = objRef.checked;
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="AccountDetails HomeImgBg">

        <div style="">
            <center>
                <h1 style="color: #85312f;" class="headfontsize setTopMargin">Upload Advisors</h1>
            </center>
        </div>

        <div class="col-md-12">
            <div class="col-md-7">
                <asp:Button ID="btnBack" Height="35px" OnClick="BackToMainPage" Font-Size="Small" Enabled="true" Text="Back to Main Page" CssClass="other-buttons" runat="server" />
            </div>
            <div class="col-md-2">

                <label class="choose">
                    <asp:FileUpload ID="fileUpload" Width="0px" ClientIDMode="Static" Height="42px" onchange="this.form.submit()" Font-Size="Small" runat="server"></asp:FileUpload>
                </label>
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnUpload" Width="146px" Height="35px" OnClick="UploadData" Enabled="false" Text="Upload Data" CssClass="other-buttons" runat="server" />
                <br />
                <br />
            </div>

        </div>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div style="min-height: 365px;">
                    <asp:GridView ID="gvDetails" HeaderStyle-Height="45px" runat="server" AutoGenerateColumns="false" CssClass="mGrid tablegrid  table-striped table-hover table-responsive" HeaderStyle-BackColor="#8e8d8a" AllowPaging="true"
                        PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr" PagerStyle-HorizontalAlign="Right" AllowSorting="true"
                        AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="true" EmptyDataText="No Advisor Found">
                        <Columns>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="90px" HeaderStyle-CssClass="textCenter" HeaderStyle-BorderStyle="None" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkBxSelect" runat="server" onclick="Check_Click(this)" Class="chkBxSelect1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="First Name" SortExpression="Firstname">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstname" runat="server" Text='<%#Eval("Firstname") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Last Name" SortExpression="Lastname">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Lastname") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Email" SortExpression="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="City" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="State" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#Eval("State") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Dealer" Visible="true">
                                <ItemTemplate>
                                    <asp:Label ID="lblDealer" runat="server" Text='<%#Eval("Dealer") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                        <%-- <AlternatingRowStyle BackColor="silver" />--%>
                    </asp:GridView>


                    <asp:Panel ID="pnlShowMsg" CssClass="MailSentPopUpModal" runat="server">
                        <div class="modal-body">
                            <br />
                            <asp:Label runat="server" ID="lblMessage"></asp:Label>
                            <br />
                            <br />
                            <asp:Button runat="server" ID="btnOk" CssClass="button_ok_poopup" Text="OK" OnClientClick="return HideModalPopupForMessage()" />
                            <br />
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake1" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="MailSentPopUp" runat="server" BehaviorID="MsgPopUp" PopupControlID="pnlShowMsg" TargetControlID="lnkFake1" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>


</asp:Content>
