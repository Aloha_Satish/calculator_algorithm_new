﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class NewsLettersAdvisor : System.Web.UI.Page
    {
        #region Keys
        protected string Key
        {
            get { return ViewState[Constants.Key].ToString(); }
            set { ViewState[Constants.Key] = value; }
        }

        #endregion Keys

        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                if (!IsPostBack)
                {
                    try
                    {

                        lblErrorMessage.Visible = false;
                        pnlAddEdit.Visible = false;
                        this.BindData();
                        Page.MaintainScrollPositionOnPostBack = true;
                        this.Key = Guid.NewGuid().ToString();
                        Cache[this.Key] = new List<HttpPostedFile>();
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ShowOtherparams, "displayTableHeader(false);", true);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                        lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                        lblErrorMessage.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }

        }

        /// <summary>
        /// Append Images while binding data into GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                /* Check if gridview row event matched with data control row */
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /* Get instance of selected column */
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(Constants.imgStatus);
                    Label Status = (Label)e.Row.FindControl(Constants.lblLetterStatus);
                    /* Create object of User */
                    Users objUser = new Users();
                    /* Assign value into image alternate text and images into image URL */
                    imgButton.AlternateText = Status.Text;
                    if (Convert.ToString(Status.Text).Equals(Constants.FlagYes))
                        imgButton.ImageUrl = Constants.ActivateImage;
                    else
                        imgButton.ImageUrl = Constants.DeactivateImage;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Activate/Deactivate Institution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Status(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    Label NewsLetterId = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblNewsLetterId);
                    ImageButton imgButton = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.imgStatus);
                    if (Convert.ToString(imgButton.AlternateText).Equals(Constants.FlagYes))
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagNo;
                        imgButton.ImageUrl = Constants.DeactivateImage;
                    }
                    else
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagYes;
                        imgButton.ImageUrl = Constants.ActivateImage;
                    }
                    NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                    objNewsLetter.updateNewsLetterStatus(NewsLetterId.Text, imgButton.AlternateText);
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.NewsLetterUpdatedSucessfuly;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    pnlAddEdit.Visible = false;
                    row.BackColor = System.Drawing.Color.White;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// We can edit Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editGridViewDetails(object sender, EventArgs e)
        {
            try
            {
                /* Change color if already different */
                foreach (GridViewRow rows in gvDetails.Rows)
                {
                    rows.BackColor = System.Drawing.Color.White;
                }
                BindInstitutionList();

                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    row.BackColor = ColorTranslator.FromHtml(Constants.GridBackColor);
                    lblErrorMessage.Visible = false;
                    lblHeader.Text = Constants.LblEditHeaderNewsLetter;
                    clsFlag.Flag = false;
                    pnlAddEdit.Visible = true;
                    #region Fill Advisor Details into Popup
                    /* Get content from gridview and assign to runtime created label */
                    Label NewsLetterId = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblNewsLetterId);
                    Label LetterUpdateBy = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLetterUpdateBy);
                    Label LetterStatus = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLetterStatus);
                    Label Subject = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblSubject);
                    Label LetterBody = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLetterBody);
                    Label LetterPublishDate = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLetterPublishDate);
                    Label Periodic = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblPeriodic);
                    Label AdvisorList = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorList);
                    Label CustomersFlag = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomersFlag);
                    string[] splitStrValue = AdvisorList.Text.Split(',');
                    for (int i = 0; i < splitStrValue.Length; i++)
                    {
                        for (int j = 0; j < lstInstitution.Items.Count; j++)
                        {
                            if (splitStrValue[i].ToString() == lstInstitution.Items[j].Value.ToString())
                            {
                                lstInstitution.Items[j].Selected = true;
                            }
                        }
                    }
                    /* Assign label data into textbox which are shown up in blockui popup */
                    NewsLetterIDHidden.Value = NewsLetterId.Text;
                    txtBody.InnerText = LetterBody.Text;
                    txtSubject.Text = Subject.Text;
                    lblLetterID.Text = NewsLetterId.Text;
                    datePublish.Text = LetterPublishDate.Text;

                    if (CustomersFlag.Text.Trim().Equals(Constants.FlagYes))
                        chkCustomers.Checked = true;
                    else
                        chkCustomers.Checked = false;

                    if (Periodic.Text.Trim().Equals(Constants.Weekly))
                        rdbWeekly.Checked = true;
                    else if (Periodic.Text.Trim().Equals(Constants.Monthly))
                        rdbMonthly.Checked = true;
                    else
                        rdbQuartely.Checked = true;
                    // Get Attachment details into Table 
                    NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                    DataTable dtAttachment = objNewsLetter.getAttachmentDetailsByNewsLetterId(NewsLetterId.Text);
                    if (dtAttachment.Rows.Count > Constants.zero)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ShowOtherparams, "displayTableHeader(true);", true);
                        for (int i = 0; i < dtAttachment.Rows.Count; i++)
                        {
                            TableRow tr = new TableRow();
                            TableCell tc = new TableCell();
                            TableCell tc2 = new TableCell();
                            tc.Text = "<span>" + dtAttachment.Rows[i]["FileName"].ToString() + "</span> &nbsp;&nbsp;<img src='../Images/hide.png' style='height:18px;width:18px;cursor:pointer;'/><input type='hidden' class='displayNone' id='newsletterIDTable' value='" + NewsLetterId.Text + "'";
                            tr.Cells.Add(tc);
                            attached.Rows.Add(tr);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Add new Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddNewsLetter(object sender, EventArgs e)
        {
            try
            {
                clsFlag.Flag = true;
                foreach (GridViewRow rows in gvDetails.Rows)
                {
                    rows.BackColor = System.Drawing.Color.White;
                }
                BindInstitutionList();
                pnlAddEdit.Visible = true;
                lblErrorMessage.Visible = false;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ShowOtherparams, "displayTableHeader(false);", true);
                lblHeader.Text = Constants.LblAddHeaderNewsLetter;
                #region Clear all fields
                txtBody.InnerText = String.Empty;
                txtSubject.Text = String.Empty;
                datePublish.Text = String.Empty;
                #endregion
                /* Show BlockUI PopUp */
                //popup.Show();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Get Advisor Id from gridview and assign to runtime created label */
                    Label AdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblNewsLetterId);
                    /* Initialize object */
                    NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                    /* Call function to delete selected row by passing id into function */
                    objNewsLetter.deleteNewsLetterInfo(AdvisorAdminID.Text);
                    /* Reload gridview with updated data */
                    this.BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.NewsLetterDeletedSucessfuly;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    pnlAddEdit.Visible = false;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        protected void Cancel(object sender, EventArgs e)
        {
            try
            {
                pnlAddEdit.Visible = false;
                lblErrorMessage.Visible = false;
                foreach (GridViewRow rows in gvDetails.Rows)
                {
                    rows.BackColor = System.Drawing.Color.White;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                /* Initialize NewsLetter Object */
                NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                NewsLetterAdvisorTO objNewsLetterTO = new NewsLetterAdvisorTO();
                /* Assign data into newsletter object variables */
                objNewsLetterTO.LetterUpdateBy = Session[Constants.SessionAdminId].ToString();
                objNewsLetterTO.LetterSubject = txtSubject.Text;
                objNewsLetterTO.LetterBody = txtBody.Value;
                objNewsLetterTO.PublishDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(datePublish.Text)).Replace("-", "/");
                string strValue = "";
                //GET THE SELECTED VALUE FROM CHECKBOX LIST
                for (int i = 0; i < lstInstitution.Items.Count; i++)
                {
                    if (lstInstitution.Items[i].Selected)
                    {
                        if (strValue.Length == 0)
                        {
                            strValue = lstInstitution.Items[i].Value;
                        }
                        else
                        {
                            strValue += "," + lstInstitution.Items[i].Value;
                        }
                    }
                }

                if (chkCustomers.Checked)
                    objNewsLetterTO.CustomersFlag = Constants.FlagYes;
                else
                    objNewsLetterTO.CustomersFlag = Constants.FlagNo;

                objNewsLetterTO.AdvisorList = strValue;
                if (rdbMonthly.Checked)
                    objNewsLetterTO.Periodic = rdbMonthly.Text;
                else if (rdbQuartely.Checked)
                    objNewsLetterTO.Periodic = rdbQuartely.Text;
                else
                    objNewsLetterTO.Periodic = rdbWeekly.Text;
                /* check if popup open for add or edit mode */
                if (clsFlag.Flag)
                {
                    objNewsLetterTO.InstitutionID = Session[Constants.SessionAdminId].ToString();
                    objNewsLetter.insertNewsLetterDetails(objNewsLetterTO);
                    /* Reload gridview with updated data */
                    BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.NewsLetterAddedSucessfuly;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    objNewsLetterTO.NewsLetterId = lblLetterID.Text;
                    objNewsLetter.updateNewsLetterInfo(objNewsLetterTO);
                    /* Reload gridview with updated data */
                    BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.NewsLetterUpdatedSucessfuly;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                }
                //Boolean statusMail = MailHelper.sendMail("swapnild@alohatechnology.com", txtSubject.Text, txtBody.InnerText);                
                AttachmentFilesTO attachmentFiles = new AttachmentFilesTO();
                List<HttpPostedFile> files = (List<HttpPostedFile>)Cache[this.Key];
                foreach (HttpPostedFile file in files)
                {
                    attachmentFiles.FileName = file.FileName;
                    attachmentFiles.ContentType = file.ContentType;
                    byte[] fileData = null;
                    double fileSize;
                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(file.ContentLength);
                        fileSize = file.ContentLength / 1048576f;
                    }
                    attachmentFiles.Content = fileData;
                    attachmentFiles.FileSize = fileSize.ToString();
                    if (clsFlag.Flag)
                    {
                        DataTable dtNewsLetter = objNewsLetter.getNewsLetterIdLatest();
                        if (dtNewsLetter.Rows.Count > Constants.zero)
                        {
                            attachmentFiles.NewsLetterId = dtNewsLetter.Rows[Constants.zero][Constants.NewsLetterId].ToString();
                        }
                    }
                    else
                    {
                        attachmentFiles.NewsLetterId = lblLetterID.Text;
                    }
                    objNewsLetter.insertAttachmentFileDetails(attachmentFiles);
                }
                Cache.Remove(this.Key);
                //Response.Redirect(Request.Url.AbsoluteUri);
                pnlAddEdit.Visible = false;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Validate Session
        /// </summary>
        public void validateSession()
        {
            if (Session[Constants.SessionUserId] == null)
            {
                try
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                    lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                    lblErrorMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                /* Create Object */
                DataTable tableRecords;
                NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                /* Get all institutional Data from database and bind to gridview control */
                tableRecords = objNewsLetter.getNewsLetterDetailsByID(Session[Constants.SessionAdminId].ToString());
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                }
                else
                {
                    /* Show blank table data with message */
                    tableRecords.Rows.Add(tableRecords.NewRow());
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Get column count */
                    int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    gvDetails.Rows[Constants.zero].Cells.Clear();
                    gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.NoNewsLetterFound;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        [WebMethod]
        public static void RemoveFile(string fileName, string id, string key)
        {
            try
            {
                List<HttpPostedFile> files = (List<HttpPostedFile>)HttpContext.Current.Cache[key];
                files.RemoveAll(f => f.FileName.ToLower().EndsWith(fileName.ToLower()));
                if (!String.IsNullOrEmpty(id))
                {
                    NewsLetterAdvisor objNewsLetter = new NewsLetterAdvisor();
                    objNewsLetter.deleteAttachmentByNewsLetterId(id, fileName);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        private void BindInstitutionList()
        {
            try
            {
                DataTable tableRecords;
                Institutional objInstitutional = new Institutional();
                Advisor objAdvisor = new Advisor();
                tableRecords = objAdvisor.getAdvisorDetails(Session[Constants.SessionAdminId].ToString());
                lstInstitution.DataSource = tableRecords;
                lstInstitution.DataTextField = Constants.AdvisorName;
                lstInstitution.DataValueField = Constants.CustomerAdvisorId;
                lstInstitution.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        ///Get Next Scheduled Date 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="period"></param>
        /// <returns></returns>
        public string GetNextDate(string date, string period)
        {
            try
            {
                DateTime nextdate;
                if (period.Trim().Equals(Constants.Weekly))
                {
                    nextdate = DateTime.Parse(date).AddDays(7);
                    while (nextdate <= DateTime.Now)
                    {
                        nextdate = nextdate.AddDays(7);
                    }
                    return nextdate.ToString();
                }
                else if (period.Trim().Equals(Constants.Monthly))
                {
                    nextdate = DateTime.Parse(date).AddMonths(1);
                    while (nextdate <= DateTime.Now)
                    {
                        nextdate = nextdate.AddMonths(1);
                    }
                    return nextdate.ToString();
                }
                else
                {
                    nextdate = DateTime.Parse(date).AddMonths(3);
                    while (nextdate <= DateTime.Now)
                    {
                        nextdate = nextdate.AddMonths(3);
                    }
                    return nextdate.ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
                return string.Empty;
            }
        }

        #endregion Methods

        #region TextChanged
        protected void lstInstitution_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkAll.Checked)
                {
                    for (int j = 0; j < lstInstitution.Items.Count; j++)
                    {
                        lstInstitution.Items[j].Selected = true;
                    }
                }
                else
                {
                    for (int j = 0; j < lstInstitution.Items.Count; j++)
                    {
                        lstInstitution.Items[j].Selected = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorNewsLetterAdvisor, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorNewsLetterAdvisor + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        #endregion TextChanged


    }
}
