﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageCustomers.aspx.cs" Inherits="Calculate.SuperAdmin.ManageCustomers" Title="Manage Customers" MasterPageFile="~/SiteNew.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ctl00_ClientMenu").css({ display: "none" });
            $("#ctl00_ctl00_centerDiv").removeClass('col-md-10 col-sm-10')
            $("#ctl00_ctl00_centerDiv").addClass('col-md-12 col-sm-12')
        })
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="AccountDetails HomeImgBg">
        <div class="col-md-12 col-sm-12">
            <div class="col-md-5 col-sm-5">
                <%--<img id="newcompnylogo" class="img-responsive" src="#" />--%>
            </div>
            <div class="col-md-7 col-sm-7">
                <h1 style="color: #85312f;" class="headfontsize text-left setTopMargin">Manage Clients</h1>
            </div>

        </div>
        <%-- <center><h1 class="header-center">Manage Clients</h1> </center>--%>

        <%--<div class="horizontalbrownline"></div>--%>
        <br />
        <div class="breadscrumbs margileft displayNone" style="margin-top: 5px">
            <a href="ManageInstitutions.aspx" id="manageInstitution" style="color: #85312f;" class="active breadfont">Manage Institutions > </a>
            <a href="ManageAdvisors.aspx" class="active breadfont" style="color: #85312f;" id="manageAdvisor">Manage Advisors > </a>
            <a class="displayNone breadfont" style="color: #757575;" id="displayName"></a>
        </div>
        <asp:Label ID="linkAdvisor" class="displayNone" runat="server"></asp:Label>
        <asp:Label ID="lblDisplayName" class="displayNone" runat="server"></asp:Label>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>

        <asp:UpdatePanel ID="upGrid" runat="server">

            <ContentTemplate>
                <div style="min-height: 385px; padding-top: 11px">
                    <div class="table-responsive">
                        <table class="filterGridView table managegrid">
                            <tbody>
                                <tr>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:DropDownList Height="27px" CssClass="form-control floatLeft maxWidthDropDownList minWidthDropdownSearch" ID="nameSelection" OnSelectedIndexChanged="nameSelection_TextChanged" AutoPostBack="true" DataTextField="Lastname" DataValueField="Lastname" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:TextBox CssClass="form-control textEntry handIcon floatLeft minWidthDropdownSearch" ID="dateFilter" runat="server" placeholder="Date" OnTextChanged="dateFilter_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                    <td class="searchText verticalalign">
                                        <div class="input-append" style="min-width: 170px;">
                                            <asp:TextBox CssClass="form-control handIcon textEntry floatLeft" ID="txtSearch" AutoPostBack="true" runat="server" placeholder="Search" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                                            <asp:ImageButton runat="server" ID="searchImage" ClientIDMode="Static" ImageUrl="~/Images/search_1.png" OnClick="txtSearch_TextChanged" CssClass="searchGridView floatLeft" />
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" Width="60px" OnClick="Reset" CssClass="managegridButton floatLeft" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table>
                            <tr>
                                <td>
                                    <div class="divGrid" style="overflow-x: auto;">
                                        <asp:GridView ID="gvDetails" DataKeyNames="CustomerID" HeaderStyle-Height="45px" runat="server"
                                            AutoGenerateColumns="false" CssClass="mGrid table table-striped table-hover table-responsive" HeaderStyle-BackColor="#8e8d8a" AllowPaging="true"
                                            OnPageIndexChanging="OnPaging" PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr" AllowSorting="true" OnSorting="gvDetails_Sorting"
                                            AlternatingRowStyle-CssClass="alt" PagerStyle-HorizontalAlign="Right" OnRowDataBound="gvDetails_RowDataBound" ShowHeaderWhenEmpty="true" EmptyDataText="No Customer Found">
                                            <Columns>
                                                <asp:TemplateField HeaderText="CustomerID" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" Visible="false" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%#Eval("CustomerID") %>' />
                                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("CustomerID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Spouse Last Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpouseLastname" runat="server" Text='<%#Eval("SpouseLastname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Last Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" SortExpression="Lastname" HeaderStyle-BorderStyle="None" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Lastname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="First Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" SortExpression="Firstname" HeaderStyle-BorderStyle="None" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFirstname" runat="server" Text='<%#Eval("Firstname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Client Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" Visible="false" HeaderStyle-BorderStyle="None" SortExpression="ClienName" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblClientName" runat="server" Text='<%# string.Concat(Eval("Firstname")," ",Eval("Lastname"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Birth Date" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" SortExpression="Birthdate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBirthdate" runat="server" Text='<%#Eval("Birthdate","{0:MM/dd/yyyy}") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Marital Status" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" SortExpression="MartialStatus" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMaritalStatus" runat="server" Text='<%#Eval("MartialStatus") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Spouse's First Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpouseFirstName" runat="server" Text='<%#Eval("SpouseFirstname") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Spouse's Birth Date" HeaderStyle-BackColor="#8e8d8a" ControlStyle-CssClass="ellipsis" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpouseBirthdate" runat="server" Text='<%#Eval("SpouseBirthdate") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Last Name" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" SortExpression="Lastname" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Lastname") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Password" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Email" SortExpression="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEmail" runat="server" ToolTip='<%#Eval("Email") %>' Text ='<%#Eval("Email") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Created On" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" SortExpression="UpdateDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("UpdateDate","{0:MM/dd/yyyy}") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Email" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblWifesRetAgeBenefit" runat="server" Text='<%#Eval("WifesRetAgeBenefit") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Email" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHusbandsRetAgeBenefit" runat="server" Text='<%#Eval("HusbandsRetAgeBenefit") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label Visible="false" ID="lblClaimedPerson" runat="server" Text='<%#Eval("ClaimedPerson") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis"  Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label Visible="false" ID="lblClaimedAge" runat="server" Text='<%#Eval("ClaimedAge") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis"  Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label Visible="false" ID="lblClaimedBenefit" runat="server" Text='<%#Eval("ClaimedBenefit") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnViewReport" runat="server" OnClick="View_Report" Text="View Report" CssClass="button_login" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnMailReport" runat="server" OnClick="Mail_Report" Text="Email to Client" CssClass="button_login" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="80px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnMailReportAdviosr" runat="server" OnClick="Mail_Report_Advisor" Text="Email Report" CssClass="button_login" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="40px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnEdit" runat="server" OnClick="editGridViewDetails" Text="Edit" CssClass="button_login" Width="65px" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="40px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDelete" runat="server" OnClick="Delete" Text="Delete" CssClass="button_login" Width="65px" OnClientClick="return showDeleteConfirm('Client');" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnDwnloadReport" runat="server" OnClick="View_Report" Text="Download Report" CssClass="button_login" Width="125px" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-BackColor="#8e8d8a" HeaderStyle-Width="90px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="textCenter" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgStatus" ToolTip="Suspend" OnClientClick="return showSuspendConfirm(this);" ImageUrl="~/Images/stop.png" runat="server" OnClick="Status" CommandName="Select" Visible="false" />
                                                        <asp:ImageButton ID="linkEdit" ToolTip="Edit" runat="server" ImageUrl="~/Images/edit_new.png" Visible="false" />
                                                        <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" ImageUrl="~/Images/delete_new.png" Visible="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="Details" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Button ID="lnkDetails" runat="server" Text="View Details" OnClick="View" CssClass="gridButton" />
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            </Columns>
                                            <%-- <AlternatingRowStyle BackColor="silver" />--%>
                                        </asp:GridView>
                                        <div class="managePageCount">
                                            <span>Showing <%=gvDetails.Rows.Count%> of <%= lblPageTotalNumber.Text %> Records &nbsp;</span>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Label ID="lblPageTotalNumber" runat="server" class="displayNone"></asp:Label>
                    <br />
                    <center><asp:Button ID="btnAdd" runat="server" Text="Add New Client" OnClick="AddCustomer" CssClass="button_login"></asp:Button></center>
                    <br />
                    <center> <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label></center>


                    <asp:Panel ID="pnlShowMsg" CssClass="MailSentPopUpModal" runat="server">
                        <div class="modal-body">
                            <br />
                            <asp:Label runat="server" ID="lblMessage"></asp:Label>
                            <br />
                            <br />
                            <asp:Button runat="server" ID="btnOk" CssClass="button_ok_poopup" Text="OK" OnClientClick="return HideModalPopupForMessage()" />
                            <br />
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="pnlAddEdit" runat="server" Width="850px" DefaultButton="btnSave" CssClass="ExplanationModalPopup CustomerPanel displayNone" ScrollBars="Auto">
                        <div id="textEntry" class="">
                            <%--<div class="h2PopUp modal-header text-center">
                            <asp:Label ForeColor="white" Font-Bold="false" ID="lblHeader" runat="server" Text="Add Client"></asp:Label>
                        </div>--%>
                            <div class="modal-body">
                                <%--row1--%>
                                <div class="row">
                                    <br />
                                    <div class="col-md-12">
                                        <asp:Label ForeColor="#787878" ID="lblMaritalStatus" runat="server" Text="Marital Status"></asp:Label><br />
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <asp:Label ID="lblMarr" runat="server" Font-Size="13px" Text="Married"></asp:Label><br />
                                        <asp:RadioButton ID="rdbtnMarried" ForeColor="#787878" onclick="customerSpouseinformation();" Width="45px" runat="server" GroupName="MaritalStatus" />
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <asp:Label ID="lbSing" runat="server" Font-Size="13px" Text="Single"></asp:Label><br />
                                        <asp:RadioButton ID="rdbtnSingle" ForeColor="#787878" onclick="customerSpouseinformation();" Width="40px" runat="server" GroupName="MaritalStatus" />
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <asp:Label ID="lblWid" runat="server" Font-Size="13px" Text="Widowed"></asp:Label><br />
                                        <asp:RadioButton ID="rdbtnWidowed" ForeColor="#787878" onclick="customerSpouseinformation();" Width="50px" runat="server" GroupName="MaritalStatus" />
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <asp:Label ID="lblDiv" runat="server" Font-Size="13px" Text="Divorced"></asp:Label><br />
                                        <asp:RadioButton ID="rdbtnDivorced" ForeColor="#787878" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" /><br />
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>

                                <%--Restrict app change--%>
                                 <div>
                                    <div class="row col-md-12 RestrictMarriedArea" id="RestrictMarriedAreaId" runat="server">
                                        <asp:Label runat="server" style="color:#787878;" ID="lblAnyClaim" Text="Have either you or your spouse already claimed your Social Security Benefits?"></asp:Label>
                                        <br />
                                        <asp:RadioButton runat="server" ForeColor="#787878" ID="rdbtnAnyClaimYes" GroupName="YesNo"  Text="Yes" onclick="ShowHideRestrictSection();" />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <asp:RadioButton runat="server" ForeColor="#787878" ID="rdbtnAnyClaimNo" GroupName="YesNo" Text="No" onclick="ShowHideRestrictSection();" />
                                        <br />
                                        <asp:Label ID="lblSelectYesNoError" CssClass="failureErrorNotification" ForeColor="Red" runat="server" ></asp:Label>

                                        <div id="fakedDiv" runat="server" class="row fakediv" style="height: 270px">
                                        </div>
                                    </div>
                                </div>
                                <%--Restrict app change--%>


                                <%--Row First Name Last Name--%>
                                <div class="DefaultSection displayNone" id="defaultSectionId" runat="server">
                                <div class="row">
                                    <div class="col-md-6 marginTop ">
                                        <asp:Label ForeColor="#787878" ID="lblFirstname" runat="server" Text="First Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control control" TabIndex="4" ID="txtFirstname" MaxLength="20" ValidationGroup="RegisterUserValidationGroup" runat="server"></asp:TextBox>
                                        <asp:TextBox CssClass="textEntry form-control" ID="passwordTextBox" Visible="false" MaxLength="20" runat="server"></asp:TextBox>
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtCustomerID" runat="server" Visible="false"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="reqFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Client First name is required." ToolTip="Client First name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Customer First name is required</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Client First name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Client First name</asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblFNameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div class="col-md-6 marginTop ">
                                        <asp:Label ForeColor="#787878" ID="lblLastname" runat="server" Text="Last Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control control " TabIndex="5" ID="txtLastName" MaxLength="20" ValidationGroup="RegisterUserValidationGroup" runat="server"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="reqLastName" runat="server" ControlToValidate="txtLastName" CssClass="failureErrorNotification" ErrorMessage="Client Last name is required." ToolTip="Client Last name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Customer First name is required</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regLastname" runat="server" ControlToValidate="txtLastName" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Client Last name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Client Last name</asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblLNameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                                <%--Row Email-Bdate--%>
                                <div class="row ">
                                    <div class="col-md-6 marginTop">
                                        <asp:Label ForeColor="#787878" ID="lblEmail" runat="server" Text="Email"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="6" ID="txtEmail" ValidationGroup="RegisterUserValidationGroup" MaxLength="50" AutoComplete="off" runat="server"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblEmailError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <%-- <div class="col-md-6 marginTop">
                                    <asp:Label ForeColor="#787878" ID="lblBirthdate" runat="server" Text="Birth Date"></asp:Label><br />
                                    <asp:TextBox CssClass="textEntry form-control calimg" TabIndex="7" MaxLength="10" ID="txtBirthDate" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqBirthdate" runat="server" ControlToValidate="txtBirthDate" CssClass="failureErrorNotification" ErrorMessage="Customer Date of birth is required." ToolTip="Customer Date of birth is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><BR/>*Customer Date of birth is required.</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regBirthDate" runat="server" ControlToValidate="txtBirthDate" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Customer Date Of Birth" ValidationExpression="^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><BR/>*Please enter valid Customer Date of Birth</asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtBirthDate" CssClass="failureErrorNotification" Type="Date" Display="Dynamic" Operator="LessThan" ValidationGroup="RegisterUserValidationGroup" Text="<BR/>*The Date of Birth can not be in future" />
                                </div>--%>

                                    <%--Row UserBenefit--%>
                                    <%--<div class="row">--%>
                                    <div class="col-md-6 marginTop">
                                        <asp:Label ForeColor="#787878" ID="lblUserBenefit" runat="server" Text="Your Full Retirement Age Benefit"></asp:Label><br />
                                        <asp:TextBox ID="txtUserBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="8" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <%-- <asp:RequiredFieldValidator ID="reqUserBenefit" runat="server" ControlToValidate="txtUserBenefit" CssClass="failureErrorNotification" ToolTip="Your Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regUserBenefit" runat="server" ControlToValidate="txtUserBenefit" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Your Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblUserBenefitError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <%--</div>--%>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 marginTop">
                                        <asp:Label ID="lblBirthDate" ForeColor="#787878" runat="server" Text="Birth Date :"></asp:Label></td>
                                    <br />
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-5 col-sm-5">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Month:"></asp:Label>
                                            <asp:DropDownList ID="ddlMonth_Your" runat="server" TabIndex="9" Class="Space" onchange="PopulateDays_Your()" />
                                            <br />
                                            <asp:Label ID="lblYourMonthError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblUserAgeError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Day:"></asp:Label>
                                            <asp:DropDownList ID="ddlDay_Your" runat="server" TabIndex="10" Class="Space" />
                                            <br />
                                            <asp:Label ID="lblYourDayError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Year:"></asp:Label>
                                            <asp:DropDownList ID="ddlYear_Your" runat="server" TabIndex="11" onchange="PopulateDays_Your()" />
                                            <br />
                                            <asp:Label ID="lblYourYearError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>

                                        </div>
                                        <%--<br />
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="* Required"
                                    ClientValidationFunction="Validate_Your" />--%>
                                        <br />
                                    </div>
                                </div>

                                <div class="row hideRow1">
                                    <div class="col-md-12 marginTop">
                                        <asp:Label ID="lblSpouseBirthdate" ForeColor="#787878" runat="server" Text="Spouse Birth Date :"></asp:Label></td>
                                            <br />
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-5 col-sm-5">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Month:"></asp:Label>
                                            <asp:DropDownList ID="ddlMonth_Spouse" TabIndex="12" runat="server" Class="Space" onchange="PopulateDays_Spouse()" />
                                            <br />
                                            <asp:Label ID="lblSpouseMonthError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                            <asp:Label ID="lblSpouseAgeError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Day:"></asp:Label>
                                            <asp:DropDownList ID="ddlDay_Spouse" TabIndex="13" runat="server" Class="Space" />
                                            <br />
                                            <asp:Label ID="lblSpouseDayError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <asp:Label runat="server" ForeColor="#787878" Text="Year:"></asp:Label>
                                            <asp:DropDownList ID="ddlYear_Spouse" TabIndex="15" runat="server" onchange="PopulateDays_Spouse()" />
                                            <br />
                                            <asp:Label ID="lblSpouseYearError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>

                                        <%--<br />
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="* Required" ClientValidationFunction="Validate_Your" />--%>
                                        <br />
                                    </div>
                                </div>


                                <div class="col-md-6 marginTop" style="display: none">
                                    <asp:Label ForeColor="#787878" ID="lblpassword" runat="server" Text="Password"></asp:Label><br />
                                    <asp:TextBox CssClass="textEntry form-control" MaxLength="20" Text="123465" ID="txtPassword" runat="server"></asp:TextBox>
                                </div>

                                <%--Row UserBenefit--%>
                                <div class="row">
                                    <%--<div class="col-md-6 marginTop25">
                                    <asp:Label ForeColor="#787878" ID="lblUserBenefit" runat="server" Text="Your Full Retirement Age Benefit"></asp:Label><br />
                                    <asp:TextBox ID="txtUserBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="8" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqUserBenefit" runat="server" ControlToValidate="txtUserBenefit" CssClass="failureErrorNotification" ToolTip="Your Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regUserBenefit" runat="server" ControlToValidate="txtUserBenefit" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Your Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                                </div>--%>
                                    <div class="col-md-6"></div>
                                </div>
                                <%--row3--%>
                                <div class="row " style="display: none">
                                    <div class="col-md-6 marginTop">
                                        <asp:Label ForeColor="#787878" ID="lblConfirmPassword" runat="server" Text="Confirm Password"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" MaxLength="20" Text="123465" ID="txtConfirmPassword" runat="server"></asp:TextBox>
                                        <img id="imgpass" class="img-responsive" runat="server" src="/images/confirmPassword.png" style="margin-left: 205px; margin-top: -26px; display: none;" border="0" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                                <%--row4--%>
                                <div>
                                    <div class="row">
                                        <div class="col-md-6 hideRow">
                                            <br />
                                            <asp:Label ForeColor="#787878" ID="lblSpouseFirstname" runat="server" Text="Spouse First Name"></asp:Label><br />
                                            <asp:TextBox CssClass="textEntry form-control" ID="txtSpouseFirstname" MaxLength="20" TabIndex="16" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                            <%-- <asp:RegularExpressionValidator ID="regSpouseFirstname" runat="server" ControlToValidate="txtSpouseFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid spouse's First name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid spouse's First name</asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="reqSpouseFirstname" runat="server" ControlToValidate="txtSpouseFirstname" CssClass="failureErrorNotification" ErrorMessage="Spouse's name is required." ToolTip="Spouse's Date of birth is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Spouse's name is required.</asp:RequiredFieldValidator>--%>
                                            <asp:Label ID="lblSpouseFirstnameError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <div class="col-md-6 hideRow2">
                                            <br />
                                            <asp:Label ForeColor="#787878" ID="lblSpouseBenefit" runat="server" Text="Spouse's monthly Full Retirement Age Benefit"></asp:Label><br />
                                            <asp:TextBox ID="txtSpouseBenefit" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="17" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="reqSpouseBenefit" runat="server" ControlToValidate="txtSpouseBenefit" CssClass="failureErrorNotification" ToolTip="Spouse's monthly Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regSpouseBenefit" runat="server" ControlToValidate="txtSpouseBenefit" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>--%>
                                            <asp:Label ID="lblSpouseBenefitError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                        <%--    <div class="col-md-6 hideRow1">
                                        <br />
                                        <asp:Label ForeColor="#787878" ID="lblSpouseBirthdate" runat="server" Text="Spouse Birth Date"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control calimg" ID="txtSpouseBirthdate" MaxLength="10" TabIndex="10" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqSpouseBirthdate" runat="server" ControlToValidate="txtSpouseBirthdate" CssClass="failureErrorNotification" ErrorMessage="<BR/>Spouse's Date of birth is required." ToolTip="Spouse's Date of birth is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><BR/>*Spouse's Date of birth is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regSpouseBirthdate" runat="server" ControlToValidate="txtSpouseBirthdate" CssClass="failureErrorNotification" ErrorMessage="<BR/>Please enter valid spouse's Date Of Birth" ValidationExpression="^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><BR/>*Please enter valid spouse's Date of Birth</asp:RegularExpressionValidator>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtSpouseBirthdate" Type="Date" Display="Dynamic" Operator="LessThan" ValidationGroup="RegisterUserValidationGroup" CssClass="failureErrorNotification" Text="<BR/>*The Date of Birth can not be in future" />
                                    </div>--%>
                                    </div>

                                </div>
                                <br />
                                    </div>
                                  <%--new restricted application section only for married--%>

                            <div class="RestrictSection displayNone">
                                <div class="row">
                                    <div class="col-md-6 hideRow">
                                        <br />
                                        <asp:RadioButton runat="server" style="color:#787878;" GroupName="claimgroup" ID="rdbtnuserclaim" onclick="ShowSpousalRestrictSection();" Text="I have already claimed Social Security Benefits" />
                                        <br />
                                        <asp:Label ID="lblSelectClaimingUser"  ForeColor="Red" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                    </div>
                                    <div class="col-md-6 hideRow">
                                        <br />
                                        <asp:RadioButton runat="server" style="color:#787878;" ID="rdbtnspouseclaim"  GroupName="claimgroup" onclick="ShowSpousalRestrictSection();" Text="My spouse already claimed Social Security Benefits" />
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="row" style="margin-top: 15px">
                                    <%--Husband Section--%>
                                    <div class="col-md-6">
                                        <div class="claimsection displayNone" id="UserClaimSection" runat="server">
                                            <%--User Claimed Age--%>
                                            <div class="hideRow claimuser displayNone" id="claimuser1" runat="server">
                                                <asp:Label  ID="lblClaimedAgeHusb" style="color:#787878;" runat="server" Text="Age when benefit claimed"></asp:Label><br />
                                                <asp:DropDownList runat="server" ID="ddlHusbClaimAge" TabIndex="9" CssClass="input-text textEntry form-control"></asp:DropDownList>
                                            </div>
                                            <%--User Claimed Benefit--%>
                                            <div class="hideRow claimuser displayNone" id="claimuser2" runat="server">
                                                <asp:Label  ID="lblHusbCliamBen" style="color:#787878;" runat="server" Text="Amount of claimed benefit"></asp:Label><br />
                                                <asp:TextBox runat="server" ID="txtHusbCliamBenefit" TabIndex="10" CssClass="input-text textEntry form-control"></asp:TextBox>
                                                <asp:Label ID="lblHusbClaimBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                            </div>

                                        </div>
                                        <%--User Current Benefit--%>
                                        <div class="currentbenefit displayNone" id="currentbenefitUser" runat="server">
                                            <asp:Label Visible="false"  ID="lblHusbCurBen" runat="server" Text="Amount of current benefit"></asp:Label><br />
                                            <asp:TextBox Visible="false" runat="server" ID="txtHusbCurrBen" TabIndex="11" CssClass="input-text textEntry form-control"></asp:TextBox>
                                            <asp:Label Visible="false" ID="lblHusbCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>

                                    <%--Wife Section--%>
                                    <div class="col-md-6">
                                        <div class="claimsection displayNone" id="SpouseClaimSection" runat="server">
                                            <%--Wife Claimed Age--%>
                                            <div class="hideRow claimspouse displayNone" id="claimspouse1" runat="server">
                                                <asp:Label  ID="lblClimedAgeWife" style="color:#787878;" runat="server" Text="Spouse's age when benefit claimed"></asp:Label><br />
                                                <asp:DropDownList runat="server" ID="ddlWifeClaimAge" TabIndex="12" CssClass="input-text textEntry form-control"></asp:DropDownList>
                                            </div>
                                            <%--Wife Claimed Benefit--%>
                                            <div class="hideRow claimspouse displayNone" id="claimspouse2" runat="server">
                                                <asp:Label  ID="lblWifeCliemBen" style="color:#787878;" runat="server" Text="Amount of spouse's claimed benefit"></asp:Label><br />
                                                <asp:TextBox runat="server" ID="txtWifeCliamBenefit" TabIndex="13" CssClass="input-text textEntry form-control"></asp:TextBox>
                                                <asp:Label ID="lblWifeClaimBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                            </div>

                                        </div>
                                        <%--Wife Current Benefit--%>
                                        <div class="currentbenefit displayNone" id="currentbenefitSpouse" runat="server">
                                            <asp:Label Visible="false"  ID="lblWifeCurBen" runat="server" Text="Amount of spouse's current benefit"></asp:Label><br />
                                            <asp:TextBox Visible="false" runat="server" ID="txtWifeCurrBen" TabIndex="14" CssClass="input-text textEntry form-control"></asp:TextBox>
                                            <asp:Label Visible="false" ID="lblWifeCurrBenError" CssClass="failureErrorNotification" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <%--new restricted application section only for married--%>


                                <div id="space" runat="server" class="spaceSingle displayNone">
                                    <br />
                                    <br />
                                    <br />
                                </div>
                                <%--row5--%>
                                <div class="row popupbottom">
                                    <div style="text-align: center">
                                        <asp:Button TabIndex="18" ID="btnSave" runat="server" Text="Save & View Report" OnClick="Save" OnClientClick="return Validate_UserData();" CssClass="button_login marginpopbottom" />&nbsp;&nbsp;
                                <asp:Button TabIndex="19" ID="btnCancel" OnClick="Cancel" runat="server" Text="Cancel" CssClass="button_login" />
                                    </div>
                                </div>
                                <div class="row popupbottom">
                                    <div class="col-md-12">
                                        <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="subscription" class="testmodalPopup text-center">
                            <div id="plans" class="plan-contentnew ">
                                <h2 class="center" style="color: brown; font-weight: bold;">To create a new Customer, please subscribe by paying $30!</h2>
                                <br />
                                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CloseWindow" OnClick="Close_Click" runat="server" CssClass="button_login" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlAddEdit" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:LinkButton ID="lnkFake1" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="MailSentPopUp" runat="server" BehaviorID="MsgPopUp" PopupControlID="pnlShowMsg" TargetControlID="lnkFake1" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                </div>
            </ContentTemplate>

            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvDetails" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" />
                <asp:AsyncPostBackTrigger ControlID="btnCancel" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>

    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var w = $(window).width();
        w = w - 200;
        if (w >= 540) {
            $('.divGrid').css('width', w);
        }


        function SetGridViewWidth()
        {
            var w = $(window).width();
            w = w - 200;
            if (w >= 540) {
                $('.divGrid').css('width', w);
            }
        }

        function SetTarget() {
            document.forms[0].target = "_blank";
        }

        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {

            customerSpouseinformation();

            //window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
                /* Check if id exists in current Page */
                if ($("[id$=txtBirthDate]") != null) {
                    $("[id$=txtBirthDate]").keypress(function (e) { e.preventDefault(); });
                }
                /* Check if id exists in current Page */
                if ($("[id$=txtSpouseBirthdate]") != null) {
                    $("[id$=txtSpouseBirthdate]").keypress(function (e) { e.preventDefault(); });
                }
            });

            $('#<%=btnSave.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
        });

        function PopulateDays_Your() {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            var y_Your = ddlYear_Your.options[ddlYear_Your.selectedIndex].value;
            var m_Your = ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0;
            if (ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0 && ddlYear_Your.options[ddlYear_Your.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Your.options[ddlYear_Your.selectedIndex].value, ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value - 1, 32).getDate();
                // ddlDay_Your.options.length = 0;
                AddOption_Your(ddlDay_Your, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Your(ddlDay_Your, i, i);
                }
            }
        }

        function AddOption_Your(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);

        }

        function Validate_Your(sender, args) {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            args.IsValid = (ddlDay_Your.selectedIndex != 0 && ddlMonth_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0)
        }


        function PopulateDays_Spouse() {

            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            var y_Spouse = ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value;
            var m_Spouse = ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0;
            if (ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0 && ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value, ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value - 1, 32).getDate();
                //ddlDay_Spouse.options.length = 0;
                AddOption_Spouse(ddlDay_Spouse, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Spouse(ddlDay_Spouse, i, i);
                }
            }
        }

        function AddOption_Spouse(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }

        function Validate_Spouse(sender, args) {
            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            args.IsValid = (ddlDay_Spouse.selectedIndex != 0 && ddlMonth_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0)
        }

        function HideModalPopupForMessage() {
            $find("MsgPopUp").hide();
            return false;
        }


        function customerSpouseinformation() {

            ResetFields();

            if ($("[id$=rdbtnMarried]").prop("checked")) {
                ResetRestrictSection();
                $(".fakediv").show();
                $(".RestrictMarriedArea").show();
                $(".DefaultSection").hide();
                $(".hideRow").show();
                $(".hideRow1").show();
                $(".spaceSingle").hide();
                $(".hideRow2").show();
                $("#<%=pnlAddEdit.ClientID %>").height("450px");
                $("[id$=lblSpouseBenefit]").text("Spouse's monthly Full Retirement Age Benefit");
                $("[id$=lblSpouseBirthdate]").text("Spouse's Birth Date");

                <%--var reqSpouseBenefit = document.getElementById("<%=reqSpouseBenefit.ClientID%>");
                var regSpouseBenefit = document.getElementById("<%=regSpouseBenefit.ClientID%>");
                ValidatorEnable(reqSpouseBenefit, true);
                ValidatorEnable(regSpouseBenefit, true);

                var reqspousename = document.getElementById("<%=reqSpouseFirstname.ClientID%>");
                var regspousename = document.getElementById("<%=regSpouseFirstname.ClientID%>");
                ValidatorEnable(reqspousename, true);
                ValidatorEnable(regspousename, true);--%>

            }
            else if ($("[id$=rdbtnDivorced]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                
                $(".RestrictMarriedArea").hide();
                $(".hideRow").hide();
                $(".hideRow1").show();
                $(".spaceSingle").hide();
                $(".hideRow2").show();
               <%-- $("#<%=pnlAddEdit.ClientID %>").height("496px");--%>
                $("[id$=lblSpouseBenefit]").text("Ex-Spouse's monthly Full Retirement Age Benefit");
                $("[id$=lblSpouseBirthdate]").text("Ex-Spouse's Birth Date");

                <%--var reqSpouseBenefit = document.getElementById("<%=reqSpouseBenefit.ClientID%>");
                var regSpouseBenefit = document.getElementById("<%=regSpouseBenefit.ClientID%>");

                ValidatorEnable(reqSpouseBenefit, true);
                ValidatorEnable(regSpouseBenefit, true);

                var reqspousename = document.getElementById("<%=reqSpouseFirstname.ClientID%>");
                var regspousename = document.getElementById("<%=regSpouseFirstname.ClientID%>");
                ValidatorEnable(reqspousename, false);
                ValidatorEnable(regspousename, false);--%>
            }
            else {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictMarriedArea").hide();
                $(".hideRow").hide();
                $(".hideRow1").hide();
                $(".spaceSingle").show();
                if ($("[id$=rdbtnWidowed]").prop("checked")) {
                    $(".hideRow2").show();
                    $(".spaceSingle").hide();
                    $("#<%=pnlAddEdit.ClientID %>").height("436px");
                    $("[id$=lblSpouseBenefit]").text("Deceased Spouse's monthly Full Retirement Age Benefit");
                    $("[id$=lblSpouseBirthdate]").text("Deceased Spouse's Birth Date");
                    <%--var reqSpouseBenefit = document.getElementById("<%=reqSpouseBenefit.ClientID%>");
                    var regSpouseBenefit = document.getElementById("<%=regSpouseBenefit.ClientID%>");

                    ValidatorEnable(reqSpouseBenefit, true);
                    ValidatorEnable(regSpouseBenefit, true);

                    var reqspousename = document.getElementById("<%=reqSpouseFirstname.ClientID%>");
                    var regspousename = document.getElementById("<%=regSpouseFirstname.ClientID%>");
                    ValidatorEnable(reqspousename, false);
                    ValidatorEnable(regspousename, false);--%>
                }
                else {
                    $(".hideRow2").hide();
                    $(".spaceSingle").hide();
                    $("#<%=pnlAddEdit.ClientID %>").height("346px");

                    <%-- var reqspousename = document.getElementById("<%=reqSpouseFirstname.ClientID%>");
                    var regspousename = document.getElementById("<%=regSpouseFirstname.ClientID%>");
                    ValidatorEnable(reqspousename, false);
                    ValidatorEnable(regspousename, false);

                    var reqSpouseBenefit = document.getElementById("<%=reqSpouseBenefit.ClientID%>");
                    var regSpouseBenefit = document.getElementById("<%=regSpouseBenefit.ClientID%>");
                    ValidatorEnable(reqSpouseBenefit, false);
                    ValidatorEnable(regSpouseBenefit, false);--%>


                }


               <%-- var reqEmail = document.getElementById("<%=reqEmail.ClientID%>");
                var regEmail = document.getElementById("<%=regEmail.ClientID%>");
                ValidatorEnable(reqEmail, true);
                ValidatorEnable(regEmail, true);

                var reqname = document.getElementById("<%=reqFirstname.ClientID%>");
                var regname = document.getElementById("<%=regFirstname.ClientID%>");
                ValidatorEnable(reqname, true);
                ValidatorEnable(regname, true);

                var reqLastname = document.getElementById("<%=reqLastName.ClientID%>");
                var regLastname = document.getElementById("<%=regLastname.ClientID%>");
                ValidatorEnable(reqLastname, true);
                ValidatorEnable(regLastname, true);--%>

            }
        }

        
        
        function ResetFields() {

            $("[id$=lblFNameError]").text('');
            $("[id$=lblLNameError]").text('');
            $("[id$=lblUserBenefitError]").text('');
            $("[id$=lblSpouseBenefitError]").text('');
            $("[id$=lblEmailError]").text('');
            $("[id$=lblSpouseFirstnameError]").text('');
            $("[id$=lblUserAgeError]").text('');
            $("[id$=lblSpouseAgeError]").text('');

            $("[id$=txtFirstname]").removeClass("error");
            $("[id$=txtLastName]").removeClass("error");
            $("[id$=txtUserBenefit]").removeClass("error");
            $("[id$=txtSpouseBenefit]").removeClass("error");
            $("[id$=txtEmail]").removeClass("error");
            $("[id$=txtSpouseFirstname]").removeClass("error");

            $("[id$=ddlMonth_Your]").removeClass("error");
            $("[id$=lblYourMonthError]").text('');
            $("[id$=ddlYear_Your]").removeClass("error");
            $("[id$=lblYourYearError]").text('');
            $("[id$=ddlDay_Your]").removeClass("error");
            $("[id$=lblYourDayError]").text('');
            $("[id$=ddlMonth_Spouse]").removeClass("error");
            $("[id$=lblSpouseMonthError]").text('');
            $("[id$=ddlYear_Spouse]").removeClass("error");
            $("[id$=lblSpouseYearError]").text('');
            $("[id$=ddlDay_Spouse]").removeClass("error");
            $("[id$=lblSpouseDayError]").text('');
        }

        function CalculateAge(DOB) {
            var userDob = new Date(DOB);
            var today = new Date();
            var age = Math.floor((today - userDob) / (365.25 * 24 * 60 * 60 * 1000));
            return age;
        }


        function Validate_UserData() {


            ResetFields();

            /* Validate Form Fields */
            /* Initialize Variable */
            var isValidate = true;
            //var alphaNumericRegex = /[^0-9a-z]/i;
            var alphaNumericRegex = /^[A-Za-z]+$/;
            //var numericRegex = '^[0-9]*$';
            var numericRegex = /^\d+$/;
            var errorMessage = '';

            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");

            /* Check if birth date field exists in current form or not */
            if (ddlMonth_Your.selectedIndex == 0) {
                isValidate = false;
                /* Add error css class */
                $("[id$=ddlMonth_Your]").addClass("error");
                $("[id$=lblYourMonthError]").text("* Month Required");
            }
            if (ddlYear_Your.selectedIndex == 0) {
                isValidate = false;
                /* Add error css class */
                $("[id$=ddlYear_Your]").addClass("error");
                $("[id$=lblYourYearError]").text("* Year Required");
            }

            if (ddlDay_Your.selectedIndex == 0) {
                isValidate = false;
                /* Add error css class */
                $("[id$=ddlDay_Your]").addClass("error");
                $("[id$=lblYourDayError]").text("* Day Required");
            }

            //Check radio button is selected or not
            if ($("[id$=rdbtnMarried]").prop("checked")) {
                if (!$("[id$=rdbtnAnyClaimYes]").prop("checked") && !$("[id$=rdbtnAnyClaimNo]").prop("checked")) {
                    $("[id$=lblSelectYesNoError]").text("* Please select an option");
                    isValidate = false;
                    return isValidate;
                }
                else
                    $("[id$=lblSelectYesNoError]").text("");

                if ($("[id$=rdbtnAnyClaimYes]").prop("checked")) {
                    if (!$("[id$=rdbtnuserclaim]").prop("checked") && !$("[id$=rdbtnspouseclaim]").prop("checked")) {
                        $("[id$=lblSelectClaimingUser]").text("* Please select claiming person.");
                        isValidate = false;
                    }
                    else
                        $("[id$=lblSelectClaimingUser]").text("");
                }
            }

            //Check User age is less than 70
            if (ddlMonth_Your.selectedIndex != 0 && ddlDay_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0) {

                var day = $("#ctl00_MainContent_ddlDay_Your").val();
                var year = $("#ctl00_MainContent_ddlYear_Your").val();
                var month = $("#ctl00_MainContent_ddlMonth_Your").val();
                var age = calculate_age(month, day, year);
                if (age > 70) {
                    isValidate = false;
                    $("[id$=lblUserAgeError]").text("*Birth date should not exceed 70 years.");
                    $("[id$=ddlDay_Your]").addClass("error");
                    $("[id$=ddlYear_Your]").addClass("error");
                    $("[id$=ddlMonth_Your]").addClass("error");
                }
                else if (age < 18) {
                    isValidate = false;
                    $("[id$=lblUserAgeError]").text("*Birth date should not less than 18 years");
                    $("[id$=ddlDay_Your]").addClass("error");
                    $("[id$=ddlYear_Your]").addClass("error");
                    $("[id$=ddlMonth_Your]").addClass("error");
                }
            }


            /* Check if field exists in current form or not */
            if ($("[id$=txtFirstname]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtFirstname]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtFirstname]").addClass("error");
                    $("[id$=lblFNameError]").text("* First Name Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!alphaNumericRegex.test($("[id$=txtFirstname]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtFirstname]").addClass("error");
                        $("[id$=lblFNameError]").text("* First Name should be Character");

                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtFirstname]").removeClass("error");

                    }
                }
            }

            /* Check if field exists in current form or not */
            if ($("[id$=txtLastName]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtLastName]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtLastName]").addClass("error");
                    $("[id$=lblLNameError]").text("* Last Name Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!alphaNumericRegex.test($("[id$=txtLastName]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtLastName]").addClass("error");
                        $("[id$=lblLNameError]").text("* Last Name should be Character");

                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtLastName]").removeClass("error");

                    }
                }
            }

            /* Check if field exists in current form or not */
            if ($("[id$=txtUserBenefit]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtUserBenefit]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtUserBenefit]").addClass("error");
                    $("[id$=lblUserBenefitError]").text("* User Benefit Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!numericRegex.test($("[id$=txtUserBenefit]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtUserBenefit]").addClass("error");
                        $("[id$=lblUserBenefitError]").text("* User Benefit should be Numeric");

                    }
                        // else if ($("[id$=txtUserBenefit]").val() == '0') {
                        //     isValidate = false;
                        //    /* Add error css class */
                        //     $("[id$=txtUserBenefit]").addClass("error");
                        //    $("[id$=lblUserBenefitError]").text("* User Benefit should not be Zero");
                        // }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtUserBenefit]").removeClass("error");

                    }
                }
            }

            //Vheck validation of spousal benefits
            if ($("[id$=rdbtnMarried]").prop("checked") || $("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnDivorced]").prop("checked")) {

                if ($("[id$=rdbtnMarried]").prop("checked") || $("[id$=rdbtnDivorced]").prop("checked")) {
                    var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
                    var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
                    var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");

                    if (ddlMonth_Spouse.selectedIndex == 0) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=ddlMonth_Spouse]").addClass("error");
                        $("[id$=lblSpouseMonthError]").text("* Month Required");
                    }
                    if (ddlYear_Spouse.selectedIndex == 0) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=ddlYear_Spouse]").addClass("error");
                        $("[id$=lblSpouseYearError]").text("* Year Required");
                    }

                    if (ddlDay_Spouse.selectedIndex == 0) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=ddlDay_Spouse]").addClass("error");
                        $("[id$=lblSpouseDayError]").text("* Day Required");
                    }


                    //Check spouse age is less than 70
                    if (ddlMonth_Spouse.selectedIndex != 0 && ddlDay_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0) {
                        var day = $("#ctl00_MainContent_ddlDay_Spouse").val();
                        var year = $("#ctl00_MainContent_ddlYear_Spouse").val();
                        var month = $("#ctl00_MainContent_ddlMonth_Spouse").val();
                        var age = calculate_age(month, day, year);
                        if (age > 70) {
                            isValidate = false;
                            $("[id$=ddlDay_Spouse]").addClass("error");
                            $("[id$=ddlYear_Spouse]").addClass("error");
                            $("[id$=ddlMonth_Spouse]").addClass("error");
                            $("[id$=lblSpouseAgeError]").text("*Birth date should not exceed 70 years.");
                        }
                        else if (age < 18) {
                            isValidate = false;
                            $("[id$=ddlDay_Spouse]").addClass("error");
                            $("[id$=ddlYear_Spouse]").addClass("error");
                            $("[id$=ddlMonth_Spouse]").addClass("error");
                            $("[id$=lblSpouseAgeError]").text("*Birth date should not less than 18 years.");
                        }
                    }


                }


                /* Check if field exists in current form or not */
                if ($("[id$=txtSpouseBenefit]").val() != null) {
                    /* Validate First Name Field */
                    if ($("[id$=txtSpouseBenefit]").val() == '') {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtSpouseBenefit]").addClass("error");
                        $("[id$=lblSpouseBenefitError]").text("* Spouse Benefit Required");
                    }
                    else {
                        /* Check AlphaNumeric String */
                        if (!numericRegex.test($("[id$=txtSpouseBenefit]").val())) {
                            isValidate = false;
                            /* Add error css class */
                            $("[id$=txtSpouseBenefit]").addClass("error");
                            $("[id$=lblSpouseBenefitError]").text("* Spouse Benefit should be Numeric");

                        }
                            //else if ($("[id$=txtSpouseBenefit]").val() == '0') {
                            //    isValidate = false;
                            //    /* Add error css class */
                            //    $("[id$=txtSpouseBenefit]").addClass("error");
                            //    $("[id$=lblSpouseBenefitError]").text("* Spouse Benefit should not be zero");
                            //}
                        else {
                            /* Remove Css Class */
                            $("[id$=txtSpouseBenefit]").removeClass("error");

                        }
                    }
                }
            }

            /* Check if field exists in current form or not */
            if ($("[id$=txtEmail]").val() != null) {
                /* Validate Email Field */
                if ($("[id$=txtEmail]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtEmail]").addClass("error");
                    $("[id$=lblEmailError]").text("* Email Address Required");
                }
                else {
                    // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
                    /* Validation Email Address Format */
                    if (!emailfilter1.test($("[id$=txtEmail]").val())) {
                        $("[id$=lblEmailError]").text("* Invalid Email Address");
                        /* Add error css class */
                        $("[id$=txtEmail]").addClass("error");
                        isValidate = false;
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtEmail]").removeClass("error");
                    }
                }
            }


            /* Check if field exists in current form or not */
            if ($("[id$=txtSpouseFirstname]").val() != null) {
                if ($("[id$=rdbtnMarried]").prop("checked")) {
                    /* Validate First Name Field */
                    if ($("[id$=txtSpouseFirstname]").val() == '') {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtSpouseFirstname]").addClass("error");
                        $("[id$=lblSpouseFirstnameError]").text("* Spouse First Name Required");
                    }
                    else {
                        /* Check AlphaNumeric String */
                        if (!alphaNumericRegex.test($("[id$=txtSpouseFirstname]").val())) {
                            isValidate = false;
                            /* Add error css class */
                            $("[id$=txtSpouseFirstname]").addClass("error");
                            $("[id$=lblSpouseFirstnameError]").text("* Spouse first name should be Character");
                        }
                        else {
                            /* Remove Css Class */
                            $("[id$=txtSpouseFirstname]").removeClass("error");
                        }
                    }
                }
                else {
                    $("[id$=txtSpouseFirstname]").removeClass("error");
                }
            }

            return isValidate;

        }


        function ShowHideRestrictSection() {
            $(".DefaultSection").show();
            if ($("[id$=rdbtnAnyClaimYes]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "block");
                $(".currentbenefit").css("display", "none");
                $("[id$=lblSelectYesNoError]").text("");

            }
            else if ($("[id$=rdbtnAnyClaimNo]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "none");
                $(".claimsection").css("display", "none");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                $("[id$=lblWifeCurBen]").text("Amount of spouse's monthly full retirement age benefit");
                $("[id$=lblSelectYesNoError]").text("");
            }

            $("[id$=rdbtnuserclaim]").prop("checked", false);
            $("[id$=rdbtnspouseclaim]").prop("checked", false);

        }

        function ShowSpousalRestrictSection() {
            
            $(".claimsection").css("display", "block");
            if ($("[id$=rdbtnuserclaim]").prop("checked")) {
                $(".claimuser").css("display", "block");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of current benefit");
                $("[id$=lblWifeCurBen]").text("Amount of Spouse's monthly full retirement age benefit");
                $("[id$=lblSelectClaimingUser]").text("");
            }
            else
                $(".claimuser").css("display", "none");

            if ($("[id$=rdbtnspouseclaim]").prop("checked")) {
                $(".claimspouse").css("display", "block");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                $("[id$=lblWifeCurBen]").text("Amount of spouse's current benefit");
                $("[id$=lblSelectClaimingUser]").text("");
            }
            else
                $(".claimspouse").css("display", "none");


        }


        function ShowHideRestrictSectionEdit() {
            $(".DefaultSection").show();
            if ($("[id$=rdbtnAnyClaimYes]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "block");
                $(".currentbenefit").css("display", "none");
                $("[id$=lblSelectYesNoError]").text("");

            }
            else if ($("[id$=rdbtnAnyClaimNo]").prop("checked")) {
                $(".DefaultSection").show();
                $(".fakediv").hide();
                $(".RestrictSection").css("display", "none");
                $(".claimsection").css("display", "none");
                $(".currentbenefit").css("display", "block");
                $("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                $("[id$=lblWifeCurBen]").text("Amount of spouse's monthly full retirement age benefit");
                $("[id$=lblSelectYesNoError]").text("");
            }
        }


        function ResetRestrictSection() {
            $("[id$=rdbtnuserclaim]").prop("checked", false);
            $("[id$=rdbtnspouseclaim]").prop("checked", false);
            $("[id$=rdbtnAnyClaimYes]").prop("checked", false);
            $("[id$=rdbtnAnyClaimNo]").prop("checked", false);

            $(".RestrictSection").css("display", "none");
            $(".currentbenefit").css("display", "none");
            $(".claimsection").css("display", "none");
            
            ShowHideRestrictSection();
        }


        //function calculate_age(birth_month, birth_day, birth_year) {
        //    var today_date = new Date();
        //    var today_year = today_date.getFullYear();
        //    var today_month = today_date.getMonth();
        //    today_day = today_date.getDate();
        //    age = today_year - birth_year;

        //    if (today_month < (birth_month - 1)) {
        //        age--;
        //    }
        //    if (((birth_month - 1) == today_month) && (today_day < birth_day)) {
        //        age--;
        //    }
        //    return age;
        //}



        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>

