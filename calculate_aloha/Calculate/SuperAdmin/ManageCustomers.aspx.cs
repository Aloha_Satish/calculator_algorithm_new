﻿using Calculate.Objects;
using Calculate.RestrictAppIncome;
using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class ManageCustomers : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        string sortExpression = Constants.CustomerLastName;
        //public  string sortExpression { get; set; }


        private int Day_Your
        {
            get
            {
                if (Request.Form[ddlDay_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Your();
                ddlDay_Your.ClearSelection();
                ddlDay_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Your
        {
            get
            {
                return int.Parse(ddlMonth_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Your();
                ddlMonth_Your.ClearSelection();
                ddlMonth_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Your
        {
            get
            {
                return int.Parse(ddlYear_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Your();
                ddlYear_Your.ClearSelection();
                ddlYear_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Your
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Your + "/" + this.Day_Your + "/" + this.Year_Your);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Your = value.Year;
                    this.Month_Your = value.Month;
                    this.Day_Your = value.Day;
                }
            }
        }

        private void PopulateDay_Your()
        {
            ddlDay_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Your.Items.Add(lt);
            //int days = DateTime.DaysInMonth(this.Year_Your, this.Month_Your);
            for (int i = 1; i <= 31; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Your.Items.Add(lt);
            }
            ddlDay_Your.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Your()
        {
            ddlMonth_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Your.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Your.Items.Add(lt);
            }
            ddlMonth_Your.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Your()
        {
            ddlYear_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Your.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Your.Items.Add(lt);
            }
            ddlYear_Your.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        private int Day_Spouse
        {
            get
            {
                if (Request.Form[ddlDay_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Spouse();
                ddlDay_Spouse.ClearSelection();
                ddlDay_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Spouse
        {
            get
            {
                return int.Parse(ddlMonth_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Spouse();
                ddlMonth_Spouse.ClearSelection();
                ddlMonth_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Spouse
        {
            get
            {
                return int.Parse(ddlYear_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Spouse();
                ddlYear_Spouse.ClearSelection();
                ddlYear_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Spouse
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Spouse + "/" + this.Day_Spouse + "/" + this.Year_Spouse);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Spouse = value.Year;
                    this.Month_Spouse = value.Month;
                    this.Day_Spouse = value.Day;
                }
            }
        }

        private void PopulateDay_Spouse()
        {
            ddlDay_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Spouse.Items.Add(lt);
            //int days = DateTime.DaysInMonth(this.Year_Spouse, this.Month_Spouse);
            for (int i = 1; i <= 31; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Spouse.Items.Add(lt);
            }
            ddlDay_Spouse.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Spouse()
        {
            ddlMonth_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Spouse.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Spouse.Items.Add(lt);
            }
            ddlMonth_Spouse.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Spouse()
        {
            ddlYear_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Spouse.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Spouse.Items.Add(lt);
            }
            ddlYear_Spouse.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        #endregion Variables

        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //CompareValidator1.ValueToCompare = DateTime.Now.ToString(Constants.DateFormat);
                //CompareValidator2.ValueToCompare = DateTime.Now.ToString(Constants.DateFormat);
                ValidateSession();
                if (!IsPostBack)
                {
                    LoadClaimedAge();

                    //CommonVariables.sortExpression = Constants.CustomerFirstName;

                    lblErrorMessage.Visible = false;
                    DataTable dtAdvisor;
                    Advisor objAdvisor = new Advisor();
                    /* Show Advisor Name based on query string or session */
                    if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                    {
                        dtAdvisor = objAdvisor.getAdvisorDetailsByID(Request.QueryString[Constants.QueryStringAdvisorAdminID]);
                        Session[Constants.TempAdvisorID] = Request.QueryString[Constants.QueryStringAdvisorAdminID];
                    }
                    else
                        dtAdvisor = objAdvisor.getAdvisorDetailsByID(Session[Constants.SessionAdminId].ToString());
                    if (dtAdvisor.Rows.Count > 0)
                    {
                        if (dtAdvisor.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                        {
                            if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                            {
                                linkAdvisor.Text = Constants.RedirectBackToAdvisor + dtAdvisor.Rows[Constants.zero][Constants.QueryStringInstitutionAdminID].ToString();
                                if (dtAdvisor.Rows.Count > 0)
                                    lblDisplayName.Text = dtAdvisor.Rows[Constants.zero][Constants.CustomerFirstName].ToString() + " " + dtAdvisor.Rows[0][Constants.CustomerLastName].ToString();
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ShowOtherparams, "manageBreadscrumbCustomer();", true);
                                if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.showIntitution, "displayManageInstitutionBreadscrumbLink();", true);
                            }
                            this.BindData();
                            int Count = gvDetails.Columns.Count;
                            if (Session[Constants.UserRole].ToString().Equals(Constants.SuperAdmin))
                                gvDetails.Columns[Count - 1].Visible = false;
                            Page.MaintainScrollPositionOnPostBack = true;
                            if (Request.QueryString[Constants.id] != null)
                            {
                                lblErrorMessage.Visible = true;
                                lblErrorMessage.Text = Constants.CustomerAddedText;
                                lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                                Customers objCustomer = new Customers();
                                objCustomer.ActivateCustomer(Request.QueryString[Constants.reference].ToString());
                                this.BindData();
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                            }
                            if (Globals.Instance.BoolEmailReport)
                            {
                                //lblErrorMessage.Visible = true;
                                //lblErrorMessage.Text = Constants.MailSentSuccessfully +Session[""];
                                //lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                                //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);

                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Mail Sent Successfully to "+CommonVariables.strUserName+"')", true);
                                if (Globals.Instance.BoolIsMailSent)
                                    lblMessage.Text = "Mail sent successfully to " + CommonVariables.strUserName;
                                else
                                    lblMessage.Text = "Unable to send mail to " + CommonVariables.strUserName;
                                Globals.Instance.BoolEmailReport = false;
                                Globals.Instance.BoolIsMailSent = false;
                                MailSentPopUp.Show();
                            }
                        }
                        else
                        {
                            Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                        }
                    }

                }
                /* Initialize ScriptManager for calling Javascript Function */
                ScriptManager sm = ScriptManager.GetCurrent(Page);
                if (sm.IsInAsyncPostBack)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptDatePicker, true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptLoadDatePicker, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Filter Records based on Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dateFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reset(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                //nameSelection.SelectedIndex = 0;
                FilterGridViewData();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Append Images while binding data into GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if gridview row event matched with data control row */
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /* Get instance of selected column */
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(Constants.imgStatus);
                    Label EmailID = (Label)e.Row.FindControl(Constants.lblEmail);
                    Label CreatedOn = (Label)e.Row.FindControl(Constants.lblCreatedDate);
                    Label lblCustomerID = (Label)e.Row.FindControl(Constants.lblCustomerID);
                    CreatedOn.Text = string.Format(Constants.stringDateFormat, Convert.ToDateTime(CreatedOn.Text)).Replace("-", "/");
                    /* Create object of User */
                    Users objUser = new Users();
                    /* Get Active Flag for particular Email ID from User Login Table */
                    DataTable dtUserDetails = objUser.getUserDetails(EmailID.Text);
                    /* If Row count greater than 0,then we have data */
                    if (dtUserDetails.Rows.Count > Constants.zero)
                    {
                        /* Assign value into image alternate text and images into image URL */
                        imgButton.AlternateText = dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag].ToString();
                        if (Convert.ToString(dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag]).Equals(Constants.FlagYes))
                        {
                            imgButton.ImageUrl = Constants.ActivateImage;
                            imgButton.Attributes.Add(Constants.title, Constants.Suspend);
                        }
                        else
                        {
                            imgButton.ImageUrl = Constants.DeactivateImage;
                            imgButton.Attributes.Add(Constants.title, Constants.Activate);
                        }
                    }
                }


                if (e.Row.RowType == DataControlRowType.Header)
                {
                    foreach (TableCell tc in e.Row.Cells)
                    {
                        if (tc.HasControls())
                        {
                            LinkButton lb = (LinkButton)tc.Controls[0];
                            if (lb != null)
                            {
                                Image icon = new Image();
                                icon.ImageUrl = "~/Images/go_" + (GridViewSortDirection == SortDirection.Ascending ? "asc" : "desc") + ".png";
                                if (sortExpression == lb.CommandArgument)
                                {
                                    tc.Controls.Add(new LiteralControl(" "));
                                    tc.Controls.Add(icon);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Activate/Deactivate Institution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Status(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Update flag into UI and table */
                SiteNew.updateActiveFlag(sender, gvDetails);
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = Constants.CustomerUpdatedSucesfuly;
                lblErrorMessage.ForeColor = System.Drawing.Color.Green;

                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on name selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nameSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search text entered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Redirect to previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void backPageInstitution(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                Response.Redirect(Constants.RedirectBackToInstitution, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// We can edit Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editGridViewDetails(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    //Fill Up the drop downs for User and spouse
                    this.PopulateYear_Your();
                    this.PopulateMonth_Your();
                    this.PopulateDay_Your();

                    this.PopulateYear_Spouse();
                    this.PopulateMonth_Spouse();
                    this.PopulateDay_Spouse();

                    char[] delimiters = new char[] { '/', '-' };
                    //lblHeader.Text = Constants.LblEditHeaderCustomer;
                    txtFirstname.Focus();
                    clsFlag.Flag = false;
                    #region Fill grid data into pop up form
                    /* Get content from gridview and assign to runtime created label */
                    Label CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                    Label Firstname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblFirstname);
                    Label Lastname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLastname);
                    Label Email = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblEmail);
                    Label Password = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblPassword);
                    Label Birthdate = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblBirthdate);
                    Label SpouseBirthdate = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerSpouselblBirthdate);
                    Label SpouseLastname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerSpouselblLastName);
                    Label SpouseFirstName = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerSpouselblFirstName);
                    Label Marital = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblMaritialStatus);
                    Label WifesRetAgeBenefit = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblWifesRetAgeBenefit);
                    Label HusbandsRetAgeBenefit = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblHusbandsRetAgeBenefit);

                    Label ClaimedPerson = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblClaimedPerson);
                    Label ClaimedAge = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblClaimedAge);
                    Label ClaimedBenefit = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.CustomerlblClaimedBenefit);
                    /*take backup of email for to verify user mail id*/
                    ViewState[Constants.UserEmail] = Email.Text;
                    /* Assign label data into textbox which are shown up in blockui popup */
                    txtEmail.Text = Email.Text;
                    //txtEmail.Enabled = false;
                    txtLastName.Text = Lastname.Text;
                    txtFirstname.Text = Firstname.Text;
                    txtPassword.Text = Encryption.Decrypt(Password.Text);
                    txtPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                    txtConfirmPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                    txtConfirmPassword.Text = Encryption.Decrypt(Password.Text);
                    txtCustomerID.Text = CustomerID.Text;
                    txtSpouseFirstname.Text = SpouseFirstName.Text;
                    txtUserBenefit.Text = HusbandsRetAgeBenefit.Text;
                    //txtSpouseLastname.Text = SpouseLastname.Text;
                    if (Marital.Text.Equals(Constants.Single))
                        txtSpouseBenefit.Text = string.Empty;
                    else
                        txtSpouseBenefit.Text = WifesRetAgeBenefit.Text;

                    //Divide User Birthdate into day, month, year and assign it to drop down lists
                    string strHusbandBirth = Convert.ToDateTime(Birthdate.Text).ToShortDateString();
                    string[] HusbandBirthArray = strHusbandBirth.Split(delimiters, 3);
                    ddlYear_Your.Text = HusbandBirthArray[2];
                    this.ddlMonth_Your.SelectedValue = HusbandBirthArray[0];
                    ddlDay_Your.SelectedValue = HusbandBirthArray[1];


                    string strSpouseBirth = string.Empty;


                    //txtBirthDate.Text = string.Format(Constants.stringDateFormat, Convert.ToDateTime(Birthdate.Text)).Replace("-", "/");
                    if (SpouseBirthdate.Text.Equals(Constants.DefaultDate))
                    {
                        //txtSpouseBirthdate.Text = String.Empty;
                        strSpouseBirth = string.Empty;
                    }
                    else
                    {
                        //txtSpouseBirthdate.Text = string.Format(Constants.stringDateFormat, Convert.ToDateTime(SpouseBirthdate.Text)).Replace("-", "/");
                        strSpouseBirth = Convert.ToDateTime(SpouseBirthdate.Text).ToShortDateString();
                        string[] SpouseBirthArray = strSpouseBirth.Split(delimiters, 3);
                        ddlYear_Spouse.SelectedValue = SpouseBirthArray[2];
                        ddlMonth_Spouse.SelectedValue = SpouseBirthArray[0];
                        ddlDay_Spouse.SelectedValue = SpouseBirthArray[1];
                    }
                    rdbtnMarried.Checked = false;
                    rdbtnDivorced.Checked = false;
                    rdbtnSingle.Checked = false;
                    rdbtnWidowed.Checked = false;
                    if (string.IsNullOrEmpty(Marital.Text))
                        rdbtnSingle.Checked = true;
                    else
                    {
                        #region Enable Disable the  Validators and select the Maritial Status
                        if (Marital.Text == Constants.Married)
                        {
                            rdbtnMarried.Checked = true;
                            //reqFirstname.Enabled = true;
                            //regFirstname.Enabled = true;
                            // pnlAddEdit.Height = 537;
                            RestrictMarriedAreaId.Style.Add("display", "block");
                            defaultSectionId.Style.Add("display", "none");
                            fakedDiv.Style.Add("display", "none");
                            
                            FillRestrictedControls(ClaimedPerson.Text, ClaimedAge.Text, ClaimedBenefit.Text);
                            //ScriptManager.RegisterStartupScript(this, typeof(string), "customerSpouseinformation", "customerSpouseinformation();", true);
                            //ScriptManager.RegisterStartupScript(this, typeof(string), "ShowHideRestrictSectionEdit", "ShowHideRestrictSectionEdit();", true);
                            //ScriptManager.RegisterStartupScript(this, typeof(string), "ShowSpousalRestrictSection", "ShowSpousalRestrictSection();", true);

                            

                        }
                        if (Marital.Text == Constants.Divorced)
                        {
                            rdbtnDivorced.Checked = true;
                            RestrictMarriedAreaId.Style.Add("display", "none");
                            defaultSectionId.Style.Add("display", "block");
                            //reqFirstname.Enabled = false;
                            //regFirstname.Enabled = false;
                            //pnlAddEdit.Height = 496;
                        }
                        if (Marital.Text == Constants.Single)
                        {
                            rdbtnSingle.Checked = true;
                            RestrictMarriedAreaId.Style.Add("display", "none");
                            defaultSectionId.Style.Add("display", "block");

                            //reqFirstname.Enabled = false;
                            //regFirstname.Enabled = false;
                            //pnlAddEdit.Height = 346;

                            space.Style.Add("Display", "block");
                        }
                        if (Marital.Text == Constants.Widowed)
                        {
                            rdbtnWidowed.Checked = true;
                            RestrictMarriedAreaId.Style.Add("display", "none");
                            defaultSectionId.Style.Add("display", "block");
                            //reqFirstname.Enabled = false;
                            //regFirstname.Enabled = false;
                            //pnlAddEdit.Height = 436;
                            space.Style.Add("Display", "block");
                        }
                        #endregion Enable Disable the  Validators and select the Maritial Status
                    }
                    #endregion

                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, Constants.ClinetScriptSpouseInformation, true);
                    /* Show Popup with Advisor Fields data */
                    popup.Show();
                    /* Display Panel */
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(string), "addoverlay", "addoverlay();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        private void FillRestrictedControls(string strClaimedPerson, string strClaimedAge, string strClaimedBenefit)
        {
            try
            {
                if (!strClaimedPerson.Equals("NONE"))
                {
                    if (!strClaimedPerson.Equals(""))
                    {
                        rdbtnAnyClaimYes.Checked = true;

                        if (strClaimedPerson.Equals("HUSBAND"))
                        {
                            rdbtnuserclaim.Checked = true;
                            ddlHusbClaimAge.Text = strClaimedAge;
                            txtHusbCliamBenefit.Text = strClaimedBenefit;


                            //claimuser1.Style.Add("display", "block");
                            //claimuser2.Style.Add("display", "block");

                            //currentbenefitUser.Style.Add("display", "block");

                            UserClaimSection.Style.Add("display", "block");

                            lblHusbCurBen.Text = "Amount of current benefit";
                            lblWifeCurBen.Text = "Amount of Spouse's monthly full retirement age benefit";
                            lblSelectClaimingUser.Text = "";

                            //             $(".claimuser").css("display", "block");
                            //$(".currentbenefit").css("display", "block");
                            //$("[id$=lblHusbCurBen]").text("Amount of current benefit");
                            //$("[id$=lblWifeCurBen]").text("Amount of Spouse's monthly full retirement age benefit");
                            //$("[id$=lblSelectClaimingUser]").text("");


                            //ScriptManager.RegisterStartupScript(this, typeof(string), "ShowSpousalRestrictSection", "ShowSpousalRestrictSection();", true);
                        }
                        else
                        {
                            rdbtnspouseclaim.Checked = true;


                            //claimspouse1.Style.Add("display", "block");
                            //claimspouse2.Style.Add("display", "block");
                            //currentbenefitSpouse.Style.Add("display", "block");
                            SpouseClaimSection.Style.Add("display", "block");
                            


                            lblHusbCurBen.Text = "Amount of your full retirement age benefit";
                            lblWifeCurBen.Text = "Amount of spouse's current benefit";
                            lblSelectClaimingUser.Text = "";
                            //            $(".claimspouse").css("display", "block");
                            //$(".currentbenefit").css("display", "block");
                            //$("[id$=lblHusbCurBen]").text("Amount of your full retirement age benefit");
                            //$("[id$=lblWifeCurBen]").text("Amount of spouse's current benefit");
                            //$("[id$=lblSelectClaimingUser]").text("");


                            ddlWifeClaimAge.Text = strClaimedAge;
                            txtWifeCliamBenefit.Text = strClaimedBenefit;
                            //ScriptManager.RegisterStartupScript(this, typeof(string), "ShowSpousalRestrictSection", "ShowSpousalRestrictSection();", true);
                        }

                    }
                    else
                        rdbtnAnyClaimNo.Checked = true;
                }
                else
                {
                    rdbtnAnyClaimNo.Checked = true;
                }
                ScriptManager.RegisterStartupScript(this, typeof(string), "ShowHideRestrictSection", "ShowHideRestrictSectionEdit();", true);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Add new Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCustomer(object sender, EventArgs e)
        {
            try
            {
                #region Clear fields data
                lblErrorMessage.Visible = false;

                fakedDiv.Style.Add("display", "block");
                rdbtnAnyClaimYes.Checked = false;
                rdbtnAnyClaimNo.Checked = false;

                clsFlag.Flag = true;
                //lblHeader.Text = Constants.LblAddHeaderCustomer;
                txtEmail.Text = String.Empty;
                txtFirstname.Text = String.Empty;
                txtLastName.Text = String.Empty;
                txtPassword.Text = String.Empty;
                txtConfirmPassword.Text = String.Empty;
                rdbtnDivorced.Attributes.Add(Constants.HtmlChecked, String.Empty);
                rdbtnMarried.Attributes.Add(Constants.HtmlChecked, String.Empty);
                rdbtnSingle.Attributes.Add(Constants.HtmlChecked, String.Empty);
                rdbtnWidowed.Attributes.Add(Constants.HtmlChecked, String.Empty);
                txtPassword.Text = String.Empty;
                txtConfirmPassword.Text = String.Empty;
                //txtSpouseBirthdate.Text = String.Empty;
                txtSpouseFirstname.Text = String.Empty;
                txtSpouseBenefit.Text = string.Empty;
                txtUserBenefit.Text = string.Empty;
                //txtSpouseLastname.Text = String.Empty;
                //txtBirthDate.Text = String.Empty;
                rdbtnMarried.Checked = true;
                rdbtnDivorced.Checked = false;
                rdbtnSingle.Checked = false;
                rdbtnWidowed.Checked = false;
                txtEmail.Enabled = true;

                //Fill Up the drop downs for User and spouse
                this.PopulateYear_Your();
                this.PopulateMonth_Your();
                this.PopulateDay_Your();

                this.PopulateYear_Spouse();
                this.PopulateMonth_Spouse();
                this.PopulateDay_Spouse();

                ddlDay_Your.SelectedIndex = 0;
                ddlMonth_Your.SelectedIndex = 0;
                ddlYear_Your.SelectedIndex = 0;
                ddlDay_Spouse.SelectedIndex = 0;
                ddlMonth_Spouse.SelectedIndex = 0;
                ddlYear_Spouse.SelectedIndex = 0;
                #endregion
                txtFirstname.Focus();
                /* Show BlockUI PopUp */
                popup.Show();
                /* Display Panel */
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    /* Get Advisor Id from gridview and assign to runtime created label */
                    Label CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                    /* Initialize object */
                    Customers objCustomer = new Customers();
                    objCustomer.CustomerID = CustomerID.Text;
                    /* Call function to delete selected row by passing id into function */
                    objCustomer.deleteCustomerInfo(objCustomer);
                    /* Reload gridview with updated data */
                    this.BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.CustomerDeletedSucesFuly;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// view advisor content based on institutional Id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void View(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    Customers customer = new Customers();
                    /* Redirect to Advisor UI with institution Id */
                    Label CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                    DataTable dtCustomer = customer.getCustomerDetails(CustomerID.Text);
                    lblErrorMessage.Visible = false;
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        DataTable dtCustomerFinancialDetails = customer.getCustomerFinancialDetailsByStatus(CustomerID.Text, dtCustomer.Rows[0][Constants.CustomerMaritalStatus].ToString());
                        /* Check if customer have details or not */
                        if (dtCustomerFinancialDetails.Rows.Count > 0)
                        {
                            Session[Constants.SessionUserId] = CustomerID.Text;
                            Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                        }
                        else
                        {
                            lblErrorMessage.Text = Constants.CustomerLabel + dtCustomer.Rows[0][Constants.CustomerFirstName].ToString() + " " + dtCustomer.Rows[0][Constants.CustomerLastName].ToString();
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                    }
                    else
                    {
                        Session[Constants.SessionUserId] = CustomerID.Text;
                        Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {

                RestrictAppIncomeProp.Instance.BoolRestrictNewCase = false;
                RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = false;
                Globals.Instance.BoolIsShowRestrictFull = false;
                Globals.Instance.BoolIsHusbandClaimedBenefit = false;
                Globals.Instance.BoolIsWifeClaimedBenefit = false;


                string strUserBDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Your)).Replace("-", "/");
                string strSpouseBDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Spouse)).Replace("-", "/");
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                lblErrorMessage.Visible = false;
                /* check if popup open for add or edit mode */
                if (clsFlag.Flag)
                {
                    Advisor objAdvisor = new Advisor();
                    DataTable dtAdvisor = objAdvisor.getAdvisorDetailsByID(Session[Constants.SessionAdminId].ToString());
                    //code commented on 24/06/2016 to remove the subscription per client as the new plans created for advisor 
                    //to create the unlimited customers for a month and a year
                    //if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) && dtAdvisor.Rows[0][Constants.ChoosePlan].ToString().Equals(Constants.PerClient))
                    //{
                    //    passwordTextBox.Text = txtPassword.Text;
                    //    popup.Show();
                    //    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displayPanelPopUpSubscription(true);", true);
                    //}
                    //else
                    //{
                    //code amended on 08/14/2015
                    //code to set the customer subscription to yes if the advisor is subscribed and charge is Flat Charge
                    //if (dtAdvisor.Rows[0][Constants.IsSubscription].ToString().Equals(Constants.FlagYes) && dtAdvisor.Rows[0][Constants.ChoosePlan].ToString().Equals(Constants.FlatCharge))
                    //    objCustomer = addCustomer(false,true);
                    //else
                    //objCustomer = addCustomer(false,false);
                    //comment this line and uncomment the above if else
                    objCustomer = addCustomer(false);



                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay();", true);

                    //view report after creating new client
                    ViewReportAfterSave(objCustomer);
                }
                else
                {

                    objCustomer = new Customers();
                    objCustomer.Email = txtEmail.Text;
                    /*email address retrive from ViewState */
                    var emailText = ViewState[Constants.UserEmail];

                    /* Check if email address already exists or not */
                    if (!objCustomer.isCustomerExist(objCustomer) || txtEmail.Text.ToString() == emailText.ToString())
                    {
                        #region set Update Parameters

                        if (rdbtnMarried.Checked)
                        {
                            objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                            objCustomer.LastName = textInfo.ToTitleCase(txtLastName.Text);
                            objCustomer.Email = txtEmail.Text;
                            objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                            objCustomer.CustomerID = txtCustomerID.Text;
                            // objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtBirthDate.Text)).Replace("-", "/");
                            objCustomer.Birthdate = strUserBDate;
                            objCustomer.MartialStatus = Constants.Married;
                            //objCustomer.SpouseBirthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtSpouseBirthdate.Text)).Replace("-", "/");
                            objCustomer.SpouseBirthdate = strSpouseBDate;
                            objCustomer.SpouseFirstname = textInfo.ToTitleCase(txtSpouseFirstname.Text);
                            objCustomer.SpouseLastname = string.Empty;
                            objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                            objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;

                            //Restrict Change (added on May 11 2017)
                            objCustomer = SetCustomerProperties(objCustomer);
                        }
                        if (rdbtnSingle.Checked)
                        {
                            objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                            objCustomer.LastName = textInfo.ToTitleCase(txtLastName.Text);
                            objCustomer.Email = txtEmail.Text;
                            objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                            objCustomer.CustomerID = txtCustomerID.Text;
                            //objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtBirthDate.Text)).Replace("-", "/");
                            objCustomer.Birthdate = strUserBDate;
                            objCustomer.MartialStatus = Constants.Single;
                            objCustomer.SpouseBirthdate = string.Empty;
                            objCustomer.SpouseFirstname = string.Empty;
                            objCustomer.SpouseLastname = string.Empty;
                            objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                            objCustomer.WifesRetAgeBenefit = string.Empty;

                            objCustomer.ClaimedAge = "";
                            objCustomer.ClaimedBenefit = "";
                            objCustomer.ClaimedPerson = "";
                        }
                        if (rdbtnDivorced.Checked)
                        {
                            objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                            objCustomer.LastName = textInfo.ToTitleCase(txtLastName.Text);
                            objCustomer.Email = txtEmail.Text;
                            objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                            objCustomer.CustomerID = txtCustomerID.Text;
                            //objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtSpouseBirthdate.Text)).Replace("-", "/");
                            objCustomer.Birthdate = strUserBDate;
                            objCustomer.MartialStatus = Constants.Divorced;
                            // objCustomer.SpouseBirthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtBirthDate.Text)).Replace("-", "/");
                            objCustomer.SpouseBirthdate = strSpouseBDate;
                            objCustomer.SpouseFirstname = string.Empty;
                            objCustomer.SpouseLastname = string.Empty;
                            objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                            objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;

                            objCustomer.ClaimedAge = "";
                            objCustomer.ClaimedBenefit = "";
                            objCustomer.ClaimedPerson = "";
                        }

                        if (rdbtnWidowed.Checked)
                        {
                            objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                            objCustomer.LastName = textInfo.ToTitleCase(txtLastName.Text);
                            objCustomer.Email = txtEmail.Text;
                            objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                            objCustomer.CustomerID = txtCustomerID.Text;
                            //objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtBirthDate.Text)).Replace("-", "/");
                            objCustomer.Birthdate = strUserBDate;
                            objCustomer.MartialStatus = Constants.Widowed;
                            objCustomer.SpouseBirthdate = string.Empty;
                            objCustomer.SpouseFirstname = string.Empty;
                            objCustomer.SpouseLastname = string.Empty;
                            objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                            objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;

                            objCustomer.ClaimedAge = "";
                            objCustomer.ClaimedBenefit = "";
                            objCustomer.ClaimedPerson = "";
                        }

                        #endregion set Update Parameters

                        /* Update Institution Details into database */
                        objCustomer.updateCustomerDetailsByAdvisor(objCustomer);
                        /* Reload gridview with updated data */
                        BindData();

                        //view report after creating new client
                        ViewReportAfterSave(objCustomer);

                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.CustomerUpdatedSucesFuly;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay();", true);
                    }
                    else
                    {
                        if (rdbtnMarried.Checked == true)
                        {
                            popup.Show();
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "displayPanelPopUpCustomer(true);", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay();", true);
                        }
                        else
                        {
                            popup.Show();
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "displayPanelPopUpCustomer(false);", true);
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay();", true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, Constants.SortingDescending);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, Constants.SortingAscending);
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, "SetGridViewWidth();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        protected void Close_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectManageCustomers, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to view the customer report from advisor login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void View_Report(object sender, EventArgs e)
        {
            try
            {
                Button button = sender as Button;
                if (button.Text.Equals("Download Report"))
                    Globals.Instance.BoolAdvisorDownloadReport = true;

                Label strMaritalStatus = new Label();
                Label CustomerID = new Label();
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                    Session["AdvisorCustomer"] = CustomerID.Text;
                    strMaritalStatus = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblMaritalStatus);
                }
                if (strMaritalStatus.Text.Equals(Constants.Married))
                {
                    Customers objCustomer = new Customers();
                    //Response.Redirect(Constants.RedirectCalcMarried, false);
                    DataTable dtDetails = objCustomer.getCustomerDetails(CustomerID.Text);
                    RedirectionOfMarriedStretegy(dtDetails);

                }
                else if (strMaritalStatus.Text.Equals(Constants.Divorced))
                    Response.Redirect(Constants.RedirectCalcDivorced, false);
                else if (strMaritalStatus.Text.Equals(Constants.Widowed))
                    Response.Redirect(Constants.RedirectCalcWidowed, false);
                else
                    Response.Redirect(Constants.RedirectCalcSingle, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to show report page immediately after creating the new client
        /// </summary>
        /// <param name="objCustomer"></param>
        public void ViewReportAfterSave(Customers objCustomer)
        {
            try
            {
                string strMaritalStatus = objCustomer.MartialStatus;
                Session["AdvisorCustomer"] = objCustomer.CustomerID;
                if (strMaritalStatus.Equals(Constants.Married))
                {
                    //Response.Redirect(Constants.RedirectCalcMarried, false);
                    DataTable dtDetails = objCustomer.getCustomerDetails(objCustomer.CustomerID);
                    RedirectionOfMarriedStretegy(dtDetails);

                    #region Alternative Code restrict
                    //DateTime UserDob = Convert.ToDateTime(objCustomer.Birthdate);
                    //DateTime SpouseDob = Convert.ToDateTime(objCustomer.SpouseBirthdate);
                    //DateTime dtLimit = new DateTime(1954, 01, 02);
                    //string ClaimedBenefit = string.Empty, ClaimedAge = string.Empty, CurrentBenefit = string.Empty, strClaimedPerson = string.Empty;
                    //strClaimedPerson = objCustomer.ClaimedPerson;
                    //if (!strClaimedPerson.Equals("NONE"))
                    //{
                    //    ClaimedBenefit = objCustomer.ClaimedBenefit;
                    //    ClaimedAge = objCustomer.ClaimedAge;
                    //    if (strClaimedPerson.Equals("HUSBAND"))
                    //        CurrentBenefit = objCustomer.HusbandsRetAgeBenefit;
                    //    else if (strClaimedPerson.Equals("WIFE"))
                    //        CurrentBenefit = objCustomer.WifesRetAgeBenefit;
                    //}
                    //else
                    //    Response.Redirect(Constants.RedirectCalcMarried, false);
                    //if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                    //{
                    //    if (!string.IsNullOrEmpty(CurrentBenefit))
                    //        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    //    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ClaimedAge);
                    //    if (!string.IsNullOrEmpty(ClaimedBenefit))
                    //        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                    //}
                    //if (Globals.Instance.BoolIsWifeClaimedBenefit)
                    //{
                    //    if (!string.IsNullOrEmpty(CurrentBenefit))
                    //        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    //    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ClaimedAge);
                    //    if (!string.IsNullOrEmpty(ClaimedBenefit))
                    //        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                    //}
                    ////Determine show/hide full strategy
                    //if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                    //{
                    //    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                    //    {
                    //        if (UserDob < dtLimit)
                    //        {
                    //            Globals.Instance.BoolIsShowRestrictFull = true;
                    //            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    //        }
                    //        else
                    //            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    //    }
                    //    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                    //    {
                    //        if (SpouseDob < dtLimit)
                    //        {
                    //            Globals.Instance.BoolIsShowRestrictFull = true;
                    //            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    //        }
                    //        else
                    //            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    //    }
                    //    else
                    //        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    //}
                    #endregion Alternative Code restrict
                }
                else if (strMaritalStatus.Equals(Constants.Divorced))
                    Response.Redirect(Constants.RedirectCalcDivorced, false);
                else if (strMaritalStatus.Equals(Constants.Widowed))
                    Response.Redirect(Constants.RedirectCalcWidowed, false);
                else
                    Response.Redirect(Constants.RedirectCalcSingle, false);


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// function to mail the PDF report to client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Mail_Report(object sender, EventArgs e)
        {
            Label strMaritalStatus = new Label();
            Label CustomerID = new Label();
            Globals.Instance.BoolEmailReport = true;
            Globals.Instance.strAdvisorEmail = string.Empty;
            using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
            {
                CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                Session["AdvisorCustomer"] = CustomerID.Text;
                strMaritalStatus = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblMaritalStatus);
            }
            if (strMaritalStatus.Text.Equals(Constants.Married))
                Response.Redirect(Constants.RedirectCalcMarried, false);
            else if (strMaritalStatus.Text.Equals(Constants.Divorced))
                Response.Redirect(Constants.RedirectCalcDivorced, false);
            else if (strMaritalStatus.Text.Equals(Constants.Widowed))
                Response.Redirect(Constants.RedirectCalcWidowed, false);
            else
                Response.Redirect(Constants.RedirectCalcSingle, false);
        }

        /// <summary>
        /// function to mail the PDF report to client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Mail_Report_Advisor(object sender, EventArgs e)
        {
            Label strMaritalStatus = new Label();
            Label CustomerID = new Label();
            Globals.Instance.BoolEmailReport = true;
            Advisor objAdvisor = new Advisor();
            string accountID = Session[Constants.SessionAdminId].ToString();
            DataTable dtCustomerDetails = objAdvisor.getAdvisorDetailsByID(accountID);
            Globals.Instance.strAdvisorEmail = dtCustomerDetails.Rows[Constants.zero][Constants.UserEmail].ToString();

            using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
            {
                CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblCustomerID);
                Session["AdvisorCustomer"] = CustomerID.Text;
                strMaritalStatus = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblMaritalStatus);
            }
            if (strMaritalStatus.Text.Equals(Constants.Married))
                Response.Redirect(Constants.RedirectCalcMarried, false);
            else if (strMaritalStatus.Text.Equals(Constants.Divorced))
                Response.Redirect(Constants.RedirectCalcDivorced, false);
            else if (strMaritalStatus.Text.Equals(Constants.Widowed))
                Response.Redirect(Constants.RedirectCalcWidowed, false);
            else
                Response.Redirect(Constants.RedirectCalcSingle, false);
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Decides Redirection of Married Strategy
        /// </summary>
        /// <param name="dtDetails"></param>
        private void RedirectionOfMarriedStretegy(DataTable dtDetails)
        {
            try
            {
                #region Restricted App Change
                DateTime UserDob = Convert.ToDateTime(dtDetails.Rows[0]["Birthdate"].ToString());
                DateTime SpouseDob = Convert.ToDateTime(dtDetails.Rows[0]["SpouseBirthdate"].ToString());
                DateTime dtLimit = new DateTime(1954, 01, 02);
                string strClaimedPerson = dtDetails.Rows[0]["ClaimedPerson"].ToString();
                string ClaimedBenefit = string.Empty, ClaimedAge = string.Empty, CurrentBenefit = string.Empty;
                if (!strClaimedPerson.Equals("NONE") && !string.IsNullOrEmpty(strClaimedPerson))
                {
                    ClaimedBenefit = dtDetails.Rows[0]["ClaimedBenefit"].ToString();
                    ClaimedAge = dtDetails.Rows[0]["ClaimedAge"].ToString();
                    if (strClaimedPerson.Equals("HUSBAND"))
                    {
                        Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                        CurrentBenefit = dtDetails.Rows[0]["HusbandsRetAgeBenefit"].ToString();
                    }
                    else if (strClaimedPerson.Equals("WIFE"))
                    {
                        Globals.Instance.BoolIsWifeClaimedBenefit = true;
                        CurrentBenefit = dtDetails.Rows[0]["WifesRetAgeBenefit"].ToString();
                    }
                }
                else
                {
                    Response.Redirect(Constants.RedirectCalcMarried);
                }
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                if (Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                //Determine show/hide full strategy
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        if (UserDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (SpouseDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else
                        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);

                }


                #endregion Restricted App Change
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Fill Advisor Dropdown list based on particular Institution
        /// </summary>
        /// <param name="institutionID"></param>
        public void fillDropDownList(string institutionID)
        {
            try
            {
                Advisor objAdvisor = new Advisor();
                DataTable dtAdvisor = objAdvisor.getAdvisorDetails(institutionID);
                if (dtAdvisor.Rows.Count > Constants.zero)
                {
                    /*ddlAdvisor.Items.Clear();
                    ddlAdvisor.Items.Insert(Constants.zero, Constants.DropDownCustomerDefault);
                    ddlAdvisor.Items[Constants.zero].Value = String.Empty;
                    ddlAdvisor.AppendDataBoundItems = true;
                    ddlAdvisor.DataSource = dtAdvisor;
                    ddlAdvisor.DataBind(); */
                }
                else
                {
                    /* Fill drop down list with default Data 
                    ddlAdvisor.Items.Clear();
                    ddlAdvisor.Items.Insert(Constants.zero, "No Advisor");
                    ddlAdvisor.Items[Constants.zero].Value = String.Empty;
                    ddlAdvisor.DataBind();*/
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState[Constants.sortDirection] == null)
                    ViewState[Constants.sortDirection] = SortDirection.Ascending;

                return (SortDirection)ViewState[Constants.sortDirection];
            }
            set { ViewState[Constants.sortDirection] = value; }
        }

        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                //if user session is expired redirstc him to login again
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Filter Grid View Data based on Selection Criteria
        /// </summary>
        protected void FilterGridViewData()
        {
            try
            {
                /* Create an instance for Institution Class */
                Customers objCustomer = new Customers();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                    tableRecords = objCustomer.getCustomerDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Request.QueryString[Constants.QueryStringAdvisorAdminID]);
                else
                    tableRecords = objCustomer.getCustomerDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Session[Constants.SessionAdminId].ToString());
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                }
                else
                {
                    ///* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    ///* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultCustomer;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                Customers objCustomer = new Customers();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                    tableRecords = objCustomer.getCustomerDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Request.QueryString[Constants.QueryStringAdvisorAdminID]);
                else
                    tableRecords = objCustomer.getCustomerDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Session[Constants.SessionAdminId].ToString());
                DataView dv = new DataView(tableRecords);
                dv.Sort = sortExpression + direction;
                gvDetails.DataSource = dv;
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                Customers objCustomer = new Customers();
                DataTable tableRecords;
                /* Get all institutional Data from database and bind to gridview control */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                    tableRecords = objCustomer.getCustomerDetailsByAdvisor(Request.QueryString[Constants.QueryStringAdvisorAdminID]);
                else
                    tableRecords = objCustomer.getCustomerDetailsByAdvisor(Session[Constants.SessionAdminId].ToString());
                lblPageTotalNumber.Text = tableRecords.Rows.Count.ToString();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    if (gvDetails.AllowSorting == false)
                        gvDetails.AllowSorting = true;
                    /* Fill data into gridview */
                    DataView dvRecords = tableRecords.DefaultView;
                    dvRecords.Sort = "Lastname ASC";
                    gvDetails.DataSource = dvRecords;
                    gvDetails.DataBind();
                    /* Fill Customer Name into drop down list */
                    nameSelection.Items.Clear();
                    nameSelection.Items.Insert(Constants.zero, Constants.SelectCustomer);
                    nameSelection.Items[Constants.zero].Value = String.Empty;
                    nameSelection.AppendDataBoundItems = true;
                    nameSelection.DataSource = tableRecords;
                    nameSelection.DataBind();
                }
                else
                {
                    //gvDetails.AllowSorting = false;
                    ///* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    ///* Get column count */
                    ////int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    ////gvDetails.Rows[Constants.zero].Cells.Clear();
                    ////gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    ////gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    ////gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultCustomer;
                    ////gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    ////gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                    ///* Fill drop down list with default Data */
                    //nameSelection.Items.Clear();
                    //nameSelection.Items.Insert(Constants.zero, Constants.DropDownCustomerDefault);
                    //nameSelection.Items[Constants.zero].Value = String.Empty;
                    //nameSelection.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Loads Claimed Age drop down values
        /// </summary>
        private void LoadClaimedAge()
        {
            try
            {
                ddlHusbClaimAge.Items.Add("62");
                ddlHusbClaimAge.Items.Add("63");
                ddlHusbClaimAge.Items.Add("64");
                ddlHusbClaimAge.Items.Add("65");
                ddlHusbClaimAge.Items.Add("66");
                ddlHusbClaimAge.Items.Add("67");
                ddlHusbClaimAge.Items.Add("68");
                ddlHusbClaimAge.Items.Add("69");
                ddlHusbClaimAge.Items.Add("70");

                ddlWifeClaimAge.Items.Add("62");
                ddlWifeClaimAge.Items.Add("63");
                ddlWifeClaimAge.Items.Add("64");
                ddlWifeClaimAge.Items.Add("65");
                ddlWifeClaimAge.Items.Add("66");
                ddlWifeClaimAge.Items.Add("67");
                ddlWifeClaimAge.Items.Add("68");
                ddlWifeClaimAge.Items.Add("69");
                ddlWifeClaimAge.Items.Add("70");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion Methods

        #region Subscription
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                //code changed o  08/14/2015
                //to fit the customer subscription based on advisor subscription
                //umcomment the below line and comment the current code
                //Customers objCustomer = addCustomer(true, false);
                Customers objCustomer = addCustomer(true);
                //Changes for Aloha Test Server
                //if (!String.IsNullOrEmpty(objCustomer.CustomerID))
                //Response.Redirect(Constants.TestCustomerURL + objCustomer.CustomerID + "&first_name=" + objCustomer.FirstName + "&last_name=" + objCustomer.LastName + "&billing_first_name=" + objCustomer.FirstName + "&billing_last_name=" + objCustomer.LastName + "&email=" + objCustomer.Email, true);

                //Changes for Live Server
                if (!String.IsNullOrEmpty(objCustomer.CustomerID))
                    Response.Redirect(Constants.LiveCustomerURL + objCustomer.CustomerID + Constants.FirstNameKeyInURL + objCustomer.FirstName + Constants.LastNameKeyInURL + objCustomer.LastName + Constants.BillingFirstNameKeyInURL + objCustomer.FirstName + Constants.BillingLastNameKeyInURL + objCustomer.LastName + Constants.EmailKeyInURL + objCustomer.Email, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion Subscription

        #region Customer

        /// <summary>
        /// code amended on 08/14/2015 to subscribe a customer if advisor is subscribed
        ///comment the current function declaration and uncomment the below line
        ///private Customers addCustomer(Boolean delFlag, Boolean subscriptionFlag)
        /// </summary>
        /// <param name="delFlag"></param>
        /// <returns></returns>
        private Customers addCustomer(Boolean delFlag)
        {
            try
            {
                /* Create object of Customer Class */
                string strUserBDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Your)).Replace("-", "/");
                string strSpouseBDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Spouse)).Replace("-", "/");
                Customers objCustomer = new Customers();
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                #region set Insert Parameters
                /* Assign textboxes content into institutional variables */
                objCustomer.CustomerID = Database.GenerateGID("C");
                /* Assign AdvisorID based on either querystring or Session */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringAdvisorAdminID]))
                    objCustomer.AdvisorID = Request.QueryString[Constants.QueryStringAdvisorAdminID];
                else
                    objCustomer.AdvisorID = Session[Constants.SessionAdminId].ToString();
                objCustomer.Role = Constants.Customer;
                objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                objCustomer.LastName = textInfo.ToTitleCase(txtLastName.Text);
                objCustomer.Email = txtEmail.Text;
                //objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtBirthDate.Text)).Replace("-", "/");
                objCustomer.Birthdate = strUserBDate;
                objCustomer.HusbandsRetAgeBenefit = txtUserBenefit.Text;
                //code amended on 08/14/2015 to subscribe a customer if advisor is subscribed
                //comment the current line and uncomment the below code
                //if (subscriptionFlag)
                //    objCustomer.isSubscription = Constants.FlagYes;
                //else
                //    objCustomer.isSubscription = Constants.FlagNo;
                objCustomer.isSubscription = Constants.FlagYes;
                if (delFlag)
                    objCustomer.DelFlag = Constants.FlagYes;
                else
                    objCustomer.DelFlag = Constants.FlagNo;
                if (String.IsNullOrEmpty(strSpouseBDate))
                    objCustomer.SpouseBirthdate = String.Empty;
                else
                    objCustomer.SpouseBirthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(strSpouseBDate)).Replace("-", "/");
                objCustomer.SpouseFirstname = textInfo.ToTitleCase(txtSpouseFirstname.Text);
                //objCustomer.SpouseLastname = txtSpouseLastname.Text;
                if (String.IsNullOrEmpty(txtPassword.Text))
                    txtPassword.Text = passwordTextBox.Text;
                objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                objCustomer.PasswordResetFlag = Constants.FlagNo;
                /* Assign Marital Status based on radio button checked */
                if (rdbtnDivorced.Checked)
                {
                    objCustomer.MartialStatus = Constants.Divorced;
                    objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;
                    objCustomer.ClaimedAge = "";
                    objCustomer.ClaimedBenefit = "";
                    objCustomer.ClaimedPerson = "";
                }
                else if (rdbtnMarried.Checked)
                {
                    objCustomer.MartialStatus = Constants.Married;
                    objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;

                    //Restrict Change (added on May 11 2017)
                    objCustomer = SetCustomerProperties(objCustomer);

                }
                else if (rdbtnSingle.Checked)
                {
                    objCustomer.MartialStatus = Constants.Single;
                    objCustomer.WifesRetAgeBenefit = String.Empty;
                    objCustomer.ClaimedAge = "";
                    objCustomer.ClaimedBenefit = "";
                    objCustomer.ClaimedPerson = "";

                }
                else
                {
                    objCustomer.MartialStatus = Constants.Widowed;
                    objCustomer.WifesRetAgeBenefit = txtSpouseBenefit.Text;
                    objCustomer.ClaimedAge = "";
                    objCustomer.ClaimedBenefit = "";
                    objCustomer.ClaimedPerson = "";
                }
                #endregion
                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* Insert institution details into Database */
                    objCustomer.insertCustomerDetails(objCustomer);
                    clsFlag.Flag = false;
                    /* Reload gridview with updated data */
                    BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.CustomerAddedText;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    rdbtnMarried.Checked = true;
                    txtEmail.Enabled = true;
                    return objCustomer;
                }
                else
                {
                    if (rdbtnMarried.Checked == true)
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.CustomerEmailIdAlreadyExists, true);
                    else
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.CustomerEmailIdAlreadyExists, true);
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return null;
            }
        }


        /// <summary>
        /// Set New Restricted Application Properties
        /// </summary>
        /// <param name="objCustomer">Customer Object</param>
        /// <returns>Customer Object</returns>
        private Customers SetCustomerProperties(Customers objCustomer)
        {
            try
            {
                //Added on 5 may 2017
                #region Restricted Application Income Changes Assign global values
                if (rdbtnAnyClaimYes.Checked)
                {
                    if (rdbtnuserclaim.Checked)
                    {
                        Globals.Instance.ClaimedPerson = "HUSBAND";
                        Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                        RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ddlHusbClaimAge.Text);
                        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(objCustomer.HusbandsRetAgeBenefit);
                        if (!string.IsNullOrEmpty(txtHusbCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(txtHusbCliamBenefit.Text);
                    }
                    else if (rdbtnspouseclaim.Checked)
                    {
                        Globals.Instance.ClaimedPerson = "WIFE";
                        Globals.Instance.BoolIsWifeClaimedBenefit = true;
                        RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ddlWifeClaimAge.Text);
                        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(objCustomer.WifesRetAgeBenefit);
                        if (!string.IsNullOrEmpty(txtWifeCliamBenefit.Text))
                            RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(txtWifeCliamBenefit.Text);
                    }
                }
                else
                {
                    Globals.Instance.ClaimedPerson = "NONE";

                }
                #endregion Restricted Application Income Changes Assign global values

                //Added on 5 may 2017
                #region Restricted Application Income Changes
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intHusbandClaimAge.ToString();
                        objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intHusbClaimBenefit.ToString();
                        objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;

                    }
                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intWifeClaimAge.ToString();
                        objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intWifeClaimBenefit.ToString();
                        objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                    }
                }
                else
                {
                    objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                    objCustomer.ClaimedAge = "";
                    objCustomer.ClaimedBenefit = "";
                }
                #endregion Restricted Application Income Changes
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageCustomer, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageCustomer + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

            return objCustomer;
        }
        #endregion Customer
    }
}

/// <summary>
/// Check for poup open in Add/Edit Mode
/// </summary>
public class clsFlag
{
    #region Variable
    private static bool flag = false;
    #endregion Variable

    #region Methods
    public static bool Flag
    {
        get
        {
            return flag;
        }
        set
        {
            flag = value;
        }
    }
    #endregion Methods
}