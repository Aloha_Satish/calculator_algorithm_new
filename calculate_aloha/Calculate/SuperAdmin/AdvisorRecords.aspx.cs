﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CalculateDLL;
using Symbolics;
using System.Reflection;
using System.IO;
using System.Data;
using System.Drawing;
using CalculateDLL.TO;
namespace Calculate.SuperAdmin
{
    public partial class AdvisorRecords : System.Web.UI.Page
    {
        #region Global Variables
        UploadData objUploadData;
        DataTable dt;
        int intRecordCnt = 0;
        bool isRecordExist = false;
        #endregion Variables

        #region Events

        /// <summary>
        /// Page load event also used to retrive data from csv and excel file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack && fileUpload.PostedFile != null)
                {
                    if (fileUpload.PostedFile.FileName.Length > 0)
                    {
                        //Import data from excel or csv and bind it to gridview
                        ImportData();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorAdvisorsRecords, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorsRecords + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to import data form excel or csv file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportData()
        {
            try
            {
                objUploadData = new UploadData();
                dt = new DataTable();
                string strMessage = string.Empty;

                if (fileUpload.HasFiles)
                {
                    string filename = Path.GetFileName(fileUpload.FileName);
                    if (objUploadData.ValidateFile(filename))
                    {
                        //Destination server path of file 
                        string strFilePath = Server.MapPath(Constants.Files) + filename;
                        //Save file on server for processing
                        fileUpload.SaveAs(strFilePath);
                        //Retrive data from excel or csv through datatable
                        if (Path.GetExtension(strFilePath).Equals(Constants.xls) || Path.GetExtension(strFilePath).Equals(Constants.xls))
                            dt = objUploadData.UploadExcel(strFilePath);
                        else
                            dt = objUploadData.RetriveDataFromCSV(strFilePath);
                        strMessage = Constants.ImportData;
                        //Delete file after processing
                        File.Delete(strFilePath);
                        //Enable Upload button only if data imported.
                        btnUpload.Enabled = true;
                        //Bind data to gridview
                        gvDetails.DataSource = dt;
                        gvDetails.DataBind();
                    }
                    else
                        strMessage = Constants.PlzUploadValidFile;
                }
                else
                    strMessage = Constants.PlzUploadFile;

                //show popup
                lblMessage.Text = strMessage;
                MailSentPopUp.Show();

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorAdvisorsRecords, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorsRecords + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to import data form excel or csv file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadData(object sender, EventArgs e)
        {
            try
            {
                AdvisorTO objAdvisorTO = new AdvisorTO();
                Customers objCustomer = new Customers();
                string strRecordExistMsg = string.Empty;
                string strMessage = string.Empty;
                int intUnselRecords = 0;
                foreach (GridViewRow gvRow in gvDetails.Rows)
                {
                    CheckBox chkBox = gvRow.FindControl(Constants.chkBxSelect) as CheckBox;
                    Label lblFirstname = gvRow.FindControl(Constants.lblFirstname) as Label;
                    Label lblLastname = gvRow.FindControl(Constants.lblLastname) as Label;
                    Label lblEmail = gvRow.FindControl(Constants.lblEmail) as Label;
                    Label lblCity = gvRow.FindControl(Constants.lblCity) as Label;
                    Label lblState = gvRow.FindControl(Constants.lblState) as Label;
                    Label lblDealer = gvRow.FindControl(Constants.lblDealer) as Label;
                    if (chkBox.Checked)
                    {
                        objAdvisorTO.AdvisorName = lblFirstname.Text;
                        objAdvisorTO.Role = Constants.Advisor;
                        objAdvisorTO.Firstname = lblFirstname.Text;
                        objAdvisorTO.Lastname = lblLastname.Text;
                        objAdvisorTO.Email = lblEmail.Text;
                        objAdvisorTO.City = lblCity.Text;
                        objAdvisorTO.State = lblState.Text;
                        objAdvisorTO.Dealer = lblDealer.Text;
                        objAdvisorTO.isSubscription = Constants.FlagYes;

                        //Insert record into main table
                        AddAdvisorData(objAdvisorTO);
                    }
                    else
                        intUnselRecords++;
                }
                if (isRecordExist)
                    strRecordExistMsg = Constants.RecordsExists;
                isRecordExist = false;
                strMessage = intRecordCnt + Constants.AdvisorUploaded + strRecordExistMsg;
                if (intUnselRecords == gvDetails.Rows.Count)
                {
                    strMessage = Constants.PlzSelectOneRecord;
                    //show popup
                    lblMessage.Text = strMessage;
                    MailSentPopUp.Show();
                    return;
                }

                if (intRecordCnt == 0)
                    strMessage = Constants.AllRecordsExists;
                intRecordCnt = 0;

                //show popup
                lblMessage.Text = strMessage;
                MailSentPopUp.Show();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorAdvisorsRecords, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorsRecords + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Add records to the main advisor table
        /// </summary>
        /// <param name="objAdvisorTO"></param>
        private void AddAdvisorData(AdvisorTO objAdvisorTO)
        {
            try
            {
                Advisor objAdvisor = new Advisor();
                Customers objCustomer = new Customers();
                objCustomer.Email = objAdvisorTO.Email;
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    #region set Insert Parameters
                    objAdvisorTO.AdvisorID = Database.GenerateGID(Constants.A);
                    if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                        objAdvisorTO.InstitutionAdminID = Request.QueryString[Constants.QueryStringInstitutionAdminID];
                    else
                        objAdvisorTO.InstitutionAdminID = Session[Constants.SessionAdminId].ToString();
                    objAdvisorTO.isInstitutionSubscription = Constants.FlagNo;
                    objAdvisorTO.DelFlag = Constants.FlagNo;
                    objAdvisorTO.PasswordResetFlag = Constants.FlagNo;
                    //Generate random password
                    string strPassword = objCustomer.GeneratePassword();
                    //Encrypt password
                    objAdvisorTO.Password = Encryption.Encrypt(strPassword);
                    #endregion set Insert Parameters
                    //Insert institution details into Database
                    objAdvisor.insertAdvisorDetails(objAdvisorTO);
                    #region Mail credentials to Institute
                    //Generate Mail body
                    string strBody = Constants.Hi + objAdvisorTO.Firstname + ", <br /><br />       You have been successfully registered with the Paid to Wait Calculator.  <br />Your login is " + objAdvisorTO.Email + "<br />Your password is " + strPassword + " <br /> Please <a href='http://calculatemybenefits.com/?ref=login'>Click here</a> to login. <br /><br /> Thanks & Regards,<br /> Brian Doherty";
                    //Send mail to advisor
                    MailHelper.sendMail(objAdvisorTO.Email, Constants.SSC, strBody, string.Empty);
                    #endregion Mail credentials to Institute
                    //increase records count
                    intRecordCnt++;
                }
                else
                    isRecordExist = true;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorAdvisorsRecords, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorsRecords + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to get back to main page on the basis of user role
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BackToMainPage(object sender, EventArgs e)
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    Response.Redirect(Constants.RedirectToManageAdvisor, false);
                else
                    Response.Redirect(Constants.RedirectBackToInstitution, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorAdvisorsRecords, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorsRecords + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion Events
    }
}