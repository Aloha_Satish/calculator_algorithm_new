﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class ManageInstitutions : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        #endregion Variables

        #region Events

        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateSession();
                /* Check if page is post back or not */
                if (!IsPostBack)
                {
                    lblErrorMessage.Visible = false;
                    this.BindData();
                    Page.MaintainScrollPositionOnPostBack = true;
                }
                /* Initialize ScriptManager for calling Javascript Function */
                ScriptManager sm = ScriptManager.GetCurrent(Page);
                if (sm.IsInAsyncPostBack)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptDatePicker, true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptLoadDatePicker, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Filter Records based on Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dateFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nameSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// We can edit institutional details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editGridViewDetails(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    // Do not display the advisor field
                    txtAdvisorEmail.Visible = false;
                    txtAdvisorPassword.Visible = false;
                    txtAdvisorConfirmPassword.Visible = false;
                    AdvisorDiv.Style.Add("Display", "none");
                    pnlAddEdit.Style.Add("Height", "Auto");

                    lblErrorMessage.Visible = false;
                    txtDisplayName.Focus();
                    lblHeader.Text = Constants.LblEditHeaderInstitution;
                    clsFlag.Flag = false;

                    /* Get content from gridview and assign to runtime created label */
                    Label InstitutionAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                    Users objUser = new Users();
                    DataTable dtUserDetails = objUser.getUserDetailsByUserID(InstitutionAdminID.Text);
                    if (!InstitutionAdminID.Text.Contains("DI1"))
                    {
                        if (dtUserDetails.Rows[0][Constants.UserActiveFlag].Equals(Constants.FlagYes))
                        {
                            #region Fill data into Popup
                            Label Firstname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblFirstname);
                            Label Lastname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLastname);
                            Label Email = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblEmail);
                            Label InstitutionName = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionName);
                            Label Password = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblPassword);

                            /*take backup of email for to verify user mail id*/
                            ViewState[Constants.UserEmail] = Email.Text;
                            /* Assign label data into textbox which are shown up in blockui popup */
                            txtEmail.Text = Email.Text;
                            txtSearch.Text = String.Empty;
                            txtLastname.Text = Lastname.Text;
                            txtFirstname.Text = Firstname.Text;
                            txtInstitutionID.Text = InstitutionAdminID.Text;
                            txtDisplayName.Text = InstitutionName.Text;
                            //txtPassword.Text = Encryption.Decrypt(Password.Text.ToString());
                            //txtPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                            //txtConfirmPassword.Text = Encryption.Decrypt(Password.Text);
                            //txtConfirmPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                            txtEmail.Enabled = false;
                            #endregion
                            /* Show Popup with Institutional Fields data */
                            popup.Show();
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.SuspendedInstituteEditedText;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.DefaultInstituteEditedText;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Add new institutional details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddInstitution(object sender, EventArgs e)
        {
            try
            {
                //display the advisor field
                AdvisorDiv.Style.Add("Display", "block");
                pnlAddEdit.Style.Add("Height", "500px");
                txtAdvisorEmail.Visible = true;
                txtAdvisorPassword.Visible = true;
                txtAdvisorConfirmPassword.Visible = true;
                lblErrorMessage.Visible = false;
                //pnlAddEdit.Visible = true;
                txtDisplayName.Focus();
                lblHeader.Text = Constants.LblAddHeaderInstitution;
                #region Clear all fields
                txtSearch.Text = string.Empty;
                clsFlag.Flag = true;
                txtEmail.Text = string.Empty;
                txtLastname.Text = string.Empty;
                txtFirstname.Text = string.Empty;
                txtInstitutionID.Text = string.Empty;
                txtDisplayName.Text = string.Empty;
                //txtPassword.Attributes.Clear();
                //txtConfirmPassword.Attributes.Clear();
                //txtPassword.Text = string.Empty;
                //txtConfirmPassword.Text = string.Empty;
                txtAdvisorEmail.Text = string.Empty;
                txtAdvisorPassword.Attributes.Clear();
                txtAdvisorConfirmPassword.Attributes.Clear();
                txtAdvisorPassword.Text = string.Empty;
                //txtConfirmPassword.Text = string.Empty;
                txtEmail.Enabled = true;
                #endregion
                /* Show BlockUI PopUp */
                popup.Show();
                /* Display Panel */
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Get Institution Id from gridview and assign to runtime created label */
                    Label InstitutionAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                    Users objUser = new Users();
                    DataTable dtUserDetails = objUser.getUserDetailsByUserID(InstitutionAdminID.Text);
                    if (!InstitutionAdminID.Text.Contains("DI1"))
                    {
                        if (dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag].Equals(Constants.FlagYes))
                        {
                            /* Initialize object */
                            Institutional objInstitutional = new Institutional();
                            InstitutionalTO objInstitutionalTO = new InstitutionalTO();
                            objInstitutionalTO.InstitutionAdminID = InstitutionAdminID.Text;
                            /* Call function to delete selected row by passing id into function */
                            objInstitutional.deleteInstitutionalInfo(objInstitutionalTO);
                            /* Reload gridview with updated data */
                            this.BindData();
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.InstituteDeletedText;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.InstituteSuspendedText;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.DefaultInstituteDeletedText;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// view advisor content based on institutional Id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void View(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    /* Redirect to Advisor UI with institution Id */
                    Label CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                    Response.Redirect(Constants.RedirectManageAdvisorWithInstitutionID + CustomerID.Text, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        
        /// <summary>
        /// Activate/Deactivate Institution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Status(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Redirect to Advisor UI with institution Id */
                    Label InstitutionID = (Label)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.lblUserID);
                    if (!InstitutionID.Text.Contains("DI1"))
                    {
                        lblErrorMessage.Visible = false;
                        SiteNew.updateActiveFlag(sender, gvDetails);
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.InstituteStatusUpdated;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.DefaultInstituteSuspendedText;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reset(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay(true);", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* check if popup open for add or edit mode */
                if (clsFlag.Flag)
                {
                    /* Create object of Customer Class */
                    Customers objCustomer = new Customers();
                    objCustomer.Email = txtEmail.Text;
                    /* Check if email address already exists or not */
                    if (!objCustomer.isCustomerExist(objCustomer))
                    {
                        objCustomer.Email = txtAdvisorEmail.Text;
                        if (!objCustomer.isCustomerExist(objCustomer))
                        {
                            /* initialize Instance of classes */
                            InstitutionalTO objInstitutionalTO = new InstitutionalTO();
                            Institutional objInstitutional = new Institutional();
                            #region set Insert Parameters
                            /* Assign textboxes content into institutional variables */
                            objInstitutionalTO.InstitutionAdminID = Database.GenerateGID("I");
                            objInstitutionalTO.InstitutionName = textInfo.ToTitleCase(txtDisplayName.Text);
                            objInstitutionalTO.Role = Constants.Institutional;
                            objInstitutionalTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                            objInstitutionalTO.Lastname = textInfo.ToTitleCase(txtLastname.Text);
                            objInstitutionalTO.Email = txtEmail.Text;
                            //objInstitutionalTO.Password = Encryption.Encrypt(txtPassword.Text);
                            string stPassword = objCustomer.GeneratePassword();
                            objInstitutionalTO.Password = Encryption.Encrypt(stPassword);

                            #endregion set Insert Parameters
                            /* Insert institution details into Database */
                            objInstitutional.insertInstitutionalDetails(objInstitutionalTO);

                            #region Mail credentials to Institute
                            string strBody = "Hi " + txtFirstname.Text + ", <br /><br />       You have been successfully registered with the Paid to Wait Calculator. <br /> Your login is  " + txtEmail.Text + "<br /> Your password is " + stPassword + " <br /> Please <a href='http://calculatemybenefits.com/?ref=login'>Click here</a> to login. <br /><br /> Thanks & Regards,<br /> Brian Doherty";
                            MailHelper.sendMail(txtEmail.Text, "Social Security Calculator", strBody, string.Empty);
                            #endregion Mail credentials to Institute

                            clsFlag.Flag = false;
                            /* Reload gridview with updated data */
                            BindData();
                            gvDetails.DataBind();

                            #region Add Default Advisor
                            /* initialize Instance of classes */
                            AdvisorTO objAdvisorTO = new AdvisorTO();
                            Advisor objAdvisor = new Advisor();
                            #region set Insert Parameters
                            /* Assign textboxes content into institutional variables */
                            objAdvisorTO.AdvisorID = Database.GenerateGID("DA");
                            objAdvisorTO.InstitutionAdminID = objInstitutionalTO.InstitutionAdminID;
                            objAdvisorTO.AdvisorName = Constants.Default;
                            objAdvisorTO.Role = Constants.Advisor;
                            objAdvisorTO.Firstname = Constants.Default;
                            objAdvisorTO.Lastname = Constants.Default;
                            objAdvisorTO.Email = txtAdvisorEmail.Text;
                            objAdvisorTO.Password = Encryption.Encrypt(txtAdvisorPassword.Text);
                            objAdvisorTO.isInstitutionSubscription = Constants.FlagYes;
                            objAdvisorTO.DelFlag = Constants.FlagNo;
                            objAdvisorTO.State = string.Empty;
                            objAdvisorTO.Dealer = string.Empty;
                            //change this N for subscription to be done
                            //06/15/2015
                            objAdvisorTO.isSubscription = Constants.FlagYes;
                            objAdvisorTO.ChoosePlan = Constants.FlatCharge;
                            objAdvisorTO.PasswordResetFlag = Constants.FlagNo;


                            #endregion set Insert Parameters
                            /* Insert institution details into Database */
                            objAdvisor.insertAdvisorDetails(objAdvisorTO);
                            #endregion Add Default Advisor


                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.InstituteAddedSucesFuly;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                            //txtConfirmPassword.Text = string.Empty;
                            //txtPassword.Text = string.Empty;
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.AdvisorEmialExists, true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.InstituteEmailExists, true);
                    }
                }
                else
                {
                    /* Create object of Customer Class */
                    Customers objCustomer = new Customers();
                    objCustomer.Email = txtEmail.Text;
                    /*email address retrive from ViewState */
                    var emailText = ViewState[Constants.UserEmail];
                    /* Check if email address already exists or not */
                    if (!objCustomer.isCustomerExist(objCustomer) || txtEmail.Text.ToString() == emailText.ToString())
                    {
                        /* initialize Instance of classes */
                        InstitutionalTO objInstitutionalTO = new InstitutionalTO();
                        Institutional objInstitutional = new Institutional();
                        #region set Update Parameters
                        objInstitutionalTO.InstitutionName = txtDisplayName.Text;
                        objInstitutionalTO.Firstname = txtFirstname.Text;
                        objInstitutionalTO.Lastname = txtLastname.Text;
                        objInstitutionalTO.Email = txtEmail.Text;
                        objInstitutionalTO.InstitutionAdminID = txtInstitutionID.Text;
                        #endregion set Update Parameters
                        /* Update Institution Details into database */
                        objInstitutional.updateInstitutionalInfo(objInstitutionalTO);
                        /* Reload gridview with updated data */
                        BindData();
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.InstituteUpdatedSucesFuly;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.InstituteEmailExists, true);
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay(true);", true);
            }

            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

  

        /// <summary>
        /// Append Images while binding data into GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if gridview row event matched with data control row */
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /* Get instance of selected column */
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(Constants.imgStatus);
                    Label EmailID = (Label)e.Row.FindControl(Constants.lblEmail);
                    Label InstitutionID = (Label)e.Row.FindControl(Constants.lblInstitutionAdminID);
                    Label CreatedOn = (Label)e.Row.FindControl(Constants.lblCreatedDate);
                    CreatedOn.Text = string.Format(Constants.stringDateFormat, Convert.ToDateTime(CreatedOn.Text)).Replace("-", "/");
                    /* Create object of User */
                    Users objUser = new Users();
                    /* Get Active Flag for particular Email ID from User Login Table */
                    DataTable dtUserDetails = objUser.getUserDetails(EmailID.Text);
                    /* If Row count greater than 0,then we have data */
                    if (dtUserDetails.Rows.Count > Constants.zero)
                    {
                        /* Assign value into image alternate text and images into image URL */
                        imgButton.AlternateText = dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag].ToString();
                        if (Convert.ToChar(dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag]).Equals('Y'))
                        {
                            imgButton.ImageUrl = Constants.ActivateImage;
                            if (!InstitutionID.Text.Contains("DI1"))
                            {
                                imgButton.Attributes.Add(Constants.title, Constants.Suspend);
                            }
                            else
                            {
                                imgButton.Attributes.Add(Constants.title, Constants.ReAssign);
                            }
                        }
                        else
                        {
                            imgButton.ImageUrl = Constants.DeactivateImage;
                            imgButton.Attributes.Add(Constants.title, Constants.Activate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, Constants.SortingDescending);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, Constants.SortingAscending);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to navigate on Upload advisor page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadAdvisors(object sender, EventArgs e)
        {
            try
            {
                // Navigate to Advisor Record Page to upload records
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Redirect to Advisor UI with institution Id */
                    Label InsititueID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                    Response.Redirect(Constants.RedirectToAdvisorRecordsWithID + InsititueID.Text, true);
                }
             
            }
            catch (Exception ex)
            {
                 ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Filter Grid View Data based on Selection Criteria
        /// </summary>
        protected void FilterGridViewData()
        {
            try
            {
                /* Create an instance for Institution Class */
                Institutional objInstitutional = new Institutional();
                /* Fill gridview detail based on filteration */
                DataTable tableRecords = objInstitutional.getInstitutionalDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text);
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    foreach (GridViewRow row in gvDetails.Rows)
                    {
                        Label lblInstitutionAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                        if (lblInstitutionAdminID.Text.Contains("DI1"))
                        {
                            ImageButton imgSuspend = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("imgStatus");
                            ImageButton imgEdit = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkEdit");
                            ImageButton imgDelete = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkDelete");
                            imgSuspend.Visible = false;
                            imgEdit.Visible = false;
                            imgDelete.Visible = false;
                        }
                    }
                }
                else
                {
                    /* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;

                    gvDetails.DataBind();

                    /* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultInstitution;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                Institutional objInstitutional = new Institutional();
                DataTable tableRecords = objInstitutional.getInstitutionalDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text);
                DataView dv = new DataView(tableRecords);
                dv.Sort = sortExpression + direction;
                gvDetails.DataSource = dv;
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                Institutional objInstitutional = new Institutional();
                /* Get all institutional Data from database and bind to gridview control */
                DataTable tableRecords = objInstitutional.getInstitutionalDetails();
                lblPageTotalNumber.Text = tableRecords.Rows.Count.ToString();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    if (gvDetails.AllowSorting == false)
                        gvDetails.AllowSorting = true;
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Fill Institution Name into drop down list */
                    nameSelection.Items.Clear();
                    nameSelection.Items.Insert(Constants.zero, Constants.SelectInstitution);
                    nameSelection.Items[Constants.zero].Value = String.Empty;
                    nameSelection.AppendDataBoundItems = true;
                    nameSelection.DataSource = tableRecords;
                    nameSelection.DataBind();
                    foreach (GridViewRow row in gvDetails.Rows)
                    {
                        Label lblInstitutionAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblInstitutionAdminID);
                        if (lblInstitutionAdminID.Text.Contains("DI1"))
                        {
                            ImageButton imgSuspend = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("imgStatus");
                            ImageButton imgEdit = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkEdit");
                            ImageButton imgDelete = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkDelete");
                            imgSuspend.Visible = false;
                            imgEdit.Visible = false;
                            imgDelete.Visible = false;
                        }
                    }
                }
                else
                {

                    //gvDetails.AllowSorting = false;
                    ///* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;

                    gvDetails.DataBind();
                    ///* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultInstitution;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                    ///* Fill drop down list with default Data */
                    //nameSelection.Items.Clear();
                    //nameSelection.Items.Insert(Constants.zero, Constants.DropDownInstitutionalDefault);
                    //nameSelection.Items[Constants.zero].Value = String.Empty;
                    //nameSelection.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState[Constants.sortDirection] == null)
                    ViewState[Constants.sortDirection] = SortDirection.Ascending;

                return (SortDirection)ViewState[Constants.sortDirection];
            }
            set { ViewState[Constants.sortDirection] = value; }
        }

        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                //if user session is expired redirstc him to login again
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageInstitute, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageInstitute + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods
    }
}
