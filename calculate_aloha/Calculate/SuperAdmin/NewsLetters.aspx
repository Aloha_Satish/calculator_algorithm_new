﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsLetters.aspx.cs" ValidateRequest="false" Inherits="Calculate.SuperAdmin.NewsLetters" MasterPageFile="~/AdminMaster.master" Title="Periodic NewsLetters" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="AccountDetails">
        <center>            
            <h1 style="color:#85312f;" class="headfontsize text-left">                
                Manage Periodic NewsLetter
            </h1>
        </center>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div class="table-responsive">
                    <asp:GridView ID="gvDetails" HeaderStyle-Height="45px" DataKeyNames="NewsLetterId" runat="server"
                        AutoGenerateColumns="false" CssClass="mGrid table tablegrid table-striped table-hover table-responsive"
                        HeaderStyle-BackColor="#61A6F8" AllowPaging="true" OnPageIndexChanging="OnPaging" PageSize="10" AllowSorting="true" OnSorting="gvDetails_Sorting" HeaderStyle-Font-Bold="true"
                        HeaderStyle-ForeColor="White"
                        AlternatingRowStyle-CssClass="alt" OnRowDataBound="gvDetails_RowDataBound" ShowHeaderWhenEmpty="true"
                        EmptyDataText="No Record Found" PagerStyle-CssClass="pgr" PagerStyle-HorizontalAlign="Right" PagerStyle-Wrap="False" AllowCustomPaging="False">
                        <Columns>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblNewsLetterId" runat="server" Text='<%#Eval("NewsLetterId") %>' />
                                    <asp:Label ID="lblLetterUpdateBy" runat="server" Text='<%#Eval("LetterUpdateBy") %>' />
                                    <asp:Label ID="lblLetterStatus" runat="server" Text='<%#Eval("LetterStatus") %>' />
                                    <asp:Label ID="lblLetterBody" runat="server" Text='<%#Eval("LetterBody") %>' />
                                    <asp:Label ID="lblInstitutionList" runat="server" Text='<%#Eval("InstitutionList") %>' />
                                    <asp:Label ID="lblAdvisorsFlag" runat="server" Text='<%#Eval("AdvisorsFlag") %>' />
                                    <asp:Label ID="lblCustomersFlag" runat="server" Text='<%#Eval("CustomersFlag") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" ControlStyle-CssClass="ellipsis" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Subject" SortExpression="LetterSubject">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubject" runat="server" Text='<%#Eval("LetterSubject") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Frequency" SortExpression="Periodic">
                                <ItemTemplate>
                                    <asp:Label ID="lblPeriodic" runat="server" Text='<%#Eval("Periodic") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Publish Date" SortExpression="PublishDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblLetterPublishDate" runat="server" Text='<%#Eval("PublishDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Next Scheduled Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblLetterNextDate" runat="server" Text='<%#(Eval("PublishDate").ToString()!="")?GetNextDate(Eval("PublishDate").ToString(),Eval("Periodic").ToString()):"" %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgStatus" runat="server" OnClientClick="return showSuspendConfirmNewsletter(this);" OnClick="Status" CommandName="Select" />
                                    <asp:ImageButton ID="linkEdit" ToolTip="Edit" runat="server" ImageUrl="~/Images/edit_new.png" OnClick="editGridViewDetails" />
                                    <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" ImageUrl="~/Images/delete_new.png" OnClick="Delete" OnClientClick="return showDeleteConfirm('NewsLetter');" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <%-- <AlternatingRowStyle BackColor="silver" />--%>
                    </asp:GridView>
                    <div class="managePageCount">
                        <span>Showing <%=gvDetails.Rows.Count%> of <%= lblPageTotalNumber.Text %> Records &nbsp;</span>
                    </div>
                    <asp:Label ID="lblPageTotalNumber" runat="server" class="displayNone"></asp:Label>
                </div>
                <center><asp:Button ID="btnAdd" runat="server" Text="Add New Periodic NewsLetter" OnClick="AddNewsLetter" OnClientClick="SaveContent();" CssClass="button_login" /></center>
                <br />
                <center><asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label></center>
                <br />
                <%--<asp:Panel ID="pnlAddEdit" BorderColor="#f3f3f3" BorderStyle="Dashed" runat="server" CssClass="modalPopup modalWhole" ScrollBars="Auto">
                    <asp:Label ForeColor="#85312f" CssClass="floatLeft margleft" Font-Size="20px" ID="lblHeader" runat="server" Text="Add Periodic NewsLetter"></asp:Label>
                    <br />
                    <form id="formToValidate">
                        <%--<div class="table-responsive">
                    <table id="tblNewsletter" class="table">
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label1" ForeColor="#787878" runat="server" Text="Publish Date"></asp:Label>
                                
                            </td>                           
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control"  ID="datePublish" runat="server" TabIndex="1"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" CssClass="manageImageTONewsletter" runat="server" ImageUrl="~/Images/calendar1.png" OnClientClick=" return false;"/>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="datePublish"></cc1:CalendarExtender>  
                                <asp:HiddenField ID="NewsLetterIDHidden" runat="server" />                             
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label2" ForeColor="#787878" CssClass="manageNewsletterFrequency" runat="server" Text="Frequency"></asp:Label>                                                          
                            </td>                           
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblWeek" runat="server" Font-Bold="true" Text="Weekly"></asp:Label>
                                <asp:Label ID="lblMon" runat="server" Font-Bold="true" Text="Monthly"></asp:Label>
                                <asp:Label ID="lblQuat" runat="server" Font-Bold="true" Text="Quarterly<br />"></asp:Label>
                                <asp:RadioButton ID="rdbWeekly" Width="55px" runat="server" GroupName="Range" Checked="true" TabIndex="2" />
                                <asp:RadioButton ID="rdbMonthly" Width="65px" runat="server" GroupName="Range" TabIndex="3" /> 
                                <asp:RadioButton ID="rdbQuartely" runat="server" GroupName="Range" TabIndex="4" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblLetterID" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblSubject" ForeColor="#787878" runat="server" Text="Subject"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox ID="txtSubject" CssClass="form-control newbord" MaxLength="100" placeholder="Enter Subject" runat="server" Width="735px" TabIndex="5" ></asp:TextBox>
                            </td>
                        </tr>                        
                        <tr>
                            <td id="modalTD">
                            <asp:Label ID="lblBody" ForeColor="#787878" runat="server" Text="Body"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <textarea class="txtBody" style="width:90%;" id="txtBody" TabIndex="6" runat="server"></textarea> 
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label3" ForeColor="#787878" CssClass="floatLeft" runat="server" Text="Select Institutions"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <div class="InstitutionListAdjustment">
                                    <asp:CheckBox ID="chkAll" runat="server" Text="Select All" OnCheckedChanged="lstInstitution_TextChanged" /><br />
                                    <asp:CheckBoxList ID="lstInstitution" runat="server" >    
                                                                             
                                    </asp:CheckBoxList>
                                </div>
                                <asp:CheckBox ID="chkAdvisors" runat="server" Text="Advisors" OnCheckedChanged="chkAdvisors_CheckedChanged" CssClass="adjustChkAll" />
                                <asp:CheckBox ID="chkCustomers" runat="server" Text="Customers" CssClass="adjustChkAll" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD" style="padding-top:10px;">
                                <asp:FileUpload ID="txtAttachment" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Table ClientIDMode="Static" runat="server" ID="attached" style="width:50%;text-align:left;">
                                    <asp:TableHeaderRow>
                                        <asp:TableHeaderCell HorizontalAlign="Center" ID="header1" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="FileName"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Center" ID="header2" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="Delete"></asp:TableHeaderCell>
                                    </asp:TableHeaderRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <center>
                                    <asp:Button  TabIndex="7" ID="btnSave" runat="server" Text="Save" OnClick="Save" OnClientClick="return validateData();" CssClass="button_login" />
                                    <asp:Button TabIndex="8" ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel"  CssClass="button_login" />
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="failureForgotNotification displayNone modalTD" id="errorMessage"> 
                            </td>
                        </tr>
                    </table>
                            <%--</div>
                    </form>
                </asp:Panel>--%>
                <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopupNewsLetterNew displayNone" ScrollBars="Auto">
                    <div class="modal-dialog modal-md">
                        <div class="h2PopUp modal-header">
                            <asp:Label ForeColor="white" CssClass="marglefthead" ID="lblHeader" runat="server" Text="Add Periodic NewsLetter"></asp:Label>
                        </div>
                        <br />
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Label ID="Label1" ForeColor="#787878" runat="server" Text="Publish Date"></asp:Label><br />
                                    <%--<asp:TextBox CssClass="form-control textEntry handIcon floatLeft minWidthDropdownSearch" placeholder="Date" ID="datePublish" runat="server"  AutoPostBack="true"></asp:TextBox>--%>
                                    <%-- <asp:TextBox CssClass="textEntry form-control calimg handIcon floatLeft" placeholder="Date" MaxLength="10" ID="datePublish" runat="server"></asp:TextBox>--%>
                                    <asp:TextBox CssClass="textEntry form-control" ID="datePublish" runat="server" TabIndex="1" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                    <asp:ImageButton ID="ImageButton1" CssClass="manageImageTONewsletter" runat="server" ImageUrl="~/Images/calendar1.png" OnClientClick=" return false;" />
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="datePublish" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="ImageButton1" TargetControlID="datePublish" Format="MM/dd/yyyy"></cc1:CalendarExtender>
                                    <%--<asp:TextBox ID="datePublish" runat="server" CssClass="textEntry" TabIndex="1"></asp:TextBox>--%>
                                    <asp:RequiredFieldValidator ID="reqdatePublish" runat="server" ControlToValidate="datePublish" CssClass="failureErrorNotification" ErrorMessage="Publish date is required." ToolTip="Publish date is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Publish date is required.</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regdatePublish" runat="server" ControlToValidate="datePublish" CssClass="failureErrorNotification" ErrorMessage="Please enter valid publish Date." ValidationExpression="^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid publish Date.</asp:RegularExpressionValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="datePublish" Type="Date" Display="Dynamic" Operator="GreaterThan" ValidationGroup="RegisterUserValidationGroup" Text="<BR/>*The Publish Date should be of future" />
                                    <asp:HiddenField ID="NewsLetterIDHidden" runat="server" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Label ID="Label2" ForeColor="#787878" runat="server" Text="Frequency"></asp:Label><br />
                                    <div class="col-md-2">
                                        <asp:Label ID="lblWeek" runat="server" ForeColor="#787878" Text="Weekly"></asp:Label>
                                        <br />
                                        <asp:RadioButton ID="rdbWeekly" Width="55px" runat="server" GroupName="Range" Checked="true" TabIndex="2" />
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Label ID="lblMon" runat="server" ForeColor="#787878" Text="Monthly"></asp:Label>
                                        <br />
                                        <asp:RadioButton ID="rdbMonthly" Width="65px" runat="server" GroupName="Range" TabIndex="3" />
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Label ID="lblQuat" runat="server" ForeColor="#787878" Text="Quarterly"></asp:Label>
                                        <br />
                                        <asp:RadioButton ID="rdbQuartely" runat="server" GroupName="Range" TabIndex="4" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br />
                                    <asp:Label ID="lblLetterID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblSubject" ForeColor="#787878" runat="server" Text="Subject"></asp:Label><br />
                                    <asp:TextBox ID="txtSubject" CssClass="form-control newbord" MaxLength="100" ValidationGroup="RegisterUserValidationGroup" runat="server" TabIndex="5"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqSubject" runat="server" ControlToValidate="txtSubject" CssClass="failureErrorNotification" ErrorMessage="Subject is required." ToolTip="Subject is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Subject is required.</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regSubject" runat="server" ControlToValidate="txtSubject" CssClass="failureErrorNotification" ErrorMessage="Please enter valid subject" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid subject</asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <asp:Label ID="lblBody" ForeColor="#787878" runat="server" Text="Body"></asp:Label><br />
                                    <textarea class="txtBody form-control" style="width: 100%" id="txtBody" tabindex="6" runat="server"></textarea>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Label ID="Label3" ForeColor="#787878" CssClass="floatLeft" runat="server" Text="Select Institutions : "></asp:Label>
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="InstitutionListAdjustment">
                                            <asp:CheckBox ID="chkAll" runat="server" CssClass="chkChoice" Text="Select All" OnCheckedChanged="lstInstitution_TextChanged" />
                                            <asp:CheckBoxList ID="lstInstitution"  CssClass="chkChoice" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9">
                                        <asp:CustomValidator ID="CustomValidator1" ValidationGroup="RegisterUserValidationGroup" ErrorMessage="<br/>*Please select at least one recepient."
                                            ClientValidationFunction="ValidateCheckBoxList" runat="server" CssClass="failureErrorNotification" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Label ID="Label4" ForeColor="#787878" CssClass="floatLeft" runat="server" Text="Select Recepients for the Institutions selected:"></asp:Label>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <%--<asp:CheckBoxList id="listRecepients" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem ID="chkAdvisorsl" runat="server" Text="Advisors" OnCheckedChanged="chkAdvisors_CheckedChanged"></asp:ListItem>
                                            <asp:ListItem ID="chkCustomersl" runat="server" Text="Customers"></asp:ListItem>
                                        </asp:CheckBoxList>--%>
                                        <asp:CheckBox ID="chkAdvisors" ForeColor="#787878" CssClass="chkChoice" runat="server" Text="Advisors" OnCheckedChanged="chkAdvisors_CheckedChanged" />
                                    </div>
                                    <div class="col-md-3">
                                        <asp:CheckBox ID="chkCustomers" ForeColor="#787878" CssClass="chkChoice" runat="server" Text="Customers" />
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:FileUpload ID="txtAttachment" runat="server" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Table ClientIDMode="Static" runat="server" ID="attached" Style="width: 60%; text-align: left;">
                                        <asp:TableHeaderRow>
                                            <asp:TableHeaderCell HorizontalAlign="Center" ID="header1" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="FileName"></asp:TableHeaderCell>
                                            <asp:TableHeaderCell HorizontalAlign="Center" ID="header2" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="Delete"></asp:TableHeaderCell>
                                        </asp:TableHeaderRow>
                                    </asp:Table>
                                </div>
                            </div>
                            <br />
                            <div class="row popupbottom" style="height: 40px;">
                                <div class="col-md-12 text-center marginpopbottomtop">
                                <asp:Button  TabIndex="7" ID="btnSave" runat="server" Text="Save" OnClick="Save" ValidationGroup="RegisterUserValidationGroup" CssClass="button_login" />
                                <asp:Button TabIndex="8" ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel"  CssClass="button_login" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlAddEdit" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
                </cc1:ModalPopupExtender>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvDetails" />
                <asp:AsyncPostBackTrigger ControlID="btnAdd" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
          <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>

<asp:Content ID="content3" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../css/main.css" rel="stylesheet" />
    <script src="../Scripts/nice-latest.js"></script>
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Scripts/common.js"></script>
   <script src="../Scripts/ckeditor/ckeditor.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                window.scrollTo(0, 400);
            });
        ////code to add the NIC Editor(the rich text editor) to textarea txtBody
        prm.add_endRequest(function () {
            new nicEditor({ iconsPath: 'http://js.nicedit.com/nicEditIcons-latest.gif' }).panelInstance('ctl00_ctl00_MainContent_BodyContent_txtBody');
            $('.nicEdit-main').css('width', '100%');
            $('.nicEdit-main').css('min-height', '150px');
            $('.nicEdit-main').parent().css('width', '100%');
            $('.nicEdit-panelContain').parent().css('width', '100%');
        });
        bkLib.onDomLoaded(function () {
            new nicEditor({ iconsPath: 'http://js.nicedit.com/nicEditIcons-latest.gif' }).panelInstance('ctl00_ctl00_MainContent_BodyContent_txtBody');
            //$('.nicEdit-main').css('width', '100%');
            //$('.nicEdit-main').parent().css('width', '100%');
            //$('.nicEdit-panelContain').parent().css('width', '100%');
        });
        function SaveContent() {
        }
        $(function () {

            $("[id*=chkAll]").bind("click", function () {
                if ($(this).is(":checked")) {
                    $("[id*=lstInstitution] input").attr("checked", "checked");
                } else {
                    $("[id*=lstInstitution] input").removeAttr("checked");
                }
            });
            $("[id*=lstInstitution] input").bind("click", function () {
                if ($("[id*=lstInstitution] input:checked").length == $("[id*=lstInstitution] input").length) {
                    $("[id*=chkAll]").attr("checked", "checked");
                } else {
                    $("[id*=chkAll]").removeAttr("checked");
                }
            });

            //$(function () {
            //    var DatepickerOpts = {
            //        changeMonth: true,
            //        changeYear: true,
            //        yearRange: '1910:2020',
            //        showOn: 'both',
            //        buttonImageOnly: true,
            //        buttonImage: '/Images/calendar1.png',
            //    };
            //    $("[id$=datePublish]").datepicker(DatepickerOpts);

            //if ($("[id$=datePublish]") != null) {
            //    $("[id$=datePublish]").keypress(function (e) { e.preventDefault(); });
            var Browser = {
                IsIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                Navigator: navigator.appVersion,
                Version: function () {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
            };

            if (Browser.IsIe && Browser.Version() <= 9) {
                $("#tblNewsletter").addClass("alignleft");
            }
        });
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=lstInstitution.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }
        $(function () {
                $("[id*=txtAttachment]").fileUpload({
                    'uploader': '../Scripts/uploader.swf',
                    'cancelImg': '../Images/cancel.png',
                    'buttonText': 'Attach Files',
                    'script': 'UploadCS.ashx',
                    'folder': 'uploads',
                    'multi': true,
                    'auto': true,
                    'scriptData': { key: '<%=Key %>' },
                    'onSelect': function (event, ID, file) {
                        $("#attachedfiles tr").each(function () {
                            if ($("td", this).eq(0).html() == file.name) {
                                alert(file.name + " already uploaded.");
                                $("[id*=txtAttachment]").fileUploadCancel(ID);
                                return;
                            }
                        });
                    },
                    'onComplete': function (event, ID, file, response, data) {
                        $("#header1").removeClass("displayNone");
                        $("#header2").removeClass("displayNone");
                        $("#attached").append("<tr><td style='text-align:left;'><span>" + file.name + "</span> &nbsp;&nbsp;<img src='../Images/hide.png' style='height:18px;width:18px;cursor:pointer;'/></td></tr>");
                    }
                });
            });
            $("#attached img").live("click", function () {
                var row = $(this).closest("tr");
                var fileName = $("td span", row).eq(0).html();
                var newsletterid = $("#newsletterIDTable").val();
                if (newsletterid == undefined) {
                    newsletterid = null;
                }
                $.ajax({
                    type: "POST",
                    url: "NewsLetters.aspx/RemoveFile",
                    data: '{fileName: "' + fileName + '",id: "' + newsletterid + '", key: "<%=Key %>" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function () { },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
                row.remove();
            });

            //Google Analytics
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-88761127-1', 'auto');
            ga('send', 'pageview');
    </script>
</asp:Content>
