﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Reflection;
using System.Web.UI;

namespace Calculate.SuperAdmin
{
    public partial class ChangeUI : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    validateSession();
                    this.BindData();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Save page details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                ModuleData moduleData = new ModuleData();
                moduleData.updateModuleData(ddlPages.SelectedItem.Value, txtBody.Value);
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = Constants.SuccessModuleUpdate;
                lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Updation of data edited for Page selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                ModuleData moduleData = new ModuleData();
                DataTable dtModuleData = moduleData.getModuleDataDescription(ddlPages.SelectedItem.Value); ;
                txtBody.InnerHtml = dtModuleData.Rows[Constants.zero][Constants.ModuleDataDescription].ToString();
                lblErrorMessage.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Change content of the editor based on drop down list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ModuleData moduleData = new ModuleData();
                lblErrorMessage.Visible = false;
                DataTable dtModuleData = moduleData.getModuleDataDescription(ddlPages.SelectedItem.Value);
                txtBody.InnerHtml = dtModuleData.Rows[Constants.zero][Constants.ModuleDataDescription].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Validate Session
        /// </summary>
        public void validateSession()
        {
            if (Session[Constants.SessionUserId] == null)
            {
                try
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                    ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                    ErrorMessagelable.Visible = true;
                }
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                /* Create Object */
                DataTable dtModuleName;
                ModuleData objModuleData = new ModuleData();
                dtModuleName = objModuleData.getModuleName();
                /* Check if datatable contain data or not */
                if (dtModuleName.Rows.Count > Constants.zero)
                {
                    /* Fill Advisor Name into drop down list */
                    ddlPages.Items.Clear();
                    ddlPages.DataSource = dtModuleName;
                    ddlPages.DataBind();
                }
                DataTable dtModuleData = objModuleData.getModuleDataDescription(ddlPages.SelectedItem.Value);
                txtBody.InnerHtml = dtModuleData.Rows[Constants.zero][Constants.ModuleDataDescription].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorChangeUI, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorChangeUI + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods
    }
}
