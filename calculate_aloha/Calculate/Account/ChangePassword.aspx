﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/SiteNew.master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="Calculate.Account.ChangePassword" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2 >Change Password
    </h2>
    <p>
        Use the form below to change your password.
    </p>
    <p>
        New passwords are required to be a minimum of <%= Membership.MinRequiredPasswordLength %> characters in length.
    </p>


    <asp:Label ID="NewPasswordLabel" runat="server">New Password:</asp:Label>
    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="txtNewPassword"
        CssClass="failureNotification" ErrorMessage="New Password is required." ToolTip="New Password is required."
        ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>

    <asp:Label ID="ConfirmNewPasswordLabel" runat="server">Confirm New Password:</asp:Label>
    <asp:TextBox ID="txtConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="txtConfirmNewPassword"
        CssClass="failureNotification" Display="Dynamic" ErrorMessage="Confirm New Password is required."
        ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:RequiredFieldValidator>
    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmNewPassword"
        CssClass="failureNotification" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
        ValidationGroup="ChangeUserPasswordValidationGroup">*</asp:CompareValidator>

    <asp:Button ID="ChangePasswordPushButton" runat="server" OnClick="ChangePasswordPushButton_Click" Text="Change Password" />
    <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
</asp:Content>
