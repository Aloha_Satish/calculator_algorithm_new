﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Web.UI;
namespace Calculate.Account
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion Events

        protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
        {
            Customers objCustomer = new Customers();
            objCustomer.Password =Encryption.Encrypt(txtNewPassword.Text);
            objCustomer.PasswordResetFlag = "N";
            objCustomer.Email = Session["EmailID"].ToString();
            objCustomer.updateCustomerPassword(objCustomer);
        }
    }
}
