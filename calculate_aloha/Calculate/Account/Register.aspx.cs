﻿using CalculateDLL;
using Symbolics;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Globalization;
using System.Web;
using Calculate.RestrictAppIncome;

namespace Calculate.Account
{
    public partial class Register : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableDisableButton", "EnableDisableSubmit();", true);
                //lblTerms.Text = string.Empty;

                if (!IsPostBack)
                {
                    LoadClaimedAge();

                    if (Session[Constants.Demo] != null)
                        FillDemoDropDowns();

                    string strQueryStringMode = string.Empty;
                    if (Request.QueryString[Constants.Mode] != null)
                        strQueryStringMode = Request.QueryString[Constants.Mode].ToString();
                    if (!string.IsNullOrEmpty(strQueryStringMode))
                    {
                        if (strQueryStringMode.ToLower().Equals(Constants.DemoPage.ToLower()))
                            Session[Constants.Demo] = Constants.Demo;
                        else if (strQueryStringMode.ToLower().Equals(Constants.SingleUse.ToLower()))
                            Session[Constants.Demo] = null;
                    }
                    Session["IsValidPage"] = "True";
                    this.PopulateYear_Your();
                    this.PopulateMonth_Your();
                    this.PopulateDay_Your();
                    this.PopulateYear_Spouse();
                    this.PopulateMonth_Spouse();
                    this.PopulateDay_Spouse();
                    txtCustomerName.Focus();
                    rdbtnMarried.Checked = false;

                    ClearFields();
                    ScriptManager.RegisterStartupScript(this, typeof(string), "ClearFields", "ClearText();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + "Page_Load() - " + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChkBoxIAgree.Checked)
                {

                    //lblTerms.Text = string.Empty;
                    CreateUser();
                }
                //else
                //lblTerms.Text = "*Terms & conditions must be accepted";
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.UserCreationError;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to show terms a& conditions popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAgreementDetails(object sender, EventArgs e)
        {
            try
            {
                popUpAgreementDetails.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ShowAgreementDetails();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        protected void OnUpdatePanelLoad(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "load();", true);
        }


        /// <summary>
        /// Used to download terms & conditions in pdf form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DownloadTermsCondition(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Response.ContentType = Constants.pdfContentType;
                HttpContext.Current.Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Consumer_Subscription_Agreement" + Constants.pdfFileExtension);
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, Constants.zero, Constants.zero, -15f, Constants.zero);
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerAgreement, false, true, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainPara1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainPara2, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainPara3, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title1
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle1, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain1SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title2
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle2, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain2SubTitle5Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title3
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle3, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain3SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title4
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle4, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain4SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain4SubTitle1Para1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain4SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain4SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title5
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle5, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain5SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain5SubTitle1Para1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain5SubTitle1Para2, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain5SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain5SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title6
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMainTitle6, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ConsumerMain6SubTitle5Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Close();
                HttpContext.Current.Response.Write(pdfDoc);
                HttpContext.Current.Response.End();

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to close the pop up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popUpAgreementDetails.Hide();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to check I Agree checkbox is checked or not
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void custCheckBoxAgree_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ChkBoxIAgree.Checked;
        }
        #endregion Events

        #region Private Properties

        /// <summary>
        /// Code to set and get the Day for user
        /// </summary>
        private int Day_Your
        {
            get
            {
                if (Request.Form[ddlDay_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Your();
                ddlDay_Your.ClearSelection();
                ddlDay_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to set and get the Month for user
        /// </summary>
        private int Month_Your
        {
            get
            {
                return int.Parse(ddlMonth_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Your();
                ddlMonth_Your.ClearSelection();
                ddlMonth_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to set and get the Year for user
        /// </summary>
        private int Year_Your
        {
            get
            {
                return int.Parse(ddlYear_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Your();
                ddlYear_Your.ClearSelection();
                ddlYear_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to set and get the Day for spouse
        /// </summary>
        private int Day_Spouse
        {
            get
            {
                if (Request.Form[ddlDay_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Spouse();
                ddlDay_Spouse.ClearSelection();
                ddlDay_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to set and get the Month for spouse
        /// </summary>
        private int Month_Spouse
        {
            get
            {
                return int.Parse(ddlMonth_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Spouse();
                ddlMonth_Spouse.ClearSelection();
                ddlMonth_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to set and get the Year for spouse
        /// </summary>
        private int Year_Spouse
        {
            get
            {
                return int.Parse(ddlYear_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Spouse();
                ddlYear_Spouse.ClearSelection();
                ddlYear_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        /// <summary>
        /// Code to get the selected Date of spouse from the form
        /// </summary>
        #endregion Private Properties

        #region Methods

        /// <summary>
        /// This method used to check login information
        /// This use for add customer details into database
        /// Date: 10 June 2014
        /// </summary>
        public void CreateUser()
        {
            try
            {
                /* Check if fields contain data or not */
                if ((Session[Constants.CustomerEmail].ToString() != string.Empty) && (Session[Constants.CustomerName].ToString() != string.Empty) && (Session[Constants.Password].ToString() != string.Empty))
                {
                    if (ValidateInput())
                    {
                        /* Create object of Customer Class */
                        Customers objCustomer = new Customers();
                        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                        #region Fill data into Object
                        /* Fill data into customer object */
                        objCustomer.CustomerID = Database.GenerateGID("C");
                        objCustomer.FirstName = textInfo.ToTitleCase(Session[Constants.CustomerName].ToString());
                        objCustomer.LastName = String.Empty;
                        objCustomer.Email = Session[Constants.CustomerEmail].ToString();
                        objCustomer.Password = Encryption.Encrypt(Session[Constants.Password].ToString());
                        objCustomer.Role = Constants.Customer;
                        objCustomer.AdvisorID = String.Empty;
                        objCustomer.Birthdate = this.SelectedDate_Your.ToShortDateString();
                        objCustomer.HusbandsRetAgeBenefit = txtHusbandBenefit.Text;
                        Session[Constants.txtHusbandBenefits] = txtHusbandBenefit.Text;

                        //InfusionSoft
                        Session["UserEmail"] = objCustomer.Email;

                        if (!Session[Constants.CustomerMaritalStatus].ToString().Equals(Constants.Single))
                        {
                            Session[Constants.txtWifeBenefits] = txtWifeBenefit.Text;
                        }
                        if (rdbtnDivorced.Checked)
                        {
                            objCustomer.MartialStatus = Constants.Divorced;
                            Session[Constants.RadioChecked] = Constants.Divorced;
                            objCustomer.WifesRetAgeBenefit = txtWifeBenefit.Text;
                        }
                        else if (rdbtnMarried.Checked)
                        {
                            objCustomer.MartialStatus = Constants.Married;
                            Session[Constants.RadioChecked] = Constants.Married;
                            objCustomer.WifesRetAgeBenefit = txtWifeBenefit.Text;
                            //Added on 5 may 2017
                            #region Restricted Application Income Changes
                            if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                            {
                                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                {
                                    objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intHusbandClaimAge.ToString();
                                    objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intHusbClaimBenefit.ToString();
                                    objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;

                                }
                                else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                {
                                    objCustomer.ClaimedAge = RestrictAppIncomeProp.Instance.intWifeClaimAge.ToString();
                                    objCustomer.ClaimedBenefit = RestrictAppIncomeProp.Instance.intWifeClaimBenefit.ToString();
                                    objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                                }
                            }
                            else
                            {
                                objCustomer.ClaimedPerson = Globals.Instance.ClaimedPerson;
                            }
                            #endregion Restricted Application Income Changes

                        }
                        else if (rdbtnSingle.Checked)
                        {
                            objCustomer.MartialStatus = Constants.Single;
                            Session[Constants.RadioChecked] = Constants.Single;
                            objCustomer.WifesRetAgeBenefit = string.Empty;
                        }
                        else
                        {
                            objCustomer.MartialStatus = Constants.Widowed;
                            Session[Constants.RadioChecked] = Constants.Widowed;
                            objCustomer.WifesRetAgeBenefit = txtWifeBenefit.Text;
                        }
                        objCustomer.SpouseLastname = String.Empty;
                        if (Session[Constants.BannerImage] != null)
                        {
                            if (Session[Constants.BannerImage].ToString().Equals("Y"))
                            {
                                objCustomer.isSubscription = Constants.FlagYes;
                            }
                            else
                            {
                                objCustomer.isSubscription = Constants.FlagNo;
                            }
                        }
                        else
                        {
                            objCustomer.isSubscription = Constants.FlagNo;
                        }
                        objCustomer.SpouseBirthdate = this.SelectedDate_Spouse.ToShortDateString();
                        objCustomer.SpouseFirstname = textInfo.ToTitleCase(txtSpouseFirstname.Text);
                        objCustomer.DelFlag = Constants.FlagNo;
                        objCustomer.PasswordResetFlag = Constants.FlagNo;
                        #endregion
                        /* Check if user already exists in table or not */
                        if (!objCustomer.isCustomerExist(objCustomer))
                        {
                            /* Insert Customer details into table */
                            objCustomer.insertCustomerDetails(objCustomer);
                            lblUserCreate.Visible = true;
                            lblUserCreate.Text = Constants.customerCreated;
                            ClearFields();
                            Session[Constants.SessionRole] = Symbolics.Constants.Customer;
                            Session[Constants.SessionUserId] = objCustomer.CustomerID;
                            Session[Constants.SessionRegistered] = Constants.ColaStatusT;
                            // Update column with package plan
                            objCustomer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);
                            //Changes for live Server
                            //Response.Redirect(Constants.RedirectWelcomeUser, false);
                            //Response.Redirect(Constants.NewSingleUseURL + Session[Constants.SessionUserId].ToString(), true);
                            Response.Redirect(Constants.NewSingleUseURLInfusion, false);
                        }
                        else
                        {
                            // ScriptManager.RegisterStartupScript(this, typeof(string), Constants.customerExists, Constants.ClientScripUserExist, true);
                            lblUserCreate.Text = Constants.customerExists;
                            lblUserCreate.Visible = true;
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.UserCreationError;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Fill Demo Page's Drop Downs
        /// </summary>
        private void FillDemoDropDowns()
        {
            try
            {
                ddlDemoUserBdate.Items.Clear();
                ddlDemoUserBdate.Items.Add("");
                ddlDemoUserBdate.Items.Add("06-15-1953");
                ddlDemoUserBdate.Items.Add("06-15-1956");
                ddlDemoUserBdate.Items.Add("06-15-1960");
                ddlDemoUserBdate.SelectedIndex = 0;

                ddlDemoSpouseBdate.Items.Clear();
                ddlDemoSpouseBdate.Items.Add("");
                ddlDemoSpouseBdate.Items.Add("06-15-1953");
                ddlDemoSpouseBdate.Items.Add("06-15-1956");
                ddlDemoSpouseBdate.Items.Add("06-15-1960");
                ddlDemoSpouseBdate.SelectedIndex = 0;

                ddlWifeBenefit.Items.Clear();
                ddlWifeBenefit.Items.Add("");
                ddlWifeBenefit.Items.Add("500");
                ddlWifeBenefit.Items.Add("1500");
                ddlWifeBenefit.Items.Add("2500");
                ddlWifeBenefit.SelectedIndex = 0;

                ddlHusbandBenefit.Items.Clear();
                ddlHusbandBenefit.Items.Add("");
                ddlHusbandBenefit.Items.Add("500");
                ddlHusbandBenefit.Items.Add("1500");
                ddlHusbandBenefit.Items.Add("2500");
                ddlHusbandBenefit.SelectedIndex = 0;

                rdbtnMarried.Checked = false;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                pnlStep2.Visible = true;
                pnlStep3.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// This method used to check login information
        /// This use for add demo customer details into database
        /// Date: 18 Oct 2016
        /// </summary>
        public void CreateDemoUser()
        {
            try
            {
                /* Check if fields contain data or not */
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                #region Fill data into Object
                /* Fill data into customer object */
                objCustomer.CustomerID = Database.GenerateGID("C");
                Session[Constants.SessionUserId] = objCustomer.CustomerID;
                Session[Constants.SessionRole] = "DemoCustomer";
                objCustomer.FirstName = textInfo.ToTitleCase(Session["DemoHusbandName"].ToString());
                objCustomer.LastName = String.Empty;
                objCustomer.Email = Session["DemoEmail"].ToString();
                objCustomer.Password = String.Empty;
                objCustomer.Role = "DemoCustomer";
                objCustomer.AdvisorID = String.Empty;
                objCustomer.Birthdate = Convert.ToDateTime(Session["DemoHusbandBDate"]).ToShortDateString();
                objCustomer.HusbandsRetAgeBenefit = Session["DemoHusbandBenefit"].ToString();
                Session[Constants.txtHusbandBenefits] = Session["DemoHusbandBenefit"].ToString();
                if (!Session[Constants.CustomerMaritalStatus].ToString().Equals(Constants.Single))
                    Session[Constants.txtWifeBenefits] = Session["DemoWifeBenefit"].ToString();
                if (rdbtnDivorced.Checked)
                {
                    objCustomer.SpouseBirthdate = Convert.ToDateTime(Session["DemoWifeBDate"]).ToShortDateString();
                    objCustomer.MartialStatus = Constants.Divorced;
                    objCustomer.WifesRetAgeBenefit = Session["DemoWifeBenefit"].ToString(); ;
                    objCustomer.SpouseFirstname = string.Empty;
                }
                else if (rdbtnMarried.Checked)
                {
                    objCustomer.SpouseFirstname = textInfo.ToTitleCase(Session["DemoWifeName"].ToString());
                    objCustomer.SpouseBirthdate = Convert.ToDateTime(Session["DemoWifeBDate"]).ToShortDateString();
                    objCustomer.MartialStatus = Constants.Married;
                    objCustomer.WifesRetAgeBenefit = Session["DemoWifeBenefit"].ToString();
                }
                else if (rdbtnSingle.Checked)
                {
                    objCustomer.MartialStatus = Constants.Single;
                    objCustomer.WifesRetAgeBenefit = string.Empty;
                    objCustomer.SpouseBirthdate = string.Empty;
                    objCustomer.SpouseFirstname = string.Empty;
                }
                else
                {
                    objCustomer.MartialStatus = Constants.Widowed;
                    objCustomer.WifesRetAgeBenefit = Session["DemoWifeBenefit"].ToString();
                    objCustomer.SpouseBirthdate = string.Empty;
                    objCustomer.SpouseFirstname = string.Empty;
                }
                objCustomer.isSubscription = Constants.FlagNo;
                objCustomer.SpouseLastname = String.Empty;
                objCustomer.DelFlag = Constants.FlagNo;
                objCustomer.PasswordResetFlag = Constants.FlagNo;
                /* Insert Demo Customer details into table */
                objCustomer.insertCustomerDetails(objCustomer);
                #endregion
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.UserCreationError;
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// Clear text fields
        /// </summary>
        public void ClearFields()
        {
            try
            {
                txtCustomerName.Text = string.Empty;
                txtEmail.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Code to get the selected Date of user from the form
        /// </summary>
        public DateTime SelectedDate_Your
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Your + Constants.BackSlash + this.Day_Your + Constants.BackSlash + this.Year_Your);
                }
                catch (Exception)
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Your = value.Year;
                    this.Month_Your = value.Month;
                    this.Day_Your = value.Day;
                }
            }
        }

        /// <summary>
        /// code to fill the Day drop down for user
        /// </summary>
        private void PopulateDay_Your()
        {
            try
            {
                //earlier items cleared
                ddlDay_Your.Items.Clear();
                System.Web.UI.WebControls.ListItem lt = new ListItem();
                //default item added to the list
                lt.Text = "DD";
                lt.Value = "0";
                ddlDay_Your.Items.Add(lt);
                //int days = DateTime.DaysInMonth(this.Year_Your, this.Month_Your);
                //30 days added to the list
                for (int intDaysNumber = 1; intDaysNumber <= 31; intDaysNumber++)
                {
                    lt = new ListItem();
                    lt.Text = intDaysNumber.ToString();
                    lt.Value = intDaysNumber.ToString();
                    ddlDay_Your.Items.Add(lt);
                }
                //default first day selected
                ddlDay_Your.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  code to fill the Month drop down for user
        /// </summary>
        private void PopulateMonth_Your()
        {
            try
            {
                //earlier items cleared
                ddlMonth_Your.Items.Clear();
                ListItem itemList = new ListItem();
                //default element added to the dropdown
                itemList.Text = "MM";
                itemList.Value = "0";
                ddlMonth_Your.Items.Add(itemList);
                //12 months added to the dropdown
                for (int intMonthsNumber = 1; intMonthsNumber <= 12; intMonthsNumber++)
                {
                    itemList = new ListItem();
                    itemList.Text = Convert.ToDateTime(intMonthsNumber.ToString() + "/1/1900").ToString("MMMM");
                    itemList.Value = intMonthsNumber.ToString();
                    ddlMonth_Your.Items.Add(itemList);
                }
                //by default jan selected in dropdown
                ddlMonth_Your.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to fill the Year drop down for user
        /// </summary>
        private void PopulateYear_Your()
        {
            try
            {
                //earlier items cleared
                ddlYear_Your.Items.Clear();
                ListItem itemList = new ListItem();
                //default element added
                itemList.Text = "YYYY";
                itemList.Value = "0";
                ddlYear_Your.Items.Add(itemList);
                //years added from 1900 to till date
                for (int intYearNumber = DateTime.Now.Year; intYearNumber >= 1900; intYearNumber--)
                {
                    itemList = new ListItem();
                    itemList.Text = intYearNumber.ToString();
                    itemList.Value = intYearNumber.ToString();
                    ddlYear_Your.Items.Add(itemList);
                }
                //by default 1950 selected to be displayed
                ddlYear_Your.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to get the selected date of spouse
        /// </summary>
        public DateTime SelectedDate_Spouse
        {
            get
            {
                try
                {
                    //the date combined from 3 diffrent dropdowns
                    return DateTime.Parse(this.Month_Spouse + Constants.BackSlash + this.Day_Spouse + Constants.BackSlash + this.Year_Spouse);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    //values set for use
                    this.Year_Spouse = value.Year;
                    this.Month_Spouse = value.Month;
                    this.Day_Spouse = value.Day;
                }
            }
        }

        /// <summary>
        /// code to fill the Day drop down for spouse
        /// </summary>
        private void PopulateDay_Spouse()
        {
            try
            {
                //earlier items cleared
                ddlDay_Spouse.Items.Clear();
                ListItem itemList = new ListItem();
                //default date added
                itemList.Text = "DD";
                itemList.Value = "0";
                ddlDay_Spouse.Items.Add(itemList);
                //  int days = DateTime.DaysInMonth(this.Year_Spouse, this.Month_Spouse);
                //30 days added to the dropdown
                for (int intDayNumber = 1; intDayNumber <= 31; intDayNumber++)
                {
                    itemList = new ListItem();
                    itemList.Text = intDayNumber.ToString();
                    itemList.Value = intDayNumber.ToString();
                    ddlDay_Spouse.Items.Add(itemList);
                }
                //default 1 day of month selected
                ddlDay_Spouse.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to fill the month drop down for spouse
        /// </summary>
        private void PopulateMonth_Spouse()
        {
            try
            {
                //earlier items are cleared
                ddlMonth_Spouse.Items.Clear();
                ListItem itemList = new ListItem();
                //default element added to the list
                itemList.Text = "MM";
                itemList.Value = "0";
                ddlMonth_Spouse.Items.Add(itemList);
                //the number of months added to the list
                for (int intMonthNumber = 1; intMonthNumber <= 12; intMonthNumber++)
                {
                    itemList = new ListItem();
                    itemList.Text = Convert.ToDateTime(intMonthNumber.ToString() + "/1/1900").ToString("MMMM");
                    itemList.Value = intMonthNumber.ToString();
                    ddlMonth_Spouse.Items.Add(itemList);
                }
                //by default the month january is selected
                ddlMonth_Spouse.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to fill the Year drop down for spouse
        /// </summary>
        private void PopulateYear_Spouse()
        {
            try
            {
                //clear the earlier items
                ddlYear_Spouse.Items.Clear();
                ListItem itemList = new ListItem();
                //add the default list element
                itemList.Text = "YYYY";
                itemList.Value = "0";
                ddlYear_Spouse.Items.Add(itemList);
                //add the years starting from 1990 till date
                for (int intYearNumber = DateTime.Now.Year; intYearNumber >= 1900; intYearNumber--)
                {
                    itemList = new ListItem();
                    itemList.Text = intYearNumber.ToString();
                    itemList.Value = intYearNumber.ToString();
                    ddlYear_Spouse.Items.Add(itemList);
                }
                //by default the year 1950 shown in the year drop down
                ddlYear_Spouse.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code validate the data entereed in the registration form
        /// To check if user greater than age 70
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                bool isValid = true;
                lblYourError.Visible = false;
                lblSpouseError.Visible = false;
                lblSpouseNamerequired.Visible = false;
                //to check whether the date selected is valid-within range of numbers of days in that month and year
                if (!(int.Parse(ddlDay_Your.SelectedValue) <= (DateTime.DaysInMonth(int.Parse(ddlYear_Your.SelectedValue), ddlMonth_Your.SelectedIndex))))
                {
                    lblYourError.Text = Constants.InvalidDate;
                    lblYourError.Visible = true;
                    //return validation false if date invalid
                    isValid = false;
                    //lblSpouseError.Visible = false;
                    //return false;
                }
                //to check if age of person is above 70 years
                if (DateTime.Now.AddYears(-70) > Convert.ToDateTime(this.SelectedDate_Your))
                {
                    lblYourError.Text = Constants.ErrorAgeExceeds;
                    lblYourError.Visible = true;
                    //return validation false if date invalid
                    isValid = false;
                    //lblSpouseError.Visible = false;
                    //return false;
                }
                //to check if future date of birth is selected
                if (!(DateTime.Parse(SelectedDate_Your.ToShortDateString()) < DateTime.Parse(DateTime.Now.ToShortDateString())))
                {
                    lblYourError.Text = Constants.ErrorAgeFuture;
                    lblYourError.Visible = true;
                    //return validation false if date invalid
                    isValid = false;
                    //lblSpouseError.Visible = false;
                    //return false;
                }
                if (rdbtnMarried.Checked == true || rdbtnDivorced.Checked == true)
                {
                    //to check if age of person is above 70 years
                    if (DateTime.Now.AddYears(-70) > Convert.ToDateTime(this.SelectedDate_Spouse))
                    {
                        lblSpouseError.Text = Constants.ErrorAgeExceedsSpouse;
                        lblSpouseError.Visible = true;
                        //return validation false if date invalid
                        isValid = false;
                        //lblYourError.Visible = false;
                        //return false;
                    }
                    //to check whether the date selected is valid-within range of numbers of days in that month and year
                    if (!(int.Parse(ddlDay_Spouse.SelectedValue) <= (DateTime.DaysInMonth(int.Parse(ddlYear_Spouse.SelectedValue), ddlMonth_Spouse.SelectedIndex))))
                    {
                        lblSpouseError.Text = Constants.InvalidDate;
                        lblSpouseError.Visible = true;
                        isValid = false;
                        //lblYourError.Visible = false;
                        //return false;
                    }

                    //to check if future date of birth is selected
                    if (!(DateTime.Parse(SelectedDate_Spouse.ToShortDateString()) < DateTime.Parse(System.DateTime.Now.ToShortDateString())))
                    {
                        lblSpouseError.Text = Constants.ErrorAgeFutureSpouse;
                        lblSpouseError.Visible = true;
                        //return validation false if date invalid
                        isValid = false;
                        //lblYourError.Visible = false;
                        //return false;
                    }
                }
                //in case of married the spouse date of birth details are checked
                if (rdbtnMarried.Checked == true)
                {


                    if (txtSpouseFirstname.Text.Trim().Equals(string.Empty))
                    {
                        lblSpouseNamerequired.Visible = true;
                        //pnlStep1.Visible = false;
                        //pnlStep2.Visible = true;
                        //pnlStep3.Visible = false;
                        isValid = false;
                    }
                    else
                    {
                        lblSpouseNamerequired.Visible = false;
                        //pnlStep1.Visible = false;
                        //pnlStep2.Visible = false;
                        //pnlStep3.Visible = true;
                    }
                }



                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

        /// <summary>
        /// code  when continue btton clicked on customer registration first page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnStep1_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Session["IsValidPage"] = "True";
                    rdbtnMarried.Checked = false;
                    Session[Constants.CustomerName] = txtCustomerName.Text;
                    Session[Constants.CustomerEmail] = txtEmail.Text;
                    Session[Constants.Password] = txtCustomerPassword.Text;
                    Session[Constants.ConfirmPassword] = txtConfirmCustomerPassword.Text;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                    pnlStep1.Visible = false;
                    pnlStep2.Visible = true;
                    pnlStep3.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }


        protected void btnStep1Demo_Click(object sender, EventArgs e)
        {
            try
            {
                Session["DemoHusbandName"] = txtCustomerName.Text;
                Session["DemoEmail"] = txtEmail.Text;
                ddlDemoUserBdate.Items.Clear();
                ddlDemoUserBdate.Items.Add("");
                ddlDemoUserBdate.Items.Add("06-15-1953");
                ddlDemoUserBdate.Items.Add("06-15-1956");
                ddlDemoUserBdate.Items.Add("06-15-1960");
                ddlDemoUserBdate.SelectedIndex = 0;

                ddlDemoSpouseBdate.Items.Clear();
                ddlDemoSpouseBdate.Items.Add("");
                ddlDemoSpouseBdate.Items.Add("06-15-1953");
                ddlDemoSpouseBdate.Items.Add("06-15-1956");
                ddlDemoSpouseBdate.Items.Add("06-15-1960");
                ddlDemoSpouseBdate.SelectedIndex = 0;

                ddlWifeBenefit.Items.Clear();
                ddlWifeBenefit.Items.Add("");
                ddlWifeBenefit.Items.Add("500");
                ddlWifeBenefit.Items.Add("1500");
                ddlWifeBenefit.Items.Add("2500");
                ddlWifeBenefit.SelectedIndex = 0;

                ddlHusbandBenefit.Items.Clear();
                ddlHusbandBenefit.Items.Add("");
                ddlHusbandBenefit.Items.Add("500");
                ddlHusbandBenefit.Items.Add("1500");
                ddlHusbandBenefit.Items.Add("2500");
                ddlHusbandBenefit.SelectedIndex = 0;

                rdbtnMarried.Checked = false;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                pnlStep1.Visible = false;
                pnlStep2.Visible = true;
                pnlStep3.Visible = false;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }

        }

        /// <summary>
        /// code when continue button on second page of customer registration page is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinueStep2_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInput() && Page.IsValid)
                {
                    ErrorMessagelable.Visible = false;
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableDisableButton", "EnableDisableSubmit();", true);
                    Session["IsValidPage"] = "True";
                    Session[Constants.CustomerMonth] = ddlMonth_Your.SelectedIndex;
                    Session[Constants.CustomerDay] = ddlDay_Your.SelectedIndex;
                    Session[Constants.CustomerYear] = ddlYear_Your.SelectedIndex;
                    if (rdbtnDivorced.Checked)
                    {
                        Session[Constants.MartialStatus] = Constants.Divorced;
                        pnlStep1.Visible = false;
                        pnlStep2.Visible = false;
                        pnlStep3.Visible = true;
                    }
                    else if (rdbtnWidowed.Checked)
                    {
                        Session[Constants.MartialStatus] = Constants.Widowed;
                        pnlStep1.Visible = false;
                        pnlStep2.Visible = false;
                        pnlStep3.Visible = true;
                    }
                    else if (rdbtnSingle.Checked)
                    {
                        Session[Constants.MartialStatus] = Constants.Single;
                        pnlStep1.Visible = false;
                        pnlStep2.Visible = false;
                        pnlStep3.Visible = true;
                    }
                    else
                    {
                        Session[Constants.MartialStatus] = Constants.Married;
                        if (txtSpouseFirstname.Text.Equals(string.Empty))
                        {
                            lblSpouseNamerequired.Visible = true;
                            pnlStep1.Visible = false;
                            pnlStep2.Visible = true;
                            pnlStep3.Visible = false;
                            Session["IsValidPage"] = "False";
                        }
                        else
                        {
                            lblSpouseNamerequired.Visible = false;
                            pnlStep1.Visible = false;
                            pnlStep2.Visible = false;
                            pnlStep3.Visible = true;
                        }

                        #region Restricted Application Income Changes
                        if (rdbtnAnyClaimYes.Checked)
                        {
                            if (rdbtnuserclaim.Checked)
                            {
                                Globals.Instance.ClaimedPerson = "HUSBAND";
                                Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                                RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ddlHusbClaimAge.Text);
                                if (!string.IsNullOrEmpty(txtHusbCliamBenefit.Text))
                                    RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(txtHusbCliamBenefit.Text);
                            }
                            else if (rdbtnspouseclaim.Checked)
                            {
                                Globals.Instance.ClaimedPerson = "WIFE";
                                Globals.Instance.BoolIsWifeClaimedBenefit = true;
                                RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ddlWifeClaimAge.Text);
                                if (!string.IsNullOrEmpty(txtWifeCliamBenefit.Text))
                                    RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(txtWifeCliamBenefit.Text);
                            }
                        }
                        else
                        {
                            Globals.Instance.ClaimedPerson = "NONE";
                        }
                        #endregion Restricted Application Income Changes

                    }
                    Session[Constants.CustomerSpouseMonth] = ddlMonth_Spouse.SelectedIndex;
                    Session[Constants.CustomerSpouseDay] = ddlDay_Spouse.SelectedIndex;
                    Session[Constants.CustomerSpouseYear] = ddlYear_Spouse.SelectedIndex;
                }
                else
                {
                    Session["IsValidPage"] = "False";

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "customerSpouseinformation", "customerSpouseinformation();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }



        /// <summary>
        /// Create Demo User Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateDemoUser_Click(object sender, EventArgs e)
        {
            try
            {
                Session["DemoHusbandBenefit"] = ddlHusbandBenefit.Text;
                Session["DemoWifeBenefit"] = ddlWifeBenefit.Text;
                Session["DemoHusbandBDate"] = ddlDemoUserBdate.Text;
                Session["DemoWifeBDate"] = ddlDemoSpouseBdate.Text;

                //Add demo customer in table
                CreateDemoUser();

                if (Session[Constants.MartialStatus].ToString().Equals(Constants.Married))
                    Response.Redirect(Constants.RedirectCalcMarried, false);
                else if (Session[Constants.MartialStatus].ToString().Equals(Constants.Divorced))
                    Response.Redirect(Constants.RedirectCalcDivorced, false);
                else if (Session[Constants.MartialStatus].ToString().Equals(Constants.Widowed))
                    Response.Redirect(Constants.RedirectCalcWidowed, false);
                else
                    Response.Redirect(Constants.RedirectCalcSingle, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinueStep2Demo_Click(object sender, EventArgs e)
        {
            try
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableDisableButton", "EnableDisableSubmit();", true);
                Session[Constants.CustomerMonth] = ddlMonth_Your.SelectedIndex;
                Session[Constants.CustomerDay] = ddlDay_Your.SelectedIndex;
                Session[Constants.CustomerYear] = ddlYear_Your.SelectedIndex;
                Session["DemoWifeName"] = txtSpouseDemoName.Text;

                if (rdbtnDivorced.Checked)
                {
                    Session[Constants.MartialStatus] = Constants.Divorced;
                    pnlStep1.Visible = false;
                    pnlStep2.Visible = false;
                    pnlStep3.Visible = true;
                    Label7.Text = " What is your Ex-spouse's Monthly Social Security Full Retirement Age Benefit Amount? ";
                }
                else if (rdbtnWidowed.Checked)
                {
                    Session[Constants.MartialStatus] = Constants.Widowed;
                    pnlStep1.Visible = false;
                    pnlStep2.Visible = false;
                    pnlStep3.Visible = true;
                    Label7.Text = " What is your Deceased spouse's Monthly Social Security Full Retirement Age Benefit Amount? ";
                }
                else if (rdbtnSingle.Checked)
                {
                    Session[Constants.MartialStatus] = Constants.Single;
                    pnlStep1.Visible = false;
                    pnlStep2.Visible = false;
                    pnlStep3.Visible = true;
                }
                else
                {
                    Label7.Text = " What is your spouse's Monthly Social Security Full Retirement Age Benefit Amount? ";
                    Session[Constants.MartialStatus] = Constants.Married;
                    if (txtSpouseDemoName.Text.Equals(string.Empty))
                    {
                        lblSpouseNameError.Visible = true;
                        pnlStep1.Visible = false;
                        pnlStep2.Visible = true;
                        pnlStep3.Visible = false;
                    }
                    else
                    {
                        lblSpouseNameError.Visible = false;
                        pnlStep1.Visible = false;
                        pnlStep2.Visible = false;
                        pnlStep3.Visible = true;
                    }
                }
                Session[Constants.CustomerSpouseMonth] = ddlMonth_Spouse.SelectedIndex;
                Session[Constants.CustomerSpouseDay] = ddlDay_Spouse.SelectedIndex;
                Session[Constants.CustomerSpouseYear] = ddlYear_Spouse.SelectedIndex;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }

        }
        /// <summary>
        /// code when back button on second page back button is clicked on customer registration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBackStep2_Click(object sender, EventArgs e)
        {
            try
            {
                //Old Code
                //if (Session["Demo"] == null)
                //{
                //    txtCustomerName.Text = Session[Constants.CustomerName].ToString();
                //    txtEmail.Text = Session[Constants.CustomerEmail].ToString();
                //    txtCustomerPassword.Text = Session[Constants.Password].ToString();
                //    txtConfirmCustomerPassword.Text = Session[Constants.ConfirmPassword].ToString();
                //}
                //else
                //{
                //    txtCustomerName.Text = Session["DemoHusbandName"].ToString();
                //}
                //pnlStep1.Visible = true;
                //pnlStep2.Visible = false;
                //pnlStep3.Visible = false;
                if (Session["Demo"] == null)
                    Response.Redirect(Constants.RedirectSingleUse, false);
                else
                    Response.Redirect(Constants.RedirectDemoUser, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code when back button on third page back button is clicked on customer registration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBackStep3_Click(object sender, EventArgs e)
        {
            try
            {
                ddlMonth_Your.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerMonth]);
                ddlDay_Your.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerDay]);
                ddlYear_Your.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerYear]);
                if (Session[Constants.MartialStatus].ToString().Equals(Constants.Divorced))
                    rdbtnDivorced.Checked = true;
                else if (Session[Constants.MartialStatus].ToString().Equals(Constants.Widowed))
                    rdbtnWidowed.Checked = true;
                else if (Session[Constants.MartialStatus].ToString().Equals(Constants.Single))
                    rdbtnSingle.Checked = true;
                else
                    rdbtnMarried.Checked = true;
                ddlMonth_Spouse.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerSpouseMonth]);
                ddlDay_Spouse.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerSpouseDay]);
                ddlYear_Spouse.SelectedIndex = Convert.ToInt32(Session[Constants.CustomerSpouseYear]);
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                pnlStep3.Visible = false;
                pnlStep2.Visible = true;
                pnlStep1.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Loads Claimed Age drop down values
        /// </summary>
        private void LoadClaimedAge()
        {
            try
            {
                ddlHusbClaimAge.Items.Add("62");
                ddlHusbClaimAge.Items.Add("63");
                ddlHusbClaimAge.Items.Add("64");
                ddlHusbClaimAge.Items.Add("65");
                ddlHusbClaimAge.Items.Add("66");
                ddlHusbClaimAge.Items.Add("67");
                ddlHusbClaimAge.Items.Add("68");
                ddlHusbClaimAge.Items.Add("69");
                ddlHusbClaimAge.Items.Add("70");

                ddlWifeClaimAge.Items.Add("62");
                ddlWifeClaimAge.Items.Add("63");
                ddlWifeClaimAge.Items.Add("64");
                ddlWifeClaimAge.Items.Add("65");
                ddlWifeClaimAge.Items.Add("66");
                ddlWifeClaimAge.Items.Add("67");
                ddlWifeClaimAge.Items.Add("68");
                ddlWifeClaimAge.Items.Add("69");
                ddlWifeClaimAge.Items.Add("70");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods


    }
}
