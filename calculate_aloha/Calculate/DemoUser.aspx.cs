﻿using Symbolics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class DemoUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void GetStarted(object sender, EventArgs e)
        {
            Session["DemoHusbandName"] = txtFirstName.Text;
            Session["DemoEmail"] = txtEmailAddress.Text;
            Response.Redirect(Constants.RedirectRegister, false);
        }
        
    }
}