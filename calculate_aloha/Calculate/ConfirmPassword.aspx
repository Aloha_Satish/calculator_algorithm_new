﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="ConfirmPassword.aspx.cs" Inherits="Calculate.ConfirmPassword" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="col-md-12 setSuccessPassword" style="min-height: 420px">
        <br />
        <br />
        <center> <h1>Password changed successfully. Please login with your new credentials!
        </h1>
        <asp:Button ID="btnRedirectLogin" class="button_login" runat="server" Text="Log In" OnClick="btnRedirectLogin_Click" /></center>
        <asp:Label ID="lblUserLoginError" runat="server" CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label>
    </div>
      <script type="text/javascript">
          //Google Analytics
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
              m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
          ga('create', 'UA-88761127-1', 'auto');
          ga('send', 'pageview');

    </script>
</asp:Content>
