﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Calculate.Home" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
        });
        <%}%>

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <%--  <div id="overlay" class="web_dialog_overlay"></div>--%>

    <div id="main-content">
        <div class="styleJustify HomeImgBg fontBold col-md-12 col-sm-12 HomeBackroundImage">
            <div class="col-md-8 col-sm-8" style="margin-top: 74px; font-family:'Times New Roman'">
                <p>
                    # RECEIVE SOCIAL SECURITY INCOME WHILE YOU DELAY (GET PAID TO WAIT)
                </p>
                <p>
                    # MAXIMIZE YOUR SOCIAL SECURITY BENEFITS
                </p>
                <p>
                    # MAKE YOUR CLAIMING DECISION SIMPLE AND EASY
                </p>
                <br />
            </div>
            <div class="col-md-4 col-sm-4 " style="margin-top: 63px">
                <asp:Button ID="btnClickHere" Text="Click Here For Short Demo!" Visible="false" runat="server" class="button_login fontLarge" TabIndex="1" Style="height: 60px;" OnClick="btnClickHere_Click" />
                <asp:Button ID="btnDemo" Text="Demo" runat="server" class="home-buttons" Height="30" TabIndex="1" Style="width: 147px; height: 45px" OnClick="btnDemo_Click" />
                &nbsp;&nbsp;&nbsp;
                 <asp:Button ID="Button1" Text="Sign Up Today" runat="server" class="home-buttons" TabIndex="2" Style="width: 147px; height: 45px" OnClick="btnClickHere_Click" />
                <br />
                <br />
            </div>

        </div>
        <div class="marginTop clearfix">
            <div class="col-md-12 col-sm-12 removepaddingleftright">
                <%-- <img id="Imgs" class="img-banner1" runat="server" src="/images/banner1.png" />--%>
                <div class=" styleJustify  col-md-12  divBg col-sm-12">
                    <br />
                    <h4 class="homepagetext fontBold">GET PAID TO WAIT WHILE YOU MAXIMIZE SOCIAL SECURITY!</h4>
                    <p>
                        Maximizing Social Security benefits has never been more important. With Brian Doherty's Paid To Wait Calculator, 
                    you can find out how to maximize your benefits and receive Social Security income while you wait.
                    </p>
                    <br />
                    <h4 class="homepagetext fontBold">WHAT DO YOU MEAN,"RECEIVE SOCIAL SECURITY INCOME WHILE YOU WAIT?"</h4>
                    <p>
                        While it may seem to good to be true, you can receive Social Security income while you are delaying 
                    claiming your benefits. In fact, Brian's Paid To Wait Calculator will show you the maximum amount of 
                    Social Security income you can receive while you delay. This could result in tens or even hundreds 
                    of thousands of dollars of additional Social Security income before you reach age 70!
                    </p>
                    <br />
                    <h4 class="homepagetext fontBold">DON'T DELAY ON THE PAID TO WAIT CALCULATOR</h4>
                    <p>
                        While it's wise for most Americans to delay their Social Security, don't delay on this calculator!                      
                        Get started now!
                    </p>
                    <br />
                </div>
            </div>
        </div>
    </div>
    <%--  <div id="dialog" class="loading">
        <div class="web_dialog">
            <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnHomeLogin">
                <div style="margin-left: 150px; margin-top: 60px;">
                    <asp:Label runat="server" ID="lblEmailAddress" Text="E-mail"></asp:Label>
                    <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="40" TabIndex="2" ValidationGroup="RegisterUserValidationGroup" CssClass="textEntry form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqEmailAddress" runat="server" ControlToValidate="txtEmailAddress" CssClass="failureErrorNotificationSignIn" ErrorMessage="Advisor E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regEmailAddress" runat="server" ControlToValidate="txtEmailAddress" CssClass="failureErrorNotificationSignIn" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                    <br />
                    <asp:Label runat="server" ID="lblPassword" Text="Password"></asp:Label>
                    <asp:TextBox runat="server" ID="txtPassword" autocomplete="off" TextMode="Password" CssClass="textEntry form-control" ValidationGroup="ErrorLogin" TabIndex="3" MaxLength="32"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqtxtPassword" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotificationSignIn" ErrorMessage="User Password is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password is required.</asp:RequiredFieldValidator>
                    <asp:Label ID="lblUserLoginError" runat="server" class="failureErrorNotificationSignIn" Visible="False"></asp:Label>
                    <br />
                    <a href="ForgotPassword.aspx" class="">Forgot Password</a>

                    <br />
                    <br />
                    <asp:Button ID="btnHomeLogin" Text="LOG IN" runat="server" ValidationGroup="RegisterUserValidationGroup" TabIndex="4" OnClick="checkLoginDetails" class="button_login" />
                    &nbsp;&nbsp;<asp:Button ID="btnClose" Text="Cancel" runat="server" TabIndex="5" class="button_login" />
                </div>
            </asp:Panel>
        </div>
    </div>--%>
</asp:Content>
