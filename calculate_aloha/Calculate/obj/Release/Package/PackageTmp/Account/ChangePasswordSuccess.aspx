﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="Calculate.Account.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    <div class="col-md-12 setSuccessPassword">
        <h1>
        Password changed successfully. Please login with your new credentials!
    </h1>
        <asp:Button ID="btnRedirectLogin" class="button_login" runat="server" Text="Log In" OnClick="btnRedirectLogin_Click" />
        <asp:Label ID="lblUserLoginError" runat="server"  CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label> 
    </div>
</asp:Content>
