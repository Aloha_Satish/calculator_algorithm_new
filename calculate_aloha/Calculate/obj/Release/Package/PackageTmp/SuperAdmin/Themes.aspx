﻿<%@ Page Title="Manage Themes" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeBehind="Themes.aspx.cs" Inherits="Calculate.SuperAdmin.Themes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.scrollTo(0, 400);
            var Browser = {
                IsIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                Navigator: navigator.appVersion,
                Version: function () {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
            };
            if (Browser.IsIe && Browser.Version() <= 9) {
                $("#tblTheme").addClass("alignleft");
                $("#txtHeaderColor").addClass("width47");
                $("#txtBodyColor").addClass("width47");
                $("#txtFooterColor").addClass("width47");
                $("#ddlnameSelection").addClass("width48");
            }
        });
    </script>
    <div class="AccountDetails">
        <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
        <br />
        <center>            
            <h1 style="color:#85312f;text-align:left;" class="headfontsize setTopMargin">               
                Manage Theme
            </h1>
        </center>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div style="min-height: 350px">
                    <asp:GridView AllowSorting="true" ID="gvDetails" DataKeyNames="ThemeId" HeaderStyle-Height="45px" runat="server"
                        AutoGenerateColumns="false" CssClass="mGrid table table-striped table-hover table-responsive" HeaderStyle-BackColor="#61A6F8" AllowPaging="true"
                        OnPageIndexChanging="OnPaging" PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr"
                        AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="ID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblThemeId" runat="server" Text='<%#Eval("ThemeId") %>' />
                                    <asp:Label ID="lblBackGroundBodyColor" runat="server" Text='<%#Eval("BackGroundBodyColor") %>' />
                                    <asp:Label ID="lblBackGroundFooterColor" runat="server" Text='<%#Eval("BackGroundFooterColor") %>' />
                                    <asp:Label ID="lblBackGroundHeaderColor" runat="server" Text='<%#Eval("BackGroundHeaderColor") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="Institution Name" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblInstitution" runat="server" Text='<%#Eval("InstitutionName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" ItemStyle-Width="90px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="linkEdit" ToolTip="Edit" runat="server" ImageUrl="~/Images/edit_new.png" OnClick="editGridViewDetails" />
                                    <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" ImageUrl="~/Images/delete_new.png" OnClick="Delete" OnClientClick="return showDeleteConfirm('Theme');" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <%-- <AlternatingRowStyle BackColor="silver" />--%>
                    </asp:GridView>

                    <%--  <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Image CssClass="img-responsive imgResize" ID="img" runat="server" Style="display: none; width: 100%;" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:Label ID="lblSort" ForeColor="#787878" runat="server" Text="Domain Name: "></asp:Label>
                                    <asp:Label ID="selectedThemeID" runat="server" Visible="false"></asp:Label><br />
                                    <asp:TextBox ID="txtDomainName" runat="server"></asp:TextBox>
                                    <asp:DropDownList CssClass="textEntry form-control maxWidthDropDownList" ID="ddlnameSelection" visible="true" DataTextField="InstitutionName" DataValueField="InstitutionName" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <asp:Label ID="lblLogo" ForeColor="#787878" runat="server" Text="Select Logo: "></asp:Label><br />
                                    <asp:FileUpload ID="txtAttachmentLogo" Width="100%" onchange="showimagepreview(this)" runat="server" />
                                    <asp:Label ID="lblResolution" CssClass="imgResolution" runat="server" Text="* Image Dimensions: 134px * 470px or less"></asp:Label>
                                </div>
                            </div>
                            <br />
                            <div class="row ">
                                <div class="col-md-6">
                                    <asp:Label ID="lblBGHeaderColor"  ForeColor="#787878" runat="server" Text="Header Color: "></asp:Label><br />
                                    <asp:TextBox CssClass="textEntry form-control" ID="txtHeaderColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                    <cc1:ColorPickerExtender ID="txtBGColor_ColorPickerExtender" runat="server"
                                        Enabled="True" TargetControlID="txtHeaderColor" SampleControlID="txtHeaderColor" OnClientColorSelectionChanged="Color_Changed">
                                    </cc1:ColorPickerExtender>
                                </div>
                                <div class="col-md-6">
                                    <asp:Label ID="lblBGBodyColor" ForeColor="#787878" runat="server" Text="Body Color: "></asp:Label><br />
                                    <asp:TextBox CssClass="textEntry form-control" ID="txtBodyColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                    <cc1:ColorPickerExtender ID="ColorPickerExtender1" runat="server"
                                        Enabled="True" TargetControlID="txtBodyColor" SampleControlID="txtBodyColor" OnClientColorSelectionChanged="Color_Changed">
                                    </cc1:ColorPickerExtender>
                                </div>
                            </div>
                            <br />
                            <div class="row ">
                                <div class="col-md-12">
                                    <asp:Label ID="lblBGFooterColor" ForeColor="#787878" runat="server" Text="Footer Color: "></asp:Label><br />
                                    <asp:TextBox CssClass="textEntry form-control" ID="txtFooterColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                    <cc1:ColorPickerExtender ID="ColorPickerExtender2" runat="server"
                                        Enabled="True" TargetControlID="txtFooterColor" SampleControlID="txtFooterColor" OnClientColorSelectionChanged="Color_Changed">
                                    </cc1:ColorPickerExtender>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="row popupbottom">
                                <br />
                                <div class="col-md-12">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="Save" CssClass="button_login marginpopbottom" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel" CssClass="button_login" />
                                </div>
                            </div>
                            <div class="row popupbottom">
                                <div class="col-md-12">
                                    <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>--%>

                    <center><asp:Button ID="btnAdd" runat="server" Text="Add New Theme" OnClick="AddNewTheme" CssClass="button_login" /></center>
                    <br />
                    <center>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label>
                    </center>
                    <br />
                    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopupNewsLetterNew" ScrollBars="Auto">
                        <div class="modal-dialog modal-md">
                            <div class="h2PopUp modal-header">
                                <asp:Label ForeColor="white" CssClass="marglefthead" Font-Bold="false" ID="lblHeader" runat="server" Text="Add New Theme">
                                </asp:Label>
                            </div>
                            <br />
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:Image CssClass="img-responsive imgResize" ID="img" runat="server" Style="display: none; width: 100%;" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <asp:Label ID="lblSort" ForeColor="#787878" runat="server" Visible="false" Text="Domain Name: "></asp:Label>
                                        <asp:Label ID="selectedThemeID" runat="server" Visible="false"></asp:Label><br />
                                        <asp:TextBox ID="txtDomainName" runat="server" Visible="false"></asp:TextBox>
                                        <asp:Label ID="lblInstiName" ForeColor="#787878" runat="server" Text="Select Institution : "></asp:Label>
                                        <asp:DropDownList CssClass="textEntry form-control maxWidthDropDownList" ID="ddlnameSelection" Visible="true" DataTextField="InstitutionName" DataValueField="InstitutionName" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Label ID="lblLogo" ForeColor="#787878" runat="server" Text="Select Logo: "></asp:Label><br />
                                        <asp:FileUpload ID="txtAttachmentLogo" Width="100%" onchange="showimagepreview(this)" runat="server" />
                                        <asp:Label ID="lblResolution" CssClass="imgResolution" runat="server" Text="* Image Dimensions: 134px * 470px or less"></asp:Label>
                                    </div>
                                </div>
                                <br />
                                <div class="row ">
                                    <div class="col-md-6">
                                        <asp:Label ID="lblBGHeaderColor" ForeColor="#787878" runat="server" Text="Header Color: "></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtHeaderColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="txtBGColor_ColorPickerExtender" runat="server"
                                            Enabled="True" TargetControlID="txtHeaderColor" SampleControlID="txtHeaderColor" OnClientColorSelectionChanged="Color_Changed">
                                        </cc1:ColorPickerExtender>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Label ID="lblBGBodyColor" ForeColor="#787878" runat="server" Text="Body Color: "></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtBodyColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="ColorPickerExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtBodyColor" SampleControlID="txtBodyColor" OnClientColorSelectionChanged="Color_Changed">
                                        </cc1:ColorPickerExtender>
                                    </div>
                                </div>
                                <br />
                                <div class="row ">
                                    <div class="col-md-12">
                                        <asp:Label ID="lblBGFooterColor" ForeColor="#787878" runat="server" Text="Footer Color: "></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtFooterColor" AutoPostBack="true" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="ColorPickerExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtFooterColor" SampleControlID="txtFooterColor" OnClientColorSelectionChanged="Color_Changed">
                                        </cc1:ColorPickerExtender>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row popupbottom">
                                    <br />
                                    <div class="col-md-12">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="Save" CssClass="button_login marginpopbottom" />&nbsp;&nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel" CssClass="button_login" />
                                    </div>
                                </div>
                                <div class="row popupbottom">
                                    <div class="col-md-12">
                                        <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                                        <br />
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlAddEdit" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="gvDetails" />
                <asp:PostBackTrigger ControlID="btnAdd" />
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }
        function showimagepreview(input) {
            var isValidate = true;
            var errorMessage = '';
            var imgValidate;
            if (/firefox/.test(navigator.userAgent.toLowerCase())) {
                imgValidate = /(.*?)\.(jpg|jpeg|png|gif|bmp|GIF|JPG|JPEG)$/;
            }
            else {
                imgValidate = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.png|.bmp|.gif|.GIF|.jpg|.JPG|.jpeg|.JPEG)$/;
            }
            /* Check if field exists in current form or not */
            if ($("[id$=txtAttachmentLogo]").val() != null) {
                /* Validation Email Address Format */
                if (!imgValidate.test($("[id$=txtAttachmentLogo]").val())) {
                    errorMessage = "* Only Image type allowed!!! <br>";
                    /* Add error css class */
                    $("[id$=txtAttachmentLogo]").addClass("error");
                    $('[id$=img]').attr("style", "display:none;");
                    isValidate = false;
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtAttachmentLogo]").removeClass("error");
                    if (input.files && input.files[0]) {
                        var filerdr = new FileReader();
                        filerdr.onload = function (e) {
                            $('[id$=img]').attr('src', e.target.result);
                            $('[id$=img]').attr("style", "display:block;height:134px;width:470px;");
                        }
                        filerdr.readAsDataURL(input.files[0]);
                    }
                }
            }
            $("#errorMessage").hide();
            $("#displayErrorMessage").hide();
            /* Return Error message Lists if have */
            if (errorMessage != '') {
                $("#errorMessage").show();
                $("#displayErrorMessage").show();
                $("#errorMessage").html(errorMessage);
            }
            return isValidate;
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
