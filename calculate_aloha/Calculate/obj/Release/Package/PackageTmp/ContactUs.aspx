﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="Calculate.ContactUs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1>Contact Us</h1>
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada interdum sapien non ornare. Phasellus diam elit, mattis quis pharetra eget, tempus blandit enim. Donec sit amet enim urna, sit amet egestas orci. Integer ut elit vitae ligula lacinia egestas ac et lacus. Aenean ultricies lacinia justo, a mattis libero lacinia sed. Fusce mollis pretium aliquam. Ut viverra libero in diam ultricies auctor. Donec mi diam, laoreet sed laoreet sit amet, vulputate placerat massa. Vestibulum luctus, erat ornare iaculis condimentum, risus magna lacinia nulla, sed fermentum arcu enim ac dui. Nunc fermentum ultrices tellus, id pellentesque libero scelerisque vitae. Mauris diam erat, vulputate in condimentum at, varius et ante. Vivamus cursus iaculis neque quis sagittis. Curabitur pellentesque ipsum nec ligula molestie vitae euismod lorem elementum. Nunc eu libero leo, quis placerat erat. Ut eu lacus vel est varius viverra. In scelerisque, dolor vel dapibus mattis, odio quam ornare felis, sed suscipit erat justo a dui.<br />
        <br />

        Pellentesque dictum, leo sed malesuada condimentum, eros eros molestie nibh, eu cursus leo nisl eget elit. Vestibulum non erat libero. Curabitur sit amet diam in magna pellentesque vulputate. Morbi euismod aliquam lorem accumsan iaculis. In volutpat neque eget risus auctor elementum. Etiam tincidunt, lacus nec sagittis vehicula, mauris ligula auctor felis, sit amet semper ligula massa sed eros. Duis elementum, nunc et eleifend varius, sapien eros semper nibh, ut vestibulum ipsum erat eu dui. Phasellus lacinia nisi id libero tincidunt aliquam. Suspendisse bibendum, nisi quis suscipit aliquet, quam tortor interdum turpis, id euismod lectus arcu sed orci. Fusce at tellus justo, id dignissim justo. Cras molestie, mi nec vulputate tincidunt, urna leo eleifend purus, at iaculis dolor quam auctor tortor. Nunc commodo posuere ligula, et sagittis leo euismod sed. Suspendisse blandit tincidunt est, eu condimentum dolor suscipit a.
    </p>
    <script type="text/javascript">
        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
