﻿<%@ Page Title="Forgot Password" Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Calculate.Account.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title>Demo User Registration</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap-dialog.js"></script>
    <script type="text/javascript">
        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

        //Processing Window
        ijQuery(window).load(function () {
            var notification_loader;

            ijQuery('body').hide().show();

            notification_loader = ijQuery('.notification-loader');
            notification_loader.attr('src', notification_loader.attr('rel'));
        });


        function ValidateEmail()
        {
            var isValidate = true;
            if ($("[id$=txtEmail]").val() != null) {
                /* Validate Email Field */
                if ($("[id$=txtEmail]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtEmail]").addClass("error");
                    $("[id$=lblEmailError]").text("* Email Address Required");
                }
                else {
                    // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
                    /* Validation Email Address Format */
                    if (!emailfilter1.test($("[id$=txtEmail]").val())) {
                        $("[id$=lblEmailError]").text("* Invalid Email Address");
                        /* Add error css class */
                        $("[id$=txtEmail]").addClass("error");
                        isValidate = false;
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtEmail]").removeClass("error");
                        $("[id$=lblEmailError]").text("");
                    }
                }
            }
            return isValidate;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="overlay" class="web_dialog_overlay_Pricing">
                <div class="web_dialog_Single_Use" style="display: block;">
                    <div class="row">
                        <div id="color-overlay_singleUse">
                        </div>
                        <div>
                            <div class="col-md-12 ">
                                <div class="login-symbols_register">
                                    <table>
                                        <tr>
                                            <td>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <img src="/Images/Newlogo2.png" style="height: 50px; width: 50px;" />
                                                            </td>
                                                            <td>
                                                                <div style="margin-top: 10px;">
                                                                    <h1>
                                                                        <p style="font-family: 'Times New Roman'; font-size: 24px;">
                                                                            <font color="#2f2e2e">THE PAID TO WAIT</font>
                                                                        </p>
                                                                    </h1>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </td>


                                        </tr>

                                        <tr>
                                            <td>
                                                <div>
                                                    <p style="line-height: 45px; font-size: 29px; padding-bottom: 90px; margin-left: 7px;">
                                                        <font color="#2f2e2e">SOCIAL SECURITY CALCULATOR</font>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h1 class="fontBold text-center ">Forgot Password</h1>
                            <h4 class="text-center">Enter your Email Address to receive your Password.</h4>
                            <center>  
                        <div class="col-md-12 col-sm-12" style="margin-bottom:174px">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textEntry form-control" MaxLength="50" TabIndex="1" ValidationGroup="VG1" ViewStateMode="Disabled" AutoCompleteType="Disabled"></asp:TextBox>
                        <asp:Label ID="lblEmailError" runat="server" ForeColor="Red"></asp:Label>
                        <br />
                        <asp:Button ID="btnSend" runat="server" OnClientClick="return ValidateEmail();" CausesValidation="true" CssClass="button_login" Text="Send Password" ValidationGroup="VG1" OnClick="btnSend_Click" TabIndex="2" />&nbsp;&nbsp;
                        <%--<asp:Button ID="btnCancel" runat="server" CausesValidation="true" CssClass="button_login" Text="Cancel" TabIndex="3" OnClick="btnCancel_Click" />
                            <br />--%>
                        <asp:ImageButton ID="btnImageHome" runat="server"  ImageUrl="~/Images/home-256.png" Height="30" Width="30" OnClick="GoHome" TabIndex="4" />
                        <br />
                        <asp:Label ID="sendMailStatus" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblUserLoginError" runat="server"  CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label>
                        </div>
                        </center>
                        </div>

                    </div>


                </div>

                <div class="notification" style="display: none;">
                    <div class="notification-overlay"></div>
                    <div class="notification-inner" style="left: 325px;">
                        <img rel="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" src="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" class="loading notification-loader" alt="" style="display: none;">
                        <span class="message">Processing...</span>
                        <%-- <span class="close-button" onclick="jQuery(this).parent().parent().hide()">Close</span>--%>
                    </div>
                </div>

            </div>

            <div class="footerbg" style="padding-top: 0px">
                <div>
                    <div class="clearfix">
                        <div class="col-md-12 column removepaddingleftright">
                            <div id="Div1" class="divFoot" runat="server">
                                <table class="table-responsive">
                                    <tr class="col-md-12 col-sm-12">
                                        <td class="col-md-2 col-sm-2"></td>
                                        <td class="col-md-3 col-sm-3">
                                            <div id="footer-left" style="line-height: 22.4px; padding-top: 25px; font-size: 16px;">
                                                &copy; 2017 Filtech, LLC. All Rights Reserved.
                                                <br />
                                                3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                                Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                                <br />
                                                <p style="padding-top: 10px;">
                                                    <%if (Session["UserID"] != null)
                                                      {%>
                                                    <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%}
                                                      else
                                                      { %>
                                                    <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%} %>
                                                </p>
                                                <br />
                                            </div>
                                        </td>
                                        <td class="col-md-2 col-sm-2">
                                            <table style="width: 100px">
                                                <tr>
                                                    <td>
                                                        <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                            <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                            <img src="/Images/fbicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://twitter.com/BrianDoherty57">
                                                            <img src="/Images/twittericon.png" class="img-responsive" />
                                                        </a>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>



