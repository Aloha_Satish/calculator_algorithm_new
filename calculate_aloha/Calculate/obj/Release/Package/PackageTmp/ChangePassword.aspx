﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Calculate.ChangePassword" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="col-md-12 col-sm-12" style="min-height: 425px">
        <div class="col-md-3 col-sm-3"></div>
        <div class="col-md-9 col-sm-9">
            <div class="col-md-12 col-sm-12">
                <br />
                <h1>Please change your password
                </h1>
                <asp:Label ID="NewPasswordLabel" runat="server" Text="New Password"></asp:Label>
                <br />
                <asp:TextBox ID="txtNewPassword" CssClass="textEntry form-control" runat="server" TextMode="Password" ValidationGroup="RegisterUserValidationGroup" TabIndex="1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqNewPassword" runat="server" ControlToValidate="txtNewPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password is required.</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regtxtNewPassword" runat="server" ControlToValidate="txtNewPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password must be between 6-32 characters </asp:RegularExpressionValidator>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" Text="Confirm New Password"></asp:Label>
                <br />
                <asp:TextBox ID="txtConfirmNewPassword" CssClass="textEntry form-control" runat="server" onChange="checkPasswordMatch1();" TextMode="Password" TabIndex="2" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox><img class="img-responsive" id="imgpass" runat="server" src="/images/confirmPassword.png" style="display: none; margin-left: 215px; margin-top: -25px;" />
                <asp:RequiredFieldValidator ID="reqConfirmNewPassword" runat="server" ControlToValidate="txtConfirmNewPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Confirm Password is required.</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvPasswordCompare" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmNewPassword" CssClass="failureErrorNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="RegisterUserValidationGroup">*The Password and Confirm Password must match.</asp:CompareValidator>
                <asp:RegularExpressionValidator ID="regtxtConfirmNewPassword" runat="server" ControlToValidate="txtConfirmNewPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Password must be between 6-32 characters </asp:RegularExpressionValidator>
                <asp:Label ID="sendMailStatus" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblUserLoginError" runat="server" CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <br />
                <asp:Button ID="ChangePasswordPushButton" ValidationGroup="RegisterUserValidationGroup" runat="server" class="button_login" TabIndex="3" OnClick="ChangePasswordPushButton_Click" Text="Change Password" />
                <asp:Button ID="CancelPushButton" runat="server" class="button_login" Text="Cancel" OnClick="CancelPushButton_Click" TabIndex="4" />

            </div>
        </div>
    </div>

    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function checkPasswordMatch1() {
            var password = $("[id$=txtNewPassword]").val();
            var confirmPassword = $("[id$=txtConfirmNewPassword]").val();
            if (confirmPassword.match(/\S/) && password.match(/\S/)) {
                if (password == confirmPassword)
                    $("[id$=imgpass]").css("display", "");
                else
                    $("[id$=imgpass]").css("display", "none");
            }
            else {
                $("[id$=imgpass]").css("display", "none");
            }
        }
        function ClearData() {
            var txtpass = document.getElementById("<%=txtNewPassword.ClientID%>");
            var txtconpass = document.getElementById("<%=txtConfirmNewPassword.ClientID%>");
            txtpass.value = "";
            txtconpass.value = "";
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>

</asp:Content>

