﻿<%@ Page Title="My Account Details" Language="C#" MasterPageFile="~/ClientMaster.master" AutoEventWireup="true" CodeBehind="MyAccountDetails.aspx.cs" Inherits="Calculate.MyAccountDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="clearfix">
        <br />
        <br />
        <br />
        <h1 class="fontBold text-center setTopMargin">My Account Details</h1>
        <div class="col-md-4 col-sm-4"></div>
        <div class="col-md-8 col-sm-8" style="margin-bottom: 25px">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-12 col-sm-12">
                            <asp:Label ID="lblFirstname" runat="server" Text="First name"></asp:Label>
                            <asp:Label ID="lblInstitutionName" Font-Bold="true" runat="server" Visible="false"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtFirstname" onkeypress="document.getElementById('ctl00_ctl00_MainContent_BodyContent_lblUserUpdate').innerHTML=null;" runat="server" CssClass="textEntry form-control" TabIndex="1" MaxLength="20"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqtxtFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Customer first name is required." ToolTip="Customer name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Customer name is required.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerName" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid customer first name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Customer name.</asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <br />
                            <asp:Label ID="lblEmailAddress" runat="server" Text="E-mail Address"></asp:Label>
                            <asp:TextBox ID="txtEmail" onkeypress="document.getElementById('ctl00_ctl00_MainContent_BodyContent_lblUserUpdate').innerHTML=null;" runat="server" CssClass="textEntry form-control" TabIndex="3" MaxLength="40" Enabled="false"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regtxtEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <br />
                            <asp:Label ID="lblCustomerPassword" runat="server" AssociatedControlID="lblCustomerPassword">Password</asp:Label>
                            <asp:TextBox ID="txtPassword" Enabled="false" onChange="checkPasswordMatch();" runat="server" CssClass="textEntry form-control" TextMode="Password" MaxLength="40" ValidationGroup="RegisterUserValidationGroup" TabIndex="3"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Password is required.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revCustomerPassword" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Password must be between 6-32 characters </asp:RegularExpressionValidator>

                            <asp:Label Visible="false" ID="tempPassword" runat="server"></asp:Label>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <br />
                            <asp:Label ID="lblCustomerConfirmPassword" Visible="true" runat="server" AssociatedControlID="lblCustomerConfirmPassword">Confirm Password</asp:Label>
                            <asp:TextBox ID="txtConfirmPassword" runat="server" onChange="checkPasswordMatch();" CssClass="textEntry form-control" TextMode="Password" MaxLength="40" ValidationGroup="RegisterUserValidationGroup" TabIndex="4"></asp:TextBox>
                            <img class="img-responsive" id="imgpass" runat="server" src="/images/confirmPassword.png" style="display: none; margin-left: 200px; margin-top: -25px;" border="0" />
                            <asp:RequiredFieldValidator ID="revConfirmPasswordRequired" runat="server" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" Display="Dynamic" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup"><br />*Confirm Password is required.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regtxtConfirmCustomerPassword" runat="server" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Confirm Password must be between 6-32 characters </asp:RegularExpressionValidator>
                            <asp:CompareValidator ID="cvPasswordCompare" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="RegisterUserValidationGroup"><br />*The Password and Confirm Password must match.</asp:CompareValidator>

                        </div>
                        <div class="col-md-12 col-sm-12">
                            <asp:Label ID="lblUserUpdate" runat="server" Visible="false" CssClass="setGreen"></asp:Label>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <br />
                            <asp:Button ID="changePasswordButton" OnClick="enablePassword" runat="server" Text="Change Password" class="button_login1"></asp:Button>
                            <asp:Button ID="btnSaveAccountDetails" runat="server" ValidationGroup="RegisterUserValidationGroup" Text="Save" CssClass="button_login" OnClick="btnSaveAccountDetails_Click" TabIndex="4" Visible="false" />
                            <asp:Button ID="btnCancelPassword" Visible="false" runat="server" Text="Cancel" CssClass="button_login" OnClick="btnCancelPassword_Click" TabIndex="5" />
                            <br />
                            <br />
                        </div>

                    </div>
                </div>
            </div>
            <div class="failureForgotNotification displayNone" id="errorMessage"></div>
        </div>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            var Browser = {
                IsIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                Navigator: navigator.appVersion,
                Version: function () {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
            };
            if (Browser.IsIe && Browser.Version() <= 9) {
                $("#tblAccountDetails").addClass("alignleft");
            }
        });

        function checkPasswordMatch() {
            var password = $("[id$=txtPassword]").val();
            var confirmPassword = $("[id$=txtConfirmPassword]").val();

            if (password == confirmPassword)
                $("[id$=imgpass]").css("display", "");
            else
                $("[id$=imgpass]").css("display", "none");
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
