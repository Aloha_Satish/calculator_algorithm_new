﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Calculate.SignUp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <center>
    <div class="clearfix ">
        <br />
        <br />
        <br />
        <br />
        
        <h1 class="headfontsize text-center">Have you used the calculator before?</h1>
                <asp:Button ID="UserExists" OnClick="UserExists_Click" runat="server" class="button_login " width="200px" />
                <asp:Button ID="NewUser" runat="server" OnClick="NewUser_Click" class="button_login1" width="200px" style="margin-top:20px;" />
            <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
        </center>

    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#content').attr("style", "border:none;");
            this.history.forward(-1);
        });

    </script>
    <script type="text/javascript">
        history.pushState(null, null, 'SignUp.aspx');
        window.addEventListener('popstate', function (event) {
            history.pushState(null, null, 'SignUp.aspx');
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>

