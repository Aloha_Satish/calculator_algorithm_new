﻿using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class SingleUse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Navigate to next page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetStarted(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    Session["IsValidPage"] = "True";
                    Session[Constants.CustomerName] = txtFirstName.Text;
                    Session[Constants.CustomerEmail] = txtEmailAddress.Text;
                    Session[Constants.Password] = txtPassword.Text;
                    Session[Constants.ConfirmPassword] = txtConfirmPassword.Text;
                    Response.Redirect(Constants.RedirectRegister, false);

                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "customerSpouseinformation();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", "Single Use Error", MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

    }
}