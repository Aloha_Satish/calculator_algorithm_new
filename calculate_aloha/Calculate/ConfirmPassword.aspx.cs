﻿using CalculateDLL;
using Symbolics;
using System;
using System.Reflection;

namespace Calculate
{
    public partial class ConfirmPassword : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// code to redirect to login page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRedirectLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectLogin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.ErrorForgotPassword + MethodBase.GetCurrentMethod() + ex.Message;
                lblUserLoginError.Visible = true;
            }
        }

        #endregion Events
    }
}