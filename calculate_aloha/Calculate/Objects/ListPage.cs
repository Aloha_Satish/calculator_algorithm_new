﻿using Calculate.RestrictAppIncome;
using Symbolics;
using System;
using System.Data;

namespace Calculate
{
    public class ListPage
    {
        #region Variables
        private System.IO.TextWriter wtr;
        #endregion Variables

        #region Methods
        // List method
        public void List(System.Web.UI.ControlCollection ctls, int intAgeWife, int intAgeHusb, decimal decBeneWife, decimal decBeneHusb, bool colaChecked)
        {
            try
            {
                //  List the page to sequential
                wtr.WriteLine("Wife:    " + intAgeWife.ToString() + ", " + decBeneWife.ToString(Constants.AMT_FORMAT));
                wtr.Write("Husband: " + intAgeHusb.ToString() + ", " + decBeneHusb.ToString(Constants.AMT_FORMAT));
                if (colaChecked)
                    wtr.WriteLine(", COLA included");
                else
                    wtr.WriteLine(", No COLA");

                foreach (System.Web.UI.Control ctl in ctls)
                {
                    if (ctl is System.Web.UI.WebControls.Panel && (ctl.Visible == true))
                    {
                        foreach (System.Web.UI.Control pCtl in ctl.Controls)
                        {
                            if (pCtl is System.Web.UI.WebControls.Label)
                            {
                                System.Web.UI.WebControls.Label lCtl = (System.Web.UI.WebControls.Label)pCtl;
                                wtr.WriteLine(lCtl.Text);
                                if (lCtl.Text.StartsWith("NOTE:")) wtr.WriteLine("");
                            }
                            if (pCtl is System.Web.UI.WebControls.GridView && pCtl.ID != "GridSumm")
                            {
                                foreach (System.Web.UI.Control rCtl in pCtl.Controls[0].Controls)
                                {
                                    string rowLine = string.Empty;
                                    foreach (System.Web.UI.WebControls.TableCell tCtl in rCtl.Controls)
                                    {
                                        if (rowLine == string.Empty)
                                            rowLine = tCtl.Text;
                                        else
                                            rowLine += tCtl.Text.Replace("&nbsp;", "").PadLeft(11);
                                    }
                                    if (!rowLine.StartsWith("&")) wtr.WriteLine(rowLine);
                                }
                            }
                        }
                    }
                }
                wtr.Flush();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        // Close method
        public void Close()
        {
            wtr.Close();
        }

        // Constructor
        public ListPage()
        {
            wtr = new System.IO.StreamWriter("C:\\temp\\calculate.txt");
        }


        #endregion Methods
    }
}