﻿using CalculateDLL;
using CookComputing.XmlRpc;
using InfusionSoftDotNet;
using System;
using CalculateDLL;

namespace Calculate.Objects
{
    public class InsfisuionSoftProcesses
    {
        //declare our API Key Variable
        string key = "9bf2cf7af4d7ea0e53c08a582b44ff81";
        //Create an integer variable to hold our returned contact ID.
        int result = 0;

        //Specify our xmlrpc server URL and build the interface.
        [XmlRpcUrl("https://bq296.infusionsoft.com:443/api/xmlrpc")]
        public interface iFace : IXmlRpcProxy
        {
            [XmlRpcMethod("ContactService.add")]
            int Add(string key, XmlRpcStruct map);

            [XmlRpcMethod("ContactService.addToGroup")]
            bool AddGrp(string key, int conID, int grpID);

            [XmlRpcMethod("ContactService.addToCampaign")]
            bool AddCamp(string key, int conID, int campID);
        }



        /// <summary>
        /// Used to check the user with specified email is exist or not in InfusionSoft
        /// In other words used to check user have been successfully subscribed or not 
        /// </summary>
        /// <param name="email">User Email</param>
        /// <returns>returns the InfusionSoft Id if exists otherwise returns -1</returns>
        public Int32 GetInfusionIdFromEmail(string email)
        {
            try
            {
                string resultsString = string.Empty;
                string[] returnFields = { "Id" };
                XmlRpcStruct[] results = isdnAPI.findByEmail(email, returnFields);
                if (results.Length > 0)
                    return Convert.ToInt32(results[0]["Id"]);
                else
                    return -1;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("GetIdFromEmail - Error " + ex.ToString());
                return -1;
            }

        }

        /// <summary>
        /// Used to add newly registered user into Infusion Soft
        /// </summary>
        /// <param name="user">Infusion User info</param>
        /// <returns>returns true if user added into infusion</returns>
        public bool AddContactintoInfusionSoft(InfusionUser user)
        {
            try
            {
                //build the proxy for our xmlrpc interface
                iFace proxy = XmlRpcProxyGen.Create<iFace>();

                string Result = string.Empty;

                //Make sure our text boxes are not null.
                if (user.Name != null && user.LastName != null && user.Email != null)
                {
                    //Create a struct to hold the contact records data
                    XmlRpcStruct conDat = new XmlRpcStruct();
                    conDat.Add("FirstName", user.Name);
                    conDat.Add("LastName", user.LastName);
                    conDat.Add("Email", user.Email);
                   
                    //conDat.Add("Touchstone Client", "Yes");
                    //make the call to add the contact.
                    try
                    {
                        result = proxy.Add(key, conDat);
                        Result += "Contact added - ID: " + result + System.Environment.NewLine;
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError("AddContactintoInfusionSoft - Error " + ex.ToString());
                        return false;
                    }

                    #region Commented but now May be useful in futre
                    //now we will add the contact to any groups or campaigns that were checked.
                    //if we wanted to we could check if the calls return true/false to determine output.
                    //try
                    //{
                    //    if (news1.Checked)
                    //    {
                    //        //news1 is checked so add it to the correct group ID.
                    //        proxy.AddGrp(key, result, 91);
                    //        Results.Text += "Contact " + result + " added to group 91" + System.Environment.NewLine;
                    //    }
                    //    if (news2.Checked)
                    //    {
                    //        //news2 is checked so add it to the correct group ID.
                    //        proxy.AddGrp(key, result, 92);
                    //        Results.Text += "Contact " + result + " added to group 92" + System.Environment.NewLine;
                    //    }

                    //    if (camp1.Checked)
                    //    {
                    //        //camp1 is checked so add it to the correct campaign ID.
                    //        proxy.AddCamp(key, result, 21);
                    //        Results.Text += "Contact " + result + " added to campaign 21" + System.Environment.NewLine;
                    //    }
                    //    if (camp2.Checked)
                    //    {
                    //        //camp2 is checked so add it to the correct campaign ID.
                    //        proxy.AddCamp(key, result, 23);
                    //        Results.Text += "Contact " + result + " added to campaign 23" + System.Environment.NewLine;
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    //MessageBox.Show(ex.Message);
                    //}
                    #endregion Commented but now May be useful in futre

                }
                else
                {
                    //MessageBox.Show("Error: First Name, Last Name and Email are required!"); }
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("AddContactintoInfusionSoft - Error " + ex.ToString());
                return false;
            }

        }

    }
}