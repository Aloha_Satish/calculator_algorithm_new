﻿using System;
using System.Web.UI.WebControls;

namespace Symbolics
{
    public static class Constants
    {
        #region CSS for Table
        public const BorderStyle STD_BORDER = BorderStyle.Ridge;
        public const int STD_BORDER_WIDTH = 6;
        public const string AMT_FORMAT = "$###,###,##0";
        public const string HIGHLIGHT_70 = "yellowBG";
        public const string HIGHLIGHT_GREEN = "greenBG";
        #endregion

        #region MenuItems
        public const string MenuItem = "MenuItem";
        public const string LogOut = "Log Out";
        public const string Home = "Home";
        public const string About = "About";
        public const string FAQs = "FAQs";
        public const string Media = "Media";
        public const string MyAccount = "My Account";
        #endregion MenuItems

        #region Pdf Constants
        public const string pdfTableHeader = "#f9f9f9";
        public const string oddBackgroundColor = "#FFFFFF";
        public const string gridBackgroundColor = "#F5A9A9";
        public const string evenBackgroundColor = "#efefef";
        public const float smallIndentation = 20f;
        public const float mediumIndentation = -100f;
        public const float largeIndentation = 100f;
        public const string xAxisFormat = "$#,##0";
        public const int fontSize10 = 10;
        public const int fontSize12 = 12;
        public const int fontSize14 = 14;
        public const int fontSize16 = 16;
        public const int fontSize18 = 18;
        public const string yAxisSeriesLabel = "'s Monthly Benefit";
        public const float chartImageHeight = 400f;
        public const float chartImageWidth = 500f;
        public const float pdfImageWidth = 600f;
        public const int chartImageBorderWidth = 2;
        public const int chartWidth = 850;
        public const int chartHeight = 500;
        public const int widthNone = 0;
        public const int zero = 0;
        public const string URLString = "URLString";
        public const string DefaultBanner = "/Images/BannerRegular.png";
        public const string SessionBanner = "SetBanner";
        public const string pdfHomeImage = "Images/home_page.png";
        public const string pdfFooterImage = "Images/footer_logo.png";
        public const float pdfHomeImageHeight = 650f;
        public const string report = "Getting Paid to Wait Social Security Analysis Report";
        public const string preparedOn = "Prepared on: ";
        public const string preparedBy = "Prepared by: Brian Doherty";
        public const string pdfHeaderImage = "Images/header_pdf.png";
        public const float pdfHeaderImageHeight = 100f;
        public const string pdfHorizontalLine = "Images/hori_line.png";
        public const string DivorcePopup = "Explanation For Divorce Strategies";
        public const string SinglePopup = "Explanation For Single Strategies";
        public const string WidowPopup = "Explanation For Widowed Strategies";
        public const string MarriedPopup = "Explanation For Married Strategies";
        public const string keyFigures = "Key Benefit Numbers: ";
        public const string keyFigures1 = "'s paid to wait number";
        public const string keyFigures4 = " at 70: ";
        public const string keyFigures5 = " at age 69: ";
        public const string keyFigures2 = "'s Monthly Benefit at Age 70: ";
        public const string keyFigures3 = "'s survivor Benefit)";
        public const string keyFigures6 = "This is the amount of Social Security income ";
        public const string CummuText1 = " In other words, while they waited for ";
        public const string CummuText1s = " In other words, while ";
        public const string CummuText2 = " to claim maxed out Work History Benefit of ";
        public const string CummuText2s = " waited to claim a Spousal Benefit of ";
        public const string CummuText2sWidowSurvivor = " waited to claim Surviour Benefit of ";
        public const string CummuText2sWork = " waited to claim a maxed out Work History Benefit of ";
        public const string CummuText2Work = " waited to claim a Work History Benefit of ";
        public const string CummuText3 = " per month at age 70, ";
        public const string CummuText3s = " per month at age ";
        public const string CummuText4 = "they were paid ";
        public const string CummuText5 = " while they waited.";
        public const string MaxedOutBenefits = "'s maxed out Work History Benefit.";
        public const string CummuText4s = " was paid ";
        public const string CummuText5s = " while waiting.";
        public const string keyFigures11 = " highlighted in Column E";
        public const string keyFigures13 = " highlighted in Column C";
        public const string keyFigures14 = " highlighted in Column B";
        public const string keyFigures12 = " highlighted in Column F";
        public const string keyFigures15 = " highlighted in Column D";
        public const string keyFigures7 = " was Paid to Wait.";
        public const string keyFigures17 = " were Paid to Wait.";
        public const string keyFigures8 = "'s maxed out benefit, which will also become the Survivor Benefit after the first spouse dies.";
        public const string keyFigures10 = ", switches to maxed out Work History Benefit.";
        public const string keyFigures16 = "'s Full Retirement Age Work History Benefit.";
        public const string WidowSurviourStep1 = "The Survivor Benefit is reduced to ";
        public const string WidowSurviourStep2 = " per month because ";
        public const string Age70Text = " The size of the Work History Benefit at age 70 is larger than the amount that appears on your Social Security statement because of retro-active COLA credits. We assumed an annual COLA increase of 2.5%.";
        public const string WidowSurviourStep3 = " claimed it early.";
        public const string SurvivorBenefitsKey = "'s Survoir Benefit.";
        public const string SpousalBenefitsKey = "'s Spousal Benefit.";
        public const string WHBenefitsKey = "'s Work History Benefit.";
        public const string WorkBenefitsKey = "'s  maxed out Work History Benefit.";
        public const string keyFigures9 = "Combined Annual Income when claiming strategy is complete.";
        public const string steps = "Next Steps: ";
        public const string AnualIncomeKey = "Annual Income when claiming strategy is complete.";
        public const string AnualIncomeStep = "The Annual Income is ";
        public const string steps7 = " - ";
        public const string CommaandSpace = ", ";
        public const string when = " - When ";
        public const string ExSpouseName = "Ex-Spouse";
        public const string steps25 = " - ";
        public const string FRAText = " (Full Retirement Age)";
        public const string FRATextSurviour = " (Full Retirement Age for Survivor Benefit). ";
        public const string steps8 = " is age ";
        public const string steps6WHB = " claims maxed out Work History Benefit.";
        public const string SurvoirBenefitSwitch = " switches to Survivor Benefit of ";
        public const string ReducedWorkBenefit = " claims reduced Work History Benefit of ";
        public const string ReducedSpousalBenefit = " claims reduced Spousal Benefit of ";
        public const string steps9 = " claims Work History Benefit of ";
        public const string steps10 = " per month (Column B).";
        public const string WidowReducedWHB = " claims a reduced Survivor Benefit of ";
        public const string steps24 = " claims a Survivor Benefit of ";
        public const string steps24s = " claims a Survivor Benefit of ";
        public const string steps18 = " switches to maxed out Work History Benefit of ";
        public const string SpousalSwitch = " claims Spousal Benefit of ";
        public const string SpousalSwitch1 = " must wait for ";
        public const string SpousalSwitch2 = " to claim Work History Benefit first and then claim a Spousal Benefit.";
        public const string steps22 = " switches to Full Retirement age Work History Benefit of ";
        public const string steps11 = " claims and restricts benefit to only a Spousal Benefit and receives ";
        public const string steps12 = " per month (Column C).";
        public const string steps23 = " claims the maxed out Work History Benefit of ";
        public const string steps20 = " claims and restricts benefit to only a Survivor Benefit and receives ";
        public const string DivorceSurvior = " claims and restricts benefit to only a Spousal Benefit and receives ";
        public const string ClaimandSuspendWarning = " YOU MUST CLAIM AND SUSPEND YOUR BENEFIT BEFORE MAY 1, 2016.";
        public const string CliamAndSuspendHusb = ", Claims & Suspends Work History Benefit (* in Column C). You cannot Claim & Suspend until you reach your Full Retirement Age. ";
        public const string CliamAndSuspend3 = "By Claiming and Suspending, ";
        public const string CliamAndSuspendWife = ", Claims & Suspends Work History Benefit (* in Column B). You cannot Claim & Suspend until you reach your Full Retirement Age. ";
        public const string CliamAndSuspend1 = " does not receive any income and ";
        public const string CliamAndSuspend5 = " but it allows ";
        public const string CliamAndSuspend4 = "'s Work History Benefit grows by 8% per year up until age 70, ";
        public const string CliamAndSuspend2 = " to claim a Spousal Benefit.";
        public const string steps21 = " tell Social Security to restrict benefit to only a Spousal Benefit. You also have to wait until your Full Retirement Age before you can restrict benefit to only a Spousal Benefit and and you must also wait for your spouse to first Claim & Suspend their own benefit. ";
        public const string steps17 = " (Column E).";
        public const string MarriedSpousalText = "You must also wait for your spouse to first Claim & Suspend their own benefit before claiming a Spousal Benefit.";
        public const string ColumnC = " (Column C).";
        public const string While = "While ";
        public const string CliamAndSuspend6 = " receives only the Spousal Benefit, ";
        public const string CliamAndSuspend7 = " delays claiming the Work History Benefit and it grows by 8% per year up until age 70.";
        public const string ColumnB = " (Column B).";
        public const string DivorceSpouse = " tell Social Security to restrict benefit to only a Spousal Benefit.You have to wait until your Full Retirement Age or later to restrict your benefit to only a Spousal Benefit and your ex-spouse must be at an age that makes them eligible to receive Social Security Benefits. It isn't required that your ex-spouse be receiving benefits but only that they are eligible to receive them.";
        public const string DivorceSpouse1 = " While ";
        public const string DivorceSpouse2 = " receives only the Spousal Benefit, ";
        public const string DivorceSpouse3 = " delays claiming the Work History Benefit and it grows by 8% per year up until age 70.";
        public const string SpousalText = " You have to wait until your Full Retirement Age before you can restrict your benefit to only a Spousal Benefit and you must also wait for your spouse to first claim their own Work History Benefit.";
        public const string steps15 = " This benefit will eventually become the Survivor Benefit after the first spouse dies.";
        public const string steps13 = " It is very important that ";
        public const string steps14 = " tells Social Security to restrict benefit to only a Spousal Benefit.";
        public const string steps16 = "Their Combined Annual Income is ";
        public const string steps6 = " claims Work History Benefit.";
        public const string steps1 = " claims Spousal Benefit of ";
        public const string spousalswitch = " switches to Spousal Benefit of ";
        //public const string spousaltextdivorce = "In order to claim a Spousal Benefit, your ex-spouse must be at an age that makes them eligible to receive Social Security benefits. Normally, they become eligible to receive benefits when they are 62 years old or older. It isn't required that your ex-spouse be receiving benefits but only that they are eligible to receive them.";
        public const string spousaltextdivorce = "If you have been divorced for 2 years or longer, then in order to claim a Spousal Benefit your ex-spouse must only be at an age that makes them eligible to receive Social Security Benefits. Normally, they become eligible to receive Social Security Benefits when they are 62 years old or older. It isn't required that your ex-spouse be receiving benefits but only that they are eligible to receive them. If you have been divorced for less than 2 years, then your ex-spouse must actually be receiving their benefits before you can claim a Spousal Benefit.";
        public const string steps2 = " claims Spousal Benefit.";
        public const string stepsNewSpousal = " claims Spousal Benefit.";
        public const string stepsNewWorking = " claims Work History Benefit.";
        public const string steps4 = ",  claims and suspends at full retirement age. ";
        public const string steps3 = ", claims Working Benefit at age of 70.";
        public const string steps5 = ", claims Working Benefit.";
        public const string pdfContentPdfType = "application/pdf";
        public const string pdfContentType = "Content-type";
        public const string pdfFileName = "attachment;filename=Social_Security_Analysis_";
        public const string pdfFileNameForAdvisor = "inline;filename=Social_Security_Analysis_";
        public const string pdfFileExtension = ".pdf";
        public const string pdfContentDisposition = "content-disposition";
        public const string customerDob = "Date of Birth :                                 ";
        public const string customerFullRetirementAge = "Full Retirement Age :                    ";
        public const string customerName = "Customer Name :                           ";
        public const string customerPIA = "Full Retirement Age Benefit:      ";
        public const string PdfCustomerMaritalStatus = "Marital Status :                               ";
        public const string customerIncludeCOLA = "Annual Cost of Living Increase:";
        public const string PdfRobotoFontStyle = "Fonts/RobotoCondensed-Regular.ttf";
        public const string COLAPercentage = "2.5%";
        public const string And = " & ";
        public const string RestrKeyClaimToWork = " to claim Work History Benefit of ";
        public const string RestrKeyClaimToSpousal = " to claim Spousal Benefit of ";

        public const string RestrictExplain1 = " is age ";
        public const string RestrictExplain2 = " claims spousal benefit till age 69, which adds the extra income of ";
        public const string RestrictExplain3 = " by using the restricted application, this grows the cummulative income to ";


        public const string RestrictExplainNew1 = "This extra income is a result of ";
        public const string RestrictExplainNew2 = " being qualified to use the Restricted Application claiming strategy. This allows ";
        public const string RestrictExplainNew3 = " to claim and restrict the benefit to only a Spousal Benefit at age ";
        public const string RestrictExplainNew4 = ". All of those monthly Spousal Benefit payments appear in the <span style='color: green;'>Green</span> Box in the table. The cumulative total of all those payments is ";
        public const string RestrictExplainNew5 = " of extra Social Security income.";

        public const string ExplanationQuestion1 = "What is the Primary Goal of the Paid to Wait Calculator?";
        public const string ExplanationQuestion2 = "Many Married Couples Can Get Paid to Wait";
        public const string ExplanationQuestion3 = "Can All Married Couples Get Paid to Wait?";
        public const string ExplanationQuestion4 = "How the Paid to Wait Calculator Works";
        public const string ExplanationQuestion5 = "What if You Continue to Work?";
        public const string ExplanationQuestion6 = "What if You Want to Maximize the Size of Both Benefits?";
        public const string ExplanationQuestion7 = "What You Can Expect";
        public const string ExplanationQuestion8 = "What are the “Key Benefit Numbers” in the Strategies?";
        public const string ExplanationQuestion9 = "What are the “Next Steps” in the Strategies?";
        public const string ExplanationQuestion10 = "How do I Know if I am Qualified?";
        public const string ExplanationQuestion11 = "Can All Qualified Divorced Spouses Get Paid to Wait?";
        public const string ExplanationQuestion12 = "Many Divorced Spouses Can Get Paid to Wait";
        public const string ExplanationQuestion13 = "Many Widowed/Widower Spouses Can Get Paid to Wait";
        public const string ExplanationQuestion14 = "Can All Widowed/Widower Spouses Get Paid to Wait?";
        public const string ExplanationQuestion15 = "What if I Don’t Want to Wait Too Long?";



        public const string GoodLuckText = "Good Luck With your Claiming Strategy!";
        public const string ExplanationSteps1 = "This section tells you what benefit to claim and when you should claim them. All the numbered steps in this section correspond to the highlighted Ages and Benefit Numbers in the chart.With all of our strategies we assumed a 2.5% annual Cost of Living or COLA increase.";
        public const string ExplanationSteps2 = "We used a 2.5% COLA increase because that has been the approximate average COLA increase over the last 20 years.If some of the benefit numbers in our charts don't match the benefit numbers from your Social Security statement, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your benefits.";
        public const string ExplanationKeys1 = "1) Your Paid to Wait number - This is the amount of Social Security income you will receive while you wait to maximize the size of your benefit.";
        public const string ExplanationKeys2 = "2) Maxed out Benefit - The monthly dollar amount of your maxed out Social Security Benefit. This number will be larger than the age 70 benefit number that appears on your Social Security statement because of retro-active COLA credits. The benefit amounts on your Social Security statement for your Full Retirement Age and at age 70 are smaller than the actual amounts you will receive at those ages because Social Security does not factor in annual COLA increases. The calculator assumes an annual COLA increase of 2.5%";
        public const string ExplanationKeys3 = "3) Annual Income - When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. Your Social Security income will grow larger every year with annual COLA increases.";
        public const string ExplanationWorkingDivorce1 = "Depending on your situation, the calculator may show you a maximum of two claiming strategies, but in many situations it will show you only one claiming strategy. That ONE strategy will pay you the most amount of money while you wait to maximize the size of your benefit.For some divorced spouses, especially those with a very small benefit and their ex-spouse has a much larger benefit, there isn't a strategy that will pay them to wait.In that case the goal should be to maximize the size of the Spousal Benefit.";
        public const string ExplanationWorkingDivorce2 = "Each strategy will show you a chart of your Social Security Benefit numbers with an explanation.The explanation appears first and the chart is below or after the explanation. You may have to scroll down the page to see the chart with your benefit numbers. Their are a lot of numbers in the chart but only a few important numbers are highlighted.";
        public const string ExplanationWorkingDivorce3 = "If you focus on the highlighted numbers, the chart will be easier to understand. The first column in the chart is the Age column. Numbers or ages highlighted in that column tell you the age in which you should claim a Social Security Benefit. Usually more than one age is highlighted because you claim different Social Security benefits at different ages.";
        public const string ExplanationWorkingDivorce4 = "Only a couple of other numbers are highlighted outside of the Age column. These highlighted numbers are the actual amounts of the individual benefit you claim, your annual Social Security income amount when the strategy is complete and the total amount of Social Security income you were Paid to Wait. That Paid to Wait number is highlighted in the last column on the far right, the Cumulative Annual Income column. That column adds up all the Social Security income you received in the years prior to claiming your maxed out benefit.";
        public const string ExplanationTitleDivorce1 = "You maximize the size of your Social Security Work History Benefit if you wait until age 70 to claim it.This calculator can make it easier for you to do that by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait until age 70 to claim your maxed out benefit.";
        public const string ExplanationTitleText = "You really can get Paid to Wait.";
        public const string ExplanationWorkingWidow1 = "Depending on your situation, the calculator may show you a maximum of three different claiming strategies. In some situations it may only show your one claiming strategy. When the calculator shows you multiple claiming strategies, usually the first strategy or Strategy #1, is the Strategy that will pay you the most amount of money while you wait to maximize the size of your benefit. This strategy may involve claiming either your Work History Benefit as early as age 62 or your Survivor Benefit as early as age 60. If you plan on working past these ages, this may not be the best strategy because if you make too much money from your job, you may have to give back some or even all of your Social Security Benefits. If that is the case, you may want to consider one of the other suggested strategies.";
        public const string ExplanationWorkingWidow2 = "One of the other suggested strategies will wait until your Full Retirement Age before any benefits are claimed. Once you reach your Full Retirement Age, you can receive Social Security Benefit, continue to work at your job, make as much money as you want, and never have to give back any of your Social Security Benefit.";
        public const string ExplanationWorkingWidow3 = "In the event that you don't want to wait until age 70 to claim your maxed out benefit, many times one of the suggested strategies will show you how you can paid while you wait to claim your benefit at your Full Retirement Age.";
        public const string ExplanationWorkingWidow4 = "Please note this important difference between your Social Security Benefit:";
        public const string ExplanationWorkingWidow5 = "Your Work History Benefit is maxed out if you wait until age 70 to claim it. Your Survivor Benefit is maxed out if you wait until your Full Retirement Age to claim it (for most people their Full Retirement Age is age 66 or 67 or somewhere in between those two ages). So, while it makes sense to wait and claim your Work History Benefit after your Full Retirement Age, because for every year you delay after your Full Retirement Age your Work History Benefit grows by 8%, it doesn't make any sense to wait to claim your Survivor Benefit because it does not get any bigger after your Full Retirement Age. If the best strategy for your situation would be to maximize the size of your Survivor Benefit, then the suggested strategy will show it being claimed at your Full Retirement Age and not at age 70. ";
        public const string ExplanationTitleWidow1 = "You maximize the size of your Social Security Work History Benefit if you wait until age 70 to claim it.";
        public const string ExplanationTitleWidow2 = "You maximize the size of your Survivor Benefit if you wait until your Full Retirement Age to claim it ( for most people their Full Retirement Age is age 66 or 67 or somewhere in between those two ages).";
        public const string ExplanationTitleWidow3 = "Whether it makes more sense to maximize the size of your Work History Benefit or your Survivor Benefit, either way, this calculator can make it easier for you to do that by showing you the ONE strategy that will pay you the most amount of Social Security income while you wait to claim your maxed out benefit.";

        public const string ExplanationTitleMarried1 = "The primary goal of the Paid to Wait Calculator is to maximize the size of the bigger benefit. Most married couples receive two Social Security benefit checks, one made payable to the husband and the other made payable to the wife. They should try to maximize the size of the bigger benefit. Not only will that increase their Social Security income when they are both alive but it will also maximize the size of the Survivor Benefit. After the first spouse dies, the surviving spouse will only receive one Social Security benefit check, fortunately they can continue to receive the bigger of the two checks. Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit, which will leave the surviving spouse with the biggest Survivor Benefit check possible for their particular situation.";
        public const string ExplanationTitleMarried2 = "Most married couples receive two Social Security Benefits, one made payable to the husband and the other made payable to the wife.They should try to maximize the size of the bigger benefit. In order to do that, the spouse with the bigger Social Security Benefit must wait until age 70 to claim it.";
        public const string ExplanationTitleMarried4 = "This calculator can make it easier for you to do that by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while the spouse with the bigger benefit waits until age 70 to claim it.";
        public const string ExplanationWorkingMarried1 = "Depending on your situation, the calculator may show you a maximum of three different claiming strategies. The first strategy, or Strategy #1, is the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of the bigger benefit. This strategy may involve the spouse with the smaller Social Security Benefit claiming that benefit as early as possible, even at age 62.";
        public const string ExplanationWorkingMarried2 = "If the spouse with the smaller benefit plans on working past age 62, this may not be the best strategy because if they make too much money from their job, they may have to give back some or even all of their Social Security Benefit. If that is the case, you may want to consider one of the other suggested strategies.";
        public const string ExplanationWorkingMarried3 = "At most there will be two additional suggested strategies and sometimes there will be only one. Many times these other strategies will wait until your Full Retirement Age before any benefits are claimed. Once you reach your Full Retirement Age, you can receive Social Security Benefit, continue to work at your job, make as much money as you want and never have to give back any of your Social Security Benefit. These other strategies will usually pay you less Social Security income while you wait but you and your spouse will usually receive more Combined Annual Social Security income when the claiming strategy is completed.";
        public const string ExplanationWorkingMarried4 = "In other words, starting with the year when the strategy is completed, you and your spouse will receive a larger amount of Social Security income every year for the rest of your life. The trade off for receiving that larger amount of annual Social Security income is being paid less while you wait.";
        public const string ExplanationWorkingMarried5 = "At least one of the other suggested strategies will show you how you can get paid to wait while you maximize the size of both spouse's Social Security Benefit.";
        public const string ExplanationWorkingMarried6 = "In most cases this strategy will pay you the least amount while you wait, but because it maximizes the size of both benefits, when the claiming strategy is completed, it will pay you and your spouse the largest amount of Combined Annual Social Security income every year for the rest of your lives.";
        public const string ExplanationKeysMarried1 = "1) Your Paid to Wait number - This is the amount of Social Security income you will receive while you wait to maximize the size of the bigger benefit.";
        public const string ExplanationKeysMarried2 = "2) Maxed out bigger benefit - the monthly dollar amount of the maxed out bigger benefit. Another reason it is important to max out the size of the bigger benefit is because eventually that benefit will become the Survivor Benefit after the first spouse  dies. The surviving spouse is only going to receive one Social Security check after the first spouse dies, the Survivor Benefit.Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit.";
        public const string ExplanationKeysMarried3 = "3) Combined Annual Income - When the claiming strategy is completed, this shows the combined amount of Social Security income you and your spouse will receive every year for the rest of your life. This combined total will grow larger with annual COLA increases.";
        public const string ExplanationStepsMarried1 = "This section tells you what benefit to claim and when you should claim them.";
        public const string ExplanationStepsMarried2 = "With all of our strategies we assumed a 2.5% annual Cost of Living or COLA increase. We used a 2.5% COLA increase because that has been the approximate average COLA increase over the last 20 years.";
        public const string ExplanationStepsMarried3 = "If some of the benefit numbers in our charts, don't match the benefit numbers from your Social Security statement, especially your Full Retirement Age benefit and your benefit if wait until age 70,that is because Social Security does not make any COLA assumptions when calculating your benefits.";

        //Newly Added Married case explanaion variables.
        public const string ExplanationMarriedQuestion1Answer = "The primary goal of the Paid to Wait Calculator is to maximize the size of the bigger benefit. Most married couples receive two Social Security benefit checks, one made payable to the husband and the other made payable to the wife. They should try to maximize the size of the bigger benefit. Not only will that increase their Social Security income when they are both alive but it will also maximize the size of the Survivor Benefit. After the first spouse dies, the surviving spouse will only receive one Social Security benefit check, fortunately they can continue to receive the bigger of the two checks. Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit, which will leave the surviving spouse with the biggest Survivor Benefit check possible for their particular situation.";
        public const string ExplanationMarriedQuestion2Answer = "The Paid to Wait Calculator can help make it easier to maximize the size of the bigger benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait. In other words, while the spouse with the bigger benefit is waiting to claim it at age 70, so they can maximize the size of it, the Paid to Wait Calculator will show them the one claiming strategy that will pay them the most amount of Social Security income while they wait.";
        public const string ExplanationMarriedQuestion3Answer = "No, not all married couples can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait Calculator will show you exactly how to do it.";
        public const string ExplanationMarriedQuestion4Answer1 = "Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different suggested claiming strategies, sometimes it may only show you two strategies and in some cases it could only show you one claiming strategy.";
        public const string ExplanationMarriedQuestion4Answer2 = "Most married couples can get Paid to Wait. The first suggested claiming strategy shown, or Strategy 1, is usually the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of the bigger benefit. This strategy usually recommends that the spouse with the smaller benefit, claim their benefit as early as possible, even at age 62. ";
        public const string ExplanationMarriedQuestion4Answer3 = "Remember the primary goal is to maximize the size of the bigger benefit and claiming the smaller benefit early will pay the married couple some Social Security income while they wait. How much income they receive depends on the size of the smaller benefit. Also, they will probably receive the smaller benefit for the shortest time because once the first spouse dies, the surviving spouse will continue to receive only the bigger benefit, the smaller benefit check will stop.";
        public const string ExplanationMarriedQuestion4Answer4 = "If one or both of the spouses were born before January 2, 1954, they may be able to use the Restricted Application claiming strategy. This strategy could pay the married couple additional Social Security income, even up to $60,000 in extra income, while they wait. This strategy revolves around the strategic claiming of the Spousal Benefit. The Paid to Wait Calculator will automatically determine if you are eligible to use the Restricted Application strategy and then show you exactly how it would work in your particular situation.";
        public const string ExplanationMarriedQuestion5Answer = "If the spouse with the smaller benefit is younger than their Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between age 66 to 67), the Paid to Wait Calculator will show you a second strategy or Strategy 2. In this strategy the spouse with the smaller benefit will wait until their Full Retirement Age before they claim their regular Work History Benefit. It will show this strategy in case the spouse with the smaller benefit continues to work between the ages of 62 and their Full Retirement Age. If they claim their Social Security benefits prior to their Full Retirement Age and continue to work, they could earn too much money and have to give back some or all of their Social Security Benefits. BUT, once they reach their Full Retirement Age, they can receive their Social Security benefits,  continue to work and never have to give back any of their benefits no matter how much money they make from their job. That is why the Paid to Wait Calculator will show this strategy in which the spouse with the smaller benefit waits until their Full Retirement Age to claim it. ";
        public const string ExplanationMarriedQuestion6Answer = "The last strategy the Paid to wait Calculator will show – is the one that will maximize the size of both spouse’s benefits. When this claiming strategy is complete it will provide the married couple with the largest amount of Combined Annual Social Security Income for the rest of their lives.";
        public const string ExplanationMarriedQuestion7Answer = "Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit Numbers” section and the “Next Steps” section. ";
        public const string ExplanationMarriedQuestion8Answer1 = "1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of the bigger benefit. Most married couples will have a Paid to Wait number but in some situations they will not. ";
        public const string ExplanationMarriedQuestion8Answer2 = "2) Maxed Out Bigger Benefit – the monthly dollar amount of the maxed out bigger benefit. Another reason to max out the size of the bigger benefit is because eventually that benefit will become the Survivor Benefit after the first spouse dies. The surviving spouse is only going to receive one Social Security check after the first spouse dies, the Survivor Benefit. Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The Paid to Wait Calculator assumes an annual COLA increase of 2.5%.";
        public const string ExplanationMarriedQuestion8Answer3 = "3) Combined Annual Income – When the claiming strategy is complete, this shows the combined amount of Social Security income you and your spouse will receive every year for the rest of your lives. This combined benefit amount will also increase every year because of COLA adjustments.";
        public const string ExplanationMarriedQuestion9Answer1 = "This section tells you what benefit to claim and when you should claim it. All the numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all of the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA increase.";
        public const string ExplanationMarriedQuestion9Answer2 = "We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.";
        public const string ExplanationMarriedQuestion9Answer3 = "If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on you situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.";
        public const string ExplanationMarriedQuestion9Answer4 = "On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would only receive 4 months of benefit payments before your 67th birthday. On the chart those 4 months of benefit payments could be carried over to the next year and included in the total benefit amount you received at age 67. The chart will indicate this with a # (number sign) or a *  (asterisk) that you receive 16 months of benefit payments at age 67, which includes 4 months of payments at age 66 and 12 months of benefit payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.";
        public const string MarriedCouples = "Married Couples";

        public const string StrategyExpanationTitle = "Explanation of the Strategies";
        public const string StrategyExplanationNote = "p.s. – Don’t wait until the last minute to claim your benefits, make an appointment with your local Social Security office at least one month prior to the date you want your benefits to begin. ";
        public const string StrategyInstruction = "Before you scroll down to see the charts with your Social Security benefit amounts and the step-by-step explanation of exactly when you should claim your benefits for each suggested strategy --- PLEASE READ THIS SECTION ON THE “EXPLANATION OF THE STRATEGIES”.";

        //Newly Added Divorced case explanaion variables.
        public const string ExplanationDivorcedQuestion1Answer = "The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. Qualified divorced spouses could have a number of options available to them that could make it easier for them to maximize the amount of their benefit. They could even receive a  benefit based on the size of their ex-spouse’s Social Security benefit. The Paid to Wait Calculator will automatically determine if it makes sense for you to claim a benefit based on your ex-spouse’s benefit amount.";
        public const string ExplanationDivorcedQuestion2Answer = "The Paid to wait Calculator can help make it easier to maximize the size of your benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait. In other words, while you are waiting to claim your maxed out benefit at age 70, so you can maximize the size of it, the Paid to Wait Calculator will show you the one claiming strategy that will pay you the most amount of Social Security Income while you wait.";
        public const string ExplanationDivorcedQuestion3Answer = "No, not all qualified divorced spouses can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait calculator will show you exactly how to do it.";
        public const string ExplanationDivorcedQuestion4Answer1 = "If you were married for 10 years or more before your divorce and never remarried, then you are qualified to claim a Spousal Benefit and receive a benefit amount up to 50% or half of your ex-spouse’s Full Retirement Age benefit. This does not effect the size of the benefit your ex-spouse receives, they still receive their full benefit, but if you wait until your Full Retirement Age you are entitled to receive a benefit amount equal to 50% of your ex-spouse’s benefit.";
        public const string ExplanationDivorcedQuestion4Answer2 = "The Paid to Wait Calculator assumes that you meet the qualifications and will automatically determine if it makes sense for you to claim a benefit based on your ex-spouse’s benefit amount.";
        public const string ExplanationDivorcedQuestion4Answer3 = "If you were NOT married for at least 10 years before your divorce or you have remarried, you are NOT qualified and will have to change your marital status to either “Single” or “Married” and re-run the calculation.";
        public const string ExplanationDivorcedQuestion5Answer1 = "Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies, sometimes it may show you two strategies and in some cases it could only show you one claiming strategy.";
        public const string ExplanationDivorcedQuestion5Answer2 = "You could have a choice between claiming your own regular Work History Benefit or claiming a Spousal Benefit. The Spousal Benefit could pay you a benefit mount equal to 50% or half of your ex-spouse’s Full Retirement Age benefit. The Paid to Wait Calculator will determine which benefit you should try to maximize and then show you exactly how to do that. ";
        public const string ExplanationDivorcedQuestion5Answer3 = "In some situations the Paid to Wait Calculator may recommend claiming a particular benefit first and then switching to another benefit later. For example it could tell you to claim a Spousal Benefit at your Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67) and then later switch to your regular Work History Benefit. In other cases, it may recommend claiming your regular Work History Benefit early and then switching to a Spousal Benefit at your Full Retirement Age. If you qualify to switch from one benefit to another benefit, the Paid to Wait Calculator will show you and tell you exactly what benefit to claim and when to claim it. ";
        public const string ExplanationDivorcedQuestion5Answer4 = "In some situations, you may maximize the size of your benefit if you wait until age 70 to claim it. In that case, the Paid to Wait Calculator will always show you that strategy, but just in case you do not want to wait that long, it will also show you an additional strategy in which you claim your benefit at your Full Retirement Age (once again, your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67).";
        public const string ExplanationDivorcedQuestion5Answer5 = "If you were born before January 2, 1954, you may be able to use the Restricted Application claiming strategy. This strategy could pay you up to an extra $60,000 in Social Security income while you wait to claim your maxed out benefit. This strategy revolves around the strategic claiming of the Spousal Benefit. The Paid to Wait calculator will automatically determine if you are eligible to use the Restricted Application strategy and then show you exactly how it would work in your particular situation.";
        public const string ExplanationDivorcedQuestion6Answer = "Depending on your particular situation, the Paid to Wait Calculator may recommend a claiming strategy in which you claim a benefit early, even at age 62. In that case the Paid to Wait Calculator will also show you another strategy in which you wait until your Full Retirement Age before you claim a benefit.  It will show you this second strategy in case you decide to continue to work between the ages of 62 and your Full Retirement Age.  If you claim your Social Security benefits before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. That is why the Paid to Wait Calculator will show you this strategy in which you wait until your Full Retirement Age to claim your benefit.";
        public const string ExplanationDivorcedQuestion7Answer = "Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.";
        public const string ExplanationDivorcedQuestion8Answer1 = "1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of your benefit. Many divorced spouses will have a Paid to Wait number but some will not.";
        public const string ExplanationDivorcedQuestion8Answer2 = "2) Maxed Out Benefit – the monthly dollar amount of your maxed out benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.";
        public const string ExplanationDivorcedQuestion8Answer3 = "3) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.";
        public const string ExplanationDivorcedQuestion9Answer1 = "This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.";
        public const string ExplanationDivorcedQuestion9Answer2 = "We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.";
        public const string ExplanationDivorcedQuestion9Answer3 = "If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.";
        public const string ExplanationDivorcedQuestion9Answer4 = "On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.";
        public const string DivorcedSpouses = "Divorced Spouses";

        //Newly Added Widowed case explanaion variables.
        public const string ExplanationWidowedQuestion1Answer = "The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. Qualified widowed/widower spouses could have a number of options available to them that could make it easier for them to maximize the amount of their benefit. They could have a choice between claiming a Survivor Benefit or their own Work History Benefit. The Paid to Wait Calculator will automatically determine which benefit you should claim and when you should claim it.";
        public const string ExplanationWidowedQuestion2Answer = "The Paid to wait Calculator can help make it easier to maximize the size of your benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait. In other words, while you are waiting to claim either your maxed out Work History Benefit, or your maxed out Survivor Benefit, the Paid to Wait Calculator will show you the one claiming strategy that will pay you the most amount of Social Security Income while you wait.";
        public const string ExplanationWidowedQuestion3Answer = "No, not all widowed/widower spouses can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait calculator will show you exactly how to do it.";
        public const string ExplanationWidowedQuestion4Answer1 = "Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies, sometimes it may show you two strategies and in some cases it could only show you one claiming strategy.";
        public const string ExplanationWidowedQuestion4Answer2 = "You could have a choice between claiming your own regular Work History Benefit or claiming a Survivor Benefit. Your deceased spouse’s Social Security benefit is also your Survivor Benefit. You can claim a Survivor Benefit as early as age 60, but the amount of the benefit will be reduced, or you could wait until your Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between ages 66 and 67) and receive the full benefit amount. The Paid to Wait Calculator will determine which benefit you should try to maximize and then show you exactly how to do that. ";
        public const string ExplanationWidowedQuestion4Answer3 = "In some situations the Paid to Wait Calculator may recommend claiming a particular benefit first and then switching to another benefit later. For example it could tell you to claim a Survivor Benefit at your Full Retirement Age (Once again, your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67) and then later switch to your regular Work History Benefit. In other cases, it may recommend claiming your regular Work History Benefit early and then switching to a Survivor Benefit at your Full Retirement Age. If you qualify to switch from one benefit to another benefit, the Paid to Wait Calculator will show you and tell you exactly what benefit to claim and when to claim it. ";
        public const string ExplanationWidowedQuestion4Answer4 = "Most widowed/widower spouses can get Paid to Wait. The first suggested claiming strategy shown or Strategy 1, is usually the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of your benefit. Many times this strategy will recommend claiming a Survivor Benefit early, even at age 60, or it could recommend claiming a Work History Benefit as early as   age 62. You will collect this benefit for a number of years before the Paid to wait Calculator will tell you when to switch to a bigger benefit either at your Full Retirement Age or at age 70.";
        public const string ExplanationWidowedQuestion4Answer5 = "Remember the primary goal of the Paid to Wait Calculator is to maximize the size of your benefit. Claiming one of these benefits early will make it easier to do that because it could pay you a substantial amount of Social Security income while you wait to maximize the size of the other benefit. If this fits your situation, the Paid to Wait Calculator will show you and tell you exactly how to do it.";
        public const string ExplanationWidowedQuestion5Answer = "If the Paid to Wait Calculator recommends a claiming strategy in which you claim a benefit before your Full Retirement Age, it will also show you a second strategy or Strategy 2. In this strategy it will recommend that you wait until your Full Retirement Age to claim a benefit. It will show you this second strategy in case you decide to continue to work between the ages of 62 and your Full Retirement Age.  If you claim and receive Social Security benefits before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. That is why the Paid to Wait Calculator will show you this strategy in which you wait until your Full Retirement Age to claim a benefit.";
        public const string ExplanationWidowedQuestion6Answer = "In many cases in order to receive a maximum benefit you have to wait until age 70 to claim it and the calculator will show you that strategy. If your do not want to wait that long, the Paid to Wait Calculator will also show you one claiming strategy that will be completed when you claim your benefit at your Full Retirement Age, usually sometime between age 66 and 67. The Paid to Wait Calculator will automatically determine which benefit, your Survivor Benefit or your Work History Benefit, will be bigger at your Full Retirement Age. Many times you can even get paid while you are waiting to claim your benefit at your Full Retirement Age.";
        public const string ExplanationWidowedQuestion7Answer = "Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.";
        public const string ExplanationWidowedQuestion8Answer1 = "1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of your benefit. Many widowed/widower spouses will have a Paid to Wait number but some will not.";
        public const string ExplanationWidowedQuestion8Answer2 = "2) Maxed Out Benefit – the monthly dollar amount of your maxed out benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.";
        public const string ExplanationWidowedQuestion8Answer3 = "3) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.";
        public const string ExplanationWidowedQuestion9Answer1 = "This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.";
        public const string ExplanationWidowedQuestion9Answer2 = "We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.";
        public const string ExplanationWidowedQuestion9Answer3 = "If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.";
        public const string ExplanationWidowedQuestion9Answer4 = "On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and be included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.";
        public const string WidowedSpouses = "Widowed & Widower Spouses";

        //Newly added Single case explanation variables
        public const string SinglePeople = "Single People";
        public const string ExplanationSingleQuestion1Answer = "The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. It can help you do this by showing your benefit amounts at three different claiming ages and the effect that Retro-active COLA credits will have on increasing the size of your benefit when you delay claiming it. ";
        public const string ExplanationSingleQuestion2Answer1 = "Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies and in some cases it may show you only two strategies.";
        public const string ExplanationSingleQuestion2Answer2 = "The first strategy, Strategy 1, that the Paid to Wait Calculator will show you, is the claiming strategy in which you wait until age 70 to claim your benefit. By waiting until age 70 to claim your benefit you maximize the size of your benefit or receive the biggest benefit possible for the rest of your life. ";
        public const string ExplanationSingleQuestion2Answer3 = "In the case in which you do not want to wait until age 70 to claim your benefit, the second strategy, or Strategy 2, will show you the amount of your benefit if you wait until your Full Retirement Age to claim it. Your Full Retirement Age depends on the year in which you were born and can vary between age 66 and 67. The Paid to Wait Calculator will automatically determine your Full Retirement Age for you. Your Full Retirement Age benefit amount will be approximately 24% less than the benefit amount you would receive if you waited until age 70 to claim it.";
        public const string ExplanationSingleQuestion2Answer4 = "The last strategy, Strategy 3, will show you claiming your benefit as early as possible at age 62, or some age between age 62 and your Full Retirement Age. Claiming your benefit at age 62 will pay you the smallest benefit you can receive for the rest of your life.";
        public const string ExplanationSingleQuestion2Answer5 = "You can compare the three strategies and the benefit amount difference between them at various ages such as age 75, 80 or 85.";
        public const string ExplanationSingleQuestion3Answer = "Something to keep in mind if you claim your Social Security benefits at age 62 or before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. So if you plan on continuing to work prior to your Full Retirement Age, you may want to wait at least until your Full Retirement Age or later to claim your benefits.";
        public const string ExplanationSingleQuestion4Answer = "Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.";
        public const string ExplanationSingleQuestion5Answer1 = "1) Work History Benefit – the monthly dollar amount of your Work History Benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.";
        public const string ExplanationSingleQuestion5Answer2 = "2) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.";
        public const string ExplanationSingleQuestion6Answer1 = "This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.";
        public const string ExplanationSingleQuestion6Answer2 = "We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.";
        public const string ExplanationSingleQuestion6Answer3 = "If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.";
        public const string ExplanationSingleQuestion6Answer4 = "On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.";
        //Text for disclaimer
        public const string Disclaimer = "Disclaimer";
        public const string DisclPara1 = "This report and the strategies contained herein is intended to be an introduction to some of the Social Security claiming strategies that are most likely to play a role in your retirement planning. It is not intended to be a substitute for personalized advice from a professional advisor. The information presented in this report and on the web site is for educational and illustrative purposes only; it should not be relied upon in your specific circumstances. In addition, our intent is accuracy based on current law. Please keep in mind that Congress may change the law governing benefit amounts at any time.";
        public const string DisclPara2 = "Every effort has been made to ensure this report and web site is as accurate and complete as possible. However, no representations or warranties are made with respect to the accuracy or completeness of the report or web site. It is not our intent to provide professional tax, investment, or legal advice. The strategies contained herein may not be suitable for your situation. You should consult with a professional where appropriate. This report should be used only as a general guideline and not as the ultimate source of information about Social Security claiming strategies. ";
        public const string DisclPara3 = "The content of this report is intended to be informational and educational and DOES NOT constitute financial advice. We strongly recommend that you seek the advice of a tax, legal, and financial services professional before making decisions related to any investment or other financial decisions. Neither Filtech nor its development partners is responsible for the consequences of any decisions or actions taken in reliance upon or as a result of the information provided in this report or web site.";
        public const string DisclPara4 = "The text includes information about some of the tax consequences of Social Security benefits. While nothing contained within should be constructed as tax advice, any tax-related information is not intended or written to be used, and cannot be used, for the purpose of avoiding penalties under the Internal Revenue Code or promoting, marketing, or recommending to another party any transaction or matter addressed in this report.";
        public const string DisclPara5 = "Neither Filtech nor its development partners can be held responsible for any direct or incidental loss resulting from applying any of the information provided herein, or from any other source mentioned. The information provided does not constitute any legal, tax, or accounting advice.";



        public const string PaidtoWaitTextForPDF = "Paid To Wait Amount With This Strategy";
        public const string AnnualIncomeStratComplete = "Annual Income When Strategy is Complete";
        public const string AnnualIncomeStratCompleteCombined = "Combined Annual Income When Strategy is Complete";
        public static string PaidtoWaitTextForPDF1 = "Paid To Wait Amount With" + Environment.NewLine + " This Strategy";
        public static string AnnualIncomeStratComplete1 = "Annual Income " + Environment.NewLine + "When Strategy is Complete";
        public static string AnnualIncomeStratCompleteCombined1 = "Combined Annual Income" + Environment.NewLine + " When Strategy is Complete";
        public static string RestrictedAppIcomeTextForPDF = "Extra Income from" + Environment.NewLine + "Restricted Application";
        public static string RestrictedAppIcomeText = " - Extra Income From Restricted Application.";
        public const string Strategy = "Strategy ";
        #endregion

        #region Redirect to URL
        public const string id = "id";
        public const string RedirectInitialSocialSecurity = "~/CalFinancial/InitialSocialSecurityCalculator.aspx";
        public const string RedirectConfirmPassword = "~/ConfirmPassword.aspx";
        public const string RedirectChangePassword = "~/ChangePassword.aspx";
        public const string RedirectMyAccount = "~/CalFinancial/MyAccountDetails.aspx";
        public const string RedirectPricing = "~/Pricing.aspx";
        public const string RedirectLogin = "~/Home.aspx";
        public const string RedirectSignUp = "~/SignUp.aspx";
        public const string RedirectAdvisorRegistration = "~/AdvisorRegistration.aspx";
        public const string RedirectMonthlyAdvisorRegistration = "~/MonthlyAdvisor.aspx";
        public const string RedirectAnnualAdvisorRegistration = "~/AnnualAdvisor.aspx";
        public const string RedirectRegister = "~/Account/Register.aspx";
        public const string RedirectSingleUse = "~/SingleUse.aspx";
        public const string RedirectDemoUser = "~/DemoUser.aspx";
        public const string RedirectWelcomeUser = "~/Welcome.aspx";
        public const string RedirectWelcomeAdmin = "~/WelcomeAdmin.aspx";
        public const string RedirectCalcWidowed = "~/CalFinancial/CalcWidowed.aspx";
        public const string RedirectCalcMarried = "~/CalFinancial/Calculate.aspx";
        public const string RedirectCalcDivorced = "~/CalFinancial/CalcDivorced.aspx";
        public const string RedirectCalcSingle = "~/CalFinancial/CalcSingle.aspx";
        public const string RedirectManageCustomers = "~/SuperAdmin/ManageCustomers.aspx";
        public const string RedirectToAdvisorRecordsWithID = "~/SuperAdmin/AdvisorRecords.aspx?institutionAdminID=";
        public const string RedirectToAdvisorRecords = "~/SuperAdmin/AdvisorRecords.aspx";
        public const string RedirectManageCustomersWithAdvisorID = "~/SuperAdmin/ManageCustomers.aspx?advisorAdminID=";
        public const string RedirectSocialSecurityCalculator = "~/CalFinancial/SocialSecurityCalculator.aspx";
        public const string RedirectManageAdvisorWithInstitutionID = "ManageAdvisors.aspx?institutionAdminID=";
        public const string RedirectBackToAdvisor = "ManageAdvisors.aspx?institutionAdminID=";
        public const string RedirectBackToInstitution = "~/SuperAdmin/ManageInstitutions.aspx";
        public const string RedirectManageAdvisorWithAdvisorID = "ManageCustomers.aspx?advisorAdminID=";
        public const string RedirecthomeGrand = "~/Home.aspx?url=grand";
        public const string Redirecthome = "~/Home.aspx";
        public const string RedirectToManageAdvisor = "~/SuperAdmin/ManageAdvisors.aspx";
        public const string RedirectToLogin = "~/Login.aspx";
        public const string RedirectToGettingPaidToWaitHome = "http://www.gettingpaidtowait.com/";
        public const string RedirectToRestrictMarriedStrategy = "~/RestrictAppIncome/RestrictMarriedStrategy.aspx";
        #endregion

        #region Login Message
        public const string invalidEmail = "- Invalid Email Address";
        public const string invalidPassword = "- Invalid Password";
        public const string accountSuspend = "*Your Account has been Suspended. Please contact Adminstrator.";
        public const string accountDeleted = "*Your Account has been Deleted. Please contact Adminstrator.";
        public const string customerExists = "Customer already exists in the system.";
        public const string customerCreated = "Customer account created successfully.";
        #endregion

        #region UserRole
        public const string Role = "Role";
        public const string Customer = "Customer";
        public const string Advisor = "Advisor";
        public const string Institutional = "Institutional";
        public const string SuperAdmin = "SuperAdmin";
        #endregion UserRole

        #region MartialStatus
        public const string Married = "Married";
        public const string Single = "Single";
        public const string Widowed = "Widowed";
        public const string Divorced = "Divorced";
        #endregion MartialStatus

        #region Session Variable
        public const string SessionRole = "Role";
        public const string SessionUserId = "UserID";
        public const string SessionAdminId = "AdminID";
        public const string SessionRegistered = "Registered";
        public const string TempAdvisorID = "TempAdvisorID";
        public const string AllowUserForTemp = "AllowUserForTemp";
        #endregion

        #region Customer Table
        public const string CustomerFirstName = "Firstname";
        public const string CustomerId = "CustomerID";
        public const string CustomerLastName = "Lastname";
        public const string CustomerAdvisorId = "AdvisorID";
        public const string CustomerMaritalStatus = "MartialStatus";
        public const string CustomerBirthDate = "Birthdate";
        public const string CustomerSpouseFirstName = "SpouseFirstname";
        public const string CustomerSpouseLastName = "SpouseLastname";
        public const string CustomerSpouseBirthDate = "SpouseBirthdate";
        public const string CustomerActive = "Active";
        public const string HusbandsRetAgeBenefit = "HusbandsRetAgeBenefit";
        public const string WifesRetAgeBenefit = "WifesRetAgeBenefit";
        public const string InstitutionID = "InstitutionID";
        #endregion

        #region User Login Table
        public const string UserId = "UserId";
        public const string UserActiveFlag = "ActiveFlag";
        #endregion

        #region SecurityInfoMarried Table
        public const string SecurityInfoMarriedWifesBirthDate = "WifesBirthDate";
        public const string SecurityInfoMarriedWifesRetAgeBenefit = "WifesRetAgeBenefit";
        public const string SecurityInfoMarriedHusbandsBirthDate = "HusbandsBirthDate";
        public const string SecurityInfoMarriedHusbandsRetAgeBenefit = "HusbandsRetAgeBenefit";



        public const string SecurityInfoMarriedSurvivorBenefit = "SurvivorBenefit";
        public const string SecurityInfoMarriedFirstname = "Firstname";
        public const string SecurityInfoMarriedSpouseFirstname = "SpouseFirstname";
        public const string SecurityInfoCola = "Cola";
        #endregion

        #region SecurityInfoDivorced Table
        public const string SecurityInfoExSpousesBirthDate = "ExSpousesBirthDate";
        public const string SecurityInfoSlimyExsRetAgeBenefit = "SlimyExsRetAgeBenefit";
        public const string SecurityInfoMarrDivorceRetAgeBenefit = "RetAgeBenefit";
        #endregion

        #region SecurityInfoWidowed Table
        public const string SecurityInfoWidowedSpousesRetAgeBenefit = "SpousesRetAgeBenefit";
        public const string SecurityInfoRetAgeBenefit = "RetAgeBenefit";
        #endregion

        #region Common Variable in different Tables
        public const string UserDeleteFlag = "DelFlag";
        public const string UserEmail = "Email";
        public const string UserRole = "Role";
        public const string UserPassword = "Password";
        public const string PasswordResetFlag = "PasswordResetFlag";
        public const string EmailID = "EmailID";
        public const string UserRegistered = "UserRegistered";
        public const string value = "value";
        public const string DefaultDate = "1/1/1900 12:00:00 AM";
        public const string DateFormat = "MM/dd/yyyy";
        public const string stringDateFormat = "{0:MM/dd/yyyy}";
        public const string ActivateImage = "~/Images/stop.png";
        public const string DeactivateImage = "~/Images/unsuspend_new.png";
        #endregion

        #region Error Message List
        public const string ErrorBirthDateFutureMessage = "Your Birth Date should not be in future. <br>";
        public const string ErrorSpouseBirthDateFutureMessage = "Spouse Birth Date should not be in future. <br>";
        public const string ErrorNoDownloadingPdf = "No Social Security Report found for downloading.";
        #endregion

        #region Success Message List
        public const string SuccessCustomerUpdate = "Client details updated successfully.";
        public const string SuccessModuleUpdate = "Module Description updated successfully";
        public const string NewsLetterDeletedSucessfuly = "Newsletter Deleted Successfully";
        public const string NewsLetterAddedSucessfuly = "Newsletter Added Successfully";
        public const string CustomerAddedText = "Client added successfully";
        public const string DefaultAdvisorText = "Default Advisor details should not be Edited.";
        public const string DefaultInstituteDeletedText = "Default Istitution should not be Deleted from System.";
        public const string DefaultInstituteSuspendedText = "Default Istitution should not be Suspend";
        public const string InstituteStatusUpdated = "Institution status updated successfully";
        public const string DefaultInstituteEditedText = "Default Institution Datails should not be Edited.";
        public const string InstituteDeletedText = "Institution deleted successfully";
        public const string InstituteSuspendedText = "Suspended Institution Datails should not be Deleted.";
        public const string SuspendedInstituteEditedText = "Suspended Institution Datails should not be Edited.";
        public const string AdvisorAdded = "Advisor added successfully!";
        public const string CustomerUpdatedSucesfuly = "Customer status updated successfully";
        public const string NewsLetterUpdatedSucessfuly = "Newsletter Status Updated Successfully";
        public const string UserCreationError = "User creation failed, please try again!";
        public const string InstituteAddedSucesFuly = "Institution added successfully";
        public const string InstituteUpdatedSucesFuly = "Institution updated successfully";
        public const string CustomerDeletedSucesFuly = "Client deleted successfully";
        public const string CustomerUpdatedSucesFuly = "Client updated successfully";
        public const string DefaultAdvisorDeleteText = "Default Advisor should not be Suspended from System.";
        public const string CustomerAddedSuccessfuly = " Customer Assigned successfully. ";
        public const string AdvisorDeletedSucessfuly = "Advisor deleted successfully";
        public const string AdvisorUpdatedSucessfuly = "Advisor updated successfully";
        public const string LastAdvisorText = "There is No Another Advisor to Assign Customer";
        public const string DeleteAdvisorText = "Default Advisor should not be Deleted from System.";
        public const string AdvisorUpdated = "Advisor status updated successfully.";
        public const string BookDetailsDeleted = "Selected Book details deleted successfully";
        public const string MailSentSuccessfully = "Mail sent successfully to ";
        public const string AccountCreatedSuccessfully = "Your Social Security Calculator account has been created successfully. <br>";
        public const string AccountDetailFollows = "<br> <b>Account Details are as follows :</b> <br>";
        public const string HtmlNewLine = "<br>";
        public const string UserNmEmail = "User Name/Email : ";
        public const string Pasword_ = "Password :";
        public const string AccountDetails = "Account Details";
        #endregion

        #region Exception Message
        public const string ErrorRegister = "Register - Error - ";
        public const string ErrorAdvisorRegister = "AdvisorRegister - Error - ";
        public const string ErrorCommonVariable = "CommonVariable - Error - ";
        public const string ErrorCalc = "Calc - Error - ";
        public const string ErrorCalcSingle = "CalcSingle - Error - ";
        public const string ErrorCalculate = "Calculate - Error - ";
        public const string ErrorCalcWidowed = "CalcWidowed - Error - ";
        public const string ErrorCalcDivorced = "CalcDivorced - Error - ";
        public const string ErrorChangeUI = "ChangeUI - Error - ";
        public const string ErrorManageAdvisors = "ManageAdvisors - Error - ";
        public const string ErrorAdvisorsRecords = "AdvisorsRecords - Error - ";
        public const string ErrorManageCustomer = "ManageCustomer - Error - ";
        public const string ErrorManageInstitute = "ManageInstitute - Error - ";
        public const string ErrorNewsLetter = "NewsLetter - Error - ";
        public const string ErrorNewsLetterAdvisor = "NewsLetterAdvisor - Error - ";
        public const string ErrorThemes = "Themes - Error - ";
        public const string ErrorAbout = "About - Error - ";
        public const string ErrorOrderBookDetails = "OrderBookDetails - Error - ";
        public const string ErrorFAQ = "FAQ - Error - ";
        public const string ErrorForgotPassword = "ForgotPassword - Error - ";
        public const string ErrorHome = "Home - Error - ";
        public const string ErrorMedia = "Media - Error - ";
        public const string ErrorSiteMaster = "Site Master - Error - ";
        public const string ErrorSuccessStore = "SuccessStore - Error - ";
        public const string ErrorStore = "Store - Error - ";
        public const string ErrorWelcome = "Welcome - Error - ";
        public const string ErrorAdminMaster = "AdminMaster - Error - ";
        public const string ErrorClientMaster = "ClientMaster - Error - ";
        public const string ErrorWelcomeAdmin = "WelcomeAdmin - Error - ";
        public const string ExceptionSocialSecurityCalculator = "Social Security Calculator Details - Error - ";
        public const string ExceptionMyAccountDetails = "My Account Details - Error - ";
        public const string ExceptionInitialSocialSecurity = "InitialSocialSecurity - Error - ";
        public const string ExceptionCalcWidowed = "Social Security Calculation Widowed - Error - ";
        public const string ExceptionCalcMarried = "Social Security Calculation Married - Error - ";
        public const string ExceptionCalcDivorced = "Social Security Calculation Divorced - Error - ";
        public const string ExceptionManagInstitutions = "Managed Institutions - Error - ";
        public const string ExceptionManagedCustomers = "Managed Customers - Error - ";
        public const string ExceptionManagedAdvisors = "Managed Advisors - Error - ";
        public const string InvalidDate = "*Invalid Date!";
        public const string ErrorAgeExceeds = "*Your date of birth should not exceed 70 years.";
        public const string ErrorAgeFuture = "*Your date of birth cannot be in future.";
        public const string ErrorAgeExceedsSpouse = "*Spouse's date of birth should not exceed 70 years.";
        public const string ErrorAgeFutureSpouse = "*Spouse's date of birth cannot be in future.";
        public const string years = " years and ";
        public const string months = " months ";
        public const string ErrorCustomerSuccess = "CustomerSuccess - Error - ";
        public const string ErrorInstituteAdvisors = "InstituteAdvisors - Error - ";
        public const string TouchStoneID = "I201702240533089331001";
        public const string ErrorRestrictCalculate = "RestrictMarriedStrategy - Error - ";
        public const string ErrorRestrictAppUserInfo = "RestrictAppUserInfo - Error - ";
        #endregion

        #region HTML Attributes
        public const string HtmlAutoComplete = "AutoComplete";
        public const string HtmlOff = "Off";
        public const string HtmlValue = "Value";
        public const string HtmlChecked = "checked";
        #endregion

        #region Pdf Files Table
        public const string PdfFilesCustomerId = "CustomerID";
        public const string PdfFilesContentType = "ContentType";
        public const string PdfFilesName = "PdfName";
        public const string PdfFilesContent = "PdfContent";
        #endregion

        #region Button/Label Text Changed
        public const string ForgotPwdStatusSuccess = "Password sent successfully in your email address. Please check your inbox.";
        public const string ForgotPwdStatusFailure = "Failure to send password into Email. Please try again later.";
        public const string ForgotPwdBody = " . Please login your account with these credentials.";
        public const string ForgotPwdText = "Your account password is ";
        public const string ForgotPwdSubject = "Password Information";
        public const string EnterAgainEmailID = "Please enter the valid Email ID!";
        public const string InvalidEmailID = "Invalid Email Address";
        public const string BtnSocialSecurity = "Edit my Social Security Information";
        public const string LblEditHeaderInstitution = "Edit Institution";
        public const string LblAddHeaderInstitution = "Add Institution";
        public const string LblEditHeaderCustomer = "Edit Client";
        public const string LblAddHeaderCustomer = "Add Client";
        public const string LblEditHeaderAdvisor = "Edit Advisor";
        public const string LblAddHeaderAdvisor = "Add Advisor";
        public const string LblAddHeaderNewsLetter = "Add Newsletter";
        public const string LblEditHeaderTheme = "Edit Theme";
        public const string LblEditHeaderNewsLetter = "Edit Newsletter";
        public const string LblAddHeaderTheme = "Add Theme";

        #endregion

        #region Client Script
        public const string ClientScriptDatePicker = "<script type=\"text/javascript\">datePicker(); </script>";
        public const string ClientScriptLoadDatePicker = "Sys.Application.add_load(datePicker);";
        public const string ClientScriptDatePickerName = "DatePicker";
        public const string ClinetScriptSpouseInformationName = "Show/HideSpouseInfo";
        public const string ClinetScriptSpouseInformation = "showHideSpouseInformation();";
        public const string ClientScriptSuccessMessageName = "DisplaySuccessMessage";
        public const string ClientScriptSuccessMessage = "displayFadedMessage();";
        public const string ClientScriptdisplaySubscriptionPopUp = "displaySubscriptionPopUp";
        public const string ClientScripUserExist = "UserExistdisplayFadedMessage()";
        public const string ClientScripRemoveOverlay = "removeoverlay";
        public const string ClientScripRemoveLogoAttr = "RemoveImageLogoAttr();";
        public const string ClientScripRemoveLogoAttrName = "RemoveImageLogoAttr";
        #endregion

        #region Dropdown/GridView Default Value
        public const string DropDownInstitutionalDefault = "Select";
        public const string DropDownCustomerDefault = "Select";
        public const string DropDownAdvisorDefault = "Select";
        public const string GridViewDefaultInstitution = "No Institution Found";
        public const string GridViewDefaultAdvisor = "No Advisor Found";
        public const string GridViewDefaultCustomer = "No Client Found";
        public const string GridViewDefaultTheme = "No Theme Found";
        #endregion

        #region QueryString Variables
        public const string QueryStringAdvisorAdminID = "advisorAdminID";
        public const string QueryStringInstitutionAdminID = "institutionAdminID";
        public const string ModuleDataDescription = "Description";
        #endregion

        #region Sorting
        public const string SortingAscending = " ASC";
        public const string SortingDescending = " DESC";
        public const string FlagNo = "N";
        public const string sortDirection = "sortDirection";
        public const string FlagYes = "Y";
        #endregion

        #region URLS
        public const string TrialLiveServerURL = "https://ssc.chargify.com/h/3599634/subscriptions/new?reference=";
        public const string TrialTestServerURL = "https://socialsecuritycalculator.chargify.com/h/3462846/subscriptions/new?reference=";
        public const string WithoutTrialLiveServerURL = "https://ssc.chargify.com/h/3599633/subscriptions/new?reference=";
        public const string WithoutTrialTestServerURL = "https://socialsecuritycalculator.chargify.com/h/3460552/subscriptions/new?reference=";
        public const string TestStoreURL = "https://socialsecuritycalculator.chargify.com/h/3487874/subscriptions/new";
        public const string LiveStoreURL = "https://ssc.chargify.com/h/3599640/subscriptions/new";
        public const string LiveInstitutionFlatURL = "https://ssc.chargify.com/h/3599636/subscriptions/new?reference=";
        public const string TestInstitutionFlatURL = "https://socialsecuritycalculator.chargify.com/h/3537335/subscriptions/new?reference=";
        public const string LiveAdvisorFlatURL = " https://ssc.chargify.com/h/3599629/subscriptions/new?reference=";
        public const string TestAdvisorFlatURL = "https://socialsecuritycalculator.chargify.com/h/3537332/subscriptions/new?reference=";
        public const string LiveCustomerURL = "https://ssc.chargify.com/h/3599632/subscriptions/new?reference=";
        public const string TestCustomerURL = "https://socialsecuritycalculator.chargify.com/h/3537334/subscriptions/new?reference=";
        public const string LiveAdvisorURL = "https://ssc.chargify.com/h/3599638/subscriptions/new?reference=";
        public const string TestAdvisorURL = "https://socialsecuritycalculator.chargify.com/h/3537336/subscriptions/new?reference=";
        public const string NewSingleUseURL = "https://sscalculator.chargify.com/subscribe/ppq678mvvnn7/single-use-paid-to-wait-social-security-calculator?reference=";
        public const string NewSingleUseURLInfusion = "https://bq296.infusionsoft.com/app/orderForms/5b455720-565b-4e73-bd5f-5fccfe7ac467";
        public const string MonthlyUseInfusion = "https://bq296.infusionsoft.com/app/orderForms/24c67432-7eb6-4e20-9898-f1284f6c88ad";
        public const string AnnualUseInfusion = "https://bq296.infusionsoft.com/app/orderForms/eca4709e-3202-43b6-8805-63011b912cf1";
        #endregion URLS

        #region common variables
        public const string A = "A";
        public const string C = "C";
        public const string I = "I";
        public const string cummAgeAsap = "cummAgeAsap";
        public const string cummAgeStandard = "cummAgeStandard";
        public const string cummAgeSpouse = "cummAgeSpouse";
        public const string BannerImage = "BannerImage";
        public const string Strategy1 = "Strategy 1";
        public const string Strategy2 = "Strategy 2";
        public const string Strategy3 = "Strategy 3";
        public const string imgStatus = "imgStatus";
        public const string lblEmail = "lblEmail";
        public const string lblCreatedDate = "lblCreatedDate";
        public const string PaidtoWaitText = "Paid To Wait Amount With<br />This Strategy<br />";
        public const string lblAdvisorAdminID = "lblAdvisorAdminID";
        public const string title = "title";
        public const string Suspend = "Suspend";
        public const string ReAssign = "Re-Assign";
        public const string Activate = "Activate";
        public const string lblLastname = "lblLastname";
        public const string chkBxSelect = "chkBxSelect";
        public const string lblAdvisorName = "lblAdvisorName";
        public const string lblFirstname = "lblFirstname";
        public const string lblPassword = "lblPassword";
        public const string lblState = "lblState";
        public const string lblDealer = "lblDealer";
        public const string lblCity = "lblCity";
        public const string delete = "delete";
        public const string DeleteAdvisor = "DeleteAdvisor";
        public const string AssignandDelete = "Assign&Delete";
        public const string CustomerlblBirthdate = "lblBirthdate";
        public const string CustomerSpouselblBirthdate = "lblSpouseBirthdate";
        public const string CustomerSpouselblLastName = "lblSpouseLastname";
        public const string CustomerSpouselblFirstName = "lblSpouseFirstName";
        public const string CustomerlblMaritialStatus = "lblMaritalStatus";
        public const string CustomerlblWifesRetAgeBenefit = "lblWifesRetAgeBenefit";
        public const string CustomerlblHusbandsRetAgeBenefit = "lblHusbandsRetAgeBenefit";
        public const string CustomerlblClaimedPerson = "lblClaimedPerson";
        public const string CustomerlblClaimedAge= "lblClaimedAge";
        public const string CustomerlblClaimedBenefit = "lblClaimedBenefit";
        public const string Default = "Default";
        public const string DefaultAdvisorName = "DefaultAd";
        public const string CustomerEmailIdAlreadyExists = "alert('Registered Email already exists')";
        public const string lblInstitutionAdminID = "lblInstitutionAdminID";
        public const string lblInstitutionName = "lblInstitutionName";
        public const string showIntitution = "showIntitution";
        public const string Orelly = "ORelly";
        public const string Price = "50";
        public const string ComponentID = "47964";
        public const string Author = "Tim O\'Reilly";
        public const string HarperCollins = "HarperCollins";
        public const string btnUserExistsText = "I am already a User!";
        public const string btnUserExistsTextYes = "Yes,";
        public const string btnNewUserText = "I am a first time User!";
        public const string btnNewUserTextNo = "No,";
        public const string ErrorPDF = "Pdf Header Page - Error - ";
        public const string NOT_REC = " - Not recommended";
        public const string IsSubscription = "IsSubscription";
        public const string BtnChangeText = "Back to Social Security Information";
        public const string SubscriptionPopupText = "You could earn upto ";
        public const string WithoutTrialPeriod = "WithoutTrialPeriod";
        public const string TrialPeriod = "TrialPeriod";
        public const string Param0 = "Assuming ";
        public const string Param1 = "'s current age is ";
        public const string Param2 = " and full retirement age benefit is ";
        public const string Param3 = " /month.";
        public const string Param4 = "Assuming spouse's full retirement age benefit is ";
        public const string ParamExSpouse = "Assuming ex-spouse's full retirement age benefit is ";
        public const string BackSlash = "/";
        public const string OpeningBracket = "(";
        public const string CloseingBracket = ")";
        public const string MonthlyIncomeSingle = "Monthly Income";
        public const string AnnualIncome = "Annual Income";
        public const string CumulativeIncome = "Cumulative Income";
        public const string MonthlyIncome = "'s Monthly Income";
        public const string CombinedMonthlyIncome = "Combined Monthly Income";
        public const string CombinedAnnualIncome = "Combined Annual Income";
        public const string CumulativeAnnualIncome = "Cumulative Annual Income";
        public const string SurvivorMonthlyIncome = "Survivor's Monthly Income";
        public const string ZeroWithAsterisk = "$0*";
        public const string SingleAsterisk = "*";
        public const string DoubleAsterisk = "**";
        public const string SingleHash = "#";
        public const string ZeroWithOutAsterisk = "$0";
        public const string Number70 = "70";
        public const string Number69 = "69";
        public const string GridAsap = "GridAsap";
        public const string ClaimAndSuspendinFullAge = "ClaimAndSuspendinFullAge";
        public const string ClaimAndSuspendinFullValue = "ClaimAndSuspendinFullValue";
        public const string GridStandard = "GridStandard";
        public const string GridSpouse = "GridSpouse";
        public const string GridLatest = "GridLatest";
        public const string GridBestLater = "GridBestLater";
        public const string GridSuspendLater = "GridSuspendLater";
        public const string GridBest = "GridBest";
        public const string GridEarly = "GridEarly";
        public const string GridClaim67 = "GridClaim67";
        public const string GridZero = "ZeroStrategy";
        public const string Working = "Working";
        public const string spousal = "spousal";
        public const string Age = "Age";
        public const string DAsap = "DAsap";
        public const string DStandard = "DStandard";
        public const string DSpouse = "DSpouse";
        public const string DivorceAasapCumm = "DivorceAasapCumm";
        public const string DivorceStandardCumm = "DivorceStandardCumm";
        public const string DivorceSpouseCumm = "DivorceSpouseCumm";
        public const string FullStrategy = "FullStrategy";
        public const string MaxStrategy = "MaxStrategy";
        public const string BestStrategy = "BestStrategy";
        public const string JsFunc = "JsFunc";
        public const string one = "one";
        public const string two = "two";
        public const string three = "three";
        public const string DMaxCumm = "DMaxCumm";
        public const string BillingFirstNameKeyInURL = "&billing_first_name=";
        public const string BillingLastNameKeyInURL = "&billing_last_name=";
        public const string FirstNameKeyInURL = "&first_name=";
        public const string EmailKeyInURL = "&email=";
        public const string LastNameKeyInURL = "&last_name=";
        public const string InvalidDetails = "- Please enter the valid login details!";
        public const string Weekly = "Weekly";
        public const string Monthly = "Monthly";
        public const string NoNewsLetterFound = "No NewsLetter Found";
        public const string NewsLetterId = "NewsLetterId";
        public const string FlatCharge = "FlatCharge";
        public const string PerClient = "PerClient";
        public const string Text0 = "* Your Birth Month";
        public const string Text1 = "* Your Birth Day of Month";
        public const string Text2 = "* Your Birth Year";
        public const string SpouseText0 = "* Spouse's Birth Month";
        public const string SpouseText1 = "* Spouse's Birth Day of Month";
        public const string SpouseText2 = "* Spouse's Birth Year";
        public const string RegexExpression = @"^\d+$";
        public const string ColaStatusT = "T";
        public const string SMaxCumm = "SMaxCumm";
        public const string MartialStatus = "MartialStatus";
        public const string SAsap = "SAsap";
        public const string SStandard = "SStandard";
        public const string SLatest = "SLatest";
        public const string SingleAasapCumm = "SingleAasapCumm";
        public const string SingleStandardCumm = "SingleStandardCumm";
        public const string SingleLatestCumm = "SingleLatestCumm";
        public const string MaxCumm = "MaxCumm";
        public const string TryAgain = "Please try again!";
        public const string RadioChecked = "RadioChecked";
        public const string DownloadError = "Error in file Download!";
        public const string WMaxCumm = "WMaxCumm";
        public const string WidowAasapCumm = "WidowAasapCumm";
        public const string WidowClaim67Cumm = "WidowClaim67Cumm";
        public const string WidowSpouseCumm = "WidowSpouseCumm";
        public const string WidowLatestCumm = "WidowLatestCumm";
        public const string surviourValueAtAge66 = "surviourValueAtAge66";
        public const string surviourValueAtAge70 = "surviourValueAtAge70";
        public const string workingValueAtAge66 = "workingValueAtAge66";
        public const string workingValueAtAge70 = "workingValueAtAge70";
        public const string WAsap = "WAsap";
        public const string WClaim67 = "WClaim67";
        public const string WSpouse = "WSpouse";
        public const string WLatest = "WLatest";
        public const string reference = "ref";
        public const string AdvisorPlan = "plan";
        public const string Mode = "mode";
        public const string Demo = "Demo";
        public const string DemoPage = "DemoPage";
        public const string SingleUse = "SingleUse";
        public const string RestrSingle = "Single";
        public const string RestrOptional = "Optional";

        public const string Line1 = "1. ";
        public const string Line2 = "2. ";
        public const string Line3 = "3. ";
        public const string Line4 = "4. ";
        public const string Line5 = "5. ";
        public const string apiKey = "apiKey";
        public const string SharedKey = "SharedKey";
        public const string URL = "URL";
        public const string Password = "Password";
        public const string lblNewsLetterId = "lblNewsLetterId";
        public const string Key = "Key";
        public const string ShowOtherparams = "ShowOtherparams";
        public const string lblLetterUpdateBy = "lblLetterUpdateBy";
        public const string lblSubject = "lblSubject";
        public const string lblLetterBody = "lblLetterBody";
        public const string lblLetterPublishDate = "lblLetterPublishDate";
        public const string lblPeriodic = "lblPeriodic";
        public const string lblAdvisorList = "lblAdvisorList";
        public const string lblCustomersFlag = "lblCustomersFlag";
        public const string lblLetterStatus = "lblLetterStatus";
        public const string lblAdvisorsFlag = "lblAdvisorsFlag";
        public const string lblInstitutionList = "lblInstitutionList";
        public const string lblLetterNextDate = "lblLetterNextDate";
        public const string SelectInstitution = "Select Institution";
        public const string InstituteEmailExists = "alert('Registered Institute Email already exists')";
        public const string SelectCustomer = "Select Client";
        public const string GridBackColor = "#FADAB1";
        public const string lblCustomerID = "lblCustomerID";
        public const string lblMaritalStatus = "lblMaritalStatus";
        public const string CustomerLabel = "Social security information is not available for ";
        public const string HeaderA = "<B>A</B>";
        public const string HeaderB = "<B>B</B>";
        public const string HeaderC = "<B>C</B>";
        public const string HeaderD = "<B>D</B>";
        public const string HeaderE = "<B>E</B>";
        public const string HeaderF = "<B>F</B>";
        public const string HeaderBreak = "<Br/>";
        public const string CustomerName = "CustomerName";
        public const string CustomerEmail = "CustomerEmail";
        public const string txtHusbandBenefits = "txtHusbandBenefits";
        public const string txtWifeBenefits = "txtWifeBenefits";
        #endregion common variables

        #region Session Constants
        public const string alertMessage = "alertMessage";
        public const string AdvisorEmialExists = "alert('Registered Advisor Email already exists')";
        public const string Action = "Action";
        public const string StrInstitutionAdminID = "StrInstitutionAdminID";
        public const string AdvisorID = "AdvisorID";
        public const string ActionSuspend = "Action suspend";
        public const string AssignandSuspend = "Assign & Suspend";
        public const string SuspendAdvisor = "Suspend Advisor";
        public const string imgButton = "imgButton";
        public const string ChoosePlan = "ChoosePlan";
        public const string Remaining = " Remaining.";
        public const string AdvisorName = "AdvisorName";
        public const string SelectAdvisor = "Select Advisor";
        public const string WifeCurrentAge = "WifeCurrentAge";
        public const string HusbandCurrentAge = "HusbandCurrentAge";
        public const string lblUserID = "lblUserID";
        public const string BackGroundHeaderColor = "BackGroundHeaderColor";
        public const string BackGroundBodyColor = "BackGroundBodyColor";
        public const string BackGroundFooterColor = "BackGroundFooterColor";
        public const string InstitutionLogo = "InstitutionLogo";
        public const string InstitutionName = "InstitutionName";
        public const string classCss = "class";
        public const string textsize = "textsize";
        public const string headingettheme = "headingettheme";
        public const string ImgUrl = "data:image/jpeg;base64,";
        public const string changeBackground = "changeBackground('";
        public const string WelcomeAdmin = "WelcomeAdmin.aspx";
        public const string Grid = "Grid";
        public const string Sname = "Sname";
        public const string GridAsapDivorce = "GridAsapDivorce";
        public const string GridStandardDivorce = "GridStandardDivorce";
        public const string GridSpouseDivorce = "GridSpouseDivorce";
        public const string GridAsapDivorceCumm = "GridAsapDivorceCumm";
        public const string GridStandardDivorceCumm = "GridStandardDivorceCumm";
        public const string GridSpouseDivorceCumm = "GridSpouseDivorceCumm";
        public const string dhusband = "dhusband";
        public const string GridAsapSingle = "GridAsapSingle";
        public const string GridStandardSingle = "GridStandardSingle";
        public const string GridLatestSingle = "GridLatestSingle";
        public const string GridAsapSingleCumm = "GridAsapSingleCumm";
        public const string GridStandardSingleCumm = "GridStandardSingleCumm";
        public const string GridLatestSingleCumm = "GridLatestSingleCumm";
        public const string shusband = "shusband";
        public const string GridLatestWidow = "GridLatestWidow";
        public const string GridAsapWidow = "GridAsapWidow";
        public const string GridSpouseWidow = "GridSpouseWidow";
        public const string GridClaim67Widow = "GridClaim67Widow";
        public const string GridAsapWidowCumm = "GridAsapWidowCumm";
        public const string GridLatestWidowCumm = "GridLatestWidowCumm";
        public const string GridSpouseWidowCumm = "GridSpouseWidowCumm";
        public const string GridClaim67WidowCumm = "GridClaim67WidowCumm";
        public const string whusband = "whusband";
        public const string CombinedBenefit = "CombinedBenefit";
        public const string CombinedBenefitAge = "CombinedBenefitAge";
        public const string WifeBenefit = "WifeBenefit";
        public const string HusbandBenefit = "HusbandBenefit";
        public const string GridMarried = "GridMarried";
        public const string GridMarriedCumm = "GridMarriedCumm";
        public const string husband = "husband";
        public const string wife = "wife";
        public const string Husband = "Husband";
        public const string Wife = "Wife";
        public const string MarriedStrategy4 = "Claim Early Claim Late";
        public const string Age62 = "62.";
        public const string startingAgeOfHusb = "startingAgeOfHusb";
        public const string startingAgeOfWife = "startingAgeOfWife";
        public const string startingBenefitOfWife = "startingBenefitOfWife";
        public const string startingBenefitOfHusb = "startingBenefitOfHusb";
        public const string AgeAt70Husb = "AgeAt70Husb";
        public const string AgeAt70Wife = "AgeAt70Wife";
        public const string ValueAt70Wife = "ValueAt70Wife";
        public const string ValueAt70Husb = "ValueAt70Husb";
        public const string Wife70Value = "Wife70Value";
        public const string Husb70Value = "Husb70Value";
        public const string startingAgeOfHusbWithAsterisk = "startingAgeOfHusbWithAsterisk";
        public const string SuspendValueAt70Husb = "SuspendValueAt70Husb";
        public const string startingAgeOfWifeWithAsterisk = "startingAgeOfWifeWithAsterisk";
        public const string SuspendValueAt70Wife = "SuspendValueAt70Wife";
        public const string SuspendAgeAt70Husb = "SuspendAgeAt70Husb";
        public const string SuspendAgeAt70Wife = "SuspendAgeAt70Wife";
        public const string StartingValueForDivorce = "StartingValueForDivorce";
        public const string AgeWorkingBenefitDivorce = "AgeWorkingBenefitDivorce";
        public const string DivorceAnnualIncomeValue = "DivorceAnnualIncomeValue";
        public const string DivorceAnnualIncomeAge = "DivorceAnnualIncomeAge";
        public const string WidowAnnualIncomeValue = "WidowAnnualIncomeValue";
        public const string SingleAnnualIncomeValue = "SingleAnnualIncomeValue";
        public const string AgeWorkingBenefit = "AgeWorkingBenefit";
        public const string StartingValueForWidow = "StartingValueForWidow";
        public const string AgeWorkingBenefitSingle = "AgeWorkingBenefitSingle";
        public const string StartingValueForSingle = "StartingValueForSingle";
        public const string BestDisplay = "BestDisplay";
        public const string EarlySuspendCumm = "EarlySuspendCumm";
        public const string BestCumm = "BestCumm";
        public const string valueAtAge70Suspend = "valueAtAge70Suspend";
        public const string valueAtAge70Full = "valueAtAge70Full";
        public const string valueAtAge70Max = "valueAtAge70Max";
        public const string valueAtAge69Suspend = "valueAtAge69Suspend";
        public const string valueAtAge69Full = "valueAtAge69Full";
        public const string FullCumm = "FullCumm";
        public const string SuspendCumm = "SuspendCumm";
        public const string startingBenefitsValueHusbFull = "startingBenefitsValueHusbFull";
        public const string startingBenefitsValueHusb4 = "startingBenefitsValueHusb4";
        public const string startingBenefitsValueWifeFull = "startingBenefitsValueWifeFull";
        public const string startingBenefitsValueWife4 = "startingBenefitsValueWife4";
        public const string valueAtAge694 = "valueAtAge694";
        public const string valueAtAge704 = "valueAtAge704";
        public const string valueAtAge703 = "valueAtAge703";
        public const string valueAtAge693 = "valueAtAge693";
        public const string startingBenefitsValueWife3 = "startingBenefitsValueWife3";
        public const string startingBenefitsValueHusb3 = "startingBenefitsValueHusb3";
        public const string None = "None";
        public const string Show = "Show";
        public const string EarlySuspendDisplay = "EarlySuspendDisplay";
        public const string OdlerGreater = "OdlerGreater";
        public const string EarlyStrategy = "EarlyStrategy";
        public const string BestSrategy = "BestSrategy";
        public const string ZeroStrategy = "ZeroStrategy";
        public const string SuspendStrategy = "SuspendStrategy";
        public const string GridMax = "GridMax";
        public const string Claim67Strategy = "Claim67Strategy";
        public const string ConfirmPassword = "ConfirmPassword";
        public const string CustomerMonth = "CustomerMonth";
        public const string CustomerDay = "CustomerDay";
        public const string CustomerYear = "CustomerYear";
        public const string CustomerSpouseMonth = "CustomerSpouseMonth";
        public const string CustomerSpouseDay = "CustomerSpouseDay";
        public const string CustomerSpouseYear = "CustomerSpouseYear";
        public const string WidowAnnualAsap = "WidowAnnualAsap";
        public const string WidowAnnualClaim67 = "WidowAnnualClaim67";
        public const string WidowAnnualSpouse = "WidowAnnualSpouse";
        public const string WidowAnnualLatest = "WidowAnnualLatest";
        public const string DivorceAnualStandard = "DivorceAnualStandard";
        public const string DivorceAnualAsap = "DivorceAnualAsap";
        public const string DivorceAnualSpouse = "DivorceAnualSpouse";
        public const string GridSpouseDivorceCummAge = "GridSpouseDivorceCummAge";
        public const string GridStandardDivorceCummAge = "GridStandardDivorceCummAge";
        public const string MaxxCumm = "MaxxCumm";
        public const string SpousalBenefitValue = "SpousalBenefitValue";
        public const string ErrorAdvisorEmailExist = "Advisor email already exists!";
        #endregion Session Constants

        #region Standard Text for Divorce Case
        //public const string strClaimSwichSpousal = "If you have been divorced for 2 years or longer, then in order to claim a Spousal Benefit your ex-spouse must only be at an age that makes them eligible to receive Social Security Benefits. Normally, they become eligible to receive Social Security Benefits when they are 62 years old or older. It isn't required that your ex-spouse be receiving benefits but only that they are eligible to receive them. If you have been divorced for less than 2 years, then your ex-spouse must actually be receiving their benefits before you can claim a Spousal Benefit.";
        public const string strClaimRestrictSpousal1 = "It is very important that ";
        public const string strClaimRestrictSpousal2 = " tell Social Security to restrict benefit to only a Spousal Benefit. You have to wait until your Full Retirement Age or later to restrict benefit to only a Spousal Benefit, and if you have been divorced for 2 years or longer, then your ex-spouse must only be at an age that makes them eligible to receive Social Security Benefits. Normally, they become eligible to receive Social Security Benefits when they are 62 years old or older. It isn't required that your ex-spouse be receiving benefits but only that they are eligible to receive them. If you have been divorced for less than 2 years, then your ex-spouse must actually be receiving their Social Security benefits before you can claim a Spousal Benefit. While ";
        public const string strClaimRestrictSpousal3 = " receives only the Spousal Benefit, ";
        public const string strClaimRestrictSpousal4 = " delays claiming the Work History Benefit and it grows by 8% per year up until age 70.";
        #endregion Standard Text for Divorce Case

        #region Text for notes of Husband and Wife
        public const string includes = "* includes ";
        public const string strNote1 = " months of benefit payments for ";
        public const string strWHBNote = " months of Work History Benefit payments for ";
        public const string strSpousal_WHB1 = " months of Spousal Benefit amount of ";
        public const string strSpousal_WHB2 = " at age 69 and 12 months of Work History Benefit amount of ";
        public const string strSpousal_WHB3 = " at age 70 for ";
        #endregion Text for notes of Husband and Wife

        #region Advisor Agreement
        public const string AdvisorAgreement = "Advisor Single User Subscription Agreement";
        public const string AdvisorMainPara1 = "BY ACCEPTING THIS AGREEMENT, EITHER BY CLICKING A BOX INDICATING YOUR ACCEPTANCE OR BY EXECUTING AN ORDER FORM THAT REFERENCES THIS AGREEMENT, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY AND ITS AFFILIATES TO THESE TERMS AND CONDITIONS, IN WHICH CASE THE TERMS 'YOU' OR 'YOUR' SHALL REFER TO SUCH ENTITY AND ITS AFFILIATES. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SERVICES.";
        public const string AdvisorMainPara2 = "The Social Security Calculator at www.calculatemybenefits.com (hereinafter referred to as the “Web Site”) is comprised of various web pages operated by Filtech for the purpose of providing financial professionals with analysis and comparisons of Social Security and other retirement strategies. The Web Site is a subscription-based service offered to you (the subscriber) conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the Web Site constitutes your agreement to all such terms, conditions, and notices.";
        public const string AdvisorMainPara3 = "Filtech reserves the right to change the terms, conditions, and notices under which the Web Site is offered, including but not limited to the charges associated with the use of the Web Site.";
        public const string AdvisorMainTitle1 = "License Terms and Restrictions";
        public const string AdvisorMain1SubTitle1 = "Application License.";
        public const string AdvisorMain1SubTitle1Para = "Filtech hereby grants you a non-exclusive, non-transferable, worldwide right to use the Web Site during the term of this Agreement, solely for your own internal business purposes, subject to the terms and conditions contained herein. All rights not expressly granted to you are reserved by Filtech and its licensors. You are granted a single license and may use the Web Site for your professional practice, however, each additional user must obtain a separate license for their use. You may not access the Web Site if you are a direct competitor of Filtech, except with Filtech’s prior written consent. In addition, you may not access the Web Site for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes.";
        public const string AdvisorMain1SubTitle2 = "Additional License Restrictions.";
        public const string AdvisorMain1SubTitle2Para = "You shall not (i) license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Web Site in any way; (ii) modify or make derivative works based upon the Web Site; (iii) create Internet 'links' to the Web Site (except we may agree to provide you with a single sign-on link or account creation and login links) or 'frame' or 'mirror' any web pages or content on any other server or wireless or Internet-based device; or (iv) reverse engineer or access the Web Site in order to (a) build a competitive product or service, (b) build a product using similar ideas, features, functions or graphics of the Web Site, or (c) copy any ideas, features, functions or graphics of the Web Site. User licenses cannot be shared or used by more than one individual User but may be reassigned from time to time to new Users who are replacing former Users who have terminated employment or otherwise changed job status or function and no longer use the Web Site. You may use the Web Site only for your internal business purposes and shall not: (i) knowingly interfere with or disrupt the integrity or performance of the Web Site or the integrity of the data contained therein; or (ii) attempt to gain unauthorized access to the Web Site or its related systems or networks. Your access will be limited to information and portions of databases relating solely to your subscription.";
        public const string AdvisorMain1SubTitle3 = "Account Data.";
        public const string AdvisorMain1SubTitle3Para = "Filtech does not own any data, information or material that you submit to the Web Site in the course of using the planning tools ('Customer Data'). You, not Filtech, shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Customer Data, and Filtech shall not be responsible or liable for the deletion, correction, destruction, damage, loss or failure to store any Customer Data. In the event this Agreement is terminated, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination. Filtech reserves the right to withhold, remove and/or discard Customer Data without notice for any breach, including, without limitation, your non-payment. Upon termination for cause, your right to access or use Customer Data immediately ceases, and Filtech shall have no obligation to maintain any Customer Data.";
        public const string AdvisorMain1SubTitle4 = "Intellectual Property Ownership. ";
        public const string AdvisorMain1SubTitle4Para = "Filtech alone (and its licensors, where applicable) shall own all right, title and interest, including all related Intellectual Property Rights, in and to the Web Site, reports generated from the Web Site, and any updates or subsequent revisions thereof, and any other intellectual property of Filtech provided to the User, including but not limited to any patents, copyrights, trademarks, service marks, trade names, service names, or logos now owned or that may be owned in the future by Filtech.";

        public const string AdvisorMainTitle2 = "Use of the Web Site";
        public const string AdvisorMain2SubTitle1 = "Filtech Responsibilities. ";
        public const string AdvisorMain2SubTitle1Para = "We shall: (i) use commercially reasonable efforts to make the Web Site available 24 hours a day, 7 days a week, except for: (a) planned downtime (of which We shall provide prior notice), or (b) any unavailability caused by circumstances beyond our reasonable control, including without limitation, acts of God, acts of government, floods, fires, earthquakes, civil unrest, acts of terror, strikes or other labor problems, Internet service provider failures or delays, or denial of service attacks. Furthermore, we shall maintain appropriate administrative and technical safeguards for protection of the security, confidentiality and integrity of User data.  We shall not (a) modify User data, (b) disclose User data except as compelled by law in accordance with the Compelled Legal Disclosure section below or as expressly permitted in writing by you, or (c) access User data except to provide the services and prevent or address service or technical problems, or at your request in connection with customer support matters.";
        public const string AdvisorMain2SubTitle2 = "Compelled Legal Disclosure.";
        public const string AdvisorMain2SubTitle2Para = "Filtech reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in Filtech's sole discretion.";
        public const string AdvisorMain2SubTitle3 = "Financial Calculations and Informational Content.";
        public const string AdvisorMain2SubTitle3Para = "The Web Site offers financial calculations and informational content to assist financial professionals with Social Security and retirement planning. Filtech does not guarantee the accuracy of the calculations, information, reports, or their applicability to your client’s particular situation. Calculations are based upon federal laws/regulations and information you enter, and will change with each use and over time. Filtech accepts no liability for your use or misuse of the site. In no way does Filtech guarantee or represent that you will achieve the projected results illustrated on the Web Site.";
        public const string AdvisorMain2SubTitle4 = "Links to Third Party Sites.";
        public const string AdvisorMain2SubTitle4Para = "The Web Site may contain links to other web sites ('Linked Sites'). The Linked Sites are not under the control of Filtech and we are not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Filtech is not responsible for web casting or any other form of transmission received from any Linked Site. Filtech is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site or any association with its operators.";
        public const string AdvisorMain2SubTitle5 = "User Responsibilities.";
        public const string AdvisorMain2SubTitle5Para = "You shall (i) be responsible for compliance with this Agreement, (ii) be responsible for the accuracy, quality and legality of your data and of the means by which you acquired it, (iii) use commercially reasonable efforts to prevent unauthorized access to or use of the Web Site, and notify Filtech promptly of any such unauthorized access or use, and (iv) use the Web Site only in accordance with the applicable laws and government regulations. You shall not (a) make the Web Site available to anyone other than authorized Users, (b) sell, resell, rent or lease the Web Site, (c) use the Web Site to store or transmit infringing, libelous, or otherwise unlawful or tortious material, or to store or transmit material in violation of third-party privacy rights, (d) use the Web Site to store or transmit Malicious Code, (e) interfere with or disrupt the integrity or performance of the Web Site or third-party data contained therein, or (f) attempt to gain unauthorized access to the Web Site or their related systems or networks.";

        public const string AdvisorMainTitle3 = "Term and Termination";
        public const string AdvisorMain3SubTitle1 = "Initial Term and Renewal.";
        public const string AdvisorMain3SubTitle1Para = "The initial term will commence as of the date you accept this agreement. The Initial Term is one year payable monthly or annually. Upon the expiration of the Initial Term, this Agreement will automatically renew for successive renewal terms equal in duration to the Initial Term at Filtech's then current fees. Either party may terminate this Agreement, by notifying the other party in writing at least sixty (60) days prior to termination. In the case of free trials, notifications provided through the Web Site indicating the remaining number of days in the free trial shall constitute notice of termination. In the event this Agreement is terminated, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data, more than 30 days after termination.";
        public const string AdvisorMain3SubTitle2 = "Termination for Material Breach.";
        public const string AdvisorMain3SubTitle2Para = "Any breach of your payment obligations or unauthorized use of the Web Site will be deemed a material breach of this Agreement. Filtech, in its sole discretion, may terminate your password, account or use of the Web Site if you breach or otherwise fail to comply with this Agreement. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data more than 30 days after termination, if you have materially breached this Agreement, including but not limited to failure to pay outstanding fees, and such breach has not been cured within 30 days of notice of such breach. In the event this Agreement is terminated for cause, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination.";
        public const string AdvisorMain3SubTitle3 = "Free Trial.";
        public const string AdvisorMain3SubTitle3Para = "If you register for a free trial, we will make the Web Site available to you on a trial basis free of charge until the earlier of (a) the end of the free trial period for which you registered or are registering to use the Web Site or (b) the start date of any purchased services ordered by you. Additional trial terms and conditions may appear on the trial registration web page.  Any such additional terms and conditions are incorporated into this Agreement by reference and are legally binding.";

        public const string AdvisorMainTitle4 = "Fees and Payment Terms";
        public const string AdvisorMain4SubTitle1 = "Web Site Fees.";
        public const string AdvisorMain4SubTitle1Para1 = "Upon execution of this Agreement, Filtech shall invoice subscriber $40 monthly or $395 annually (the “Initial Subscription Fee”) in consideration for the services provided to subscriber during the initial term. All other services shall be invoiced in accordance with the following:";
        public const string AdvisorMain4SubTitle1Para2 = "Filtech will notify subscriber of any changes in its fee schedule, provided that any such changes will not apply to the Initial Term or the then-current renewal term.";
        public const string AdvisorMain4SubTitle2 = "Payment Terms.";
        public const string AdvisorMain4SubTitle2Para = "All invoices are due electronically at date of invoice subject to a thirty (30) day grace period from date of invoice if there is a technical difficulty (e.g., expired or invalid credit card). All payments shall be made in U.S. dollars and shall be non-refundable. If subscriber is delinquent in payments, Filtech may suspend or cancel access to Web Site.";

        public const string AdvisorMainTitle5 = "Limitations of Liability";
        public const string AdvisorMain5SubTitle1 = "Mutual Indemnification.";
        public const string AdvisorMain5SubTitle1Para1 = "You shall indemnify and hold Filtech, its licensors and each such party's parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that use of the Customer Data infringes the rights of, or has caused harm to, a third party; (ii) a claim, which if true, would constitute a violation by you of your representations and warranties; or (iii) a claim arising from the breach by you or your Users of this Agreement, provided in any such case that Filtech (a) gives written notice of the claim promptly to you; (b) gives you sole control of the defense and settlement of the claim (provided that you may not settle or defend any claim unless you unconditionally release Filtech of all liability and such settlement does not affect Filtech's business or Web Site); (c) provides to you all available information and assistance; and (d) has not compromised or settled such claim.";
        public const string AdvisorMain5SubTitle1Para2 = "Filtech shall indemnify and hold you and your parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that the Web Site directly infringes a copyright, a U.S. patent issued as of the Effective Date, or a trademark of a third party; (ii) a claim, which if true, would constitute a violation by Filtech of its representations or warranties; or (iii) a claim arising from breach of this Agreement by Filtech; provided that you (a) promptly give written notice of the claim to Filtech; (b) give Filtech sole control of the defense and settlement of the claim (provided that Filtech may not settle or defend any claim unless it unconditionally releases you of all liability); (c) provide to Filtech all available information and assistance; and (d) have not compromised or settled such claim. Filtech shall have no indemnification obligation, and you shall indemnify Filtech pursuant to this Agreement, for claims arising from any infringement arising from the combination of the Web Site with any of your products, service, hardware or business process(s).";
        public const string AdvisorMain5SubTitle2 = "Disclaimer of Warranties.";
        public const string AdvisorMain5SubTitle2Para = "FILTECH AND ITS LICENSORS MAKE NO REPRESENTATION, WARRANTY, OR GUARANTY AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE WEB SITE OR ANY CONTENT. FILTECH AND ITS LICENSORS DO NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE WEB SITE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS WILL BE CORRECTED, OR (F) THE WEB SITE OR THE SERVER(S) THAT MAKE THE WEB SITE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE WEB SITE AND ALL CONTENT IS PROVIDED TO YOU STRICTLY ON AN 'AS IS' BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW BY FILTECH AND ITS LICENSORS.";

        public const string AdvisorMainTitle6 = "General Provisions";
        public const string AdvisorMain6SubTitle1 = "Severability.";
        public const string AdvisorMain6SubTitle1Para = "If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law, the provision shall be modified by the court and interpreted so as best to accomplish the objectives of the original provision to the fullest extent permitted by law, and the remaining provisions of this Agreement shall remain in effect.";
        public const string AdvisorMain6SubTitle2 = "Governing Law.";
        public const string AdvisorMain6SubTitle2Para = "This Agreement will be governed by the laws of the state of New York.";
        public const string AdvisorMain6SubTitle3 = "Export Compliance.";
        public const string AdvisorMain6SubTitle3Para = "The Web Site and any other technology Filtech makes available, and derivatives thereof may be subject to export laws and regulations of the United States and other jurisdictions. Each party represents that it is not named on any U.S. government denied-party list. You shall not permit users to access or use the Web Site in a U.S.-embargoed country (currently Cuba, Iran, North Korea, Sudan or Syria) or in violation of any U.S. export law or regulation.";
        public const string AdvisorMain6SubTitle4 = "Relationship of the Parties.";
        public const string AdvisorMain6SubTitle4Para = "The parties are independent contractors. This Agreement does not create a partnership, franchise, joint venture, agency, fiduciary or employment relationship between the parties.";
        public const string AdvisorMain6SubTitle5 = "Entire Agreement.";
        public const string AdvisorMain6SubTitle5Para = "This Agreement, including all exhibits and addenda hereto and all Order Forms, constitutes the entire agreement between the parties and supersedes all prior and contemporaneous agreements, proposals or representations, written or oral, concerning its subject matter.";

        #endregion  Advisor Agreement

        #region Consumer subscription agreement

        public const string ConsumerAgreement = "Consumer Single User Subscription Agreement";

        public const string ConsumerMainPara1 = "BY ACCEPTING THIS AGREEMENT, EITHER BY CLICKING A BOX INDICATING YOUR ACCEPTANCE OR BY EXECUTING AN ORDER FORM THAT REFERENCES THIS AGREEMENT, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SERVICES.";
        public const string ConsumerMainPara2 = "The Social Security Calculator at www.calculatemybenefits.com (hereinafter referred to as the “Web Site”) is comprised of various web pages operated by Filtech for the purpose of providing consumers with analysis and comparisons of Social Security and other retirement strategies. The Web Site is a paid service offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the Web Site constitutes your agreement to all such terms, conditions, and notices.";
        public const string ConsumerMainPara3 = "Filtech reserves the right to change the terms, conditions, and notices under which the Web Site is offered, including but not limited to the charges associated with the use of the Web Site.";

        public const string ConsumerMainTitle1 = "License Terms and Restrictions";
        public const string ConsumerMain1SubTitle1 = "Application License.";
        public const string ConsumerMain1SubTitle1Para = "Filtech hereby grants you a non-exclusive, non-transferable, worldwide right to use the Web Site during the term of this Agreement, solely for your own informational purposes, subject to the terms and conditions contained herein. All rights not expressly granted to you are reserved by Filtech and its licensors. You are granted a single use license, however, each additional user must obtain a separate license for their use. You may not access the Web Site if you are a direct competitor of Filtech, except with Filtech’s prior written consent. In addition, you may not access the Web Site for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes.";
        public const string ConsumerMain1SubTitle2 = "Additional License Restrictions.";
        public const string ConsumerMain1SubTitle2Para = "You shall not (i) license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Web Site in any way; (ii) modify or make derivative works based upon the Web Site; (iii) create Internet 'links' to the Web Site or 'frame' or 'mirror' any web pages or content on any other server or wireless or Internet-based device; or (iv) reverse engineer or access the Web Site in order to (a) build a competitive product or service, (b) build a product using similar ideas, features, functions or graphics of the Web Site, or (c) copy any ideas, features, functions or graphics of the Web Site. User licenses cannot be shared or used by more than one individual User. You may use the Web Site only for your informational purposes and shall not: (i) knowingly interfere with or disrupt the integrity or performance of the Web Site or the integrity of the data contained therein; or (ii) attempt to gain unauthorized access to the Web Site or its related systems or networks. Your access will be limited to information and portions of databases relating solely to your subscription.";
        public const string ConsumerMain1SubTitle3 = "Account Data.";
        public const string ConsumerMain1SubTitle3Para = "Filtech does not own any data, information or material that you submit to the Web Site in the course of using the informational tools ('Customer Data'). You, not Filtech, shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Customer Data, and Filtech shall not be responsible or liable for the deletion, correction, destruction, damage, loss or failure to store any Customer Data. Filtech reserves the right to withhold, remove and/or discard Customer Data without notice for any breach, including, without limitation, your non-payment. Upon termination for cause, your right to access or use Customer Data immediately ceases, and Filtech shall have no obligation to maintain any Customer Data.";
        public const string ConsumerMain1SubTitle4 = "Intellectual Property Ownership. ";
        public const string ConsumerMain1SubTitle4Para = "Filtech alone (and its licensors, where applicable) shall own all right, title and interest, including all related Intellectual Property Rights, in and to the Web Site, reports generated from the Web Site, and any updates or subsequent revisions thereof, and any other intellectual property of Filtech provided to the User, including but not limited to any patents, copyrights, trademarks, service marks, trade names, service names, or logos now owned or that may be owned in the future by Filtech.";

        public const string ConsumerMainTitle2 = "Use of the Web Site";
        public const string ConsumerMain2SubTitle1 = "Filtech Responsibilities. ";
        public const string ConsumerMain2SubTitle1Para = "We shall: (i) use commercially reasonable efforts to make the Web Site available 24 hours a day, 7 days a week, except for: (a) planned downtime (of which We shall provide prior notice), or (b) any unavailability caused by circumstances beyond our reasonable control, including without limitation, acts of God, acts of government, floods, fires, earthquakes, civil unrest, acts of terror, strikes or other labor problems, Internet service provider failures or delays, or denial of service attacks. Furthermore, we shall maintain appropriate administrative and technical safeguards for protection of the security, confidentiality and integrity of User data.  We shall not (a) modify User data, (b) disclose User data except as compelled by law in accordance with the Compelled Legal Disclosure section below or as expressly permitted in writing by you, or (c) access User data except to provide the services and prevent or address service or technical problems, or at your request in connection with customer support matters.";
        public const string ConsumerMain2SubTitle2 = "Compelled Legal Disclosure.";
        public const string ConsumerMain2SubTitle2Para = "Filtech reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in Filtech's sole discretion.";
        public const string ConsumerMain2SubTitle3 = "Financial Calculations and Informational Content.";
        public const string ConsumerMain2SubTitle3Para = "The Web Site offers financial calculations and informational content to provide consumers with Social Security and retirement planning information. Filtech does not guarantee the accuracy of the calculations, information, reports, or their applicability to your particular situation. Calculations are based upon federal laws/regulations and information you enter, and will change with each use and over time. Filtech accepts no liability for your use or misuse of the site. In no way does Filtech guarantee or represent that you will achieve the projected results illustrated on the Web Site.";
        public const string ConsumerMain2SubTitle4 = "Links to Third Party Sites.";
        public const string ConsumerMain2SubTitle4Para = "The Web Site may contain links to other web sites ('Linked Sites'). The Linked Sites are not under the control of Filtech and we are not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Filtech is not responsible for web casting or any other form of transmission received from any Linked Site. Filtech is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site or any association with its operators.";
        public const string ConsumerMain2SubTitle5 = "User Responsibilities.";
        public const string ConsumerMain2SubTitle5Para = "You shall (i) be responsible for compliance with this Agreement, (ii) be responsible for the accuracy, quality and legality of your data and of the means by which you acquired it, (iii) use commercially reasonable efforts to prevent unauthorized access to or use of the Web Site, and notify Filtech promptly of any such unauthorized access or use, and (iv) use the Web Site only in accordance with the applicable laws and government regulations. You shall not (a) make the Web Site available to anyone other than yourself, (b) sell, resell, rent or lease the Web Site, (c) use the Web Site to store or transmit infringing, libelous, or otherwise unlawful or tortious material, or to store or transmit material in violation of third-party privacy rights, (d) use the Web Site to store or transmit Malicious Code, (e) interfere with or disrupt the integrity or performance of the Web Site or third-party data contained therein, or (f) attempt to gain unauthorized access to the Web Site or their related systems or networks.";

        public const string ConsumerMainTitle3 = "Term and Termination";
        public const string ConsumerMain3SubTitle1 = "Initial Term and Renewal.";
        public const string ConsumerMain3SubTitle1Para = "The initial term will commence as of the date you accept this agreement. The Initial Term is for a one-time use. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data at Filtech’s sole discretion.";
        public const string ConsumerMain3SubTitle2 = "Termination for Material Breach.";
        public const string ConsumerMain3SubTitle2Para = "Any breach of your payment obligations or unauthorized use of the Web Site will be deemed a material breach of this Agreement. Filtech, in its sole discretion, may terminate your password, account or use of the Web Site if you breach or otherwise fail to comply with this Agreement. ";
        public const string ConsumerMain3SubTitle3 = "Free Trial.";
        public const string ConsumerMain3SubTitle3Para = "If you register for a free trial, we will make the Web Site available to you on a trial basis free of charge until the earlier of (a) the end of the free trial period for which you registered or are registering to use the Web Site or (b) the start date of any purchased services ordered by you. Additional trial terms and conditions may appear on the trial registration web page.  Any such additional terms and conditions are incorporated into this Agreement by reference and are legally binding.";

        public const string ConsumerMainTitle4 = "Fees and Payment Terms";
        public const string ConsumerMain4SubTitle1 = "Web Site Fees.";
        public const string ConsumerMain4SubTitle1Para1 = "Upon execution of this Agreement, Filtech shall invoice subscriber $__________________ for a single-use in consideration for the services provided to subscriber.";
        public const string ConsumerMain4SubTitle2 = "Payment Terms.";
        public const string ConsumerMain4SubTitle2Para = "All invoices are due electronically at date of use. All payments shall be made in U.S. dollars and shall be non-refundable. If subscriber is delinquent in payments, Filtech may suspend or cancel access to Web Site.";

        public const string ConsumerMainTitle5 = "Limitations of Liability";
        public const string ConsumerMain5SubTitle1 = "Mutual Indemnification.";
        public const string ConsumerMain5SubTitle1Para1 = "You shall indemnify and hold Filtech, its licensors and each such party's parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that use of the Customer Data infringes the rights of, or has caused harm to, a third party; (ii) a claim, which if true, would constitute a violation by you of your representations and warranties; or (iii) a claim arising from the breach by you or your Users of this Agreement, provided in any such case that Filtech (a) gives written notice of the claim promptly to you; (b) gives you sole control of the defense and settlement of the claim (provided that you may not settle or defend any claim unless you unconditionally release Filtech of all liability and such settlement does not affect Filtech's business or Web Site); (c) provides to you all available information and assistance; and (d) has not compromised or settled such claim.";
        public const string ConsumerMain5SubTitle1Para2 = "Filtech shall indemnify and hold you harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that the Web Site directly infringes a copyright, a U.S. patent issued as of the Effective Date, or a trademark of a third party; (ii) a claim, which if true, would constitute a violation by Filtech of its representations or warranties; or (iii) a claim arising from breach of this Agreement by Filtech; provided that you (a) promptly give written notice of the claim to Filtech; (b) give Filtech sole control of the defense and settlement of the claim (provided that Filtech may not settle or defend any claim unless it unconditionally releases you of all liability); (c) provide to Filtech all available information and assistance; and (d) have not compromised or settled such claim. Filtech shall have no indemnification obligation, and you shall indemnify Filtech pursuant to this Agreement, for claims arising from any infringement arising from the combination of the Web Site with any of your products, service, hardware or business process(s).";
        public const string ConsumerMain5SubTitle2 = "Disclaimer of Warranties.";
        public const string ConsumerMain5SubTitle2Para = "FILTECH AND ITS LICENSORS MAKE NO REPRESENTATION, WARRANTY, OR GUARANTY AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE WEB SITE OR ANY CONTENT. FILTECH AND ITS LICENSORS DO NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE WEB SITE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS WILL BE CORRECTED, OR (F) THE WEB SITE OR THE SERVER(S) THAT MAKE THE WEB SITE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE WEB SITE AND ALL CONTENT IS PROVIDED TO YOU STRICTLY ON AN 'AS IS' BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW BY FILTECH AND ITS LICENSORS.";

        public const string ConsumerMainTitle6 = "General Provisions";
        public const string ConsumerMain6SubTitle1 = "Severability.";
        public const string ConsumerMain6SubTitle1Para = "If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law, the provision shall be modified by the court and interpreted so as best to accomplish the objectives of the original provision to the fullest extent permitted by law, and the remaining provisions of this Agreement shall remain in effect.";
        public const string ConsumerMain6SubTitle2 = "Governing Law.";
        public const string ConsumerMain6SubTitle2Para = "This Agreement will be governed by the laws of the state of New York.";
        public const string ConsumerMain6SubTitle3 = "Export Compliance.";
        public const string ConsumerMain6SubTitle3Para = "The Web Site and any other technology Filtech makes available, and derivatives thereof may be subject to export laws and regulations of the United States and other jurisdictions. Each party represents that it is not named on any U.S. government denied-party list. You shall not permit users to access or use the Web Site in a U.S.-embargoed country (currently Cuba, Iran, North Korea, Sudan or Syria) or in violation of any U.S. export law or regulation.";
        public const string ConsumerMain6SubTitle4 = "Relationship of the Parties.";
        public const string ConsumerMain6SubTitle4Para = "The parties are independent contractors. This Agreement does not create a partnership, franchise, joint venture, agency, fiduciary or employment relationship between the parties.";
        public const string ConsumerMain6SubTitle5 = "Entire Agreement.";
        public const string ConsumerMain6SubTitle5Para = "This Agreement, including all exhibits and addenda hereto and all Order Forms, constitutes the entire agreement between the parties and supersedes all prior and contemporaneous agreements, proposals or representations, written or oral, concerning its subject matter.";

        #endregion Consumer subscription agreement

        #region Procedure Names
        public const string updateAdvisorSubscription = "updateAdvisorSubscription";
        public const string updateCustomerSubscription = "updateCustomerSubscription";
        public const string updateInstitutionSubscription = "updateInstitutionSubscription";
        #endregion Procedure Names

        #region Advisor Plan
        public const string MonthlyPlan = "Monthly";
        public const string AnnualPlan = "Annual";
        #endregion Advisor plan

        #region Upload Advisors
        public const string ImportData = "Data Imported Successfully.";
        public const string PlzUploadValidFile = "Please upload a File with extension xls, xlsx, csv.";
        public const string PlzUploadFile = "Please select file first.";
        public const string Files = "~/Files/";
        public const string xls = ".xls";
        public const string xlsx = ".xlsx";
        public const string Excel07 = "Excel07+ConString";
        public const string RecordsExists = " Some records are already exist which haven't been uploaded.";
        public const string AdvisorUploaded = " advisors uploaded !";
        public const string PlzSelectOneRecord = "Please select at least one record!";
        public const string AllRecordsExists = "All selected records are already exist!";
        public const string SSC = "Social Security Calculator";
        //public const string YouRegistered = ", <br /><br />       You have been successfully registered with Social Security Calculator. <br />Your login is : " + objAdvisorTO.Email + "<br />Your password is : " + strPassword + " <br /> Please <a href='http://calculatemybenefits.com/?ref=login'>Click here</a> to login. <br /><br /> Thanks & Regards,<br /> Brian Doherty";
        public const string Hi = "Hi ";
        #endregion

    }
}