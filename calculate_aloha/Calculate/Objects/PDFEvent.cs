﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Reflection;
using System.Web;

namespace CalculateDLL
{
    /// <summary>
    /// PDF Event Listner for writing content accordingly
    /// </summary>
    public class PDFEvent : PdfPageEventHelper
    {
        #region Events
        /// <summary>
        /// Function call when pdf start from every new page
        /// </summary>
        /// <param name="pdfWriter"></param>
        /// <param name="pdfDoc"></param>
        public override void OnStartPage(PdfWriter pdfWriter, Document pdfDoc)
        {
            try
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~") + Constants.pdfHeaderImage);
                /* Changes in image properties */
                image.ScaleAbsolute(Constants.pdfImageWidth, Constants.pdfHeaderImageHeight);

                //image.Alignment = Element.ALIGN_CENTER;

                image.IndentationLeft = 0f;
                image.IndentationRight = 0f;
                image.Alignment = Element.ALIGN_CENTER;
                
                pdfDoc.Add(image);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorPDF, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// Function call when pdf reach at the end of page
        /// </summary>
        /// <param name="pdfWriter"></param>
        /// <param name="pdfDoc"></param>
        public override void OnEndPage(PdfWriter pdfWriter, Document pdfDoc)
        {
            try
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~") + Constants.pdfFooterImage);
                /* Changes in image properties */
                image.ScaleToFit(25f, 25f);
                image.Alignment = iTextSharp.text.Image.ALIGN_BOTTOM;
                PdfPTable footerTbl = new PdfPTable(1);
                footerTbl.TotalWidth = 125;
                footerTbl.HorizontalAlignment = Element.ALIGN_BOTTOM;
                PdfPCell cell = new PdfPCell(image);
                cell.Border = 0;
                cell.PaddingLeft = 15;
                cell.PaddingTop = 2;
                footerTbl.AddCell(cell);
                footerTbl.WriteSelectedRows(0, -1, 500, 40, pdfWriter.DirectContent);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorPDF, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }
        #endregion
    }
}
