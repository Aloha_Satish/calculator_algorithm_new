﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;

namespace Calculate
{
    public partial class FAQ : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                this.getModuleDetails();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorFAQ + "Page_Load()" + ex.ToString());
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Get Description of page
        /// </summary>
        public void getModuleDetails()
        {
            try
            { 
                ModuleData moduleData = new ModuleData();
                DataTable dtModuleData = moduleData.getModuleDataDescription(Constants.FAQs); ;
                divFAQ.InnerHtml = dtModuleData.Rows[0][Constants.ModuleDataDescription].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorFAQ + "getModuleDetails()" + ex.ToString());
            }
        }

        #endregion Methods
    }
}