﻿using CalculateDLL;
using Symbolics;
using System;
using System.Reflection;

namespace Calculate
{
    public partial class SignUp : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// code called when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Session.Clear();
                UserExists.Text = Constants.btnUserExistsTextYes + Environment.NewLine + Constants.btnUserExistsText;
                NewUser.Text = Constants.btnNewUserTextNo + Environment.NewLine + Constants.btnNewUserText;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// redirect to login when user exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UserExists_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectLogin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to redirect user to registration form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NewUser_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectRegister, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods
    }
}