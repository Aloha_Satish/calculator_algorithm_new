﻿using CalculateDLL;
using Symbolics;
using System;

namespace Calculate
{
    public partial class AdminMasterDemo : System.Web.UI.MasterPage
    {
        #region Events
        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ValidateSession();
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(Constants.ErrorAdminMaster + "Page_Load() - " + ex.ToString());
                }
            }
        }
        /// <summary>
        /// code to validate the seesion
        /// </summary>
        public void ValidateSession()
        {
            if (Session[Constants.SessionUserId] == null)
            {
                try
                {
                    Response.Redirect(Constants.RedirectLogin, false);
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(Constants.ErrorAdminMaster + "ValidateSession() - " + ex.ToString());
                }
            }
        }

        /// <summary>
        /// imgbtnWelcome_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnWelcome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectWelcomeAdmin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorAdminMaster + "imgbtnWelcome_Click() - " + ex.ToString());
            }
        }

        protected void imgbtnSocialSecurity_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorAdminMaster + "imgbtnSocialSecurity_Click() - " + ex.ToString());
            }
        }

        protected void imgbtnMyAccount_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectMyAccount, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorAdminMaster + "imgbtnMyAccount_Click() - " + ex.ToString());
            }
        }

        #endregion Events
    }
}