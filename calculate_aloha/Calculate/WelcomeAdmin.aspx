﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeBehind="WelcomeAdmin.aspx.cs" Inherits="Calculate.WelcomeAdmin" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="BodyContent">
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=btnWithoutTrial.ClientID%>').click(function () {
                $.unblockUI();
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=btnAdvisorFlat.ClientID%>').click(function () {
                $('#subscriptionAdvisor').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=btnAdvisorPerCLient.ClientID%>').click(function () {
                $.unblockUI();
                $('#subscriptionAdvisor').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=Button1.ClientID%>').click(function () {
                $.unblockUI();
                $('#subscriptionAdvisor').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CancelPopup.ClientID%>').click(function () {
                $.unblockUI();
                $('#subscriptionAdvisor').parent().appendTo($("form:first"));
                return true;
            });
            $('.blockPage').removeAttr('style');
            //$('.blockPage').addClass('subpop');
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <center>
        <br />
        <br />
        <br />
   <h1 class="headfontsize text-center">Welcome <asp:Label ID="lblWelcome" runat="server"></asp:Label> !</h1>
        <div style="margin-bottom:250px">
        Are you ready to find out how much you can get paid to wait?</div>
        <br />
        </center>
    <div id="subscription" class="modalPopup popupheight">
        <div class="modal-dialog modal-md">
            <div id="plans">
                <h2 class="h2PopUp modal-header colwhite">Select your account which fits your need?</h2>
                <div class="modal-body">
                    <div class="row padbot">
                        <div class="col-md-6">
                            <div style="opacity: 0.65;">
                                <h3 class="head3">Flat Charge</h3>
                                <div class="plan-content">
                                    <p>Subscribe Only Once </p>
                                    <ul>
                                        <li>User have to pay $100</li>
                                        <li>only once</li>
                                    </ul>
                                    <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnInstitutionFlat_Click" />
                                </div>
                                <!-- plan-content -->
                            </div>
                            <!-- plan -->
                        </div>
                        <div class="col-md-6">
                            <div style="opacity: 1;">
                                <h3 class="head3">Per Client Charge</h3>
                                <div class="plan-content">
                                    <p>Subscribe with every client</p>
                                    <ul>
                                        <li>User have to pay $60</li>
                                        <li>for every new </li>
                                    </ul>
                                    <asp:Button ID="btnWithoutTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnInstitutionPerClient_Click" />
                                </div>
                                <!-- plan-content -->
                            </div>
                            <!-- plan -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="subscriptionAdvisor" class="testmodalPopup text-center">
        <h1 style="font-weight: bold;">Select your account which fits your need?</h1>
        <div class="col-md-12 col-sm-12 text-center">
            <div class="col-md-6 col-sm-6 ">
                <div class=" plan-contentnew ">
                    <b>Monthly Charge</b>
                    <br />
                    <br />
                    Subscribe by paying $40.00 per month!
                    <br />
                    <br />
                    <asp:Button ID="btnAdvisorFlat" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnAdvisorFlat_Click" />&nbsp;&nbsp;<asp:Button ID="CancelPopup" OnClick="Cancel_Click" runat="server" CssClass="button_login" Text="Cancel" />
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class=" plan-contentnew ">
                    <b>Annual Charge</b>
                    <br />
                    <br />
                    Subscribe by paying $395.00 per year!
                    <br />
                    <br />
                    <asp:Button ID="btnAdvisorPerCLient" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnAdvisorPerClient_Click" />&nbsp;&nbsp;<asp:Button ID="Button1" OnClick="Cancel_Click" runat="server" CssClass="button_login" Text="Cancel" />
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
            </div>
        </div>
    </div>
    <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    <%--<div id="subscriptionAdvisor" class="testmodalPopup">
            <div class="plan-contentnew">
                <h2 style="font-weight: bold;">Select your account which fits your need?</h2>
                                <h3 >Flat Charge</h3>
                               
                                    Subscribe Only Once
                                    <br />
                                        User have to pay $50
                                    <br />
                                        only once
                                    <br />
                                    <asp:Button ID="btnAdvisorFlat" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnAdvisorFlat_Click" />
                               </div>
         <div class="plan-contentnew">
                                <h3 >Per Client Charge</h3>
                              
                                    Subscribe with every new
                                    <br />
                                        User have to pay $30<br />
                                        for every new 
                                    <br />
                                    <asp:Button ID="btnAdvisorPerCLient" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnAdvisorPerClient_Click" />
                                
                     </div>
    </div>--%>
</asp:Content>

