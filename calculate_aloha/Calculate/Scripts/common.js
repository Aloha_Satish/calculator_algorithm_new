﻿/* Menu Selection */
$1_7_1(function () {
    $1_7_1(window).hashchange(function () {

        if (document.URL.indexOf("/Home.aspx") > 0) {
            $("#homeMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#home").css("color", "#333333");
        }
        if (document.URL.indexOf("/Welcome.aspx") > 0) {
            $("#welcomeMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
        }
        if (document.URL.indexOf("/WelcomeAdmin.aspx") > 0) {
            $("#welcomeAdminMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
        }
        if (document.URL.indexOf("/About.aspx") > 0) {
            $("#aboutMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#aboutus").css("color", "#333333");

        }
        if (document.URL.indexOf("/FAQ.aspx") > 0) {
            $("#faqMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#faq").css("color", "#333333");

        }
        if (document.URL.indexOf("/Media.aspx") > 0) {
            $("#mediaMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#media").css("color", "#333333");
        }
        if (document.URL.indexOf("/Store.aspx") > 0) {
            $("#storeMenu").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#store").css("color", "#333333");
        }

        if (document.URL.indexOf("/Register.aspx") > 0) {
            $("#login").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
        }
        if (document.URL.indexOf("/SignUp.aspx") > 0) {
            $("#signup").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#ctl00_A3").css("color", "#333333");
        }
        if (document.URL.indexOf("/SignUp.aspx") > 0) {
            $("#logout").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
        }
        if (document.URL.indexOf("/ChangePassword.aspx") > 0) {
            $("#changepassword").css("background", "url('/images/butbg.png') repeat-x scroll left top rgba(0, 0, 0, 0)");
        }
        if (document.URL.indexOf("/CalFinancial/MyAccountDetails.aspx") > 0) {
            $("#myaccount").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#ctl00_ctl00_A4").css("color", "#333333");
        }
        if (document.URL.indexOf("/Pricing.aspx") > 0) {
            $("#pricing").css("background", "url('/images/butbg.png') repeat scroll left top rgba(0, 0, 0, 0)");
            $("#pricing").css("color", "#333333");
        }

        /* Write Code for selected item in left menu */
        if ($("[id$=ClientMenu] ul li a") != null) {
            $("[id$=ClientMenu] ul li a").each(function () {
                var url = $(this).attr("href");
                var selectedTab = false;
                if (document.URL.indexOf(url) > 0) {
                    selectedTab = true;
                }
                if (document.URL.indexOf("/Manage") != -1 && url.indexOf("Manage") != -1) {
                    selectedTab = true;
                }
                if ((document.URL.indexOf("SocialSecurityCalculator.aspx") != -1 || document.URL.indexOf("CalcSingle.aspx") != -1 || document.URL.indexOf("Calculate.aspx") != -1 || document.URL.indexOf("CalcWidowed.aspx") != -1 || document.URL.indexOf("CalcDivorced.aspx") != -1) && url.indexOf("InitialSocialSecurityCalculator") != -1) {
                    selectedTab = true;
                }
                if (selectedTab) {
                    $(this).attr("id", "selectLeftMenuTab");
                }
            });
        }
    })
    $1_7_1(window).hashchange();
});

function HideEmptyTextLabel() {
    $('.spaceInKeysAndSteps input').filter(function () {
        return $(this).val().trim() == '';
    }).hide();
}





/* Manage Theme */
function changeBackground(footercolor, headerColor, backgroundColor, image) {

    //$("#navigation").css("background", headerColor);
    ////if (screen.width >= 768) {
    //$("#navigatebg").css("background", headerColor);
    //$(".footerbg").css("background-color", footercolor);

    //Newly added
    $("#newcompnylogo").removeAttr();
    $("#newcompnylogo1").removeAttr();
    $("#newcompnylogo1").css("display", "none");
    if (image != '') {
        $("#newcompnylogo").css("width", "200px");
        $("#newcompnylogo").css("height", "90px");
        $("#newcompnylogo").attr("src", image);
    }
    else { $("#newcompnylogo").css("display", "none"); }
    //footer background color
    $(".mainbodydiv").css("background-color", backgroundColor);//body background color
    $(".menuheader").css("background-color", headerColor);//header background color
    $("#ctl00_ctl00_footer").css("background-color", footercolor);//header background color
    $("#ctl00_footer").css("background-color", footercolor);//header background color
    //}
    //$("#ctl00_ctl00_wrapper").css("background-color", backgroundColor);
    //$("#ctl00_ctl00_CompanyLogo").attr("src", image);
    //$("#ctl00_footer").css("background-color", footercolor);
    //$("#navigation").css("background-color", headerColor);
    //$("#ctl00_wrapper").css("background-color", backgroundColor);
    //$("#bodybg").css("background-color", "white");
    //$("#ctl00_CompanyLogo").attr("src", image);
    //$("#ctl00_CompanyLogo").css("width", "144px");
    //$("#ctl00_ctl00_CompanyLogo").css("width", "144px");
}

//Removes company's logo from header
function RemoveImageLogoAttr() {
    $("#newcompnylogo").css("display", "none");
}


/*function to chage the banner on the redirection on home page from specific pages*/
function ChangeBanner(image) {
    $("#Imgs").removeAttr('src');
    $("#Imgs").attr("src", image);
}

/* Show textbox color for Theme in Edit Mode */
function changeEditThemeBackground(headerColor, backgroundColor, footercolor) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_txtHeaderColor").css("background-color", headerColor);
        $("#ctl00_ctl00_MainContent_BodyContent_txtBodyColor").css("background-color", backgroundColor);
        $("#ctl00_ctl00_MainContent_BodyContent_txtFooterColor").css("background-color", footercolor);
    }, 100);
}

/* Show Subscription div */
function showSubscription() {
    $("#subscription").show();
}

/* Today Date format */
function getTodayDate() {
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    if (day < 10) {
        day = '0' + day
    }
    if (month < 10) {
        month = '0' + month
    }
    return month + "-" + day + "-" + year;
}

/* Display Panel */
function displayPanel() {
    if ($("[id$=pnlAddEdit]") != null) {
        $("[id$=pnlAddEdit]").show();
        $("[id$=pnlAddEdit]").removeClass("displayNone");
    }
    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
}
function displayPanelExplanation() {
    if ($("[id$=pnlExplanation]") != null) {
        $("[id$=pnlExplanation]").show();
        $("[id$=pnlExplanation]").removeClass("displayNone");
    }
    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
}
function addoverlay() {
    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });

}
function removeoverlay() {
    $('html, body').css({
        'overflow': 'visible',
        'height': '100%'
    });
}

/* Display Panel */
function displayPanelDelete() {
    if ($("[id$=pnlDelete]") != null) {
        $("[id$=pnlDelete]").show();
        $("[id$=pnlDelete]").removeClass("displayNone");
    }
}

function displayErrorMessage() {
    $("#displayErrorMessage").show();
}
function displayErrorMessage2() {
    $("#displayErrorMessage2").show();
}
function displayErrorMessage3() {
    $("#displayErrorMessage3").show();
}
function displayErrorMessage4() {
    $("#displayErrorMessage4").show();
}

function hideErrorMessage() {
    $("#displayErrorMessage").hide();
}
function hideErrorMessage2() {
    $("#displayErrorMessage2").hide();
}



function showAdvisorIntoPopup() {
    $("#reassignAdvisor").show();
}

function hideAdvisorIntoPopup() {
    $("#reassignAdvisor").hide();
}

/* Disable DatePicker Calendar */
function disableDatePicker() {
    $("[id$=txtBirthDate]").datepicker().datepicker('disable');
    $("[id$=txtSpouseBirthdate]").datepicker().datepicker('disable');
    $("[id$=BirthWifeYYYY]").datepicker().datepicker('disable');
    $("[id$=BirthHusbandYYYY]").datepicker().datepicker('disable');
}


/* Show Panel */
function displayPanelPopUpAssignAd(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlDelete").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlDelete").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlDelete").css("left", "203.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlDelete").css("top", "141.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlDelete").css("width", "600px");

        if (display == true) {
            $("#AssignAdvisorError").html("Please Select Advisor");
        }
        else {
            $("#AssignAdvisorError").html("Please select Customer");
        }
        $("#AssignAdvisorError").show();
    }, 100);
}
/* Show Panel */
function displayPanelPopUp(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "205.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "40.5px");
        $("#errorMessage").html("Registered Email already exists");
        if (display == true) {
            showHideSpouseInformation();
            $("#ctl00_ctl00_MainContent_BodyContent_txtPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#ctl00_ctl00_MainContent_BodyContent_txtConfirmPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#subscription").show();
            ctl00_ctl00_MainContent_BodyContent_txtPassword
        }
        else {
            $("#errorMessage").show();
        }
    }, 100);
}
function displayPanelPopUpSubscription(display) {
    setTimeout(function () {
        $("[id$=pnlAddEdit]").show();
        $("[id$=pnlAddEdit]").removeClass("displayNone");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "205.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "150.5px");
        if (display == true) {
            showHideSpouseInformation();
            $("#ctl00_ctl00_MainContent_BodyContent_txtPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#ctl00_ctl00_MainContent_BodyContent_txtConfirmPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#textEntry :input").attr("disabled", "disabled");
            $("#subscription").show();
            ctl00_ctl00_MainContent_BodyContent_txtPassword
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }
        else {
            $("#errorMessage").show();
        }
    }, 100);
}

/* Show Panel */
function displayPanelPopUpCustomer(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "205.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "40.5px");
        $("#errorMessage").html("Registered Email already exists");
        if (display == true) {
            showHideSpouseInformation();
            $("#ctl00_ctl00_MainContent_BodyContent_txtPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#ctl00_ctl00_MainContent_BodyContent_txtConfirmPassword").val($("#ctl00_ctl00_MainContent_BodyContent_passwordTextBox").val());
            $("#subscription").show();
            ctl00_ctl00_MainContent_BodyContent_txtPassword
        }
        else {
            $("#hideRow").hide();
        }
        $("#errorMessage").show();
    }, 100);
}

/* Show Panel */
function displayPanelPopUpInstitute(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "205.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "70.5px");
        $("#errorMessage").html("Registered Institute Email already exists");
        $("#errorMessage").show();
    }, 100);
}

/* Show Panel */
function displayPanelPopUpAdvisor(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "205.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "70.5px");
        $("#errorMessage").html("Registered Advisor Email already exists");
        $("#errorMessage").show();
    }, 100);
}

/* Show Panel */
function displayPanelPopUpTheme(display) {
    setTimeout(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").show();
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("display", "block");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("left", "244.5px");
        $("#ctl00_ctl00_MainContent_BodyContent_pnlAddEdit").css("top", "165.5px");
        $("#errorMessage").html("* Theme for Selected Institution already exists");
        if (display == true) {
            showHideSpouseInformation();
            $("#subscription").show();
        }
        else {
            $("#errorMessage").show();
        }
    }, 100);
}

/* Manage Advisor Breadscrumb */
function manageBreadscrumb() {
    $("#displayName").html($("[id$=lblDisplayName]").html());
    $("div").find(".breadcrumbs").css("display", "");
    $("div").find(".breadcrumbs").css("position", "absolute");
}

/* Manage Customer Breadscrumb */
function manageBreadscrumbCustomer() {
    $("#displayName").html($("[id$=lblDisplayName]").html());
    $("#manageAdvisor").attr("href", $("[id$=linkAdvisor]").html());
    $("#displayName").show();
    $("#manageAdvisor").show();
    $("div").find(".breadscrumbs").css("display", "inline");
    //$("div").find(".breadscrumbs").css("position", "absolute");
    $("#manageInstitution").css("display", "none");
}

function displayManageInstitutionBreadscrumbLink() {
    manageBreadscrumbCustomer();
    $("#manageInstitution").show();
    $("#manageInstitution").css("display", "");
    $("#manageAdvisor").removeClass("active");
    $("div").find(".breadscrumbs").css("display", "inline");
}

/* Display Faded Message for Add/Edit Record */
function displayFadedMessage() {
    setTimeout(function () {
        $("[id$=lblErrorMessage]").fadeOut(5000);
    }, 1000);
}


/* Display Faded Message for Add/Edit Record */
function displaySuccessRegister() {
    $("#status").show();
    setTimeout(function () {
        $("#status").fadeOut(1200);
    }, 1000);

}

/* Validate Form Fields */
function validateData() {
    /* Initialize Variable */
    var isValidate = true;
    var alphaNumericRegex = /[^0-9a-z]/i;
    var errorMessage = '';
    /* Check if field exists in current form or not */
    if ($("[id$=txtDisplayName]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtDisplayName]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtDisplayName]").addClass("error");
            errorMessage += "* Institutiton Name Required <br>";
        }
        else {

            if (alphaNumericRegex.test($("[id$=txtDisplayName]").val())) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtDisplayName]").addClass("error");
                errorMessage += "* Institution Name must be alphanumeric <br>";
            }
            else {
                /* Remove Css Class */
                $("[id$=txtDisplayName]").removeClass("error");
            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtAdvisorName]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtAdvisorName]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtAdvisorName]").addClass("error");
            errorMessage += "* Advisor Name Required <br>";
        }
        else {
            /* Check AlphaNumeric String */
            if (alphaNumericRegex.test($("[id$=txtDisplayName]").val())) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtAdvisorName]").addClass("error");
                errorMessage += "* Advisor Name must be alphanumeric <br>";
            }
            else {
                /* Remove Css Class */
                $("[id$=txtAdvisorName]").removeClass("error");
            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtFirstname]").val() != null) {
        /* Validate First Name Field */
        if ($("[id$=txtFirstname]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtFirstname]").addClass("error");
            errorMessage += "* First Name Required <br>";
        }
        else {
            /* Check AlphaNumeric String */
            if (alphaNumericRegex.test($("[id$=txtFirstname]").val())) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtFirstname]").addClass("error");
                errorMessage += "* First Name should be alphanumeric <br>";

            }
            else {
                /* Remove Css Class */
                $("[id$=txtFirstname]").removeClass("error");

            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtEmail]").val() != null) {
        /* Validate Email Field */
        if ($("[id$=txtEmail]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtEmail]").addClass("error");
            errorMessage += "* Email Address Required <br>";
        }
        else {
            // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
            /* Validation Email Address Format */
            if (!emailfilter1.test($("[id$=txtEmail]").val())) {
                errorMessage += "* Invalid Email Address <br>";
                /* Add error css class */
                $("[id$=txtEmail]").addClass("error");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtEmail]").removeClass("error");
            }
        }
    }
    /* Check if field exists in current form or not */
    //if ($("[id$=txtPassword]").val() != null) {
    //    /* Validate Password Field */
    //    if ($("[id$=txtPassword]").val() == '') {
    //        isValidate = false;
    //        /* Add error css class */
    //        $("[id$=txtPassword]").addClass("error");
    //        errorMessage += "* Password Required <br>";
    //    }
    //    else {
    //        /* Check Password Length */
    //        if (parseInt($("[id$=txtPassword]")[0].textLength) < parseInt(6) || parseInt($("[id$=txtPassword]")[0].textLength) > parseInt(32)) {
    //            isValidate = false;
    //            /* Add error css class */
    //            $("[id$=txtPassword]").addClass("error");
    //            errorMessage += "* Password length should be 6-32 characters. <br>";
    //        }
    //        else {
    //            /* Remove Css Class */
    //            $("[id$=txtPassword]").removeClass("error");
    //        }
    //    }
    //}

    ///* Check if field exists in current form or not */
    //if ($("[id$=txtConfirmPassword]").val() != null) {
    //    /* Validate Confirm Password Field */
    //    if ($("[id$=txtConfirmPassword]").val() == '') {
    //        isValidate = false;
    //        /* Add error css class */
    //        $("[id$=txtConfirmPassword]").addClass("error");
    //        errorMessage += "* Confirm Password Required <br>";
    //    }
    //    else {
    //        /* Check Confirm Password Length */
    //        if (parseInt($("[id$=txtConfirmPassword]")[0].textLength) < parseInt(6) || parseInt($("[id$=txtConfirmPassword]")[0].textLength) > parseInt(32)) {
    //            isValidate = false;
    //            /* Add error css class */
    //            $("[id$=txtConfirmPassword]").addClass("error");
    //            errorMessage += "* Confirm Password length should be 6-32 characters. <br>";
    //        }
    //        else {
    //            /* Compare password and confirm password string */
    //            if ($("[id$=txtConfirmPassword]").val() != $("[id$=txtPassword]").val()) {
    //                errorMessage += "* Password does not match with Confirm Password <br>";
    //                /* Add error css class */
    //                $("[id$=txtConfirmPassword]").addClass("error");
    //                $("[id$=txtPassword]").addClass("error");
    //                isValidate = false;
    //            }
    //            else {
    //                /* Remove Css Class */
    //                $("[id$=txtConfirmPassword]").removeClass("error");
    //                $("[id$=txtPassword]").removeClass("error");
    //            }
    //        }
    //    }
    //}

    /* Check if field exists in current form or not */
    if ($("[id$=txtBirthDate]").val() != null) {
        /* Validate First Name Field */
        if ($("[id$=txtBirthDate]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtBirthDate]").addClass("error");
            errorMessage += "* Birth Date Required <br>";
        }
        else {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;//January is 0! 
            var yyyy = today.getFullYear();
            var CurrentDate = mm + "/" + dd + "/" + yyyy;

            var enterDate = $("[id$=txtBirthDate]").val();
            if (Date.parse(enterDate) < Date.parse(CurrentDate)) {
                var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
                if (!(date_regex.test(testDate))) {
                    return false;
                }
                /* Remove Css Class */
                $("[id$=txtBirthDate]").removeClass("error");
            }
                //    var birthDate = $("[id$=txtBirthDate]").val().replace("/", "-");
                //    birthDate = birthDate.replace("/", "-");
                //    /* Compare Date Range */
                //    if (parseInt(birthDate.replace(/-/g, ""), 10) > parseInt(getTodayDate().replace(/-/g, ""), 10)) {
                //        isValidate = false;
                //        /* Add error css class */
                //        $("[id$=txtBirthDate]").addClass("error");
                //        errorMessage += "* Birth date should not be greater than current date <br>";
                //    }
            else {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtBirthDate]").addClass("error");
                errorMessage += "* Birth date should not be greater than current date <br>";
            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtSpouseFirstname]").val() != null) {
        if ($("[id$=rdbtnMarried]").prop("checked")) {
            /* Validate First Name Field */
            if ($("[id$=txtSpouseFirstname]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtSpouseFirstname]").addClass("error");
                errorMessage += "* Spouse First Name Required <br>";
            }
            else {
                /* Check AlphaNumeric String */
                if (alphaNumericRegex.test($("[id$=txtSpouseFirstname]").val())) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseFirstname]").addClass("error");
                    errorMessage += "* Spouse first name should be alphanumeric <br>";
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtSpouseFirstname]").removeClass("error");
                }
            }
        }
        else {
            $("[id$=txtSpouseFirstname]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtSpouseBirthdate]").val() != null) {
        /* Validate First Name Field */
        if ($("[id$=rdbtnMarried]").prop("checked")) {
            if ($("[id$=txtSpouseBirthdate]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtSpouseBirthdate]").addClass("error");
                errorMessage += "* Spouse Birth Date Required <br>";
            }
            else {
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;//January is 0! 
                var yyyy = today.getFullYear();
                var CurrentDate = mm + "/" + dd + "/" + yyyy;

                var enterDate = $("[id$=txtSpouseBirthdate]").val();


                if (Date.parse(enterDate) < Date.parse(CurrentDate)) {

                    /* Remove Css Class */
                    $("[id$=txtSpouseBirthdate]").removeClass("error");
                }
                    //var spouseBirthDate = $("[id$=txtSpouseBirthdate]").val().replace("/", "-");
                    //spouseBirthDate = spouseBirthDate.replace("/", "-");
                    ///* Compare Date Range */
                    //if (parseInt(spouseBirthDate.replace(/-/g, ""), 10) > parseInt(getTodayDate().replace(/-/g, ""), 10)) {
                    //    isValidate = false;
                    //    /* Add error css class */
                    //    $("[id$=txtSpouseBirthdate]").addClass("error");
                    //    errorMessage += "* Spouse birth date should not be greater than current date <br>";
                    //}
                else {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseBirthdate]").addClass("error");
                    errorMessage += "* Birth date should not be greater than current date <br>";
                }
            }
        }
        else {
            $("[id$=txtSpouseBirthdate]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=datePublish]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=datePublish]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=datePublish]").addClass("error");
            errorMessage += "* Publish Date Required <br>";
        }
        else {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;//January is 0! 
            var yyyy = today.getFullYear();
            var CurrentDate = mm + "/" + dd + "/" + yyyy;
            var enterDate = $("[id$=datePublish]").val();
            if (Date.parse(enterDate) > Date.parse(CurrentDate)) {

                /* Remove Css Class */
                $("[id$=datePublish]").removeClass("error");
            }
                //var birthDate = $("[id$=datePublish]").val().replace("/", "-");
                //birthDate = birthDate.replace("/", "-");
                //if (parseInt(birthDate.replace(/-/g, ""), 10) <= parseInt(getTodayDate().replace(/-/g, ""), 10)) {
                //    isValidate = false;
                //    /* Add error css class */
                //    $("[id$=datePublish]").addClass("error");
                //    errorMessage += "* Publish date should not be lesser or equal than current date <br>";
                //}
            else {
                isValidate = false;
                /* Add error css class */
                $("[id$=datePublish]").addClass("error");
                errorMessage += "* Publish date should not be lesser or equal than current date <br>";
            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtSubject]").val() != null) {
        var str = $("[id$=txtSubject]").val();

        /* Check if field is blank or not */
        if ($("[id$=txtSubject]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtSubject]").addClass("error");
            errorMessage += "* NewsLetter Subject Required <br>";
        }
        else if (str.toString().trim() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtSubject]").addClass("error");
            errorMessage += "* NewsLetter Subject Should Not Be Blank <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=txtSubject]").removeClass("error");
        }
    }
    /* Check if field exists in current form or not */
    if ($("[id$=txtHeaderColor]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtHeaderColor]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtHeaderColor]").addClass("error");
            errorMessage += "* Header Color Required <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=txtHeaderColor]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtBodyColor]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtBodyColor]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtBodyColor]").addClass("error");
            errorMessage += "* Body Color Required <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=txtBodyColor]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtAttachmentLogo]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtAttachmentLogo]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtAttachmentLogo]").addClass("error");
            errorMessage += "* Image is Required <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=txtAttachmentLogo]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtFooterColor]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=txtFooterColor]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtFooterColor]").addClass("error");
            errorMessage += "* Footer Color Required <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=txtFooterColor]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=ddlnameSelection]").val() != null) {
        /* Check if field is blank or not */
        if ($("[id$=ddlnameSelection]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $('[id$=ddlnameSelection]').addClass("error");
            errorMessage += "* Please Select Institution <br>";
        }
        else {
            /* Remove Css Class */
            $("[id$=ddlnameSelection]").removeClass("error");
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtAttachmentLogo]").val() != null) {
        debugger;
        if ($("[id$=txtAttachmentLogo]").val() != '') {
            var imgValidate;
            if (/firefox/.test(navigator.userAgent.toLowerCase())) {
                imgValidate = /(.*?)\.(jpg|jpeg|png|gif|bmp|GIF|JPG|JPEG)$/;
            }
            else {
                imgValidate = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.png|.bmp|.gif|.GIF|.jpg|.JPG|.jpeg|.JPEG)$/;
            }
            /* Validation Email Address Format */
            if (!imgValidate.test($("[id$=txtAttachmentLogo]").val())) {
                errorMessage += "* Only Image type allowed!!! <br>";
                /* Add error css class */
                $("[id$=txtAttachmentLogo]").addClass("error");
                $('[id$=img]').attr("style", "display:none;");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtAttachmentLogo]").removeClass("error");
            }
        }
        else {
            if ($(".imgResize")[0] == undefined) {
                errorMessage += "* Please Attach Institution Logo <br>";
                /* Add error css class */
                $("[id$=txtAttachmentLogo]").addClass("error");
                $('[id$=img]').attr("style", "display:none;");
                isValidate = false;
            }
        }
    }
    /* Check if field exists in current form or not */
    if ($("[id$=txtAdvisorEmail]").val() != null) {
        /* Validate Email Field */
        if ($("[id$=txtAdvisorEmail]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtAdvisorEmail]").addClass("error");
            errorMessage += "* Email Address Required <br>";
        }
        else {
            // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var emailfilter = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
            /* Validation Email Address Format */
            if (!emailfilter.test($("[id$=txtAdvisorEmail]").val())) {
                errorMessage += "* Invalid Advisor Email Address <br>";
                /* Add error css class */
                $("[id$=txtAdvisorEmail]").addClass("error");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtAdvisorEmail]").removeClass("error");
            }
        }
    }
    /* Check if field exists in current form or not */
    if ($("[id$=txtAdvisorPassword]").val() != null) {
        /* Validate Password Field */
        if ($("[id$=txtAdvisorPassword]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtAdvisorPassword]").addClass("error");
            errorMessage += "* Advisor Password Required <br>";
        }
        else {
            /* Check Password Length */
            if (parseInt($("[id$=txtAdvisorPassword]")[0].textLength) < parseInt(6) || parseInt($("[id$=txtAdvisorPassword]")[0].textLength) > parseInt(32)) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtAdvisorPassword]").addClass("error");
                errorMessage += "* Advisor Password length should be 6-32 characters. <br>";
            }
            else {
                /* Remove Css Class */
                $("[id$=txtAdvisorPassword]").removeClass("error");
            }
        }
    }

    /* Check if field exists in current form or not */
    if ($("[id$=txtAdvisorConfirmPassword]").val() != null) {
        /* Validate Confirm Password Field */
        if ($("[id$=txtAdvisorConfirmPassword]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtAdvisorConfirmPassword]").addClass("error");
            errorMessage += "* Advisor Confirm Password Required <br>";
        }
        else {
            /* Check Confirm Password Length */
            if (parseInt($("[id$=txtAdvisorConfirmPassword]")[0].textLength) < parseInt(6) || parseInt($("[id$=txtAdvisorConfirmPassword]")[0].textLength) > parseInt(32)) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtAdvisorConfirmPassword]").addClass("error");
            }
            else {
                /* Compare password and confirm password string */
                if ($("[id$=txtAdvisorConfirmPassword]").val() != $("[id$=txtAdvisorPassword]").val()) {
                    errorMessage += "* Advisor Password does not match with Confirm Password <br>";
                    /* Add error css class */
                    $("[id$=txtAdvisorConfirmPassword]").addClass("error");
                    $("[id$=txtAdvisorPassword]").addClass("error");
                    isValidate = false;
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtAdvisorConfirmPassword]").removeClass("error");
                    $("[id$=txtAdvisorPassword]").removeClass("error");
                }
            }
        }
    }

    $("#errorMessage").hide();
    $("#displayErrorMessage").hide();
    /* Return Error message Lists if have */
    if (errorMessage != '') {
        $("#errorMessage").show();
        $("#displayErrorMessage").show();
        $("#errorMessage").html(errorMessage);
    }
    return isValidate;
}

/* Validate Assign Advisor popup Fields */
//function validateAssignAdvisorData() {
//    /* Initialize Variable */
//    var isValidate = true;
//    var alphaNumericRegex = /[^0-9a-z]/i;
//    var AssignAdvisorError = '';
//    /* Check if field exists in current form or not */
//    if ($("[id$=txtDisplayName]").val() != null) {
//        /* Check if field is blank or not */
//        if ($("[id$=txtDisplayName]").val() == '') {
//            isValidate = false;
//            /* Add error css class */
//            $("[id$=txtDisplayName]").addClass("error");
//            AssignAdvisorError += "* Institutiton Name Required <br>";
//        }
//        else {
//            /* Remove Css Class */
//            $("[id$=txtDisplayName]").removeClass("error");
//        }
//    }

//    /* Check if field exists in current form or not */
//    if ($("[id$=txtAdvisorName]").val() != null) {
//        /* Check if field is blank or not */
//        if ($("[id$=txtAdvisorName]").val() == '') {
//            isValidate = false;
//            /* Add error css class */
//            $("[id$=txtAdvisorName]").addClass("error");
//            AssignAdvisorError += "* Advisor Name Required <br>";
//        }
//        else {
//            /* Check AlphaNumeric String */
//            if (alphaNumericRegex.test($("[id$=txtDisplayName]").val())) {
//                isValidate = false;
//                /* Add error css class */
//                $("[id$=txtAdvisorName]").addClass("error");
//                AssignAdvisorError += "* Advisor Name must be alphanumeric <br>";
//            }
//            else {
//                /* Remove Css Class */
//                $("[id$=txtAdvisorName]").removeClass("error");
//            }
//        }
//    }

//    /* Check if field exists in current form or not */
//    if ($("[id$=txtFirstname]").val() != null) {
//        /* Validate First Name Field */
//        if ($("[id$=txtFirstname]").val() == '') {
//            isValidate = false;
//            /* Add error css class */
//            $("[id$=txtFirstname]").addClass("error");
//            AssignAdvisorError += "* First Name Required <br>";
//        }
//        else {
//            /* Check AlphaNumeric String */
//            if (alphaNumericRegex.test($("[id$=txtFirstname]").val())) {
//                isValidate = false;
//                /* Add error css class */
//                $("[id$=txtFirstname]").addClass("error");
//                AssignAdvisorError += "* First Name should be alphanumeric <br>";

//            }
//            else {
//                /* Remove Css Class */
//                $("[id$=txtFirstname]").removeClass("error");

//            }
//        }
//    }

//    $("#AssignAdvisorError").hide();
//    $("#displayErrorMessage").hide();
//    /* Return Error message Lists if have */
//    if (errorMessage != '') {
//        $("#AssignAdvisorError").show();
//        $("#displayErrorMessage").show();
//        $("#AssignAdvisorError").html(AssignAdvisorError);
//    }
//    return isValidate;
//}


function checkPasswordMatch() {
    var password = $("[id$=txtPassword]").val();
    var confirmPassword = $("[id$=txtConfirmPassword]").val();
    if (confirmPassword.match(/\S/) && password.match(/\S/)) {
        if (password == confirmPassword)
            $("[id$=imgpass]").css("display", "");
        else
            $("[id$=imgpass]").css("display", "none");
    }
    else {
        $("[id$=imgpass]").css("display", "none");
    }
}

/* Show Confirm Box before delete records from grid  */
function showDeleteConfirm(content) {
    /* Show ConfirmBox */
    var confirmBox = confirm("Are you sure you want to delete selected " + content + "?");
    if (confirmBox) {
        /* If true, then delete records from Database */
        return true;
    }
    else {
        /* No effect in UI and no deletion in database */
        return false;
    }
}

/* Show Confirm Box before delete records from grid  */
function showSuspendConfirm(element) {
    /* Show ConfirmBox */
    var confirmBox = "";
    /* Check the status of selected row item */
    if ($(element).attr("alt") == 'N') {
        confirmBox = confirm("Are you sure you want to activate the account?");
    }
    else {
        confirmBox = confirm("Are you sure you want to deactivate the account?");
    }
    if (confirmBox) {
        /* If true, then suspend/unsuspend from Database */
        return true;
    }
    else {
        /* No effect in UI and no suspend/unsuspend in database */
        return false;
    }
}

/* Show Confirm Box before delete records from grid  */
function showSuspendConfirmNewsletter(element) {
    /* Show ConfirmBox */
    var confirmBox = "";
    /* Check the status of selected row item */
    if ($(element).attr("alt") == 'N') {
        confirmBox = confirm("Are you sure you want to activate the Newsletter?");
    }
    else {
        confirmBox = confirm("Are you sure you want to deactivate the Newsletter?");
    }
    if (confirmBox) {
        /* If true, then suspend/unsuspend from Database */
        return true;
    }
    else {
        /* No effect in UI and no suspend/unsuspend in database */
        return false;
    }
}

/* Show Popup if User is Inactive */
function displaySubscriptionPopUp(messageDisplay) {
    $.blockUI({ message: $('#subscription') });

    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });

    if (messageDisplay == true) {
        displaySuccessRegister();
    }
}

/* Show Popup if User is Inactive */
function displayAdvisorSubscriptionPopUp() {
    $.blockUI({ message: $('#subscriptionAdvisor') });
    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
}

/* Show Popup if User is Inactive */
function displayInstitutionSubscriptionPopUp() {
    $.blockUI({ message: $('#subscription') });
    $('html, body').css({
        'overflow': 'hidden',
        'height': '100%'
    });
}

function SubscribeStatus() {
    $("#status").html("Customer Subscribed Successfully");
    $("#status").show();
    setTimeout(function () {
        $("#status").fadeOut(1200);
    }, 1000);
}

function StorePaymentStatus() {
    $("#status").show();
    setTimeout(function () {
        $("#status").fadeOut(4000);
    }, 1000);
}


/* Show/Hide Spouse Information based on Radio Button Checked */
function showHideSpouseInformation() {
    displayPanel();
    /* Check if Married RadioButton checked or not */
    if ($("[id$=rdbtnMarried]").prop("checked")) {
        $(".hideRow").show();
        $(".hideRow1").show();
        $(".hideRow2").show();
        $(".showRow").hide();
        $(".DivorceText").hide();
        $("[id$=lblSpouseBenefit]").text("Spouse's monthly Full Retirement Age Benefit");
        $("[id$=lblSpouseBirthdate]").text("Spouse's Birth Date");
    }
    else if ($("[id$=rdbtnDivorced]").prop("checked")) {
        $(".hideRow").hide();
        $(".hideRow1").show();
        $(".hideRow2").show();
        $(".showRow").hide();
        $(".DivorceText").show();
        $("[id$=lblSpouseBenefit]").text("Ex-Spouse's monthly Full Retirement Age Benefit");
        $("[id$=lblSpouseBirthdate]").text("Ex-Spouse's Birth Date");
    }
    else {
        if ($("[id$=rdbtnSingle]").prop("checked")) {
            $(".hideRow2").hide();
            $(".spaceSingle").hide();
            $("[id$=pnlAddEdit]").height("450px");
            //$("#<%=pnlAddEdit.ClientID %>").height("450px");

        }
        else if ($("[id$=rdbtnWidowed]").prop("checked")) {
            $(".hideRow2").show();
            $("[id$=lblSpouseBenefit]").text("Deceased Spouse's monthly Full Retirement Age Benefit");
            $("[id$=lblSpouseBirthdate]").text("Deceased Spouse's Birth Date");
        }
        $(".hideRow").hide();
        $(".hideRow1").hide();
        $(".showRow").show();
        $(".DivorceText").hide();
    }
    $('html, body').css({
        'overflow': 'visible',
        'height': '100%'
    });
    $(".spaceSingle").hide();
}

/* Display Calendar */
function datePicker() {
    /* Date Picker Options */
    var DatepickerOpts = {
        showOn: 'both',
        changeMonth: true,
        changeYear: true,
        yearRange: '1910:2040',
        buttonImageOnly: true,
        buttonImage: '/Images/calendar1.png',
    };
    /* Check if id exists in current Page */
    if ($("[id$=dateFilter]") != null) {
        $("[id$=dateFilter]").datepicker(DatepickerOpts);
        $("[id$=dateFilter]").keypress(function (e) { e.preventDefault(); });
    }
    /* Check if id exists in current Page */
    if ($("[id$=txtBirthDate]") != null) {
        $("[id$=txtBirthDate]").datepicker(DatepickerOpts);
    }
    /* Check if id exists in current Page */
    if ($("[id$=txtSpouseBirthdate]") != null) {
        $("[id$=txtSpouseBirthdate]").datepicker(DatepickerOpts);
    }

    /* Check if id exists in current Page */
    if ($("[id$=datePublish]") != null) {
        $("[id$=datePublish]").datepicker(DatepickerOpts);
    }
}

function displayTableHeader(bool) {
    if (bool == true) {
        $("#header1").removeClass("displayNone");
        $("#header2").removeClass("displayNone");
    }
    else {
        $("#header1").addClass("displayNone");
        $("#header2").addClass("displayNone");
    }
}

function dateFormatPartititon() {
    var WifepickerOpts = {
        changeMonth: true,
        changeYear: true,
        yearRange: '1910:2040',
        showOn: 'button',
        buttonImageOnly: true,
        buttonImage: '/Images/calendar1.png',
        onSelect: function (dateStr) {
            var date = $("#<%=BirthWifeYYYY.ClientID%>").val().toString().split("/", 3);
            $("#<%=BirthWifeMM.ClientID %>").val(date[0]);
            $("#<%=BirthWifeDD.ClientID%>").val(date[1]);
            $("#<%=BirthWifeYYYY.ClientID%>").val(date[2]);
        }
    };
    var HusbandpickerOpts = {
        changeMonth: true,
        changeYear: true,
        yearRange: '1910:2040',
        showOn: 'button',
        buttonImageOnly: true,
        buttonImage: '/Images/calendar1.png',
        onSelect: function (dateStr) {
            var date = $("#<%=BirthHusbandYYYY.ClientID%>").val().toString().split("/", 3);
            $("#<%=BirthHusbandMM.ClientID %>").val(date[0]);
            $("#<%=BirthHusbandDD.ClientID%>").val(date[1]);
            $("#<%=BirthHusbandYYYY.ClientID%>").val(date[2]);
        }
    };
    /* Check if id exists in current Page */
    if ($("[id$=BirthWifeYYYY]") != null) {
        $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);
    }
    /* Check if id exists in current Page */
    if ($("[id$=BirthHusbandYYYY]") != null) {
        $("[id$=BirthHusbandYYYY]").datepicker(HusbandpickerOpts);
    }
}

function showHideNewsLetterContent(visible) {
    if (visible == true) {
        CKEDITOR.inlineAll();
        $("#addNewsLetterTable").show();
    }
    else {
        $("[id$=txtBody]").val("");
        $("#addNewsLetterTable").hide();
    }
}

//$(document).ready(function () {
//    console.log("document loaded");
//   // alert('Hi');
//    /* 21/11/2014 to Disable the EnterKey Event Registration Page*/
//    $("#txtCustomerName").keydown(function () {
//        return (event.keyCode != 13);
//    });
//    $("#txtEmail").keydown(function () {
//        return (event.keyCode != 13);
//    });
//    $("#txtCustomerPassword").keydown(function () {
//        return (event.keyCode != 13);
//    });
//    $("#txtConfirmCustomerPassword").keydown(function () {
//        return (event.keyCode != 13);
//    });
//    $("#txtEmailAddress").keydown(function () {
//        return (event.keyCode != 13);
//    });
//    $("#txtPassword").keydown(function () {
//        return (event.keyCode != 13);
//    });

//});

//function keyPress(ID) {

//};

//$("#txtCustomerName").keypress(function () {
//    console.log(this.Key);
//});

function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
}
document.onkeypress = stopRKey;

/* for ForgotPasseord Page Error Message Fade  Devloper: Aloha   Date:-01-12-2014*/
$(function () {
    setTimeout(function () {
        $("[id$=EmailRequired]").fadeOut(5000);
    }, 5000);
});
$(function () {
    setTimeout(function () {
        $("[id$=revEmail]").fadeOut(3000);
    }, 3000);
});
$(function () {
    setTimeout(function () {
        $("[id$=lblUserExist]").fadeOut(5000);
    }, 5000);
});

/* Function for ManagerAdvisor Page Assign Advisor popup Select All CheckBox text Change     Dev: Aloha   Date:-01-12-2014*/

$(function () {
    $("[id*=chkSelectAll]").bind("click", function () {
        alert("HI");
        if ($(this).is(":checked")) {
            $("[id*=lstCustomers] input").attr("checked", "checked");
        } else {
            $("[id*=lstCustomers] input").removeAttr("checked");
        }
    });
    $("[id*=lstCustomers] input").bind("click", function () {
        if ($("[id*=lstCustomers] input:checked").length == $("[id*=lstCustomers] input").length) {
            $("[id*=chkSelectAll]").attr("checked", "checked");
        } else {
            $("[id*=chkSelectAll]").removeAttr("checked");
        }
    });
});

$(function () {
    /* Check if id exists in current Page */
    if ($("[id$=txtBirthDate]") != null) {
        //alert('txtBirthDatefunctionCommon.js');
        $("[id$=txtBirthDate]").keypress(function (e) { e.preventDefault(); });
    }
});
/* Check if id exists in current Page */
if ($("[id$=txtBirthDate]") != null) {
    //alert('txtBirthDateCommon.js');
    $("[id$=txtBirthDate]").keypress(function (e) { e.preventDefault(); });
}

//date 12.6.2015 by Aloha
//code to change the images on hover of menu items
$(function () {

    $("#homeMenu").mouseover(function () {
        $("#Img1").attr("src", "/images/home.png");
    })
    $("#homeMenu").mouseleave(function () {
        $("#Img1").attr("src", "/images/home1.png");
    })

    $("#homeMenuAdvisor").mouseover(function () {
        $("#Img1LogAdvisor").attr("src", "/images/MyAccount.png");
    })
    $("#homeMenuAdvisor").mouseleave(function () {
        $("#Img1LogAdvisor").attr("src", "/images/MyAccount2.png");
    })

    $("#homeMenuLog").mouseover(function () {
        $("#Img1Log").attr("src", "/images/MyAccount.png");
    })
    $("#homeMenuLog").mouseleave(function () {
        $("#Img1Log").attr("src", "/images/MyAccount2.png");
    })


    $("#aboutMenu").mouseover(function () {
        $("#ImgAbout").attr("src", "/images/about.png");
    })
    $("#aboutMenu").mouseleave(function () {
        $("#ImgAbout").attr("src", "/images/about1.png");
    })

    $("#faqMenu").mouseover(function () {
        $("#ImgFaqs").attr("src", "/images/faqs.png");
    })
    $("#faqMenu").mouseleave(function () {
        $("#ImgFaqs").attr("src", "/images/faqs1.png");
    })

    $("#mediaMenu").mouseover(function () {
        $("#ImgMedia").attr("src", "/images/media.png");
    })
    $("#mediaMenu").mouseleave(function () {
        $("#ImgMedia").attr("src", "/images/media1.png");
    })

    $("#storeMenu").mouseover(function () {
        $("#ImgStore").attr("src", "/images/store.png");
    })
    $("#storeMenu").mouseleave(function () {
        $("#ImgStore").attr("src", "/images/store1.png");
    })

    $("#signup").mouseover(function () {
        $("#ImgSignUp").attr("src", "/images/signup.png");
    })
    $("#signup").mouseleave(function () {
        $("#ImgSignUp").attr("src", "/images/signup1.png");
    })

    $("#pricing").mouseover(function () {
        $("#ImgPricing").attr("src", "/images/NewPri2.png");
    })
    $("#pricing").mouseleave(function () {
        $("#ImgPricing").attr("src", "/images/NewPri1.png");
    })

    $("#login").mouseover(function () {
        $("#ImgLogin").attr("src", "/images/NewLogin1.png");
    })
    $("#login").mouseleave(function () {
        $("#ImgLogin").attr("src", "/images/NewLogin2.png");
    })

    $("#myaccount").mouseover(function () {
        $("#ImgMyAccount").attr("src", "/images/accountProfile2.png");
    })
    $("#myaccount").mouseleave(function () {
        $("#ImgMyAccount").attr("src", "/images/accountProfile1.png");
    })

    $("#logout").mouseover(function () {
        $("#logoutlink").attr("src", "/images/logout.png");
    })
    $("#logout").mouseleave(function () {
        $("#logoutlink").attr("src", "/images/logout1.png");
    })

    $("#changepassword").mouseover(function () {
        $("#ImgChangePwd").attr("src", "/images/CHANGE PWD.png");
    })
    $("#changepassword").mouseleave(function () {
        $("#ImgChangePwd").attr("src", "/images/CHANGE PWD1.png");
    })

    $("#welcomeMenu").mouseover(function () {
        $("#welcome").attr("src", "/images/Hover_welcome.png");
    })
    $("#welcomeMenu").mouseleave(function () {
        $("#welcome").attr("src", "/images/welcome1.png");
    })

    $("#welcomeAdminMenu").mouseover(function () {
        $("#welcomeAdmin").attr("src", "/images/Hover_welcome.png");
    })
    $("#welcomeAdminMenu").mouseleave(function () {
        $("#welcomeAdmin").attr("src", "/images/welcome1.png");
    })
})

function EnableDisableSubmit() {
    $("#ctl00_MainContent_ChkBoxIAgree").prop('checked', true);
    if ($("#ctl00_MainContent_ChkBoxIAgree").is(':checked')) {
        $("#ctl00_MainContent_btnCreateUser").removeAttr('disabled');
    }
    $("#ctl00_MainContent_ChkBoxIAgree").change(function () {
        if ($("#ctl00_MainContent_ChkBoxIAgree").is(':checked')) {
            $("#ctl00_MainContent_btnCreateUser").removeAttr('disabled');
        } else {
            $("#ctl00_MainContent_btnCreateUser").attr('disabled', 'disabled');
        }
    });
}

function ShowAgreementDetails() {
    if ($("[id$=pnlAgreement]") != null) {
        $("[id$=pnlAgreement]").show();
        $("[id$=pnlAgreement]").removeClass("displayNone");
    }
    //$('html, body').css({
    //    'overflow': 'hidden',
    //    'height': '100%'
    //});
}
function EndRequestHandler(sender, args) {
    //if (args.get_error() == undefined)
    //alert("No error");
    //else
    //alert("There was an error" + args.get_error().message);
}
function load() {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
}
function ShowDialog(modal) {

    $("#overlay").show();
    //$("#dialog").fadeIn(300);
    $("#dialog").show();
    $("#navigatebg").addClass("web_dialog_overlay");
    if (modal) {
        $("#overlay").unbind("click");
    }
    else {
        $("#overlay").click(function (e) {
            HideDialog();
        });
    }


    if (modal) {
        $("[id$=lblEmailError]").css("display", "none");
        $("#errordiv").css("display", "block");

    }
    else
        $("#errordiv").css("display", "none");
}
function HideDialog() {
    $("#overlay").hide();
    $("#dialog").fadeOut(300);
    $("#navigatebg").removeClass("web_dialog_overlay");

}

function ValidateLoginDetails() {
    var isValidate = true;
    var emailErrorMessage = '';
    var pwdErrorMessage = '';
    if ($("[id$=txtEmailAddress]").val() != null) {
        /* Validate Email Field */
        if ($("[id$=txtEmailAddress]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtEmailAddress]").addClass("error");
            emailErrorMessage += "- Email is required";
        }
        else {
            var emailfilter1 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
            /* Validation Email Address Format */
            if (!emailfilter1.test($("[id$=txtEmailAddress]").val())) {
                emailErrorMessage += "- Invalid email";
                /* Add error css class */
                $("[id$=txtEmailAddress]").addClass("error");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtEmailAddress]").removeClass("error");
            }
        }
    }

    if ($("[id$=txtPassword]").val() != null) {
        if ($("[id$=txtPassword]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtPassword]").addClass("error");
            pwdErrorMessage += "- Password is required ";
        }
    }

    $("[id$=lblEmailError]").text(emailErrorMessage);
    $("[id$=lblPwdError]").text(pwdErrorMessage);

    if ($("[id$=lblEmailError]").val() != null)
        $("[id$=lblEmailError]").css("display", "block");
    else
        $("[id$=lblEmailError]").css("display", "none");

    if ($("[id$=lblPwdError]").val() != null)
        $("[id$=lblPwdError]").css("display", "block");
    else
        $("[id$=lblPwdError]").css("display", "none");


    if (isValidate == false)
        $("#errordiv").css("display", "block");
    else
        $("#errordiv").css("display", "none");
    return isValidate;
}


//Email Validation
function ValidateRegistrationEmail() {

    var isValidate = true;
    if ($("[id$=txtEmailAddress]").val() != null) {
        /* Validate Email Field */
        if ($("[id$=txtEmailAddress]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtEmailAddress]").addClass("errorRegister");
        }
        else {
            var emailfilter1 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;

            /* Validation Email Address Format */
            if (!emailfilter1.test($("[id$=txtEmailAddress]").val())) {
                emailErrorMessage += "- Invalid email";
                /* Add error css class */
                $("[id$=txtEmailAddress]").addClass("errorRegister");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtEmailAddress]").removeClass("errorRegister");
            }
        }
    }
    return isValidate;
}

function ValidateRegistrationFirstName() {

}

function ValidateRgistrationDetails() {
    var isValidate = true;
    var emailErrorMessage = '';
    var pwdErrorMessage = '';
    var fnameErrorMessage = '';
    var cfmpwdErrorMessage = '';
    var alphaNumericRegex = /^[A-Za-z]+$/;
    if ($("[id$=txtEmailAddress]").val() != null) {
        /* Validate Email Field */
        if ($("[id$=txtEmailAddress]").val() == '') {
            isValidate = false;
            emailErrorMessage += "- Email address is required";
            /* Add error css class */
            $("[id$=txtEmailAddress]").addClass("errorRegister");
        }
        else {
            var emailfilter1 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


            //var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;

            /* Validation Email Address Format */
            if (!emailfilter1.test($("[id$=txtEmailAddress]").val())) {
                emailErrorMessage += "- Invalid email";
                /* Add error css class */
                $("[id$=txtEmailAddress]").addClass("errorRegister");
                isValidate = false;
            }
            else {
                /* Remove Css Class */
                $("[id$=txtEmailAddress]").removeClass("errorRegister");
            }
        }
    }

    if ($("[id$=txtPassword]").val() != null) {
        if ($("[id$=txtPassword]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtPassword]").addClass("errorRegister");
            pwdErrorMessage += "- Password is required ";
        }
        //else {
        //    if ($("[id$=txtPassword]").val().length < 6 || $("[id$=txtPassword]").val().length > 32) {
        //        isValidate = false;
        //        /* Add error css class */
        //        $("[id$=txtPassword]").addClass("errorRegister");
        //        pwdErrorMessage += "- Password must be between 6-32 characters  ";
        //    }
        //}
    }

    //Confirm Password
    if ($("[id$=txtConfirmPassword]").val() != null) {
        if ($("[id$=txtConfirmPassword]").val() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtConfirmPassword]").addClass("errorRegister");
            cfmpwdErrorMessage += "- Confirm password is required ";
        }
        //else {
        //    if ($("[id$=txtConfirmPassword]").val().length < 6 || $("[id$=txtConfirmPassword]").val().length > 32) {
        //        isValidate = false;
        //        /* Add error css class */
        //        $("[id$=txtConfirmPassword]").addClass("errorRegister");
        //        cfmpwdErrorMessage += "- Confirm Password must be between 6-32 characters  ";
        //    }
        //}
    }

    //First Name
    if ($("[id$=txtFirstName]").val() != null) {
        if ($("[id$=txtFirstName]").val().trim() == '') {
            isValidate = false;
            /* Add error css class */
            $("[id$=txtFirstName]").addClass("errorRegister");
            fnameErrorMessage += "- First name is required ";
        }
        else {
            /* Check AlphaNumeric String */
            if (!alphaNumericRegex.test($("[id$=txtFirstName]").val())) {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtFirstName]").addClass("errorRegister");
                fnameErrorMessage += "- First name should be valid";
            }
            else {
                /* Remove Css Class */
                $("[id$=txtFirstName]").removeClass("errorRegister");
                fnameErrorMessage += "";
            }
        }
    }


    if ($("[id$=txtConfirmPassword]").val() != $("[id$=txtPassword]").val()) {
        cfmpwdErrorMessage = "- The Password and Confirm Password must match.";
        isValidate = false;
        $("[id$=txtConfirmPassword]").addClass("errorRegister");
    }



    $("[id$=lblEmailError]").text(emailErrorMessage);
    $("[id$=lblPwdError]").text(pwdErrorMessage);
    $("[id$=lblFirstNameError]").text(fnameErrorMessage);
    $("[id$=lblConfirmPwdError]").text(cfmpwdErrorMessage);

    if ($("[id$=lblEmailError]").val() != null)
        $("[id$=lblEmailError]").css("display", "block");
    else
        $("[id$=lblEmailError]").css("display", "none");

    if ($("[id$=lblPwdError]").val() != null)
        $("[id$=lblPwdError]").css("display", "block");
    else
        $("[id$=lblPwdError]").css("display", "none");

    if ($("[id$=lblFirstNameError]").val() != null)
        $("[id$=lblFirstNameError]").css("display", "block");
    else
        $("[id$=lblFirstNameError]").css("display", "none");

    if ($("[id$=lblConfirmPwdError]").val() != null)
        $("[id$=lblConfirmPwdError]").css("display", "block");
    else
        $("[id$=lblConfirmPwdError]").css("display", "none");



    if (isValidate == false)
        $("#errordiv").css("display", "block");
    else {
        $("#errordiv").css("display", "none");
        $(".notification").css("display", "block");
    }
    return isValidate;
}

function HideDisplayErrorMsgBox(isValidate) {
    if (isValidate == false)
        $("#errordiv").css("display", "block");
    else {
        $("#errordiv").css("display", "none");
        $(".notification").css("display", "block");
    }

}

function SetResetLabels() {
    if ($("[id$=lblEmailError]").val() != null)
        $("[id$=lblEmailError]").css("display", "block");
    else
        $("[id$=lblEmailError]").css("display", "none");

    if ($("[id$=lblPwdError]").val() != null)
        $("[id$=lblPwdError]").css("display", "block");
    else
        $("[id$=lblPwdError]").css("display", "none");

    if ($("[id$=lblFirstNameError]").val() != null)
        $("[id$=lblFirstNameError]").css("display", "block");
    else
        $("[id$=lblFirstNameError]").css("display", "none");

    if ($("[id$=lblConfirmPwdError]").val() != null)
        $("[id$=lblConfirmPwdError]").css("display", "block");
    else
        $("[id$=lblConfirmPwdError]").css("display", "none");
}

//Function used to calculate age using date of birth
function calculate_age(birth_month, birth_day, birth_year) {
    var today_date = new Date();
    var today_year = today_date.getFullYear();
    var today_month = today_date.getMonth();
    today_day = today_date.getDate();
    age = today_year - birth_year;

    if (today_month < (birth_month - 1)) {
        age--;
    }
    if (((birth_month - 1) == today_month) && (today_day < birth_day)) {
        age--;
    }
    return age;
}

