﻿using Calculate.Objects;
using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Reflection;

namespace Calculate
{
    public partial class Login : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// function to check login details for a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void checkLoginDetails(object sender, EventArgs e)
        {
            try
            {
                /* Create object of User Class */
                Users objUsers = new Users();
                Customers objCustomer = new Customers();
                /* Fetched data for user based on email ID */
                DataTable dtUserDetails = objUsers.getUserDetails(txtEmailAddress.Text);
                Session[Constants.CustomerId] = dtUserDetails.Rows[0][Constants.SessionUserId];
                Session["UserEmail"] = txtEmailAddress.Text;//Store in session to make transaction
                //To make sure user is not demo user.
                Session["Demo"] = null;
                /* Check if email exists in table or not */
                if (dtUserDetails.Rows.Count == Constants.zero)
                {
                    /* Display error message when user email id not exists in table */
                    lblEmailError.Visible = true;
                    lblEmailError.Text = Constants.invalidEmail;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(true);", true);
                }
                else
                {
                    if (dtUserDetails.Rows[Constants.zero][Constants.UserDeleteFlag].ToString().Equals(Constants.FlagYes))
                    {
                        /* Display Error Message in UI */
                        lblEmailError.Text = Constants.accountDeleted;
                        lblEmailError.Visible = true;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(true);", true);
                    }
                    else
                    {
                        if (dtUserDetails.Rows[Constants.zero][Constants.PasswordResetFlag].ToString().Equals(Constants.FlagYes) && (txtPassword.Text.Trim()).Equals(dtUserDetails.Rows[Constants.zero][Constants.Password].ToString()))
                        {
                            /* Assign User details into Session */
                            //Session[Constants.SessionRole] = dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString();
                            //Session[Constants.SessionUserId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
                            Session[Constants.EmailID] = txtEmailAddress.Text.Trim();
                            /* Redirect to User in UI based on its role */
                            Response.Redirect(Constants.RedirectChangePassword, false);
                        }
                        else
                        {
                            /* Check if Passowrd matched or not */
                            if (Encryption.Decrypt(dtUserDetails.Rows[Constants.zero][Constants.UserPassword].ToString()).Equals(txtPassword.Text))
                            {
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(false);", true);
                                /* Check if user activated or not */
                                if (Convert.ToString(dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag]).Equals(Constants.FlagNo))
                                {
                                    /* Display Error Message in UI */
                                    lblEmailError.Text = Constants.accountSuspend;
                                    lblEmailError.Visible = true;
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(true);", true);
                                }
                                else
                                {
                                    /* Assign User details into Session */
                                    Session[Constants.SessionRole] = dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString();
                                    Session[Constants.SessionUserId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
                                    DataTable dtCustomerDetails = objCustomer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                                    //string strMaritalStatus = dtCustomerDetails.Rows[Constants.zero][Constants.MartialStatus].ToString();
                                    /* Redirect to User in UI based on its role */
                                    if (dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString().Equals(Constants.Customer))
                                    {
                                        if (dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                                        {
                                            Session[Constants.UserRegistered] = Constants.value;

                                            /*Previously redirects to  InitialSocialSecurity Page*/
                                            Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
                                           
                                        }
                                        else
                                        {
                                            ///
                                            //Add new code to update IsSubscription in case of session expire while doing payment
                                            UpdateIsSubscriptionForUser();
                                            //
                                            //Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
                                        }
                                    }
                                    else
                                    {
                                        /* Redirect to Admin Welcome Page based on User Role */
                                        Session[Constants.SessionAdminId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
                                        if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                                        {
                                            Advisor objAdvisor = new Advisor();
                                            dtCustomerDetails = objAdvisor.getAdvisorDetailsByID(Session[Constants.SessionAdminId].ToString());
                                            if (!dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                                            {

                                                ///
                                                //Add new code to update IsSubscription in case of session expire while doing payment
                                                UpdateIsSubscriptionForUser();
                                                //

                                                //Changed to navigate to chargify page on the bases of plan
                                                //if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.MonthlyPlan))
                                                //    Response.Redirect(Constants.MonthlyUseInfusion, false);
                                                //else if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.AnnualPlan))
                                                //    Response.Redirect(Constants.AnnualUseInfusion, false);
                                            }
                                            else
                                            {

                                                Response.Redirect(Constants.RedirectManageCustomers, false);
                                            }
                                        }
                                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                                        {
                                            Response.Redirect(Constants.RedirectBackToInstitution, false);
                                        }
                                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                                        {
                                            Response.Redirect(Constants.RedirectToManageAdvisor, false);
                                        }
                                        else
                                            Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                                    }
                                }
                            }
                            else
                            {
                                /* Display Error message when invalid password entered by User */
                                lblPwdError.Visible = true;
                                lblPwdError.Text = Constants.invalidPassword;
                                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(true);", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "checkLoginDetails()" + ex.ToString());
                lblPwdError.Text = Constants.InvalidDetails;
                lblPwdError.Visible = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowDialog", "ShowDialog(true);", true);
            }
        }

        #endregion Events

        #region Methods
        /// <summary>
        /// Update IsSubscription for user 
        /// </summary>
        private bool UpdateIsSubscriptionForUser()
        {
            bool boolIsUpdate = false;
            string strEmail = string.Empty, strReference = string.Empty, strPassword = string.Empty, strRoleChar = string.Empty;
            int intInfusionID = 0;
            try
            {
                InsfisuionSoftProcesses objUserAuth = new InsfisuionSoftProcesses();
                Customers cs = new Customers();
                Users objUsers = new Users();
                strEmail = txtEmailAddress.Text;
                DataTable dtCustomerDetails = cs.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                //Get Id of user from Infusion Soft
                intInfusionID = objUserAuth.GetInfusionIdFromEmail(strEmail);
                DataTable dtUserDetails = objUsers.getUserDetails(strEmail);
                strReference = dtUserDetails.Rows[0]["UserId"].ToString();
                if (!string.IsNullOrEmpty(strReference))
                    strRoleChar = strReference.Substring(0, 1);
                if (intInfusionID > -1)
                {
                    cs.UpdateIsSubscription(strReference);
                    boolIsUpdate = true;

                    if (strRoleChar.Equals(Constants.C))
                    {
                        Session[Constants.UserRegistered] = Constants.value;
                        /*Previously redirects to  InitialSocialSecurity Page*/
                        Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
                    }
                    else if (strRoleChar.Equals(Constants.A))
                    {
                        Response.Redirect(Constants.RedirectManageCustomers, false);
                    }
                    else if (strRoleChar.Equals(Constants.I))
                    {
                        Response.Redirect(Constants.RedirectToManageAdvisor, false);
                    }

                }
                else
                {
                    if (strRoleChar.Equals(Constants.A))
                    {
                        if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.MonthlyPlan))
                            Response.Redirect("https://sscalculator.chargify.com/subscribe/mh5s5ys9j94g/social-security-calculator-monthly-plan?reference=" + Session[Constants.SessionAdminId].ToString(), false);
                        else if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.AnnualPlan))
                            Response.Redirect("https://sscalculator.chargify.com/subscribe/v94b4g26s2pv/social-security-calculator-annual-plan?reference=" + Session[Constants.SessionAdminId].ToString(), false);
                    }
                    else if (strRoleChar.Equals(Constants.C))
                    {
                        Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
                    }
                }
                return boolIsUpdate;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "UpdateIsSubscriptionForUser()" + ex.ToString());
                return boolIsUpdate ;
            }
        }
        #endregion Methods


    }
}