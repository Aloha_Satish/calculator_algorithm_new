﻿<%@ Page Title="Privacy Policy" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="FrmPrivacyPolicy.aspx.cs" Inherits="Calculate.FrmPrivacyPolicy" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div style="align-content: center; margin-left: 20px; margin-right: 20px; font-family: Arial" class="row">
        <h1>Privacy Policy</h1>
        <div>
            <p>
                This privacy policy discloses the privacy practices for <a href="http://www.calculatemybenefits.com">http://www.calculatemybenefits.com</a>. This privacy policy applies solely to information collected by this web site and provides the following:
                <br />
                <br />
                1.What personally identifiable information is collected from you through the web site, how it is used and with whom it may be shared.<br />
                2. What choices are available to you regarding the use of your data.<br />
                3. The security procedures in place to protect the misuse of your information.<br />
                4. How you can correct any inaccuracies in the information.<br />
            </p>

        </div>
        <div>
            <p style="font-size: large; font: bold; color: black">Information Collection, Use, and Sharing</p>
            <p>
                Filtech is the sole owner of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.
                <br />
                <br />
                Filtech will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.
                <br />
                <br />
                Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.
                <br />
            </p>
            <p style="font-size: large; font: bold; color: black">Your Access to and Control Over Information </p>
            <p>
                You may opt out of any future contacts from Filtech at any time. You can contact us via the email address or phone number given on our website:
                <br />
                <br />
                • See what data we have about you, if any. 
                <br />
                <br />
                • Change/correct any data we have about you.
                <br />
                <br />
                • Have us delete any data we have about you.
                <br />
                <br />
                • Express any concern you have about our use of your data.
                <br />
            </p>
            <p style="font-size: large; font: bold; color: black">Security </p>
            <p>
                Filtech takes precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.
                <br />
                <br />
                Wherever Filtech collects sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock icon at the bottom of your web browser, or looking for "https" at the beginning of the address of the web page.
                <br />
                <br />
                While Filtech uses encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.
                <br />
            </p>
            <p style="font-size: large; font: bold; color: black">Orders / Subscriptions </p>
            <p>
                Filtech requests information from you on our order/subscription form. To buy from us, you must provide contact information (like name and shipping address) and financial information (like credit card number, expiration date). This information is used for billing purposes and to fill your orders. If we have trouble processing an order, we'll use this information to contact you.
                <br />
            </p>
            <p style="font-size: large; font: bold; color: black">Updates</p>
            <p>
                Our Privacy Policy may change from time to time and all updates will be posted on this page.
            </p>
        </div>
    </div>
   
    <script type="text/javascript">
        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>

</asp:Content>
