﻿<%@ Page Title="Store" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="Store.aspx.cs" Inherits="Calculate.Store" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="col-md-12 divBg ">
        <h1 style="color: brown" class="setTopMargin">Store</h1>
        <center>
        <b>Getting Paid to Wait</b>
        <br />
        By Brian Doherty
        <br /><br />
        <div class="row clearfix ">
            <div class="col-md-6 column">
                <div class="bookImageAlignment" >
                    <img src="Images/Book.png" class="bookImage" />
                 </div>
            </div>
            <div class="col-md-6 column">
                 <div class="row clearfix">
                    <div class="col-md-5 column">
                        <div class="input-group storeCheckoutAlignment floatleft">
                            <asp:TextBox placeholder="Quantity"  MaxLength="3" TextMode="Number" ID="unit" runat="server" CssClass="form-control textEntry" Height="30px" ValidationGroup="StoreValidation"></asp:TextBox>
                            <br /><br />
                            <asp:Button runat="server" CssClass="btn button_login" ValidationGroup="StoreValidation" ID="checkout" OnClick="checkout_Click" Text="Proceed to Checkout" />
                        </div>
                    </div>
                    <div class="col-md-7 column" style="padding-left: 7px;">
                        <div class="storeCheckoutAlignment floatleft">
                            <asp:RequiredFieldValidator ID="QuantityRequired" ValidationGroup="StoreValidation" runat="server" ControlToValidate="unit" CssClass="failureForgotNotification" ErrorMessage=" Quantity is required."  Display="Dynamic" />
                            <asp:RangeValidator ID="RangeValidatorunit" Type="Integer" ValidationGroup="StoreValidation" CssClass="failureForgotNotification" ControlToValidate="unit" MaximumValue="1000" MinimumValue="0" runat="server" class="width240" ErrorMessage="Quantity Should be in between 1 to 1000" Display="Dynamic" ></asp:RangeValidator>
                        </div>

                    </div>
                     </div>
                </div>
            </div>
        </center>
        <br />
        <br />
        <div class="styleJustify ">
            <p class="floatLeft">
                <h1>Getting Paid To Wait: Bigger Social Security Benefits – The Simple and Easy Way</h1>
            </p>
            In Getting Paid to Wait, Brian Doherty shows readers the one strategy to earn the most Social Security income while delaying for maximum benefit. Social Security benefits increase by about 76% when you delay claiming them until age 70. At that point, you can lock in the highest Social Security benefit possible for the rest of your life. 

Everyone wants to maximize this valuable source of retirement income, especially those who have limited savings and no pension to rely on. But the challenge is waiting the eight years from age 62 to 70 without claiming Social Security benefits.
    <br />
            <br />
            <p>Brian offers a unique solution that makes it easier to delay than ever before by providing income while you wait. In fact, he shows the most profitable way to delay – the one strategy that provides the most Social Security income while you are delaying until age 70. His powerful Getting Paid To Wait SM strategy can substantially improve retirement lifestyle for individuals, couples or families. Order your book today!</p>

            <p>
                <b>Publication Date: January 2015 
        <br />
                    209 Pages, Hardcover
        <br />
                    Acanthus Publishing
        <br />
                    ISBN-13: 978-0-9905536-4-9<br />
                    Special Introductory Price: $19.95 
                </b>
            </p>
        </div>


    </div>
    <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>

    <script type="text/javascript">
        $(document).ready(function () {
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
            <%}%>
            $('[id$=unit]').attr("min", "1");

            $("#ImgLogin").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
            <%}%>
            $('[id$=unit]').attr("min", "1");

            $("#ImgLogin").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });
        });
    </script>
</asp:Content>--%>
