﻿<%@ Page Title="Institute Advisor" Language="C#" AutoEventWireup="true" CodeBehind="InstituteAdvisor.aspx.cs" Inherits="Calculate.InstituteAdvisor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title></title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />

    <script type="text/javascript">

        function SuccessFunction() {
            // They clicked Yes
            window.open("http://calculatemybenefits.com/login.aspx");
        }

        function HideModalPopupForMessage() {
            $find("MsgPopUp").hide();
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <div style="margin-top: 100px">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <asp:Label runat="server" Font-Bold="true" Text="Personal Information"></asp:Label>
                    <br />
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3 text-left">
                    <asp:Label ID="lblFirstname" ForeColor="#787878" runat="server" Text="First Name"></asp:Label>
                    <br />
                    <asp:TextBox CssClass="textEntry form-control" TabIndex="2" ID="txtFirstname" MaxLength="20" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Advisor First name is required." ToolTip="Advisor First name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*First name is required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Advisor First name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Advisor first name</asp:RegularExpressionValidator>
                </div>
                <div class="col-md-3 text-left">
                    <asp:Label ID="lblLastname" ForeColor="#787878" runat="server" Text="Last Name"></asp:Label>
                    <br />
                    <asp:TextBox CssClass="textEntry form-control" TabIndex="3" ID="txtLastname" MaxLength="20" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="regLastname" runat="server" ControlToValidate="txtLastname" CssClass="failureErrorNotification" ErrorMessage="Advisor last name is required." ToolTip="Advisor last name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Last name is required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="reqLastname" runat="server" ControlToValidate="txtLastname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Advisor last name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Advisor last name</asp:RegularExpressionValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-left">
                    <asp:Label ID="lblEmailad" ForeColor="#787878" runat="server" Text="Email"></asp:Label>
                    <br />
                    <asp:TextBox CssClass="textEntry form-control" TabIndex="4" autocomplete="off" MaxLength="50" ID="txtEmail" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:HiddenField ID="HiddenFieldEmail" runat="server" />
                    <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                </div>

            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <asp:Label runat="server" Font-Bold="true" Text="Broker Dealer Information"></asp:Label>
                    <br />
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3 text-left">
                    <asp:Label ID="lblBrokerDealer" ForeColor="#787878" runat="server" Text="Name of Broker Dealer"></asp:Label>
                    <br />
                    <asp:TextBox CssClass="textEntry form-control" TabIndex="5" autocomplete="off" MaxLength="50" ID="txtDealer" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:RequiredFieldValidator ID="reqDealer" runat="server" ControlToValidate="txtDealer" CssClass="failureErrorNotification" ErrorMessage="Broker Dealer is required." ToolTip="Broker Dealer is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Broker Dealer is required.</asp:RequiredFieldValidator>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3 text-left">
                    <asp:Label ID="lblCity" ForeColor="#787878" runat="server" Text="City"></asp:Label>
                    <br />
                    <asp:TextBox CssClass="textEntry form-control" TabIndex="6" autocomplete="off" MaxLength="50" ID="txtCity" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqCity" runat="server" ControlToValidate="txtCity" CssClass="failureErrorNotification" ErrorMessage="City is required." ToolTip="City is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*City is required.</asp:RequiredFieldValidator>
                </div>
                <div class="col-md-3 text-left">
                    <asp:Label ID="lblStateAd" ForeColor="#787878" runat="server" Text="State"></asp:Label>
                    <br />
                    <%--<asp:TextBox CssClass="textEntry form-control" TabIndex="5" AutoComplete="on"  ID="txtState" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>--%>
                    <asp:DropDownList CssClass="textEntry form-control" ID="ddlState" Width="208px" TabIndex="7" ValidationGroup="RegisterUserValidationGroup" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="reqState" runat="server" ControlToValidate="ddlState" CssClass="failureErrorNotification" ErrorMessage="Please select a State" ToolTip="Please select a State" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please select a State</asp:RequiredFieldValidator>
                </div>
            </div>
            <br />

            <div class="row ">
                <div class="col-md-11 col-sm-11">
                    <center>
             <asp:Button TabIndex="9" ID="btnSave" runat="server" Text="Submit" OnClick="Save" CssClass="button_login marginpopbottom" ValidationGroup="RegisterUserValidationGroup" />&nbsp;&nbsp;
           </center>
                </div>
                <br />

            </div>
            <div class="row">
                <div class="col-md-11 col-sm-11">
                    <center>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label>
                </center>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                </div>
            </div>
        </div>

        <asp:Panel ID="pnlShowMsg" CssClass="MailSentPopUpModalInstituteAdvisor" Style="padding-left: 20px" runat="server">
            <div class="modal-body row" style="margin-right: 1px; text-align:center">
                <br />
                <asp:Label runat="server" ID="lblMessage"></asp:Label>
                <br />
                <br />
            </div>
            <div class="row" style="margin-right: 25px">
                <%--<div class="col-md-1 col-sm-1"></div>--%>
                <%--<div class="col-md-4 col-sm-4">--%>
                <centre>
                    <asp:Button runat="server" ID="btnYes" CssClass="button_ok_poopup" Text="OK" OnClientClick="return SuccessFunction()" />
                    </centre>
                <%--</div>--%>
                <%--  <div class="col-md-2 col-sm-2">
                    <asp:Button runat="server" ID="btnNo" CssClass="button_ok_poopup" Text="No" OnClientClick="return HideModalPopupForMessage()" />
                </div>
                <div class="col-md-1 col-sm-1">
                </div>--%>
                <br />
                <br />

            </div>
        </asp:Panel>

        <asp:LinkButton ID="lnkFake1" runat="server"></asp:LinkButton>
        <cc1:ModalPopupExtender ID="MailSentPopUp" runat="server" BehaviorID="MsgPopUp" PopupControlID="pnlShowMsg" TargetControlID="lnkFake1" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>

    </form>
</body>

</html>
