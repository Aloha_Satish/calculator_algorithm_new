﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class AdvisorRegistration : System.Web.UI.Page
    {
        #region Events

        enum AdvisorPlan { Monthly, Annual };

        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtCustomerName.Focus();
                    txtCustomerName.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// Custome validation to check checkbox is checked or not
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void custCheckBoxIAgree_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ChkBoxIAgree.Checked;
        }
        /// <summary>
        /// code called to create new advisor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChkBoxIAgree.Checked)
                    addAdvisor();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion Events

        #region Methods


        /// <summary>
        /// Used to show terms a& conditions popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ShowAgreementDetails(object sender, EventArgs e)
        {
            try
            {
                popUpAgreementDetails.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ShowAgreementDetails();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to download terms & conditions in pdf form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DownloadTermsCondition(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Response.ContentType = Constants.pdfContentType;
                HttpContext.Current.Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Advisor_Subscription_Agreement" + Constants.pdfFileExtension);
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, Constants.zero, Constants.zero, -15f, Constants.zero);
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorAgreement, false, true, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainPara1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainPara2, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainPara3, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title1
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle1, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain1SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title2
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle2, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain2SubTitle5Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title3
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle3, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain3SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title4
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle4, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain4SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain4SubTitle1Para1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain4SubTitle1Para2, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain4SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain4SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title5
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle5, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain5SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain5SubTitle1Para1, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain5SubTitle1Para2, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain5SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain5SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                //Title6
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMainTitle6, false, false, true));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle1Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle2Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle3Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle4Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.AdvisorMain6SubTitle5Para, false, false, false));
                pdfDoc.Add(new iTextSharp.text.Paragraph(" "));

                pdfDoc.Close();
                HttpContext.Current.Response.Write(pdfDoc);
                HttpContext.Current.Response.End();

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to close the pop up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popUpAgreementDetails.Hide();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// code for creating a new advisor
        /// </summary>
        private void addAdvisor()
        {
            try
            {
                string strQueryStringPlan = string.Empty;
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                objCustomer.Email = txtEmail.Text;
                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* initialize Instance of classes */
                    AdvisorTO objAdvisorTO = new AdvisorTO();
                    Advisor objAdvisor = new Advisor();
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    #region set Insert Parameters
                    /* Assign textboxes content into institutional variables */
                    objAdvisorTO.AdvisorID = Database.GenerateGID("A");
                    objAdvisorTO.InstitutionAdminID = "DI1";
                    objAdvisorTO.AdvisorName = textInfo.ToTitleCase(txtCustomerName.Text);
                    objAdvisorTO.Role = Constants.Advisor;
                    objAdvisorTO.Firstname = textInfo.ToTitleCase(txtCustomerName.Text);
                    objAdvisorTO.Lastname = string.Empty;
                    objAdvisorTO.Email = txtEmail.Text;
                    objAdvisorTO.Password = Encryption.Encrypt(txtCustomerPassword.Text);
                    objAdvisorTO.isInstitutionSubscription = Constants.FlagNo;
                    objAdvisorTO.DelFlag = Constants.FlagNo;
                    objAdvisorTO.PasswordResetFlag = Constants.FlagNo;
                    objAdvisorTO.ChoosePlan = Globals.Instance.strAdvisorPlan;
                    objAdvisorTO.isSubscription = Constants.FlagNo;
                    objAdvisorTO.State = string.Empty;
                    objAdvisorTO.Dealer = string.Empty;
                    objAdvisorTO.City = string.Empty;
                    #endregion set Insert Parameters
                    /* Insert institution details into Database */
                    objAdvisor.insertAdvisorDetails(objAdvisorTO);
                    clsFlag.Flag = false;
                    /* Reload gridview with updated data */
                    lblUserCreate.Visible = true;
                    lblUserExist.Visible = false;
                    lblUserCreate.Text = Constants.AdvisorAdded;
                    ClearFields();
                    Session[Constants.SessionRole] = Symbolics.Constants.Advisor;
                    Session[Constants.SessionUserId] = objAdvisorTO.AdvisorID;
                    Session[Constants.SessionAdminId] = objAdvisorTO.AdvisorID;

                    //Previously navigating to pop up to select monthly or annual plan
                    //Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                    //Changed to naviage on chargify for payment. changed on 05 oct 2016
                    MailSentPopUp.Show();


                    Session[Constants.SessionRegistered] = Constants.ColaStatusT;
                }
                else
                {
                    lblUserExist.Text = Constants.customerExists;
                    lblUserExist.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to navigate on particular plan on the basis of querystring and session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RedirectToChargify(object sender, EventArgs e)
        {
            try
            {
                string strQueryStringPlan = string.Empty;
                if (Request.QueryString[Constants.AdvisorPlan] != null)
                    strQueryStringPlan = Request.QueryString[Constants.AdvisorPlan].ToString();
                if (!string.IsNullOrEmpty(strQueryStringPlan))
                {
                    if (strQueryStringPlan.ToLower().Equals(Constants.MonthlyPlan.ToLower()))
                    {
                        Response.Redirect("https://sscalculator.chargify.com/subscribe/mh5s5ys9j94g/social-security-calculator-monthly-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
                    }
                    else if (strQueryStringPlan.ToLower().Equals(Constants.AnnualPlan.ToLower()))
                    {
                        Response.Redirect("https://sscalculator.chargify.com/subscribe/v94b4g26s2pv/social-security-calculator-annual-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
                    }
                }
                else
                {
                    if (Globals.Instance.strAdvisorPlan.Equals(Constants.MonthlyPlan))
                        Response.Redirect("https://sscalculator.chargify.com/subscribe/mh5s5ys9j94g/social-security-calculator-monthly-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
                    else
                        Response.Redirect("https://sscalculator.chargify.com/subscribe/v94b4g26s2pv/social-security-calculator-annual-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code t clear the text from the feilds
        /// </summary>
        public void ClearFields()
        {
            try
            {
                txtCustomerName.Text = string.Empty;
                txtEmail.Text = string.Empty;
                txtCustomerName.Text = string.Empty;
            }
            catch (Exception ex)
            {

                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to redirect back to customer registration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustomerLink_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectRegister, false);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods
    }
}