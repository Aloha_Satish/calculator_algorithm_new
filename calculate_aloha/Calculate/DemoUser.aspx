﻿<%@ Page Language="C#" Title="Demo Calculator" AutoEventWireup="true" CodeBehind="DemoUser.aspx.cs" Inherits="Calculate.DemoUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title>Demo User Registration</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap-dialog.js"></script>
    <script type="text/javascript">
        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

        //Processing Window
        ijQuery(window).load(function () {
            var notification_loader;

            ijQuery('body').hide().show();

            notification_loader = ijQuery('.notification-loader');
            notification_loader.attr('src', notification_loader.attr('rel'));
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="overlay" class="web_dialog_overlay_Pricing">
                <div class="web_dialog_Single_Use" style="display: block;">
                    <div class="row">
                        <div id="color-overlay_singleUse">
                        </div>
                        <div>
                            <div class="col-md-12 ">
                                <div class="login-symbols_register">
                                    <table>
                                        <tr>
                                            <td>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <img src="/Images/Newlogo2.png" style="height: 50px; width: 50px;" />
                                                            </td>
                                                            <td>
                                                                <div style="margin-top: 10px;">
                                                                    <h1>
                                                                        <p style="font-family: 'Times New Roman'; font-size: 24px;">
                                                                            <font color="#2f2e2e">THE PAID TO WAIT</font>
                                                                        </p>
                                                                    </h1>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </td>


                                        </tr>

                                        <tr>
                                            <td>
                                                <div>
                                                    <p style="line-height: 45px; font-size: 29px; padding-bottom: 90px; margin-left: 7px;">
                                                        <font color="#2f2e2e">SOCIAL SECURITY CALCULATOR</font>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <%--Starting Blank Space--%>
                        <div class="col-md-2">
                            <div class="contents">
                                <div id="errordiv" class="RegistrationErrorWindow" style="display: none; margin-top: 104px !important">
                                    <div class="email-form-messagebox-header">Please Fix These Errors</div>
                                    <div class="email-form-messagebox" style="background-color: white; border-radius: 5px;">
                                        <span>
                                            <asp:Label ID="lblFirstNameError" ForeColor="Red" Font-Size="12px" runat="server" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblEmailError" ForeColor="Red" Font-Size="12px" runat="server" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblPwdError" ForeColor="Red" Font-Size="12px" runat="server" Text="" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblConfirmPwdError" ForeColor="Red" Font-Size="12px" runat="server" Text="" Visible="true"></asp:Label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--Registration Window--%>
                        <div class="col-md-3">
                            <div class="contents">
                                <h1>
                                    <p style="text-align: center; line-height: 50.4px; font-size: 36px;">DEMO</p>
                                </h1>
                            </div>

                            <div class="contents">
                                <p style="text-align: left; line-height: 22.4px; font-size: 16px;">
                                    Try the calculator out. In less than 5 minutes, see how simple and easy it is to get your custom Social Security plan.&nbsp;
                                </p>
                            </div>
                            <asp:Panel ID="pnlStep1" DefaultButton="btnGetStarted" runat="server" Visible="true">
                                <div>
                                    <asp:TextBox runat="server" ID="txtFirstName" CssClass="textBoxSingleUse" placeholder="First Name" />
                                    <br />
                                    <asp:TextBox runat="server" ID="txtEmailAddress" CssClass="textBoxSingleUse" placeholder="Email" />
                                    <br />
                                    <asp:Label ID="lblEmailInstruct" Style="font-size: 11px; color: inactivecaptiontext;" Text="(Please enter your E-mail address so that we can send your Social Security Report.)" runat="server"></asp:Label>
                                    <br />
                                    <br />
                                    <asp:Button runat="server" ID="btnGetStarted" OnClientClick="return ValidateRgistrationDetails();" Text="Get Started" class="btnSingleUse" OnClick="GetStarted" />
                                    <br />
                                    <br />
                                    <div class="contents">
                                        <a href="/Login.aspx" style="line-height: 22.4px; font-size: 16px; padding-top: 10px"><u>Already have an account?</u> </a>
                                    </div>
                                </div>
                            </asp:Panel>

                        </div>

                        <%--Error Window--%>
                        <div class="col-md-1">
                            
                        </div>


                        <%--Arrow--%>
                        <div class="col-md-1">
                            <div class="VerticalLine" style="margin-top: 119px">
                                <div class="line-vertical" style="width: 10px; height: 100%; border-right: 2px solid #ffffff;"></div>
                            </div>
                        </div>

                        <%--Sample Report--%>
                        <div>
                            <div class="col-md-4">
                                <div>
                                    <div class="contents">
                                        <h1>
                                            <center>
                                        <p style="text-align: left; line-height: 50.4px; font-size: 36px; text-align:center;">SAMPLE REPORT</p>
                                            </center>
                                        </h1>
                                    </div>

                                    <div class="contents" style="text-align: center">
                                        <img src="/Images/Screenshot-2.png" style="height: 257px; width: 184px; margin-top: 23px;">
                                    </div>

                                    <div class="contents">
                                        <p style="line-height: 22.4px; font-size: 16px; padding-top: 29px; text-align: center;"><a href="/DownloadSampleReport.ashx">Download Sample report</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>


                </div>

                <div class="notification" style="display: none;">
                    <div class="notification-overlay"></div>
                    <div class="notification-inner" style="left: 325px;">
                        <img rel="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" src="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" class="loading notification-loader" alt="" style="display: none;">
                        <span class="message">Processing...</span>
                        <%-- <span class="close-button" onclick="jQuery(this).parent().parent().hide()">Close</span>--%>
                    </div>
                </div>

            </div>

            <div class="footerbg" style="padding-top: 0px">
                <div>
                    <div class="clearfix">
                        <div class="col-md-12 column removepaddingleftright">
                            <div id="Div1" class="divFoot" runat="server">
                                <table class="table-responsive">
                                    <tr class="col-md-12 col-sm-12">
                                        <td class="col-md-2 col-sm-2"></td>
                                        <td class="col-md-3 col-sm-3">
                                            <div id="footer-left" style="line-height: 22.4px; padding-top: 25px; font-size: 16px;">
                                                &copy; 2017 Filtech, LLC. All Rights Reserved.
                                                <br />
                                                3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                                Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                                <br />
                                                <p style="padding-top: 10px;">
                                                    <%if (Session["UserID"] != null)
                                                      {%>
                                                    <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%}
                                                      else
                                                      { %>
                                                    <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%} %>
                                                </p>
                                                <br />
                                            </div>
                                        </td>
                                        <td class="col-md-2 col-sm-2">
                                            <table style="width: 100px">
                                                <tr>
                                                    <td>
                                                        <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                            <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                            <img src="/Images/fbicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://twitter.com/BrianDoherty57">
                                                            <img src="/Images/twittericon.png" class="img-responsive" />
                                                        </a>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
