﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class SocialSecurityCalculator : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        #endregion Variables

        #region Events

        /// <summary>
        /// Function call while page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                /* Check if page is post back or not */

                if (!IsPostBack)
                {

                    this.PopulateYear_Your();
                    this.PopulateMonth_Your();
                    this.PopulateDay_Your();

                    this.PopulateYear_Spouse();
                    this.PopulateMonth_Spouse();
                    this.PopulateDay_Spouse();

                    if (!Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        BtnChange.Visible = true;
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        /* Disabled textbox and radio button so that institutional user can only view customer details */
                        btnSaveBasicInfoAndProceed.Text = "Next - Social Security Information";
                        ddlDay_Your.Enabled = false;
                        ddlMonth_Your.Enabled = false;
                        ddlYear_Your.Enabled = false;
                        //txtBirthDate.Enabled = false;
                        txtFirstname.Enabled = false;
                        // txtLastname.Enabled = false;
                        //txtSpouseBirthdate.Enabled = false;
                        txtSpouseFirstname.Enabled = false;
                        //txtSpouseLastname.Enabled = false;
                        rdbtnDivorced.Enabled = false;
                        rdbtnMarried.Enabled = true;
                        rdbtnSingle.Enabled = false;
                        rdbtnWidowed.Enabled = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableDatePicker", "disableDatePicker();", true);
                    }
                    /* Put focus on First Name Textbox Field */
                    txtFirstname.Focus();
                    /* Fill Customer Information based on Session UserID */
                    fillBasicInformation();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClinetScriptSpouseInformationName, Constants.ClinetScriptSpouseInformation, true);
                }
                else
                {
                    if (Request.Form[ddlDay_Your.UniqueID] != null)
                    {
                        this.PopulateDay_Your();
                        ddlDay_Your.ClearSelection();
                        ddlDay_Your.Items.FindByValue(Request.Form[ddlDay_Your.UniqueID]).Selected = true;
                    }
                    if (Request.Form[ddlDay_Spouse.UniqueID] != null)
                    {
                        this.PopulateDay_Spouse();
                        ddlDay_Spouse.ClearSelection();
                        ddlDay_Spouse.Items.FindByValue(Request.Form[ddlDay_Spouse.UniqueID]).Selected = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "Page_Load() - " + ex.ToString());
            }

        }

        /// <summary>
        /// Save Basic Customer Information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBasicInfoAndProceed_Click(object sender, EventArgs e)
        {
            try
            {
                Session["CustAfterTrans"] = string.Empty;

                updateCustomerDetails();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "btnSaveBasicInfoAndProceed_Click() - " + ex.ToString());
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Validate User based on Session Data
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "validateSession() - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        private void fillBasicInformation()
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());

                #region set Values to fields

                txtFirstname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerFirstName].ToString();
                //txtLastname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerLastName].ToString();
                if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerBirthDate].ToString()) && dtCustomerDetails.Rows[Constants.zero][Constants.CustomerBirthDate].ToString() != Constants.DefaultDate)
                {
                    //txtBirthDate.Text = Convert.ToDateTime(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerBirthDate]).ToShortDateString();
                    char[] delimiters = new char[] { '/', '-' };
                    string HusbandBirth = Convert.ToDateTime(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerBirthDate]).ToShortDateString();
                    string[] HusbandBirthArray = HusbandBirth.Split(delimiters, 3);
                    ddlYear_Your.SelectedValue = HusbandBirthArray[2];
                    ddlMonth_Your.SelectedValue = HusbandBirthArray[0];
                    ddlDay_Your.SelectedValue = HusbandBirthArray[1];
                }


                if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseBirthDate].ToString()) && dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseBirthDate].ToString() != Constants.DefaultDate)
                {
                    //txtSpouseBirthdate.Text = Convert.ToDateTime(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseBirthDate]).ToShortDateString();
                    char[] delimiters = new char[] { '/', '-' };
                    string SpouseBirth = Convert.ToDateTime(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseBirthDate]).ToShortDateString();
                    string[] SpouseBirthArray = SpouseBirth.Split(delimiters, 3);
                    ddlYear_Spouse.SelectedValue = SpouseBirthArray[2];
                    ddlMonth_Spouse.SelectedValue = SpouseBirthArray[0];
                    ddlDay_Spouse.SelectedValue = SpouseBirthArray[1];
                }
                txtSpouseFirstname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseFirstName].ToString();
                // txtSpouseLastname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerSpouseLastName].ToString();
                switch (dtCustomerDetails.Rows[Constants.zero][Constants.CustomerMaritalStatus].ToString())
                {
                    case Constants.Divorced:
                        rdbtnDivorced.Checked = true;
                        break;
                    case Constants.Married:
                        rdbtnMarried.Checked = true;
                        break;
                    case Constants.Single:
                        rdbtnSingle.Checked = true;
                        break;
                    case Constants.Widowed:
                        rdbtnWidowed.Checked = true;
                        break;
                    default:
                        rdbtnSingle.Checked = true;
                        break;
                }

                #endregion set Values to fields
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "fillBasicInformation() - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used to update Customer Basic Informations.
        /// </summary>
        private void updateCustomerDetails()
        {
            try
            {
                if (ValidateInput())
                {
                    /* Create an object of Customer Class */
                    Customers objCustomer = new Customers();
                    #region Assign values into Customer CLass Object
                    objCustomer.CustomerID = Session[Constants.SessionUserId].ToString();
                    objCustomer.FirstName = textInfo.ToTitleCase(txtFirstname.Text);
                    objCustomer.LastName = string.Empty;
                    // objCustomer.Birthdate = this.SelectedDate_Your.ToShortDateString();
                    objCustomer.Birthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Your)).Replace("-", "/");

                    //To remove the Spouse data to be input in database
                    objCustomer.SpouseFirstname = textInfo.ToTitleCase(txtSpouseFirstname.Text);
                    objCustomer.SpouseLastname = string.Empty;
                    if (rdbtnMarried.Checked || rdbtnDivorced.Checked)
                        objCustomer.SpouseBirthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(this.SelectedDate_Spouse)).Replace("-", "/");
                    else
                        objCustomer.SpouseBirthdate = String.Empty;
                    //objCustomer.SpouseBirthdate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(txtSpouseBirthdate.Text)).Replace("-", "/");
                    /* Get Marital Status based on checked radio button */
                    if (rdbtnDivorced.Checked)
                        objCustomer.MartialStatus = Constants.Divorced;
                    else if (rdbtnMarried.Checked)
                        objCustomer.MartialStatus = Constants.Married;
                    else if (rdbtnSingle.Checked)
                        objCustomer.MartialStatus = Constants.Single;
                    else
                        objCustomer.MartialStatus = Constants.Widowed;

                    #endregion Assign values
                    objCustomer.updateCustomerTableInfo(objCustomer);
                    /* Update Customer Information into Database */
                    objCustomer.updateCustomerPersonalInfo(objCustomer);

                    if (rdbtnWidowed.Checked)
                        Response.Redirect(Constants.RedirectCalcWidowed, false);
                    else if (rdbtnMarried.Checked)
                        Response.Redirect(Constants.RedirectCalcMarried, false);
                    else if (rdbtnDivorced.Checked)
                        Response.Redirect(Constants.RedirectCalcDivorced, false);
                    else
                        Response.Redirect(Constants.RedirectCalcSingle, false);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "updateCustomerDetails() - " + ex.ToString());
            }
        }

        protected bool ValidateInput()
        {
            try
            {
                bool isValid = true;

                if (DateTime.Now.AddYears(-70) > Convert.ToDateTime(this.SelectedDate_Your))
                {
                    // if (errorMessage.Text != "")
                    errorMessage.Text = "* Your age cannot exceed 70.";
                    ScriptManager.RegisterStartupScript(this, typeof(string), "displayErrorMessage", "displayErrorMessage();", true);
                    isValid = false;
                    errorMessage.Visible = true;
                }
                else
                {
                    errorMessage.Text = "";
                    ScriptManager.RegisterStartupScript(this, typeof(string), "hideErrorMessage", "hideErrorMessage();", true);
                }

                if (rdbtnMarried.Checked == true)
                {
                    if (DateTime.Now.AddYears(-70) > Convert.ToDateTime(this.SelectedDate_Spouse))
                    {
                        // if (errorMessage.Text != "")
                        errorMessage0.Text = "* Spouse's age cannot exceed 70.";
                        ScriptManager.RegisterStartupScript(this, typeof(string), "displayErrorMessage", "displayErrorMessage();", true);
                        errorMessage0.Visible = true;
                        isValid = false;

                    }
                    else
                    {
                        errorMessage0.Text = "";
                        errorMessage0.Visible = false;
                    }
                }

                //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, (!String.IsNullOrEmpty(errorMessage.Text)) ? "displayErrorMessage();" : "hideErrorMessage();", true);

                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "ValidateInput() - " + ex.ToString());
                return false;
            }
        }

        private int Day_Your
        {
            get
            {
                if (Request.Form[ddlDay_Your.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Your.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Your.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Your();
                ddlDay_Your.ClearSelection();
                ddlDay_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Your
        {
            get
            {
                return int.Parse(ddlMonth_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Your();
                ddlMonth_Your.ClearSelection();
                ddlMonth_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Your
        {
            get
            {
                return int.Parse(ddlYear_Your.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Your();
                ddlYear_Your.ClearSelection();
                ddlYear_Your.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Your
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Your + "/" + this.Day_Your + "/" + this.Year_Your);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Your = value.Year;
                    this.Month_Your = value.Month;
                    this.Day_Your = value.Day;
                }
            }
        }

        private void PopulateDay_Your()
        {
            ddlDay_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Your.Items.Add(lt);
            int days = DateTime.DaysInMonth(this.Year_Your, this.Month_Your);
            for (int i = 1; i <= days; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Your.Items.Add(lt);
            }
            ddlDay_Your.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Your()
        {
            ddlMonth_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Your.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Your.Items.Add(lt);
            }
            ddlMonth_Your.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Your()
        {
            ddlYear_Your.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Your.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Your.Items.Add(lt);
            }
            ddlYear_Your.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        private int Day_Spouse
        {
            get
            {
                if (Request.Form[ddlDay_Spouse.UniqueID] != null)
                {
                    return int.Parse(Request.Form[ddlDay_Spouse.UniqueID]);
                }
                else
                {
                    return int.Parse(ddlDay_Spouse.SelectedItem.Value);
                }
            }
            set
            {
                this.PopulateDay_Spouse();
                ddlDay_Spouse.ClearSelection();
                ddlDay_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Month_Spouse
        {
            get
            {
                return int.Parse(ddlMonth_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateMonth_Spouse();
                ddlMonth_Spouse.ClearSelection();
                ddlMonth_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        private int Year_Spouse
        {
            get
            {
                return int.Parse(ddlYear_Spouse.SelectedItem.Value);
            }
            set
            {
                this.PopulateYear_Spouse();
                ddlYear_Spouse.ClearSelection();
                ddlYear_Spouse.Items.FindByValue(value.ToString()).Selected = true;
            }
        }

        public DateTime SelectedDate_Spouse
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.Month_Spouse + "/" + this.Day_Spouse + "/" + this.Year_Spouse);
                }
                catch
                {
                    return DateTime.MinValue;
                }
            }
            set
            {
                if (!value.Equals(DateTime.MinValue))
                {
                    this.Year_Spouse = value.Year;
                    this.Month_Spouse = value.Month;
                    this.Day_Spouse = value.Day;
                }
            }
        }

        private void PopulateDay_Spouse()
        {
            ddlDay_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "DD";
            lt.Value = "0";
            ddlDay_Spouse.Items.Add(lt);
            int days = DateTime.DaysInMonth(this.Year_Spouse, this.Month_Spouse);
            for (int i = 1; i <= days; i++)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlDay_Spouse.Items.Add(lt);
            }
            ddlDay_Spouse.Items.FindByValue(DateTime.Now.Day.ToString()).Selected = true;
        }

        private void PopulateMonth_Spouse()
        {
            ddlMonth_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "MM";
            lt.Value = "0";
            ddlMonth_Spouse.Items.Add(lt);
            for (int i = 1; i <= 12; i++)
            {
                lt = new ListItem();
                lt.Text = Convert.ToDateTime(i.ToString() + "/1/1900").ToString("MMMM");
                lt.Value = i.ToString();
                ddlMonth_Spouse.Items.Add(lt);
            }
            ddlMonth_Spouse.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;
        }

        private void PopulateYear_Spouse()
        {
            ddlYear_Spouse.Items.Clear();
            ListItem lt = new ListItem();
            lt.Text = "YYYY";
            lt.Value = "0";
            ddlYear_Spouse.Items.Add(lt);
            for (int i = DateTime.Now.Year; i >= 1900; i--)
            {
                lt = new ListItem();
                lt.Text = i.ToString();
                lt.Value = i.ToString();
                ddlYear_Spouse.Items.Add(lt);
            }
            ddlYear_Spouse.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
        }

        #endregion Methods

        #region BtnClick
        protected void BtnChange_Click(object sender, EventArgs e)
        {
            try
            {
                /* Get Institution Admin ID */
                Customers objCustomer = new Customers();
                DataTable dtCustomerData = objCustomer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    Response.Redirect(Constants.RedirectManageCustomers, false);
                else
                    Response.Redirect(Constants.RedirectManageCustomersWithAdvisorID + dtCustomerData.Rows[Constants.zero][Constants.CustomerAdvisorId].ToString(), false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionSocialSecurityCalculator + "BtnChange_Click() - " + ex.ToString());
            }
        }

        #endregion BtnClick
    }
}