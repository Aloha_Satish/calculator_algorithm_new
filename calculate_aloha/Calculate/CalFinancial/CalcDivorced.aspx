﻿<%@ Page Title="Calculator Divorced" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="CalcDivorced.aspx.cs" Inherits="Calculate.WebForm3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize3 text-center setTopMargin">Calculation and Strategies for Divorced Couples</h1>
    <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:Panel ID="PanelEntry" runat="server">
        <div class="col-md-12 col-sm-12 failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="row margileft margiright">
            <div class="col-md-12 col-sm-12">
                <p class="Font16 text-justify">
                    In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                </p>
                <br />
                <asp:Label ID="LabelPlease" runat="server" Font-Bold="True" Font-Names="Arial" Text="Please enter your information:"></asp:Label>
                <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                <asp:Label ID="LabelWifeBene" runat="server" Text="What is your monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneWife" CssClass="textEntry form-control" MaxLength="7" runat="server" TabIndex="1" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Your monthly Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Your monthly Full Retirement Age Benefit must be a number" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeYYYY" runat="server" MaxLength="4" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <asp:Label ID="LabelHusbandBene" runat="server" Text="What is your Ex-Spouse's monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneHusband" CssClass="textEntry form-control" MaxLength="7" runat="server" TabIndex="2" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Ex-Spouse's monthly Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Ex-Spouse's monthly Full Retirement Age Benefit is required.</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Ex-Spouse's monthly Full Retirement Age Benefit must be a number" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Ex-Spouse's monthly Full Retirement Age Benefit must be a number.</asp:RegularExpressionValidator>
                <asp:TextBox Visible="false" ID="BirthHusbandMM" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandDD" Visible="false" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandTempYYYY" Visible="false" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandYYYY" MaxLength="4" Visible="false" runat="server" Width="80px" CssClass="textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <%--<asp:Button ID="btnSaveBasicInfoAndProceed" runat="server" TabIndex="4" Text="Previous - Basic Information" OnClick="btnSaveBasicInfoAndProceed_Click" CssClass="button_login" />--%>
                <asp:Button ID="BtnCalculate" ValidationGroup="RegisterUserValidationGroup" runat="server" Text="Next - View Report" CssClass="button_login btnmarginsingle" OnClick="Calculate_Click" TabIndex="5" />
                <br />
                <br />
                <br />
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelParms" runat="server">
        <div class="row">
            <br />
            <div class="col-md-7 col-sm-7 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        Ex-Spouse
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Date of Birth:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold ">
                        Marital Status:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        Divorced
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-4 col-sm-4">
                        2.5%
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <%if (Session["AdminID"] != null)
                  {%>
                <asp:Button ID="btnManageCustomer" runat="server" CssClass="button_login " OnClick="Backto_Managecustomer" Text="Back to All Clients" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnDownloadReport" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download Report" />
                <%} %>
                <%else
                  {
                   if (Session["Demo"] == null)
                   {   
                %>
                <asp:Button ID="BtnChange" runat="server" OnClick="Calculate_Click" Text="Edit Information" CssClass="button_login" TabIndex="11" />
                <%} %>
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Text="Explanation of Strategies" Width="163px" OnClick="btnExplain_Click" />
                <asp:Button ID="Button2" runat="server" CssClass="button_login" OnClick="PrintAll_Click" Text="Download Report" />
                <%} %>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div class="margileft margiright">
            <br />
            <table>
                <%--<tr>
                    <td align="center">  <asp:Image ID="Image1" class="displayNone" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" />
                        <asp:Label ID="Label1" Style="display: none; color: green; font-weight: bold; position: static;" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Image ID="imgCheck" class="displayNone" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" />
                        <asp:Label ID="lblBestPlan" Style="display: none; color: green; font-weight: bold; position: static;" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                    <td></td>
                </tr>--%>
                <tr class="fontLarge">
                    <td id="strategy1" runat="server" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy1" runat="server" GroupName="Divorced" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber1" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" /><br />
                         <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle1" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue1" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>
                    <td id="strategy2" runat="server" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy2" Text="Strategy 2" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy2" runat="server" GroupName="Divorced" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber2" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" /><br />
                         <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle2" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue2" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>
                    <td id="strategy3" runat="server" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy3" runat="server" GroupName="Divorced" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber3" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" /><br />
                         <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle3" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue3" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <!-- end slider-container -->
        <!-- Strategies -->
        <!--strategy-1 -->
        <div id="strategy-BestStrategy" class="strategy-container displayNone">
            <asp:Panel ID="PanelAsap" runat="server">
                <asp:Label ID="LabelAsap" runat="server" class="displayNone" Text="Claim Your Work History Benefit as Early as Possible"></asp:Label>
                <asp:Panel runat="server" ID="ExplainAsap" CssClass="text-justify margiright">
                    <asp:Label ID="StrategyAsap" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteAsap" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures2" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Names="Arial" ForeColor="Black" Font-Bold="true"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures2" runat="server" Text="Next Steps:" CssClass="margileft" Font-Names="Arial" ForeColor="Black" Font-Bold="true"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                            <br />
                        </div>
                    </div>
                </asp:Panel>
                <%--  <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                      <asp:Button ID="btnAsapDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridAsap" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <br />
                <center>  <div class="table-responsive">
                    <asp:Chart ID="ChartAsap" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br />
                    <br />
                    <asp:Label ID="ErrorMessageLabel" runat="server" Text="Label" Visible="False"></asp:Label>
                </div></center>
            </asp:Panel>
        </div>
        <!-- strategy-2 -->
        <div id="strategy-SuspendStrategy" class="strategy-container">
            <asp:Panel ID="PanelStandard" runat="server">
                <asp:Label ID="LabelStandard" runat="server" class="displayNone" Text="Claim Your Work History Benefit at FRA"></asp:Label>
                <asp:Panel runat="server" ID="ExplainStandard" CssClass="text-justify margiright">
                    <asp:Label ID="StrategyStandard" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteStandard" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Names="Arial" ForeColor="Black" Font-Bold="true"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblNewKey" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures1" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Names="Arial" ForeColor="Black" Font-Bold="true"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                       <asp:Button ID="btnDownLoadStandard" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridStandard" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <br />
                <center> <div class="table-responsive">
                    <asp:Chart ID="ChartStandard" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br />
                    <br />
                </div></center>
            </asp:Panel>
        </div>
        <!--strategy-3 -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="PanelSpouse" runat="server">
                <asp:Label ID="LabelSpouse" runat="server" class="displayNone" Text="Claim Your Spousal Benefit at FRA & Delay Work History Benefit"></asp:Label>
                <asp:Panel runat="server" ID="ExplainSpouse" CssClass="text-justify margiright">
                    <asp:Label ID="StrategySpouse" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteSpouse" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="KeyFigures" runat="server" Text="Key Benefit Numbers:" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Font-Bold="true"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label><br />
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label><br />
                        </div>
                       <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblRestrictIcomeKey" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label><br />
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps9" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnSpouseDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridSpouse" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <br />
                <center> <div class="table-responsive">
                    <asp:Chart ID="ChartSpouse" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br />
                    <br />
                </div></center>
            </asp:Panel>
        </div>

        <a href="../FrmDisclaimer.aspx">Disclaimer </a>

    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center> <div id="plans" class="plan-contentnew">
            <br />
            <asp:Label ID="MaxCumm" class="subspopuplabelsColor" runat="server"></asp:Label>
            <br />
            <h2 class="subspopupText">While growing your Social Security Check to the largest amount possible!
                <br />
                <br />
                Find out how, for just 39.95 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CancelPopup" OnClick="Cancel_Click" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
        </div></center>
    </div>

    <asp:Panel ID="pnlExplanation" ScrollBars="Auto" runat="server" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Explanation of the Strategies
            <asp:Button ID="btnCancelTop" runat="server" CssClass="button_login floatRight" OnClick="imgCancel_Click" Text="Back to the Strategies" />
        </div>
        <div class="textExplanation plan-contentnew">
            <br />
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <center>
                    <div style="font-size: large; font: bold;">
                        Divorced Couples
                    </div>
                        </center>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What is the Primary Goal Of the Paid to Wait Calculator?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. Qualified divorced spouses could have a number of options available to them that could make it easier for them to maximize the amount of their benefit. They could even receive a  benefit based on the size of their ex-spouse’s Social Security benefit. The Paid to Wait Calculator will automatically determine if it makes sense for you to claim a benefit based on your ex-spouse’s benefit amount.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Many Divorced Spouses Can Get Paid to Wait&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The Paid to wait Calculator can help make it easier to maximize the size of your benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait. In other words, while you are waiting to claim your maxed out benefit at age 70, so you can maximize the size of it, the Paid to Wait Calculator will show you the one claiming strategy that will pay you the most amount of Social Security Income while you wait.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Can All Qualified Divorced Spouses Get Paid to Wait?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    No, not all qualified divorced spouses can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait calculator will show you exactly how to do it.  
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How do I Know if I am Qualified?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    If you were married for 10 years or more before your divorce and never remarried, then you are qualified to claim a Spousal Benefit and receive a benefit amount up to 50% or half of your ex-spouse’s Full Retirement Age benefit. This does not effect the size of the benefit your ex-spouse receives, they still receive their full benefit, but if you wait until your Full Retirement Age you are entitled to receive a benefit amount equal to 50% of your ex-spouse’s benefit.<br />
                                    <br />
                                    The Paid to Wait Calculator assumes that you meet the qualifications and will automatically determine if it makes sense for you to claim a benefit based on your ex-spouse’s benefit amount.<br />
                                    <br />
                                    If you were NOT married for at least 10 years before your divorce or you have remarried, you are NOT qualified and will have to change your marital status to either “Single” or “Married” and re-run the calculation.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How the Paid to Wait Calculator Works&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies, sometimes it may show you two strategies and in some cases it could only show you one claiming strategy.<br />
                                    <br />
                                    You could have a choice between claiming your own regular Work History Benefit or claiming a Spousal Benefit. The Spousal Benefit could pay you a benefit mount equal to 50% or half of your ex-spouse’s Full Retirement Age benefit. The Paid to Wait Calculator will determine which benefit you should try to maximize and then show you exactly how to do that.
                                    <br />
                                    <br />
                                    In some situations the Paid to Wait Calculator may recommend claiming a particular benefit first and then switching to another benefit later. For example it could tell you to claim a Spousal Benefit at your Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67) and then later switch to your regular Work History Benefit. In other cases, it may recommend claiming your regular Work History Benefit early and then switching to a Spousal Benefit at your Full Retirement Age. If you qualify to switch from one benefit to another benefit, the Paid to Wait Calculator will show you and tell you exactly what benefit to claim and when to claim it.
                                    <br />
                                    <br />
                                    In some situations, you may maximize the size of your benefit if you wait until age 70 to claim it. In that case, the Paid to Wait Calculator will always show you that strategy, but just in case you do not want to wait that long, it will also show you an additional strategy in which you claim your benefit at your Full Retirement Age (once again, your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67).<br />
                                    <br />
                                    If you were born before January 2, 1954, you may be able to use the Restricted Application claiming strategy. This strategy could pay you up to an extra $60,000 in Social Security income while you wait to claim your maxed out benefit. This strategy revolves around the strategic claiming of the Spousal Benefit. The Paid to Wait calculator will automatically determine if you are eligible to use the Restricted Application strategy and then show you exactly how it would work in your particular situation.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if You Continue to Work?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your particular situation, the Paid to Wait Calculator may recommend a claiming strategy in which you claim a benefit early, even at age 62. In that case the Paid to Wait Calculator will also show you another strategy in which you wait until your Full Retirement Age before you claim a benefit.  It will show you this second strategy in case you decide to continue to work between the ages of 62 and your Full Retirement Age.  If you claim your Social Security benefits before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. That is why the Paid to Wait Calculator will show you this strategy in which you wait until your Full Retirement Age to claim your benefit.<br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What You Can Expect&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Key Benefit Numbers” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of your benefit. Many divorced spouses will have a Paid to Wait number but some will not.
                                    <br />
                                    2) Maxed Out Benefit – the monthly dollar amount of your maxed out benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.
                                    <br />
                                    3) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Next Steps” in the Strategies&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.<br />
                                    <br />
                                    We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.<br />
                                    <br />
                                    If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.<br />
                                    <br />
                                    On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.
                                </p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <p>
                p.s. – Don’t wait until the last minute to claim your benefits, make an appointment with your local Social Security office at least one month prior to the date you want your benefits to begin. 
                <br />
                <br />
            </p>

            <p class="text-center fontBold" style="color: #85312F">
                Good Luck with your claiming strategy!!<br />
                <br />
                <asp:Button ID="print" runat="server" Text="Print" CssClass="button_login" OnClick="exportToPdf" />
                <asp:Button ID="btnCancelTopBottom" runat="server" CssClass="button_login" OnClick="imgCancel_Click" Text="Back to the Strategies" />
            </p>
        </div>
    </asp:Panel>

    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlExplanation" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>
<%--Java Scripts on the page--%>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/showstrategies.js"></script>

    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CancelPopup.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('.blockPage').removeAttr('style');
            // $('.blockPage').addClass('subpop');
        });

        function openPDF(pdf) {
            alert("1");
            window.open(pdf);
            return false;
        }
        $(function () {
            var WifepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.jpg'
            };
            $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);

            var HusbandpickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.jpg'
            };
            $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);
            // Control slider with external navigation
            $('div.slider-label').bind('click', function () {
                var newValue = $(this).attr('id').replace('slider-label-', '');
                s.slider('value', newValue);
                s.slider('option', 'stop').call(s, null, {
                    value: newValue
                });
            });
            $('[id$=GridAsap],[id$=GridStandard],[id$=GridSpouse]').addClass('gridborder');
            $('[id$=ChartAsap],[id$=ChartSpouse],[id$=ChartStandard]').css('height', '');
            $('[id$=ChartAsap],[id$=ChartSpouse],[id$=ChartStandard]').addClass('img-responsive mobheight');
            if ($('[id$=PanelGrids]').is(':visible')) {
                $('#Div1').removeClass('slider-label-selected');
                $('#Div2').addClass('slider-label-selected');
                $('[id$=Img3]').attr('src', '/images/staticslider.png');
                $('[id$=Img2]').attr('src', '/images/upperstaticslider2.png');
            }
            else {
                $('[id$=Img2]').attr('src', '/images/upperstaticslider1.png');
            }
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

    </script>
</asp:Content>
<%--End of java scripts--%>