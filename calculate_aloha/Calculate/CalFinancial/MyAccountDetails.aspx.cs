﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Web.UI;
using System.Reflection;
using System.Globalization;

namespace Calculate
{
    public partial class MyAccountDetails : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        #endregion Variables

        #region Events

        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                /* Check if Page is not postback */
                if (!IsPostBack)
                {
                    lblUserUpdate.Text = null;
                    txtConfirmPassword.Visible = false;
                    lblCustomerConfirmPassword.Visible = false;
                    txtFirstname.Focus();
                    /* Fill Account information */
                    ShowBasicInformation();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Update customer account details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveAccountDetails_Click(object sender, EventArgs e)
        {
            try
            {
                lblUserUpdate.Text = String.Empty;
                UpdateCustomerAccountDetails();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Hide/Show Textbox with their values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void enablePassword(object sender, EventArgs e)
        {
            try
            {
                lblUserUpdate.Text = String.Empty;
                txtPassword.Attributes.Add(Constants.HtmlAutoComplete, Constants.HtmlOff);
                changePasswordButton.Visible = false;
                txtConfirmPassword.Visible = true;
                lblCustomerConfirmPassword.Visible = true;
                txtPassword.Attributes.Add(Constants.HtmlValue, String.Empty);
                txtConfirmPassword.Attributes.Add(Constants.HtmlValue, String.Empty);
                txtPassword.Enabled = true;
                txtEmail.Enabled = true;
                btnCancelPassword.Visible = true;
                txtPassword.Text = String.Empty;
                btnSaveAccountDetails.Visible = true;
                txtConfirmPassword.Text = String.Empty;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Hide/Show Textbox while click over Cancel Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelPassword_Click(object sender, EventArgs e)
        {
            try
            {
                changePasswordButton.Visible = true;
                lblUserUpdate.Text = String.Empty;
                txtConfirmPassword.Visible = false;
                lblCustomerConfirmPassword.Visible = false;
                txtPassword.Enabled = false;
                txtPassword.Attributes.Add(Constants.HtmlValue, tempPassword.Text);
                btnCancelPassword.Visible = false;
                btnSaveAccountDetails.Visible = false;
                ShowBasicInformation();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        /// Date: 11 June 2014
        private void ShowBasicInformation()
        {
            try
            {
                Customers objCustomer = new Customers();
                Advisor objAdvisor = new Advisor();
                Institutional objInstitution = new Institutional();
                DataTable dtCustomerDetails;
                string accountID = String.Empty;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                    accountID = Session[Constants.SessionUserId].ToString();
                else
                    accountID = Session[Constants.SessionAdminId].ToString();
                if (accountID.IndexOf("A") >= Constants.zero || accountID.IndexOf("D") >= Constants.zero)
                    dtCustomerDetails = objAdvisor.getAdvisorDetailsByID(accountID);
                else if (accountID.IndexOf("I") >= Constants.zero)
                {
                    dtCustomerDetails = objInstitution.getInstitutionalDetailsByID(accountID);
                    lblInstitutionName.Text = dtCustomerDetails.Rows[0][Constants.InstitutionName].ToString();
                }
                else
                    dtCustomerDetails = objCustomer.getCustomerDetails(accountID);
                txtFirstname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerFirstName].ToString();
                //txtLastname.Text = dtCustomerDetails.Rows[Constants.zero][Constants.CustomerLastName].ToString();
                txtEmail.Text = dtCustomerDetails.Rows[Constants.zero][Constants.UserEmail].ToString();
                /*take backup of email for to verify user mail id*/
                ViewState[Constants.UserEmail] = txtEmail.Text;
                txtPassword.Text = Encryption.Decrypt(dtCustomerDetails.Rows[Constants.zero][Constants.UserPassword].ToString());
                tempPassword.Text = Encryption.Decrypt(dtCustomerDetails.Rows[Constants.zero][Constants.UserPassword].ToString());
                txtPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(dtCustomerDetails.Rows[Constants.zero][Constants.UserPassword].ToString()));
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Update Customer Account Details 
        /// </summary>
        private void UpdateCustomerAccountDetails()
        {
            try
            {
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                objCustomer.Email = txtEmail.Text;
                /*email address retrive from ViewState */
                var emailText = ViewState[Constants.UserEmail];

                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer) || txtEmail.Text.ToString() == emailText.ToString())
                {
                    string accountID = String.Empty;
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        accountID = Session[Constants.SessionUserId].ToString();
                    else
                        accountID = Session[Constants.SessionAdminId].ToString();
                    Advisor objAdvisor = new Advisor();
                    Institutional objInstitutional = new Institutional();
                    objCustomer.CustomerID = accountID;
                    objCustomer.FirstName =textInfo.ToTitleCase(txtFirstname.Text);
                    objCustomer.LastName = string.Empty;
                    objCustomer.Email = txtEmail.Text;
                    if (String.IsNullOrEmpty(txtPassword.Text))
                    {
                        objCustomer.Password = Encryption.Encrypt(tempPassword.Text);
                        txtPassword.Text = tempPassword.Text;
                        txtConfirmPassword.Text = tempPassword.Text;
                        txtPassword.Attributes.Add(Constants.HtmlValue, tempPassword.Text);
                        txtConfirmPassword.Attributes.Add(Constants.HtmlValue, tempPassword.Text);
                    }
                    else
                    {
                        objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                        txtPassword.Attributes.Add(Constants.HtmlValue, txtPassword.Text);
                        txtConfirmPassword.Attributes.Add(Constants.HtmlValue, txtPassword.Text);
                        tempPassword.Text = txtPassword.Text;
                    }
                    if (accountID.IndexOf("A") >= Constants.zero)
                    {
                        //AdvisorTO objAdvisorTO = new AdvisorTO();
                        //objAdvisorTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                        //objAdvisorTO.Lastname = string.Empty;
                        //objAdvisorTO.Email = txtEmail.Text;
                        //objAdvisorTO.AdvisorID = accountID;
                        //objAdvisorTO.Password = Encryption.Encrypt(txtPassword.Text);
                        //objAdvisor.updateAdvisorInfo(objAdvisorTO);
                        
                        objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                        objCustomer.PasswordResetFlag = Constants.FlagNo;
                        objCustomer.Email = txtEmail.Text;
                        objCustomer.updateCustomerPassword(objCustomer);
                        Session.Clear();
                        Response.Redirect(Constants.RedirectConfirmPassword, false);
                    }
                    else if (accountID.IndexOf("I") >= Constants.zero)
                    {
                        //InstitutionalTO objInstitutionTO = new InstitutionalTO();
                        //objInstitutionTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                        //objInstitutionTO.Lastname = string.Empty;
                        //objInstitutionTO.InstitutionName = lblInstitutionName.Text;
                        //objInstitutionTO.Email = txtEmail.Text;
                        //objInstitutionTO.InstitutionAdminID = accountID;
                        //objInstitutionTO.Password = Encryption.Encrypt(txtPassword.Text);
                        //objInstitutional.updateInstitutionalInfo(objInstitutionTO);

                        objCustomer.Password = Encryption.Encrypt(txtPassword.Text);
                        objCustomer.PasswordResetFlag = Constants.FlagNo;
                        objCustomer.Email =txtEmail.Text;
                        objCustomer.updateCustomerPassword(objCustomer);
                        Session.Clear();
                        Response.Redirect(Constants.RedirectConfirmPassword, false);
                    }
                    else
                        objCustomer.updateCustomerDetails(objCustomer);
                    lblUserUpdate.Text = Constants.SuccessCustomerUpdate;
                    lblUserUpdate.Visible = true;
                    txtConfirmPassword.Visible = false;
                    lblCustomerConfirmPassword.Visible = false;
                    changePasswordButton.Visible = true;
                    txtPassword.Enabled = false;
                    btnCancelPassword.Visible = false;
                    btnSaveAccountDetails.Visible = false;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "displayPanelPopUp();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Validate Session
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionMyAccountDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionMyAccountDetails + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Methods
    }
}