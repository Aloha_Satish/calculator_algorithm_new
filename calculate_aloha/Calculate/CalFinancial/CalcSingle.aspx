﻿<%@ Page Title="Calculator Single" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="CalcSingle.aspx.cs" Inherits="Calculate.WebForm2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize3 text-center setTopMargin">Calculation and Strategies for Singles</h1>
    <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:Panel ID="PanelEntry" runat="server">
        <div class="errorMessageTable failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="clearfix">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-12 col-sm-12">
                    <p class="Font16 text-justify">
                        In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement ewill provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                    </p>
                    <br />
                    <asp:Label ID="LabelPlease" runat="server" Font-Bold="True" Font-Names="Arial" Text="Please enter your information:"></asp:Label>
                    <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                    <asp:Label ID="LabelWifeBene" runat="server" Text="What is your monthly Full Retirement Age Benefit?"></asp:Label>
                    <asp:TextBox ID="BeneWife" CssClass="textEntry form-control marginTop" MaxLength="7" runat="server" ValidationGroup="RegisterUserValidationGroup" TabIndex="1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Your monthly Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Your monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Your monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                    <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                    <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                    <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                    <asp:TextBox ID="BirthWifeYYYY" runat="server" MaxLength="4" Visible="false" Width="80px" CssClass="textEntry"></asp:TextBox>
                </div>
                <div class="col-md-12 col-sm-12">
                    <br />
                    <%--<asp:Button ID="btnSaveBasicInfoAndProceed" TabIndex="3" runat="server" Text="Previous - Basic Information" OnClick="btnSaveBasicInfoAndProceed_Click" CssClass="button_login" />--%>
                    <asp:Button ID="BtnCalculate" runat="server" Text="Next - View Report" TabIndex="4" CssClass="button_login btnmarginsingle" ValidationGroup="RegisterUserValidationGroup" OnClick="Calculate_Click" />
                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelParms" runat="server">
        <div class="row">
            <br />
            <div class="col-md-7 col-sm-7 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold">
                        Date of Birth:
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold">
                        Full Retirement Age:
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold ">
                        Marital Status:
                    </div>
                    <div class="col-md-6 col-sm-6 ">
                        Single
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-6 col-sm-6 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-6 col-sm-6">
                        2.5%
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-5 column text-left">

                <%if (Session["AdminID"] != null)
                  {%>
                <asp:Button ID="btnManageCustomer" runat="server" CssClass="button_login " OnClick="Backto_Managecustomer" Text="Back to All Clients" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnDownloadReport" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download Report" />
                <%}%>
                <%else
                  {
                      if (Session["Demo"] == null)
                      {%>
                <asp:Button ID="BtnChange" runat="server" OnClick="Calculate_Click" Text="Edit Information" CssClass="button_login" />
                <%} %>
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Width="163px" Text="Explanation of Strategies" OnClick="btnExplain_Click" />
                <asp:Button ID="Button2" runat="server" CssClass="button_login" OnClick="PrintAll_Click" Text="Download Report" />
                <%} %>


                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <asp:Button ID="Button4" runat="server" CssClass="button_login" OnClick="PrintAll_Click" Text="Download" />
                <%} %>--%>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div>
            <br />
            <br />
            <table>
                <%--<tr>
                    <td align="center">
                        <asp:Image ID="imgCheck" Style="display: none;" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" /><asp:Label ID="Label5" Style="display: none; color: green; font-weight: bold; position: static;" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                    <td></td>
                    <td></td>
                </tr>--%>
                <tr class="fontLarge">
                    <td runat="server" id="strategy1" class="text-center">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy1" runat="server" GroupName="o" />
                        <p>
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" />
                    </td>
                    <td runat="server" id="strategy2" class="text-center">

                        <asp:Label ID="lblStrategy2" Text="Strategy 2" Font-Bold="true" runat="server"></asp:Label>
                        <br />

                        <asp:RadioButton ID="rdbtnStrategy2" runat="server" GroupName="o" />
                        <p>
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" />
                    </td>
                    <td runat="server" id="strategy3" class="text-center">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy3" runat="server" GroupName="o" />
                        <p>
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" />

                    </td>
                </tr>

            </table>
            <br />
            <br />
        </div>
        <!-- end slider-container -->
        <!-- Strategies -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy1" runat="server">
                <asp:Label ID="LabelAsap" runat="server" class="displayNone" Text="Claim as Early as Possible"></asp:Label>
                <asp:Panel runat="server" ID="pnlText1" class="text-justify margiright">
                    <asp:Label ID="StrategyAsap" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures2" runat="server" Text="Key Benefit Numbers:" Font-Bold="true" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures2" runat="server" Text="Next Steps:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps5" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps6" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps7" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <asp:Label ID="NoteAsap" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                    <br />
                </asp:Panel>
                <div class="table-responsive">
                    <asp:GridView ID="GridAsap" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblNoteWife1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> <div class="table-responsive">
                    <asp:Chart ID="ChartAsap" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Color="#b85f5b" ToolTip="Value of X: #VALY Value of Y #VALY" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br />
                </div>
                    </center>
            </asp:Panel>
        </div>
        <!-- end strategy-1 -->
        <!-- Strategies -->
        <div id="strategy-SuspendStrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy2" runat="server">
                <asp:Label ID="LabelStandard" runat="server" class="displayNone" Text="Standard Claim at age Full Retirement Age"></asp:Label>
                <asp:Panel runat="server" ID="pnlText2" CssClass="text-justify margiright">

                    <asp:Label ID="StrategyStandard" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteStandard" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures1" runat="server" Text="Next Steps:" Font-Bold="true" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps4" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps8" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <div class="table-responsive">
                    <asp:GridView ID="GridStandard" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblNoteWife0" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> <div class="table-responsive">
                    <asp:Chart ID="ChartStandard" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Color="#b85f5b" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br />
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-2 -->
        <!-- Strategies -->
        <div id="strategy-BestStrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy3" runat="server">
                <asp:Label ID="LabelLatest" runat="server" class="displayNone" Text="Claim as Late as Possible"></asp:Label>
                <asp:Panel runat="server" ID="pnlText3" class="text-justify  margiright">
                    <asp:Label ID="StrategyLatest" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteLatest" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="KeyFigures" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures" runat="server" Text="Next Steps:" Font-Bold="true" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps9" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <div class="table-responsive">
                    <asp:GridView ID="GridLatest" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <center>  <div class="table-responsive">
                    <asp:Chart ID="ChartLatest" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Color="#b85f5b" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                     <br />
                            <br />
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-3 -->
        <a href="../FrmDisclaimer.aspx">Disclaimer </a>
        <asp:Label ID="ErrorMessageLabel" runat="server" Text="Label" Visible="False"></asp:Label>
    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center>  <div id="plans" class="plan-contentnew">
          <br />
          <asp:Label ID="MaxCumm"  class="subspopuplabelsColor" runat="server"></asp:Label>
          <br />
          <h2 class="subspopupText">While growing your Social Security Check to the largest amount possible! <br />
               <br />  
              Find out how, for just 39.95 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CancelPopup" OnClick="Cancel_Click" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
        </div>
        </center>
    </div>

    <asp:Panel ID="pnlExplanation" ScrollBars="Auto" runat="server" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Explanation of the Strategies
            <asp:Button ID="btnCancelTop" runat="server" CssClass="button_login floatRight" OnClick="imgCancel_Click" Text="Back to the Strategies" />
        </div>
        <div class="textExplanation plan-contentnew">
            <br />
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <center>
                    <div style="font-size: large; font: bold;">
                         Single People
                    </div>
                        </center>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What is the Primary Goal Of the Paid to Wait Calculator?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. It can help you do this by showing your benefit amounts at three different claiming ages and the effect that Retro-active COLA credits will have on increasing the size of your benefit when you delay claiming it. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How the Paid to Wait Calculator Works&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies and in some cases it may show you only two strategies. 
                                    <br />
                                    <br />
                                    The first strategy, Strategy 1, that the Paid to Wait Calculator will show you, is the claiming strategy in which you wait until age 70 to claim your benefit. By waiting until age 70 to claim your benefit you maximize the size of your benefit or receive the biggest benefit possible for the rest of your life. 
                                    <br />
                                    <br />
                                    In the case in which you do not want to wait until age 70 to claim your benefit, the second strategy, or Strategy 2, will show you the amount of your benefit if you wait until your Full Retirement Age to claim it. Your Full Retirement Age depends on the year in which you were born and can vary between age 66 and 67. The Paid to Wait Calculator will automatically determine your Full Retirement Age for you. Your Full Retirement Age benefit amount will be approximately 24% less than the benefit amount you would receive if you waited until age 70 to claim it.
                                    <br />
                                    <br />
                                    The last strategy, Strategy 3, will show you claiming your benefit as early as possible at age 62, or some age between age 62 and your Full Retirement Age. Claiming your benefit at age 62 will pay you the smallest benefit you can receive for the rest of your life.
                                    <br />
                                    <br />
                                    You can compare the three strategies and the benefit amount difference between them at various ages such as age 75, 80 or 85.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if You Continue to Work?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Something to keep in mind if you claim your Social Security benefits at age 62 or before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. So if you plan on continuing to work prior to your Full Retirement Age, you may want to wait at least until your Full Retirement Age or later to claim your benefits.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What You Can Expect&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Key Benefit Numbers” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Work History Benefit – the monthly dollar amount of your Work History Benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.
                                    <br />
                                    2) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Next Steps” in the Strategies&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.
                                    <br />
                                    <br />
                                    We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.
                                    <br />
                                    <br />
                                    If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.
                                    <br />
                                    <br />
                                    On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.

                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <p>
                p.s. – Don’t wait until the last minute to claim your benefits, make an appointment with your local Social Security office at least one month prior to the date you want your benefits to begin. 
                <br />
                <br />
            </p>
            <p class="text-center fontBold" style="color: #85312F">
                Good Luck with your claiming strategy!<br />
                <br />
                <asp:Button ID="print" runat="server" Text="Print" CssClass="button_login" OnClick="exportToPdf" />
                <asp:Button ID="btnCancelTopBottom" runat="server" CssClass="button_login " OnClick="imgCancel_Click" Text="Back to the Strategies" />
            </p>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlExplanation" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>
<%--JAVA SCRIPTS ON THE PAGE--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/showstrategies.js"></script>

    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CancelPopup.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('.blockPage').removeAttr('style');
            // $('.blockPage').addClass('subpop');
        });
        $(function () {
            setTimeout(function () {
                $("[id$=ErrorMessage0]").fadeOut(1500);
            }, 5000);
        });
        $(function () {
            var WifepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.jpg'

            };

            //Code to use slider instead of radio buttons
            $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);

            // Control slider with external navigation
            $('div.slider-label').bind('click', function () {
                var newValue = $(this).attr('id').replace('slider-label-', '');
                s.slider('value', newValue);
                s.slider('option', 'stop').call(s, null, {
                    value: newValue
                });
            });
            $('[id$=GridAsap],[id$=GridStandard],[id$=GridLatest]').addClass('gridborder');
            $('[id$=ChartAsap],[id$=ChartLatest],[id$=ChartStandard]').css('height', '');
            $('[id$=ChartAsap],[id$=ChartLatest],[id$=ChartStandard]').addClass('img-responsive mobheight');
            if ($('[id$=PanelGrids]').is(':visible')) {
                $('[id$=Img3]').attr('src', '/images/staticslider.png');
                $('[id$=Img2]').attr('src', '/images/upperstaticslider2.png');
            }
            else {
                $('[id$=Img2]').attr('src', '/images/upperstaticslider1.png');
            }
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
<%--end of java scripts--%>