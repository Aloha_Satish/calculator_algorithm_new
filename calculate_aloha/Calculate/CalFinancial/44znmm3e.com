﻿<%@ Page Title="Calculator Married" Language="C#" MasterPageFile="~/ClientMaster.master" ValidateRequest="False" AutoEventWireup="true" CodeBehind="Calculate.aspx.cs" Inherits="Calculate._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize3 text-center">Calculation and Strategies for Married Couples</h1>
    <%--code for the slider on the top of page--%>
    <%-- <asp:Panel ID="Panel1" runat="server">
        <div class="slider-single-label" id="Div1">
            Basic Information
        </div>
        <div class="slider-single-label slider-label-selected" id="Div2">
            Social Security Information
        </div>
        <div class="slider-single-label" id="Div3">
            Your Results
        </div>
        <div class="clearfloats"></div>
        <br />
        <center><div><img id="Img1" runat="server" src="/images/staticslider.png" style="height:auto;position:relative;margin-right:-1px;" /><img id="Img2" runat="server" src="/images/upperstaticslider1.png" class="imgupslide" border="0" /><img id="Img3" runat="server" src="/images/statslider.png" style="position:static;margin-left:-5px;" border="0" />
            <div></div>
            <div>
                <img id="Img4" runat="server" src="/images/staticslider.png" style="margin-top:-50px;" />
            </div>
        </div>
        </center>
    </asp:Panel>--%>
    <asp:Panel ID="PanelEntry" runat="server">
        <div class="col-md-12 col-sm-12 failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="row margleft margiright">
            <div class="col-md-12 col-sm-12">
                <p class="Font16 text-justify">
                    In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                </p>
                <br />
                <asp:Label ID="LabelPlease" runat="server" Font-Bold="True" Font-Names="Arial" Text="Please enter your information:"></asp:Label>
                <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                <asp:Label ID="Label1" runat="server" Text="What is your Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneHusband" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="1" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Your Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ToolTip="Your Full Retirement Age Benefit must be a number" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeYYYY" runat="server" MaxLength="4" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
                <asp:TextBox ID="Your_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
                <asp:TextBox ID="Spouse_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <asp:Label ID="LabelHusbandBene" runat="server" Text="What is your Spouse's monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneWife" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="2" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Spouse's monthly Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                <asp:TextBox Visible="false" ID="BirthHusbandMM" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandDD" Visible="false" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandTempYYYY" Visible="false" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandYYYY" MaxLength="4" runat="server" Width="80px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12 displayNone">
                <asp:Label ID="Label2" runat="server" Text="Show Survivor Benefit" Visible="false"></asp:Label>

                <asp:CheckBox ID="CheckBoxSurv" runat="server" Text="" TabIndex="3" Visible="false" />
            </div>
            <div class="col-md-12 col-sm-12 displayNone">
                <asp:Label ID="Label3" runat="server" Text="Include 2.5% COLA"></asp:Label>

                <asp:CheckBox ID="CheckBoxCola" runat="server" Text="" TabIndex="4" />
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <asp:Button ID="btnSaveBasicInfoAndProceed" runat="server" Text="Previous - Basic Information" TabIndex="5" OnClick="btnSaveBasicInfoAndProceed_Click" CssClass="button_login" />
                <asp:Button ID="BtnCalculate" ValidationGroup="RegisterUserValidationGroup" CssClass="button_login btnmarginsingle btnmarginmarried" runat="server" Text="Next - View Report" OnClick="Calculate_Click" TabIndex="6" />

                <br />
                <br />
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelParms" runat="server">
        <div class="row">
            <br />
            <div class="col-md-8 col-sm-8 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Date of Birth :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold ">
                        Marital Status :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        Married
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-4 col-sm-4">
                        2.5%
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <br />
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Height="40px" Text="Explanation of Strategies" OnClick="btnExplain_Click" />
                <asp:Button ID="BtnChange" runat="server" CssClass="button_login " Height="40px" OnClick="Calculate_Click" Text="Edit Information" />
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div>
            <br />
            <table class="text-center">
                <%--<tr>
                    <td align="center">
                        <asp:Image ID="imgCheck" class="displayNone" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" />
                        <asp:Label ID="lblBestPlan" class="imageStyle" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <tr class="fontLarge">
                    <td id="sliderlabel1" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" ForeColor="Brown" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdClaim_Suspend" Checked="true" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber1" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" />
                    </td>
                    <td id="sliderlabel2" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy2" Text="Strategy 2" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdClaim_Full_Retirement" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber2" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" />
                    </td>
                    <td id="sliderlabel3" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdClaim_Claim_Now_Cliam_Later" runat="server" GroupName="marriedScenario" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber3" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" />
                    </td>
                </tr>
            </table>
            <br />
            <br />

        </div>
        <%--<div id="slider-control-wrapper">
                <div id="slider-control"></div>
            </div>--%>
        <%--</div>--%>
        <!-- end slider-container -->
        <!-- Strategies -->
        <div id="strategy-EarlyStrategy" class="strategy-container">
            <asp:Panel ID="EarlySuspend" runat="server">
                <asp:Panel runat="server" ID="pnlText1" CssClass="text-justify margileft margiright">
                    <asp:Label ID="LabelEarlySuspend" runat="server" Text="Early Claim & Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeyFigures" runat="server" Font-Bold="true" Font-Names="Arial" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                   <div class="text-justify margin200 margiright">
                     <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey1" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey2" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey3" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                       </div>
                    <div class="spaceInLblKeysAndSteps text-justify">
                        <asp:Label ID="lblStepsFigures" Font-Bold="true" runat="server" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>
                    </div>
                    <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep8" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="Label4" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep9" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep10" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep11" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                        </div>
                    <br />
                </asp:Panel>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnEarlyDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <div class="table-responsive">
                    <asp:GridView ID="GridEarly" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartEarly" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <!-- end strategy-1 -->
        <div id="strategy-BestSrategy" class="strategy-container">
            <asp:Panel ID="PanelBest" runat="server">
                <asp:Panel ID="pnlText2" runat="server" class="text-justify margileft margiright">
                    <asp:Label ID="LabelBest" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeys" runat="server" Text="Key Benefit Numbers:" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>

                    <div class="text-justify margin200 margiright">
                     <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey4" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey5" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblKey6" runat="server" Font-Names="Arial"  CssClass="indent200" ForeColor="Black"></asp:Label>
                    </div>
                        </div>
                 <br />
                        <asp:Label ID="lblSteps" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                  <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep4" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="Label5" runat="server" Font-Names="Arial" CssClass="indent"  ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep5" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep6" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblStep7" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                    </div>
                      </div>
                    <br />
                </asp:Panel>
                <br />
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnBestDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <div class="table-responsive">
                    <asp:GridView ID="GridBest" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="ChartBest" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-2 -->
        <div id="strategy-SuspendStrategy" class="strategy-container" <%--style="display: block;"--%>>
            <asp:Panel ID="PanelSuspendLater" runat="server">
                <asp:Panel runat="server" ID="pnlText3" class="text-justify margileft margiright">
                    <asp:Label ID="NoteSuspendLater" Visible="false" runat="server"></asp:Label>
                    <asp:Label ID="LabelSuspendLater" runat="server" Text="Claim and Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeySuspend" runat="server" Text="Key Benefit Numbers:" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey1" runat="server" CssClass="indent200" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey2" runat="server" CssClass="indent200" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey3" runat="server" CssClass="indent200" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsSuspend" runat="server" CssClass="spaceInLblKeysAndSteps" Text="Next Steps:" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>

                    <div class="text-justify margin100 margiright">

                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend1" runat="server" CssClass="indent" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend2" runat="server" CssClass="indent" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend3" runat="server" CssClass="indent" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend4" runat="server" CssClass="indent" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend5" runat="server" CssClass="indent" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <br />
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnSuspendDownLoad" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <div class="table-responsive">
                    <asp:GridView ID="GridSuspendLater" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <center> <div class="table-responsive" >
                    <asp:Chart ID="ChartSuspendLater" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-3 -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="PanelBestLater" runat="server">
                <asp:Panel runat="server" ID="ExplainBestLater">
                    <asp:Label ID="LabelBestLater" runat="server" class="displayNone" Text="Claim Early Claim Late "></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial" CssClass="indent200" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures1" runat="server" Text="Next Steps:" CssClass="margileft spaceInLblKeysAndSteps" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps0" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label6" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial" CssClass="indent" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <br />
                <center> 
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnFullDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <div class="table-responsive">
                    <br />
                    <asp:GridView ID="GridBestLater" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <center>  <div class="table-responsive" >
                    <asp:Chart ID="ChartBestLater" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-4 -->
        <div class="strategy-container">
            <asp:Panel ID="PanelNowLater" runat="server">
                <div class="clearfix">
                    <div class="col-md-10 column">
                        <h2>
                            <asp:Label ID="LabelNowLater" runat="server" Text="Claim Early Claim Late"></asp:Label></h2>
                    </div>
                    <div class="col-md-2 column">
                        <asp:Button ID="Button4" runat="server" CssClass="button_login downloadbtn" OnClick="PrintAll_Click" Text="Download" />
                    </div>
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridNowLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainNowLater">
                <p>
                    <asp:Label ID="StrategyNowLater" runat="server">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteNowLater" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelLater" runat="server">
                <h2>
                    <asp:Label ID="LabelLater" runat="server" Text="Full Retirement & Claim Late"></asp:Label></h2>
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLater">
                <p>
                    <asp:Label ID="StrategyLater" runat="server">
          In this strategy the spouse with the lower benefit starts social security at full
          retirement age. The spouse with the higher benefit uses the spousal benefit to get
          some additional income while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLater" runat="server"></asp:Label>
                    <br />
                    <br />
                    If you compare this strategy to the Claim Now, Claim More Later Stratege above,
          you will see that the age 70 benefit is higher while the income prior to age 70
          is lower. This strategy can be better in the long run if you can afford to defer
          the income.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelSuspend" runat="server">
                <h2>
                    <asp:Label ID="LabelSuspend" runat="server" Text="Early Claim and Suspend"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkSuspend" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridSuspend" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainSuspend">
                <p>
                    <asp:Label ID="StrategySuspend" runat="server">
            In this strategy the spouse with the lower benefit starts social security as soon as they are eligable. The spouse with the higher benefit claims and suspends so that the spousal benefit can be used while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteSuspend" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelStandard" runat="server">
                <h2>
                    <asp:Label ID="LabelStandard" runat="server" Text="Compromise Strategy"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkStandard" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridStandard" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainStandard">
                <p>
                    <asp:Label ID="StrategyStandard" runat="server">
          In this strategy both husband and wife start social security at full retirement age.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteStandard" runat="server"></asp:Label>
                    <br />
                    <br />
                    This strategy uses the standard claim at full retirement age (usually 66). It is
          better than claiming as soon as possible because the age 70 benefit is higher and
          the survivor's benefit is higher. However, the other strategies (Claim Now Claim
          More Later, Claim and Suspend, etc.) offer much better age 70 and survivor benefits.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelLatest" runat="server">
                <h2>
                    <asp:Label ID="LabelLatest" runat="server" Font-Bold="True" Font-Names="Arial" Text="Claim as Late as Possible" ForeColor="Black"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkLatest" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLatest" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLatest">
                <p>
                    <asp:Label ID="StrategyLatest" runat="server">
          In this strategy both husband and wife start social security at age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLatest" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <br />
        <br />
    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center>  <div id="plans" class="plan-contentnew">
          <br />
          <asp:Label ID="MaxCumm"  class="subspopuplabelsColor" runat="server"></asp:Label>
          <br />
          <h2 class="subspopupText">While growing your Social Security Check to the largest amount possible! <br />
               <br />  
              Find out how, for just 5 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CloseWindow" OnClick="Close_Click" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
          <br />
        </div></center>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>

    <asp:Panel ID="pnlExplanation" ScrollBars="Auto" runat="server" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Explanation for Married Strategies
            <asp:LinkButton ID="lnkCancelTop" runat="server" CssClass="LinkButtonStyle " OnClick="imgCancel_Click" Text="Back to the Strategies"></asp:LinkButton>
        </div>
        <div class="textExplanation plan-contentnew">
            <br />
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What is the Primary Goal of Social Security Calculator?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The primary goal of this Social Security calculator is to show you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait to maximize the size of your benefit.<br />
                                    <br />
                                    Most married couples receive two Social Security benefits, one made payable to the husband and the other made payable to the wife.<br />
                                    <br />
                                    They should try to maximize the size of the bigger benefit. In order to do that, the spouse with the bigger Social Security benefit must wait until age 70 to claim it.<br />
                                    <br />
                                    This calculator can make it easier for you to do that by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while the spouse with the bigger benefit waits until age 70 to claim it.<br />
                                    <br />
                                    You really can get Paid to Wait.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How the Social Security Calculator Works?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your situation, the calculator may show you a maximum of three different claiming strategies.
                The first strategy, or Strategy #1, is the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of the bigger benefit.
                This strategy may involve the spouse with the smaller Social Security benefit claiming that benefit as early as possible, even at age 62.<br />
                                    <br />
                                    If the spouse with the smaller benefit plans on working past age 62, this may not be the best strategy because if they make too much money from their job, they may have to give back some or even all of their Social Security benefits.
                If that is the case, you may want to consider one of the other suggested strategies.<br />
                                    <br />
                                    At most there will be two additional suggested strategies and sometimes there will be only one.
                Many times these other strategies will wait until your Full Retirement Age before any benefits are claimed.
                Once you reach your Full Retirement Age, you can receive Social Security benefits, continue to work at your job, make as much money as you want and never have to give back any of your Social Security benefits.
                These other strategies will usually pay you less Social Security income while you wait but you and your spouse will usually receive more Combined Annual Social Security income when the claiming strategy is completed.<br />
                                    <br />
                                    In other words, starting with the year when the strategy is completed, you and your spouse will receive a larger amount of Social Security income every year for the rest of your life.
                The trade off for receiving that larger amount of annual Social Security income is being paid less while you wait.<br />
                                    <br />
                                    At least one of the other suggested strategies will show you how you can get paid to wait while you maximize the size of both spouse's Social Security benefits.<br />
                                    <br />
                                    In most cases this strategy will pay you the least amount while you wait, but because it maximizes the size of both benefits, when the claiming strategy is completed, 
                it will pay you and your spouse the largest amount of Combined Annual Social Security income every year for the rest of your lives.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the Key Benefit Numbers in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Your Paid to Wait number - This is the amount of Social Security income you will receive while you wait to maximize the size of the bigger benefit.<br />
                                    <br />
                                    2) Maxed out bigger benefit - the monthly dollar amount of the maxed out bigger benefit. Another reason it is important to max out the size of the bigger benefit is because eventually that benefit will become the Survivor Benefit after the first spouse  dies.
                The surviving spouse is only going to receive one Social Security check after the first spouse dies, the Survivor Benefit.Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit.<br />
                                    <br />
                                    3) Combined Annual Income - When the claiming strategy is completed, this shows the combined amount of Social Security income you and your spouse will receive every year for the rest of your life. This combined total will grow larger with annual COLA increases.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the Next Steps in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    This section tells you what benefits to claim and when you should claim them.<br />
                                    <br />
                                    With all of our strategies we assumed a 2.5% annual Cost of Living or COLA increase. We used a 2.5% COLA increase because that has been the approximate average COLA increase over the last 20 years.<br />
                                    <br />
                                    If some of the benefit numbers in our charts, don't match the benefit numbers from your Social Security statement, especially your Full Retirement Age benefit and your benefit if wait until age 70,that is because Social Security does not make any COLA assumptions when calculating your benefits.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-center fontBold" style="color: #85312F">Good Luck with your claiming strategy!</p>
            <center> <asp:Button ID="btnCancelTopBottom" runat="server" CssClass="button_login " OnClick="imgCancel_Click" Text="Back to the Strategies"/></center>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlExplanation" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript">


        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CloseWindow.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            <%--$('#<%=btnWithoutTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });--%>
            $('.blockPage').removeAttr('style');
            //$('.blockPage').addClass('subpop');
        });
        $(function () {
            var WifepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png'

            };
            $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);
            var HusbandpickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png'
            };
            $("[id$=BirthHusbandYYYY]").datepicker(HusbandpickerOpts);
            // Control slider with external navigation
            $('div.slider-label').bind('click', function () {
                var newValue = $(this).attr('id').replace('slider-label-', '');
                s.slider('value', newValue);
                s.slider('option', 'stop').call(s, null, {
                    value: newValue
                });
            });
            $('[id$=GridEarly],[id$=GridBest],[id$=GridSuspendLater],[id$=GridBestLater]').addClass('gridborder');
            $('[id$=ChartEarly],[id$=ChartBest],[id$=ChartSuspendLater],[id$=ChartBestLater]').css('height', '');
            $('[id$=ChartEarly],[id$=ChartBest],[id$=ChartSuspendLater],[id$=ChartBestLater]').addClass('img-responsive mobheight');
            if ($('[id$=PanelGrids]').is(':visible')) {
                $('[id$=Img3]').attr('src', '/images/staticslider.png');
                $('[id$=Img2]').attr('src', '/images/upperstaticslider2.png');
            }
            else {
                $('[id$=Img2]').attr('src', '/images/upperstaticslider1.png');
            }
        });

        function bestsetNew(sFirstStartegy, sSecondStartegy) {
            $('#strategy-' + sFirstStartegy).css('display', 'block');
            document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'black';
            document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'brown';
            document.getElementById("<%=lblStrategy1.ClientID%>").style.fontWeight = 'Bold';
            document.getElementById("<%=lblStrategy3.ClientID%>").style.fontWeight = 'Bold';
            document.getElementById("<%=lblStrategy3.ClientID%>").innerHTML = 'Strategy 2';
            $("[id$=rdClaim_Suspend]").click(function () {
                document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'brown';
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sFirstStartegy).css('display', 'block');
            });
            $("[id$=rdClaim_Claim_Now_Cliam_Later]").click(function () {
                document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'brown';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.fontWeight = 'Bold';
                document.getElementById("<%=lblStrategy3.ClientID%>").style.fontWeight = 'Bold';
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sSecondStartegy).css('display', 'block');
            });
        }
        //function to make visible the stretgies on the girds according to best plan
        function bestset(sFirstStartegy, sSecondStartegy, sThirdStartegy) {
            $('#strategy-' + sFirstStartegy).css('display', 'block');
            document.getElementById("<%=lblStrategy2.ClientID%>").style.color = 'black';
            document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'black';
            document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'brown';
            document.getElementById("<%=lblStrategy1.ClientID%>").style.fontWeight = 'Bold';
            document.getElementById("<%=lblStrategy2.ClientID%>").style.fontWeight = 'Bold';
            document.getElementById("<%=lblStrategy3.ClientID%>").style.fontWeight = 'Bold';
            $("[id$=rdClaim_Suspend]").click(function () {
                document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy2.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'brown';
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sFirstStartegy).css('display', 'block');

            });

            $("[id$=rdClaim_Full_Retirement]").click(function () {
                document.getElementById("<%=lblStrategy2.ClientID%>").style.color = 'brown';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.fontWeight = 'Bold';
                document.getElementById("<%=lblStrategy2.ClientID%>").style.fontWeight = 'Bold';
                document.getElementById("<%=lblStrategy3.ClientID%>").style.fontWeight = 'Bold';
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sSecondStartegy).css('display', 'block');
            });

            $("[id$=rdClaim_Claim_Now_Cliam_Later]").click(function () {
                document.getElementById("<%=lblStrategy3.ClientID%>").style.color = 'brown';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy2.ClientID%>").style.color = 'black';
                document.getElementById("<%=lblStrategy1.ClientID%>").style.fontWeight = 'Bold';
                document.getElementById("<%=lblStrategy2.ClientID%>").style.fontWeight = 'Bold';
                document.getElementById("<%=lblStrategy3.ClientID%>").style.fontWeight = 'Bold';
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sThirdStartegy).css('display', 'block');
            });
        }
    </script>
</asp:Content>
