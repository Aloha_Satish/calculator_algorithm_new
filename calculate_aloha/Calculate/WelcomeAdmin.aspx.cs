﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Reflection;
using System.Web.UI;

namespace Calculate
{
    public partial class WelcomeAdmin : System.Web.UI.Page
    {
        #region EventAndMethods
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                if (!IsPostBack)
                {
                    if (Request.QueryString[Constants.reference] != null)
                        processSubscription();
                    // Get Customer details
                    getCustomerDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// get Customer Details to display user name
        /// </summary>
        private void getCustomerDetails()
        {
            try
            {
                DataTable dtCustomerDetails = new DataTable();
                if (Session[Constants.SessionRole].ToString() == Constants.SuperAdmin)
                {
                    Customers customer = new Customers();
                    dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionAdminId].ToString());
                }
                else if (Session[Constants.SessionRole].ToString() == Constants.Institutional)
                {
                    Institutional objInstitutional = new Institutional();
                    //Take Institution ID in session for Assign & Delete Pop Up     Dev:- Aloha    Date:-12/22/2014
                    Session["StrInstitutionAdminID"] = Session[Constants.SessionAdminId];
                    dtCustomerDetails = objInstitutional.getInstitutionalDetailsByID(Session[Constants.SessionAdminId].ToString());
                    //if (!dtCustomerDetails.Rows[0]["isSubscription"].ToString().Equals("Y"))
                    //    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayInstitutionSubscriptionPopUp();", true);
                }
                else
                {
                    Advisor objAdvisor = new Advisor();
                    dtCustomerDetails = objAdvisor.getAdvisorDetailsByID(Session[Constants.SessionAdminId].ToString());
                    if (!dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                    {
                        //Previously naviagates to subscription popup
                        //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayAdvisorSubscriptionPopUp();", true);

                        
                        //Changed to navigate to chargify page on the bases of plan
                        //if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.MonthlyPlan))
                        //    Response.Redirect("https://sscalculator.chargify.com/subscribe/mh5s5ys9j94g/social-security-calculator-monthly-plan?reference=" + Session[Constants.SessionAdminId].ToString(), false);
                        //else if (dtCustomerDetails.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.AnnualPlan))
                        //    Response.Redirect("https://sscalculator.chargify.com/subscribe/v94b4g26s2pv/social-security-calculator-annual-plan?reference=" + Session[Constants.SessionAdminId].ToString(), false);

                    }
                }
                if (dtCustomerDetails.Rows.Count > 0)
                    lblWelcome.Text = " " + dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInstitutionFlat_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Institutional objInstitutional = new Institutional();
                objInstitutional.updateInstitutionalSubscriptionPlan(Session[Constants.SessionAdminId].ToString(), Constants.FlatCharge);

                //Changes for live Server
                Response.Redirect(Constants.LiveInstitutionFlatURL + Session[Constants.SessionUserId].ToString(), true);

                // Redirect to chargify url 
                //Response.Redirect(Constants.TestInstitutionFlatURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInstitutionPerClient_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Institutional objInstitutional = new Institutional();
                objInstitutional.updateInstitutionalSubscriptionPlan(Session[Constants.SessionAdminId].ToString(), Constants.PerClient);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdvisorFlat_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Advisor objAdvisor = new Advisor();
                //objAdvisor.updateAdvisorSubscriptionPlan(Session[Constants.SessionAdminId].ToString(), Constants.FlatCharge);

                //Changes for live Server
                // Response.Redirect(Constants.LiveAdvisorFlatURL + Session[Constants.SessionUserId].ToString(), false);
                Response.Redirect("https://sscalculator.chargify.com/subscribe/mh5s5ys9j94g/social-security-calculator-monthly-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.TestAdvisorFlatURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdvisorPerClient_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Advisor objAdvisor = new Advisor();
                //objAdvisor.updateAdvisorSubscriptionPlan(Session[Constants.SessionAdminId].ToString(), Constants.PerClient);
                // Redirect to chargify url 
                //Response.Redirect(Constants.TestAdvisorFlatURL + Session[Constants.SessionUserId].ToString(), true);
                Response.Redirect("https://sscalculator.chargify.com/subscribe/v94b4g26s2pv/social-security-calculator-annual-plan?reference=" + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to update the subscription plan
        /// </summary>
        private void processSubscription()
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                {
                    Institutional objInstitutional = new Institutional();
                    objInstitutional.updateInstitutionSubscription(Request.QueryString[Constants.reference].ToString());
                }
                else
                {
                    Advisor objAdvisor = new Advisor();
                    objAdvisor.updateAdvisorSubscription(Request.QueryString[Constants.reference].ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

        }

        /// <summary>
        /// Cancel button's click event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                System.Web.HttpContext.Current.Session.Clear();
                Response.Redirect(Constants.Redirecthome, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to validate the session
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorWelcomeAdmin, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorWelcomeAdmin + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion EventAndMethods
    }
}